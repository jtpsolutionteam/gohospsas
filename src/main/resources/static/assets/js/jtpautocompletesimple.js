/*
var path;
var fieldFilter;
var fieldReturn;

var options = {
		url: function(fieldValue) {
			return '/usuarios/lkgerente?txGerente='+fieldValue;
		},
		getValue: fieldReturn,
		minCharNumber: 3,
		//requestDelay: 300,
		ajaxSettings: {
			contentType: 'application/json'
		},

    template: {
        type: "description",
        fields: {
            description: "txApelido"
        }
    },

    list: {
        match: {
            enabled: true
        }
    },

    theme: "plate-dark"
};
$("#txGerente").easyAutocomplete(options);		
*/

var JTPAutocompleteSimple = JTPAutocompleteSimple || {};

JTPAutocompleteSimple.AutocompleteSimple = (function() {
	
	function AutocompleteSimple(inputField) {
		this.campoForm = $(inputField);		
	}
	
	AutocompleteSimple.prototype.iniciar = function(path,fieldFilter,fieldReturn) {
		var options = {
			url: function(value) {
				return path+'?'+fieldFilter+'=' + value;
			},
			getValue: fieldReturn,
			minCharNumber: 1,
			requestDelay: 300,
			ajaxSettings: {
				contentType: 'application/json'
			},
		    list: {
		        match: {
		            enabled: true
		        }
		    },
		    
		    template: {
		        type: "links",
		        fields: {
		            link: "txUrlPesquisar"
		        }
		    },

		    theme: "plate-dark"
		};
		
		this.campoForm.easyAutocomplete(options);
	}
	
	return AutocompleteSimple
	
}());


$(function() {

	//var autocomplete = new JTPAutocompleteSimple.AutocompleteSimple('#txPesquisar');
	//autocomplete.iniciar('/usuarios/pesquisar','txPesquisar','txPesquisar');		
	
})


