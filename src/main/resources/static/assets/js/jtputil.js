/*
				 $.each($('#tabImportacaoGestaoObj').serializeArray(), function(index, value){
					    console.log(value.name);
					    var t = "#"+value.name;
					    
					    $( t ).blur(function() {
					    	analistaVirtual(t);
					    });
					    
				 });*/




function gerarXML() {
	 
  
	showPleaseWait('gerando... Aguarde...');

    var post_url = "/faturamento/gerarxml";
    var request_method = 'POST'; 
    var formElement = document.querySelector("#tabFaturamentoGerarObj");
    console.log(formElement);
    var formData = new FormData(formElement);
    $.ajax({
        url : post_url,
        type: request_method,
        data : formData,
        contentType: false,
        cache: false,        
        processData:false,		        
     beforeSend:function() { 
		//showPleaseWait('Salvando... Aguarde...');
     },
     complete:function(response) {
    	// hidePleaseWait();
     },error:function() {

     },success:function(response) {

    	hidePleaseWait();
 
    	var blob = new Blob([response]);

    	saveAs(blob, "ans.xml");
    	
    	
     }
     
    });    
}





function faturamentoGerarLote(form, message) {
	
	var txLote = $("#txLoteGerar").val();
	
	if (txLote == "") {
		
		toastr["error"]("No. do Lote Obrigatório!");
		
	}else {
		console.log("message:"+message);
		if (message != undefined) {
			showPleaseWait(message);	  
		}else {
			showPleaseWait('Salvando... Aguarde...');
		}

		$("#txLoteNovo").val(txLote);
		document.forms[form].submit();
	}
}


function CheckAllDataGridCheckBoxes() {

	var ck = $(".classcheckbox").val();
	
	if (ck == "on") {
	  $(".classcheckbox").prop('checked', true);
	}else {
		$(".classcheckbox").prop('checked', false);
	}
		   
		

	
	
/*
for(i = 0; i < document.forms[0].elements.length; i++) {

var elm = document.forms[0].elements[i]

console.log(elm);


if (elm.type == ‘checkbox’) {

if (re.test(elm.name)) {

elm.checked = checkVal
*/
//}
	
	
	
}


function gravarcondicaofaturamento(cdTratativa, cdCondicao, txAcao, txAux) {
	
	var request_method = "GET";
	
	$.ajax({
        url : "/faturamento/gravarcondicao/"+cdTratativa+"/"+cdCondicao+"/"+txAcao+"/"+txAux,
        type: request_method,
        contentType: false,
        cache: false,
        //async: false,
        processData:false,
        beforeSend:function() { 
    		//showPleaseWait('Salvando... Aguarde...');
	     },
	     complete:function(response) {
	    	// hidePleaseWait();
	     },
	     error:function() {

	     },
	     success:function(response) {
	 		var txResposta = response;
	 		
	 		if (txResposta == "OK") {
	 			toastr["success"]("Atualização feita com sucesso!");
	 		}else {
	 			toastr["success"](txResposta);
	 		}

	     }
	});
	
}



function showPergunta(mensagem, url, tipo, urlConsulta) {		
	Swal.fire({
        title: mensagem,
        //text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        confirmButtonText: 'SIM',
        buttonsStyling: false
      }).then((result) => {
          
    	  if (result.value) {
    		 if (tipo == 'ajax') {
    			 
    			 executaPergunta(url, urlConsulta);
    			 
    		 }else { 
    	       document.location=url;
    		 }
    	  }  
      })
	
}


function executaPergunta(getUrl, urlConsulta) {
	
	showPleaseWait('Solicitando... Aguarde...');
	
	var request_method = "GET";
	$.ajax({
        url : getUrl,
        type: request_method,
        contentType: false,
        cache: false,
        //async: false,
        processData:false,
        beforeSend:function() { 
    		//showPleaseWait('Salvando... Aguarde...');
	     },
	     complete:function(response) {
	    	hidePleaseWait();
	     },
	     error:function() {
			hidePleaseWait();
	     },
	     success:function(response) {
	     hidePleaseWait();
	 		var txResposta = response;

		 		if (urlConsulta != null) {
		 			console.log(urlConsulta);
		 			document.location=urlConsulta;
		 		}else {
		 			document.location=txResposta;
		 		}
	 		
			

	     }
	});
	
}



function goBack() {
  window.history.back();
}

function tratativasTeste(cdTipo) {

	var post_url = "/tratativastestes/consultartratativas";
	if (cdTipo == 2) {
	
		post_url = "/tratativastestes/confirmartratativas";
		
	}else if (cdTipo == 3) {
		
		post_url = "/tratativastestes/atualizatratativas";
	}
	
	var request_method = 'GET';
	$.ajax({
        url : post_url,
        type: request_method,
        contentType: false,
        cache: false,
        //async: false,
        processData:false,
        beforeSend:function() { 
    		//showPleaseWait('Salvando... Aguarde...');
	     },
	     complete:function(response) {
	    	// hidePleaseWait();
	     },
	     error:function() {

	     },
	     success:function(response) {
	 		var txResposta = response;
			console.log(txResposta);

	     }
	});
	
}




function goBack() {
  window.history.back();
}

var pathUrl = ""
	function pathurl(path) {
	pathUrl = path;
	console.log(pathUrl);
}



function gravarBtnLookup(urlAction, form, fields, modal, aux) {
	 
    if (aux != null) {
    	var aux = $("#"+aux).val();
    	$("#aux").val(aux);
    }

	showPleaseWait('salvando... Aguarde...');

    var post_url = urlAction;
    var request_method = 'POST'; 
    var formElement = document.querySelector("#"+form);
    console.log(formElement);
    var formData = new FormData(formElement);
    $.ajax({
        url : post_url,
        type: request_method,
        data : formData,
        contentType: false,
        cache: false,
        processData:false,		        
     beforeSend:function() { 
		//showPleaseWait('Salvando... Aguarde...');
     },
     complete:function(response) {
    	// hidePleaseWait();
     },error:function() {

     },success:function(response) {

    	hidePleaseWait();
    
   	 	var txResposta = response;		   	
   	 	for(var index in txResposta) {				    
		    var attr = txResposta[index];
		    
		    var fieldsArray = fields.split(";");
		    
		    $("#"+fieldsArray[0]).val(attr["cd_mensagem"]);
		    $("#"+fieldsArray[1]).val(attr["tx_mensagem"]);
		    
		    console.log(attr["cd_mensagem"]);
		    console.log(attr["tx_mensagem"]);
		    $("#"+modal).modal("hide");
   	 	}
     }
     
    });    
}

function limpaGridDinamico(vlLength) {
	
	for (var i = 0; i<1000; i++) {
	  var element = document.getElementById("idGrid");
	  if (element != null) {
	    element.parentNode.removeChild(element);
	  }	  
	}
	
}

$('#lkBtnPermissao').bind('keydown', function(evt) {
	
	montaGridBtnModal(pathUrl+"lookups/lkrelatoriopermissao?txGrupoAcesso=","tBodyBtnPermissao","txGrupoAcesso",returnStringDigitada(evt,"lkBtnPermissao"),"cdGrupoAcesso=cdGrupoAcesso;txGrupoAcesso=txGrupoAcesso", "cadastrorelatoriobtnpermissao", 2, "listaPermissao");
	
});


function returnStringDigitada(evt, lkField) {
	 
		var keyDigitada = evt.keyCode;
		console.log(keyDigitada);

		if (keyDigitada == 8) {
			var txDigitacao = $("#"+lkField).val();
			var vlLength = txDigitacao.length;
			var txDigitacaoFinal = txDigitacao.substring(0,vlLength-1);
			console.log(txDigitacaoFinal);
		}else {
			var txDigitacao = keyPressed(evt); 
			var txDigitacaoFinal = $("#"+lkField).val()+txDigitacao;
			console.log($("#"+lkField).val()+txDigitacao);		
		}
	 
  return txDigitacaoFinal; 
	 
 }
	
 function returnLookup(fieldId1, fieldValue, fieldId2, fieldText, modal) {
	 
	 $("#"+fieldId1.replace("lk","")).val(fieldValue);
	 $("#"+fieldId2.replace("lk","")).val(fieldText);
	 $("#"+modal).modal("hide");
	 
 }	
 
 
function returnLookupListGroup(fieldId1, fieldValue, fieldId2, fieldText, modal, listGroup) {
	 
	var ul = document.querySelector("#"+listGroup);
	
	var li = document.createElement("li");
	li.setAttribute("class", "list-group-item align-items-center");
	
	var span = document.createElement("span");
	span.setAttribute("class", "drag-indicator");
	li.appendChild(span);
	
	var div = document.createElement("div");
	div.textContent = fieldText;
	li.appendChild(div);
	
	var divInput = document.createElement("div");
	divInput.setAttribute("class", "btn-group ml-auto")
	
	var input = document.createElement("input");
	input.setAttribute("class", "inputNone")
	input.setAttribute("id", fieldId1);
	input.setAttribute("name", fieldId1);
	input.setAttribute("type", "hidden");
	input.setAttribute("value", fieldValue);
	
	divInput.appendChild(input);
	
	li.appendChild(divInput);
	
	ul.appendChild(li);

	
	/*
	<li class="list-group-item align-items-center drag-handle"><span class="drag-indicator"></span>
	<div>Alcance</div>
	<div class="btn-group ml-auto">
		<input class="inputNone" type="hidden" id="cdCodigo" name="cdCodigo" value="1">
	</div></li>*/
	
	 $("#"+modal).modal("hide");
	 
 }	
	
 function montaGridBtnModal(txUrl,txTbody,txField,textField,fieldLookup,modal,tipoReturn,listGroup) {
	 
	    //tipoReturn 
	 	// 1 = campo tipo input
	 	// 2 = list-group-item
	 
		var post_url = txUrl+textField;
		//console.log(post_url);
		var request_method = 'GET';
		var txTd = "";
		$.ajax({
	        url : post_url,
	        type: request_method,
	        contentType: false,
	        cache: false,
	        //async: false,
	        processData:false,
	        beforeSend:function() { 
	    		//showPleaseWait('Salvando... Aguarde...');
		     },
		     complete:function(response) {
		    	// hidePleaseWait();
		     },
		     error:function() {

		     },
		     success:function(response) {
		 		var txResposta = response;
				console.log(txResposta);
				var tr = "";
			    var tbody = document.querySelector("#"+txTbody);
			    //console.log(tbody);
				limpaGridDinamico(txResposta.length);
				
				var fieldLookupArray = fieldLookup.split(";");
				
				for(var index in txResposta) {
				    var attr = txResposta[index];

					var field1Array = fieldLookupArray[0].split("=");
					var field2Array = fieldLookupArray[1].split("=");
				     
					tr = document.createElement("tr");
					var td = document.createElement("td");
					td.setAttribute("id", "idGrid");
					td.setAttribute("class", "jtp-grid-td-image");
					if (tipoReturn != null) { 
					  td.setAttribute("onclick", "returnLookupListGroup(\"lk"+field1Array[0]+attr[field1Array[1]]+"\",\""+attr[field1Array[1]]+"\",\"lk"+field2Array[0]+"\",\""+attr[txField]+"\",\""+modal+"\",\""+listGroup+"\");");
					}else {
					  td.setAttribute("onclick", "returnLookup(\"lk"+field1Array[0]+"\",\""+attr[field1Array[1]]+"\",\"lk"+field2Array[0]+"\",\""+attr[txField]+"\",\""+modal+"\");");
					}
					td.textContent = attr[txField];
					
					//Elemento input hidden					
					var input = document.createElement("input");
					input.setAttribute("id", "lk"+field1Array[0]);
					input.setAttribute("name", "lk"+field1Array[0]);
					input.setAttribute("type", "hidden");
					input.setAttribute("value", attr[field1Array[1]]);
					td.appendChild(input);
					tr.appendChild(td);
					
					console.log(attr[txField]);
					
					//tbody.appendChild(tr);
					tbody.appendChild(tr);

				}
		     }
		});
	 
 }	



 var vlLogDocto = 0;
 function showLogDoctosAnexos(dtCriacao, txUsuarioInclusao, dtAlteracao, txUsuarioAlteracao) {
	 vlLogDocto = (vlLogDocto + 1);
	 var modalpergunta = '<div class="modal fade" id="Log'+vlLogDocto+'" tabindex="-1" role="dialog">\
	  <div class="modal-dialog modal-lg" role="document">\
	    <div class="modal-content">\
	      <div class="modal-header">\
	        <h5 class="modal-title">Documentos Anexos - Log</h5>\
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
	          <span aria-hidden="true">&times;</span>\
	        </button>\
	      </div>\
	      <div class="modal-body">\
	  <div class="container-fluid">\
	    <div class="row">\
	     <div class="col-sm-4">\
		  <div class="form-group">\
			<label for="dtCriacao"><strong>Dt.Criação</strong></label>\
			<div class="form-group">\
				<span><strong>'+dtCriacao+'</strong></span>\
			</div>\
		  </div>\
	     </div>\
		 <div class="col-sm-4">\
		  <div class="form-group">\
			<label for="txUsuarioInclusao"><strong>Usuário</strong></label>\
			<div class="form-group">\
				<span><strong>'+txUsuarioInclusao+'</strong></span>\
			</div>\
		  </div>\
	    </div>\
	   </div>';
	  if (dtAlteracao != '') {
	  modalpergunta += '<div class="row">\
     <div class="col-sm-4">\
	  <div class="form-group">\
		<label for="dtAlteracao"><strong>Dt.Alteração</strong></label>\
		<div class="form-group">\
			<span><strong>'+dtAlteracao+'</strong></span>\
		</div>\
	  </div>\
     </div>\
	 <div class="col-sm-4">\
	  <div class="form-group">\
		<label for="txUsuarioAlteracao"><strong>Usuário</strong></label>\
		<div class="form-group">\
			<span><strong>'+txUsuarioAlteracao+'</strong></span>\
		</div>\
	  </div>\
    </div>\
   </div>';
	  }
	  modalpergunta += '</div>\
	</div>\
		  <div class="modal-footer">\
	       <button type="button" class="btn btn-secondary" data-dismiss="modal">SAIR</button>\
	      </div>\
	    </div>\
	  </div>\
	</div>'			
 
	$(document.body).append(modalpergunta);
    $("#Log"+vlLogDocto).modal("show");
	
} 



var totNumerario = 0;
$('.jtpCheckBoxNumerario').click(function(){
	
	var txVl = $(this).val().split(";");
	var vl = txVl[0];
	var cdNumerario = txVl[1];	
	if (isNaN(cdNumerario)) {
	 if ($(this).prop("checked")) {
		 totNumerario = (totNumerario + parseFloat(vl));
		 //console.log($("#4").val());
	 }else{
		 totNumerario = (totNumerario - parseFloat(vl));
		 //console.log("Não checked");
	 }	
	 toastr["warning"]("Total do Numerário: R$ "+totNumerario.toFixed(2));
	}	 
});

var totPagamento = 0;
$('.jtpCheckBoxPagamento').click(function(){
	
	var txVl = $(this).val().split(";");
	var vl = txVl[2];
	if (!isNaN(vl)) {
	 if ($(this).prop("checked")) {
		 totPagamento = (totPagamento + parseFloat(vl));
		 //console.log($("#4").val());
	 }else{
		 totPagamento = (totPagamento - parseFloat(vl));
		 //console.log("Não checked");
	 }
	
	 toastr["warning"]("Total do Pagamento: R$ "+totPagamento.toFixed(2));
	}else {
		toastr["error"]("Valor inválido! Verifique o valor...");
	}	 
});


 var vlNomeModal = 0;
 function showPerguntaModal(titulo, mensagem, action) {
	 vlNomeModal = (vlNomeModal + 1);
	 var modalpergunta = '<div class="modal" id="Perg'+vlNomeModal+'" tabindex="-1" role="dialog">\
						  <div class="modal-dialog" role="document">\
						    <div class="modal-content">\
						      <div class="modal-header">\
						        <h5 class="modal-title">'+titulo+'</h5>\
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
						          <span aria-hidden="true">&times;</span>\
						        </button>\
						      </div>\
						      <div class="modal-body">\
						        <p>'+mensagem+'</p>';
	 							if (titulo.includes('Numerário')) {
	 							  modalpergunta += '<p>Valor do Numerário: R$ '+totNumerario.toFixed(2)+'</p>';
	 							}else if (titulo.includes('Pagamento'))  {
	 							  modalpergunta += '<p>Valor da Sol.Pagamento: R$ '+totPagamento.toFixed(2)+'</p>';	
	 							}
	 							modalpergunta += '</div>\
						      <div class="modal-footer">';
						        if (titulo.includes('Numerário')) {
						        	modalpergunta += "<button type=\"button\" class=\"btn btn-primary\" onclick=\"javascript: solicitacaonumerariopagamento('"+action+"','#Perg"+vlNomeModal+"','1');\">SIM</button>";
						        	modalpergunta += '<button type="button" class="btn btn-secondary" data-dismiss="modal">NÃO</button>';
						        }else if (titulo.includes('Pagamento')) {
						        	modalpergunta += "<button type=\"button\" class=\"btn btn-primary\" onclick=\"javascript: solicitacaonumerariopagamento('"+action+"','#Perg"+vlNomeModal+"','2');\">SIM</button>";
						        	modalpergunta += '<button type="button" class="btn btn-secondary" data-dismiss="modal">NÃO</button>';
						        }else {	
						        	modalpergunta += "<button type=\"button\" class=\"btn btn-primary\" onclick=\"javascript: executaAction('"+action+"');\">SIM</button>";
						        	modalpergunta += '<button type="button" class="btn btn-secondary" data-dismiss="modal">NÃO</button>';
						        }
						        
						     modalpergunta += '</div>\
						    </div>\
						  </div>\
						</div>'			
	$(document.body).append(modalpergunta);
    $("#Perg"+vlNomeModal).modal("show");
	
} 

 function executaAction(urlAction) {
	 document.location=urlAction; 
 }
 
 function solicitacaonumerariopagamento(urlAction, modal, tipo) {
	 
	    if (totNumerario == 0 && totPagamento == 0) {
	    	toastr["error"]("Você não selecionou nenhuma despesa!");
	    }else {
		    totNumerario = 0;
		    totPagamento = 0;
		 	console.log(urlAction+" - "+modal)
		 	
			showPleaseWait('Solicitando... Aguarde...');
	
		    var post_url = urlAction;
		    var request_method = 'POST'; 
		    var formElement = document.querySelector("form");
		    var formData = new FormData(formElement);
		    var ckFechar = true;
		    $.ajax({
		        url : post_url,
		        type: request_method,
		        data : formData,
		        contentType: false,
		        cache: false,
		        processData:false
		    }).done(function(response){ 
		    	//waitingDialog.hide();
		    	hidePleaseWait();
		    	
		    
		   	 var txErros = response;		   	
			 for(var index in txErros) {				    
				    var attr = txErros[index];
				    //console.log(attr["defaultMessage"]);				    
				    if ((attr["defaultMessage"]) != null) {
				      console.log(attr["defaultMessage"]);					      
				      toastr["error"](attr["defaultMessage"]);
		    		}else {		   		   	 
					 for(var index in txErros) {
						 var cdMensagem = attr["cd_mensagem"]; 
						 var txTipoMensagem = "success"; 
						 if (cdMensagem == 2) {
							 txTipoMensagem = "error";  
						 }	
						 
						  toastr[txTipoMensagem](attr["tx_mensagem"]);
						  $(modal).modal("hide");
						  showPleaseWait("Consultando... Aguarde...");
						  var txNref = $("#txNref").val();
						  var cdModulo = $("#cdModulo").val();
						  var cdCliente = $("#cdCliente").val();
						  var cdTipo = $("#cdTipo").val();
						  var urlConsulta = urlAction.replace("solicitarnumerario","")+txNref+"/"+cdModulo+"/"+cdCliente+"/"+cdTipo;
						  if (tipo == 2) {
							  urlConsulta = urlAction.replace("solicitarpagamento","")+txNref+"/"+cdModulo+"/"+cdCliente+"/"+cdTipo;
						  }
						  document.location=urlConsulta;							 						 
					 }		    			
		    		}
				}
		    });
    }
}


function pesquisar(url,form,page) {
	
	 var txUrl = url+"?";
	 var txFields = '';	
	 console.log('entrou');
	 
	 $.each($('#'+form).serializeArray(), function(index, value){
		    //console.log(value.name);
		    var txField = value.name;
		    if (txField.includes("Filtro")){
		     var t = "#"+value.name;
		     console.log(t);		     
		     txFields += value.name+"="+$(t).val()+"&";
		     console.log(txFields);
		    }
	 });
	txUrl += txFields+"page="+page;
	console.log(txUrl);
	document.location=txUrl;
}


function analistaVirtual(service) {
 /*
    var post_url = ""; 
    var request_method = 'GET'; //$(this).attr("method"); //get form GET/POST method
    var form_data = new FormData(this); //Creates new FormData object
    //var formElement = document.querySelector("form");
    //var formData = new FormData(formElement);
    $.ajax({
        url : post_url,
        type: request_method,
        data : formData,
        contentType: false,
        cache: false,
        processData:false
    }).done(function(response){ 
    
       
   	    var txErros = response;		   	
	    for(var index in txErros) {				    
		    var attr = txErros[index];
		    //console.log(attr["defaultMessage"]);				    
		    if ((attr["defaultMessage"]) != null) {
		      console.log(attr["defaultMessage"]);					      
		      toastr["error"](attr["defaultMessage"]);    		
		    }
		}    
    	
    });

	*/
}


function checkPermissaoCampos(listPermissaoCampos) {
	
	for(var index in listPermissaoCampos) {				    
	    var attr = listPermissaoCampos[index];
	    console.log(attr["txObjCampo"]);
	    var txObjCampo = attr["txObjCampo"];
	    if (attr["ckAlterar"] == "2") {
	    	console.log("alterar");
	    	$('[name="' + txObjCampo + '"]').attr('readonly', 'readonly');	    	
	    }	    
	    if (attr["ckVisualizar"] == 2) {
	    	$('[name="' + txObjCampo + '"]').attr('hidden','true');   	
	    }
	}    
	
}

function checkPermissaoBotao(listPermissaoBotao) {
	
	for(var index in listPermissaoBotao) {				    
	    var attr = listPermissaoBotao[index];
	    //console.log(attr["txObjBotao"]);
	    
	    if (attr["ckVisualiza"] == 2) {
	    	$('#' + attr["txObjBotao"]).attr('hidden','true');   	
	    }

	}    
	
}



function consultaTelasAdm(url) {
	
	var grupovisao = $("#cdGrupoVisao").val();
	var tela = $("#cdTela").val();
	
	if (grupovisao != "" && tela != "") {
		if (grupovisao != null) {
		 document.location=url+"/pesquisar/"+grupovisao+"/"+tela;
		}else {
		 document.location=url+"/pesquisar/"+tela;	
		}
		
	}else {
		jtpNotifySimple('Grupo de Visão/Tela inválida!','error');
	}
	
}




$(function() {
  
  // Somente números
  $( '.jtpSomenteNumeros').on( 'keydown', function(e) {
    var keyCode = e.keyCode || e.which,
      pattern = /\d/,
      // Permite somente Backspace, Delete e as setas direita e esquerda, números do teclado numérico - 96 a 105 - (além dos números)
      keys = [ 46, 8, 9, 37, 39, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105 ];
  
    if( ! pattern.test( String.fromCharCode( keyCode ) ) && $.inArray( keyCode, keys ) === -1 ) {
      return false;
    }
  });
  
});



$('.jtpDate').keypress(function(evt){
	 
	$(this).attr('maxlength',11); 
	var keyDigitada = evt.keyCode;
	console.log(keyDigitada);
	if (keyDigitada != 8 && keyDigitada != 46) {
    var vlLength  = $(this).val().length;				    
    //console.log(vlLength);
    
    	if (vlLength == 2 || vlLength == 5) {
	    	$(this).val($(this).val()+"/");
	    }

    	if (vlLength == 9) {
      	 var str = keyPressed(evt);    	
	   	 if(!isDate($(this).val()+str)) {
			 jtpNotifySimple($(this).val()+str+' - Data inválida!','error');
		 	 $(this).focus();					 	 
		 }
    	} 
    }													
  });

 
 $('.jtpDateTime').keypress(function(evt){
	    $(this).attr('maxlength',19);
		var keyDigitada = evt.keyCode;
		//console.log(keyDigitada);
		if (keyDigitada != 8 && keyDigitada != 46) {
	    var vlLength  = $(this).val().length;				    
	    //console.log(vlLength);
	    
	    	if (vlLength == 2 || vlLength == 5) {
		    	$(this).val($(this).val()+"/");
		    }

	    	if (vlLength == 10) {
		    	$(this).val($(this).val()+" ");
		    }

	    	if (vlLength == 13 || vlLength == 16) {
		    	$(this).val($(this).val()+":");
		    }

	    	if (vlLength == 18) {
	    		var str = keyPressed(evt);
	    		var field = $(this).val()+str;
		    	//console.log(field);
		    	var hr = field.substring(11,13);
		    	var mm = field.substring(14,16);
		    	var ss = field.substring(17,19);
		    	//console.log(hr);
		    	//console.log(mm);
		    	//console.log(ss);
		    	var hrs = hr + ":" + mm + ":" + ss;
		    	if (hr > 24) {
		    		jtpNotifySimple(hrs + ' - Hora inválida!','error');
		    	}else if (mm > 60) {
		    		jtpNotifySimple(hrs + ' - Hora inválida!','error');
		    	}else if (ss > 60) {
		    		jtpNotifySimple(hrs + ' - Hora inválida!','error');
		    	}		    	
		    }

	    	if (vlLength == 9) {
	    		 var str = keyPressed(evt);
			   	 if(!isDate($(this).val()+str)) {			   		 
					 jtpNotifySimple($(this).val()+str+' - Data inválida!','error');
				 	 $(this).focus();					 	 
			   	 } 
	    	}
	    }													
  });

 
 
 function keyPressed(evt){
	    evt = evt || window.event;
	    var key = evt.keyCode || evt.which;
	    return String.fromCharCode(key); 
	}

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "1000",
  "hideDuration": "1000",
  "timeOut": "30000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}


 function showPleaseWait(mensagem) {
	    var modalLoading = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" role="dialog">\
	        <div class="modal-dialog">\
	            <div class="modal-content">\
	                <div class="modal-header">\
	                    <h4 class="modal-title">'+mensagem+'</h4>\
	                </div>\
	                <div class="modal-body">\
	                  <div class="loader float-right">\
	                  </div>\
	                </div>\
	            </div>\
	        </div>\
	    </div>';
	    $(document.body).append(modalLoading);
	    $("#pleaseWaitDialog").modal("show");
	}
 
 function hidePleaseWait() {
	    $("#pleaseWaitDialog").modal("hide");
	}

function gravarModal(urlAction, urlConsulta, modal, classForm) {
	 
		    //waitingDialog.show('Salvando... Aguarde...', {dialogSize: 'sm', progressType: 'success'});		    
	 
			showPleaseWait('Salvando... Aguarde...');
			//$(".loader").fadeOut("slow");
	
		    var post_url = urlAction;     //$(this).attr("action"); //get form action url
		    var request_method = 'POST'; //$(this).attr("method"); //get form GET/POST method
		    //var form_data = new FormData(this); //Creates new FormData object
		    var formElement = document.querySelector("form");
		    if (classForm != null) {
		    	console.log(formElement);
		    	formElement = document.querySelector(classForm);
		    	console.log(formElement);
		    }
		    var formData = new FormData(formElement);
		    var ckFechar = true;
		    $.ajax({
		        url : post_url,
		        type: request_method,
		        data : formData,
		        contentType: false,
		        cache: false,
		        processData:false
		    }).done(function(response){ 
		    	//waitingDialog.hide();
		    	hidePleaseWait();
		    	
		    	
		   	 var txErros = response;		   	
			 for(var index in txErros) {				    
				    var attr = txErros[index];
				    //console.log(attr["defaultMessage"]);				    
				    if ((attr["defaultMessage"]) != null) {
				      console.log(attr["defaultMessage"]);					      
				      toastr["error"](attr["defaultMessage"]);
		    		}else {		   		   	 
					 for(var index in txErros) {
						 if (urlConsulta == null || urlConsulta == '') {
						   toastr["success"](attr["tx_mensagem"]);
						   $(modal).modal('hide');
						 }else if (urlConsulta == 'refresh') {
							 toastr["success"](attr["tx_mensagem"]);
							 $(modal).modal('hide');
							 window.location.reload();
						 }else {	 
							 toastr["success"](attr["tx_mensagem"]);							 
							 $(modal).modal('hide');	
							 showPleaseWait('Consultando... Aguarde...');
							 document.location=urlConsulta;							 
						 }
					 }		    			
		    		}
				}			 
		    });
		  
 }

function ExecutarChamada(urlAction, urlConsulta, Mensagem, keyField) {
	 
	var k = $("#"+keyField).val();	
	if (k == '') {
		toastr["error"](Mensagem);	
	}else {
	
	showPleaseWait('Enviando... Aguarde...');

    var post_url = urlAction;     //$(this).attr("action"); //get form action url
    var request_method = 'GET'; //$(this).attr("method"); //get form GET/POST method
    var ckFechar = true;
    $.ajax({
        url : post_url,
        type: request_method,
        contentType: false,
        cache: false,
        processData:false
    }).done(function(response){ 
    	hidePleaseWait();
   	 var txErros = response;		   	
	 for(var index in txErros) {				    
		    var attr = txErros[index];
		    //console.log(attr["defaultMessage"]);				    
		    if ((attr["defaultMessage"]) != null) {
		      console.log(attr["defaultMessage"]);					      
		      toastr["error"](attr["defaultMessage"]);
    		}else {		   		   	 
			 for(var index in txErros) {
				 if (attr["cd_mensagem"] == 2) {
    				toastr["error"](attr["tx_mensagem"]);
    			 }else {
				 if (urlConsulta == null || urlConsulta == '') {
				   toastr["success"](attr["tx_mensagem"]);
				 }else if (urlConsulta == 'refresh') {
					 toastr["success"](attr["tx_mensagem"]);
					 //window.location.reload();
				 }else {	 
					 toastr["success"](attr["tx_mensagem"]);							 
					 showPleaseWait('Consultando... Aguarde...');
					 document.location=urlConsulta;							 
				 }
    		   } 
			 }		    			
    		}
		}			 
    });
  }
}


function verificaData(dtField) {

	 var txtVal =  $(dtField).val();
	 if (txtVal != ""){
	 if(!isDate(txtVal))     
		 jtpNotifySimple(txtVal+' - Data inválida!','danger');
	 	 $(dtField).focus();
	 	 
	}
}


function isDate(txtDate)
{
    var currVal = txtDate;
    if(currVal == '')
        return false;
    
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    
    if (dtArray == null) 
        return false;
    
    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[3];
    dtDay= dtArray[1];
    dtYear = dtArray[5];        
    
    if (dtMonth < 1 || dtMonth > 12) 
        return false;
    else if (dtDay < 1 || dtDay> 31) 
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
        return false;
    else if (dtMonth == 2) 
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
                return false;
    }
    return true;
}


function openModal(modal, field, txMensagem) {
	
	var txField = document.getElementById(field).value;
	
	if (txField == "") {
		jtpNotifySimple(txMensagem, 'danger');
	}else {
		document.getElementById("tabLiGestaoStatusObj").elements.namedItem("txNref").value = txField;
		//document.getElementById(field).value = txField;
	  	$(modal).modal('show');		
	  	
	}
}


function jtpNotifySimple(txMessage, typeMessage) {
	    //Mensagem
	 var txMensagem = txMessage;
	 console.log(txMessage);
	 console.log(typeMessage);
	 //var txTipo = "success";
	 if (txMensagem != "" && txMensagem != null) {
		 /*
			$.notify({
				// options
				message: txMensagem
			},{
				// settings
				type: typeMessage,
				allow_dismiss: false,
				showProgressbar: false
			});*/
		 toastr[typeMessage](txMensagem);
	 }

}



function jtpNotify(objError){
	
	 //Errors
	 var txErros = objError;
	 for(var index in txErros) { 
		    var attr = txErros[index];
		    //console.log(attr["defaultMessage"]);
		    /*
			$.notify({
				// options
				message: attr["defaultMessage"]
			},{
				// settings
				type: 'danger',
				allow_dismiss: false,
				showProgressbar: false
			});*/
		    toastr["error"](attr["defaultMessage"]);
		}
	
}


function abrirtela(tela) {

	var arrayNull = tela.split("null");
	var t = tela;
	for (i=0; i<=arrayNull.length-1; i++){
		console.log(i);
		t = t.replace("null", '');
	}
	
	document.location=t;
}


$(function() {
	$('[rel="tooltip"]').tooltip();
})


function limpalookup(campo1, campo2) {
	var campo = document.getElementById(campo1).value;
	if (campo == "") {
		document.getElementById(campo2).value = '';
	}
	
}


function gravar(form, message) {
	//console.log("entrou!");
	//waitingDialog.show('Salvando... Aguarde...', {dialogSize: 'sm', progressType: 'success'});
	console.log("message:"+message);
	if (message != undefined) {
		showPleaseWait(message);	  
	}else {
		showPleaseWait('Salvando... Aguarde...');
	}
	document.forms[form].submit();
}

function teste() {
	console.log("entrou!");
}

function gravarFlexivel(urlAction, classForm, ckRefresh, urlConsulta) {
	console.log("entrou!")
	showPleaseWait('Salvando... Aguarde...');

    var post_url = urlAction;     //$(this).attr("action"); //get form action url
    var request_method = 'POST'; //$(this).attr("method"); //get form GET/POST method
    var formElement = document.querySelector("form");
    if (classForm != null) {
    	console.log(formElement);
    	formElement = document.querySelector(classForm);
    	console.log(formElement);
    }
    var formData = new FormData(formElement);
    var ckFechar = true;
    $.ajax({
        url : post_url,
        type: request_method,
        data : formData,
        contentType: false,
        cache: false,
        processData:false
    }).done(function(response){ 
    	//waitingDialog.hide();
    	hidePleaseWait();
    	
    	if (response == "OK") {
    		toastr["success"]("Mensagem enviada com sucesso!");
    		
    		if (ckRefresh) {
        		window.location.reload();
        	}
    		
    		if (urlConsulta != null) {
    			document.location=urlConsulta;
        	}
    		
    	}else {
    		toastr["error"](response);
    	}
    	
    	
    	
    });
  
}



var pathLog = "";
function pathlog(path) {
	pathLog = path;
	console.log(pathLog);
}

function logField(serviceClass, Field) {

	var txRef = logResultKeyField(serviceClass, Field);
    console.log(txRef);
    
    if (txRef != "") {
    
		var txValue = document.getElementById(txRef).value;
		console.log(txValue);
		
	    var post_url = pathLog+"/"+serviceClass+"/"+Field+"/"+txValue;
	    var request_method = 'GET'; //$(this).attr("method"); //get form GET/POST method
	    $.ajax({
	        url : post_url,
	        type: request_method,
	        contentType: false,
	        cache: false,
	        processData:false
	    }).done(function(response){ 
	
	     var txLog = response;		
	     vlLogDocto = (vlLogDocto + 1);
	     var txModal = '<div class="modal fade" id="ModalLogField'+vlLogDocto+'" tabindex="-1" role="dialog">\
	     <div class="modal-dialog modal-lg" role="document">\
	       <div class="modal-content">\
	         <div class="modal-header">\
	           <h5 class="modal-title">Log de Campos</h5>\
	           <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
	             <span aria-hidden="true">&times;</span>\
	           </button>\
	         </div>\
	         <div class="modal-body">\
	     	<div class="container-fluid">\
				<div class="col-sm-12">\
			<section class="card card-fluid">\
				<div class="table-responsive">\
					<table class="table table-hover fixed_header">\
						<thead>\
							<tr>\
								<th style="min-width: 150px">Valor</th>\
								<th style="min-width: 150px">Data/Hora</th>\
	    	 					<th style="min-width: 180px">Usuário</th>\
	     						<th style="min-width: 180px">Ação</th>\
							</tr>\
						</thead>\
						<tbody>';

	     var txLinhas = "";
		 for(var index in txLog) {				    
			    var attr = txLog[index];	   		   	 
			    console.log(attr["txValor"]);
			    txLinhas +='<tr>\
							<td class="jtp-grid-td">'+attr["txValor"]+'</td>\
							<td class="jtp-grid-td">'+attr["dtData"]+'</td>\
							<td class="jtp-grid-td">'+attr["txUsuario"]+'</td>\
							<td class="jtp-grid-td">'+attr["txTipo"]+'</td>\
						  </tr>';
		  }	
		 
		 txModal += txLinhas + ' </tbody>\
					   </table>\
					 </div>\
					</section>\
					</div>';
		 
		 txModal += 	'</div>\
			 		</div>\
			  		<div class="modal-footer">\
			 		<button type="button" class="btn btn-secondary" data-dismiss="modal">SAIR</button>\
			 		</div>\
			 		</div>\
			 		</div>\
			 		</div>'			
		     
			 $(document.body).append(txModal);
		 	 $("#ModalLogField"+vlLogDocto).modal("show");
	    });	
    }else {
    	toastr["error"]("Erro ao solicitar o log do campo! Favor entrar em contato com o administrador do sistema.");	
    } 
    
}









function logResultKeyField(serviceClass, Field) {

	var txLog = "";
	
    var post_url = pathLog+"/verificakeyfield/"+serviceClass+"/"+Field;     //$(this).attr("action"); //get form action url
    var request_method = 'GET'; //$(this).attr("method"); //get form GET/POST method
    var ckFechar = true;
    $.ajax({
        url : post_url,
        type: request_method,
        async: false, //Sincrona
        contentType: false,
        cache: false,
        processData:false
    }).done(function(response){ 

     txLog = response;		   	
     				 
    });	
   
	return txLog;
}


function openWindowSimple(url, blank) {
	//console.log(url);
	if (blank == "_blank") {
	 window.open(url, "_blank");	
	}else {
	 document.location=url;
	} 
}

function openWindowButton(url, field, message, field2) {
	
	var txValue = document.getElementById(field).value;
	if (txValue == "") {
		jtpNotifySimple(message,'error');
	}else {
	  if (field2 == null) {	
	   document.location=url+"/"+txValue;
	  }else {
		  var txValue2 = document.getElementById(field2).value;
		  document.location=url+"/"+txValue+"/"+txValue2;  
	  }
	}	
}

function openWindowButtonDoctosAnexos(url, message, fieldRef,  fieldServiceChave, fieldControllerChave) {
	
	var txRef = document.getElementById(fieldRef).value;
	if (txRef == "") {
		jtpNotifySimple(message,'error');
	}else {
		document.location=url+"/"+txRef+"/"+fieldServiceChave+"/"+fieldControllerChave;
	}	
}




function openWindowButtonCusto(url, field, message, field2, cdModulo, cdTipo) {
	
	var txValue = document.getElementById(field).value;
	if (txValue == "") {
		jtpNotifySimple(message,'error');
	}else {
		  var txValue2 = document.getElementById(field2).value;
		  document.location=url+"/"+txValue+"/"+txValue2+"/"+cdModulo+"/"+cdTipo;  
	}	
} 


$(document).ready(
		function() {
			
			$(".textFloat").priceFormat({
			    prefix: '',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 2,
			    clearOnEmpty: true
			});
			
			$(".textFloat3").priceFormat({
			    prefix: '',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 3,
			    clearOnEmpty: true
			});
			
			$(".textFloat4").priceFormat({
			    prefix: '',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 4,
			    clearOnEmpty: true
			});
			
			$(".textFloat5").priceFormat({
			    prefix: '',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 5,
			    clearOnEmpty: true
			});
			
			$(".textFloat7").priceFormat({
			    prefix: '',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 7,
			    clearOnEmpty: true
			});
			
			$(".jtpFloat").maskMoney({
				prefix:  '',
				decimal: ',',
				thousands: '.'				
			});
			
		});	

