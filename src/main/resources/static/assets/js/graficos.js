
function graficosmes() {

	//grafico de barras com arquivo json
		var dadosJson = $.ajax({
			//url: 'dados.json',
			url: '/graficos/consultar/totaltratativames',
			dataType: 'json',
			async: false
		}).responseText;

		var tabela = new google.visualization.DataTable(dadosJson);

		tabela.sort([{ column: 1, desc: true }]);

		var opcoes = {
					title: 'INDICADOR MENSAL',
					height: 800,					
					legend: 'none',
					is3D: true,
					chartArea:{top:30},
					hAxis: {
						gridlines: {
							color: 'transparent'
						},
						textPosition: 'none'
					},
					annotations: 
					{
						alwaysOutside: true
					}
		}

		var grafico = new google.visualization.BarChart(
			document.getElementById('graficoBarrasJson'));	
		grafico.draw(tabela, opcoes);	
		
}		


function graficosdiario() {

	//grafico de barras com arquivo json
		var dadosJson = $.ajax({
			//url: 'dados.json',
			url: '/graficos/consultar/totaltratativadia',
			dataType: 'json',
			async: false
		}).responseText;

		var tabela = new google.visualization.DataTable(dadosJson);

		tabela.sort([{ column: 1, desc: true }]);

		var opcoes = {
					title: 'INDICADOR DIÁRIO',
					height: 800,					
					legend: 'none',
					is3D: true,
					colors: ['#32CD32'],
					chartArea:{top:30},
					hAxis: {
						gridlines: {
							color: 'transparent'
						},
						textPosition: 'none'
					},
					annotations: 
					{
						alwaysOutside: true
					}
		}

		var grafico = new google.visualization.BarChart(
			document.getElementById('graficoBarrasJson'));	
		grafico.draw(tabela, opcoes);	
		
}		