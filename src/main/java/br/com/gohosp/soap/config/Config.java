package br.com.gohosp.soap.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class Config {
    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("br.com.gohosp.soap.model.tratativa");
        return marshaller;
    }
 
    @Bean
    public SOAPConnector soapConnector(Jaxb2Marshaller marshaller) {
        SOAPConnector client = new SOAPConnector();
        //client.setDefaultUri("https://sulamerica--pjreembdev.cs92.my.salesforce.com/services/Soap/class/WebServiceTratativa");
        //client.setDefaultUri("https://sulamerica--uat.cs93.my.salesforce.com/services/Soap/class/WebServiceTratativa");
        client.setDefaultUri("https://sulamerica.my.salesforce.com/services/wsdl/class/WebServiceTratativa");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}