package br.com.gohosp.soap.config;

import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.TransformerException;

import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;

import br.com.gohosp.soap.model.tratativa.SessionHeader;

public class SoapRequestHeaderModifier implements WebServiceMessageCallback {

	private SessionHeader sessionHeader;
	
	public SoapRequestHeaderModifier(SessionHeader sessionHeader) {
		this.sessionHeader = sessionHeader;
	}
	
	@Override
	public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
		SoapHeader soapHeader = ((SoapMessage)message).getSoapHeader();

        try {
            JAXBContext context = JAXBContext.newInstance(SessionHeader.class);

            Marshaller marshaller = context.createMarshaller();
            marshaller.marshal(sessionHeader, soapHeader.getResult());

        } catch (JAXBException e) {
            throw new IOException("error while marshalling authentication.");
        }
	}
}