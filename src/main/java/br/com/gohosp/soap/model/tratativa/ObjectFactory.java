//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.11 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2021.02.22 às 07:56:12 PM BRT 
//


package br.com.gohosp.soap.model.tratativa;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.gohosp.soap.model.tratativa package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RetornoConsultaTratativaAssunto_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "assunto");
    private final static QName _RetornoConsultaTratativaCarteira_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "carteira");
    private final static QName _RetornoConsultaTratativaCodSegurado_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "codSegurado");
    private final static QName _RetornoConsultaTratativaCpfSegurado_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "cpfSegurado");
    private final static QName _RetornoConsultaTratativaDataIncioVPP_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "dataIncioVPP");
    private final static QName _RetornoConsultaTratativaDataNascimento_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "dataNascimento");
    private final static QName _RetornoConsultaTratativaDataSolicitacao_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "dataSolicitacao");
    private final static QName _RetornoConsultaTratativaEmpresa_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "empresa");
    private final static QName _RetornoConsultaTratativaEstado_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "estado");
    private final static QName _RetornoConsultaTratativaMensagem_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "mensagem");
    private final static QName _RetornoConsultaTratativaMsgId_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "msgId");
    private final static QName _RetornoConsultaTratativaNomeSegurado_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "nomeSegurado");
    private final static QName _RetornoConsultaTratativaNumVPP_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "numVPP");
    private final static QName _RetornoConsultaTratativaPrestador_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "prestador");
    private final static QName _RetornoConsultaTratativaProdSaude_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "prodSaude");
    private final static QName _RetornoConsultaTratativaSexo_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "sexo");
    private final static QName _RetornoConsultaTratativaSlaTratativa_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "slaTratativa");
    private final static QName _RetornoConsultaTratativaTelCelular_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "telCelular");
    private final static QName _RetornoConsultaTratativaTelCobranca_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "telCobranca");
    private final static QName _RetornoConsultaTratativaTelComercial_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "telComercial");
    private final static QName _RetornoConsultaTratativaTelContato_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "telContato");
    private final static QName _RetornoConsultaTratativaTelContato2_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "telContato2");
    private final static QName _RetornoConsultaTratativaTelContato3_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "telContato3");
    private final static QName _RetornoConsultaTratativaTelPrestador_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "telPrestador");
    private final static QName _RetornoConsultaTratativaTelResencial_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "telResencial");
    private final static QName _RetornoConsultaTratativaTratativaId_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "tratativaId");
    private final static QName _RetornoConsultarStatusTratativaSituacao_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "situacao");
    private final static QName _EntradaAtualizarTratativaCid_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "cid");
    private final static QName _EntradaAtualizarTratativaCondTriagem_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "condTriagem");
    private final static QName _EntradaAtualizarTratativaDataInternacao_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "dataInternacao");
    private final static QName _EntradaAtualizarTratativaDataPrimContato_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "dataPrimContato");
    private final static QName _EntradaAtualizarTratativaDescCID_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "descCID");
    private final static QName _EntradaAtualizarTratativaDesfecho_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "desfecho");
    private final static QName _EntradaAtualizarTratativaEmailResponsavel_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "emailResponsavel");
    private final static QName _EntradaAtualizarTratativaERespostaDefinitiva_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "eRespostaDefinitiva");
    private final static QName _EntradaAtualizarTratativaGrauParentesco_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "GrauParentesco");
    private final static QName _EntradaAtualizarTratativaMedico_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "medico");
    private final static QName _EntradaAtualizarTratativaMotivoAlta_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "motivoAlta");
    private final static QName _EntradaAtualizarTratativaMotivoNContato_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "motivoNContato");
    private final static QName _EntradaAtualizarTratativaNomeContato_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "nomeContato");
    private final static QName _EntradaAtualizarTratativaNomeResponsavel_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "nomeResponsavel");
    private final static QName _EntradaAtualizarTratativaNomeUsuario_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "nomeUsuario");
    private final static QName _EntradaAtualizarTratativaRealizouContato_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "realizouContato");
    private final static QName _EntradaAtualizarTratativaTelHospital_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "telHospital");
    private final static QName _EntradaAtualizarTratativaTipoAcomodacao_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "tipoAcomodacao");
    private final static QName _EntradaAtualizarTratativaTipoAssistencia_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "tipoAssistencia");
    private final static QName _EntradaAtualizarTratativaTipoInternacao_QNAME = new QName("http://soap.sforce.com/schemas/class/WebServiceTratativa", "tipoInternacao");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.gohosp.soap.model.tratativa
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AllowFieldTruncationHeader }
     * 
     */
    public AllowFieldTruncationHeader createAllowFieldTruncationHeader() {
        return new AllowFieldTruncationHeader();
    }

    /**
     * Create an instance of {@link CallOptions }
     * 
     */
    public CallOptions createCallOptions() {
        return new CallOptions();
    }

    /**
     * Create an instance of {@link DebuggingHeader }
     * 
     */
    public DebuggingHeader createDebuggingHeader() {
        return new DebuggingHeader();
    }

    /**
     * Create an instance of {@link LogInfo }
     * 
     */
    public LogInfo createLogInfo() {
        return new LogInfo();
    }

    /**
     * Create an instance of {@link DebuggingInfo }
     * 
     */
    public DebuggingInfo createDebuggingInfo() {
        return new DebuggingInfo();
    }

    /**
     * Create an instance of {@link SessionHeader }
     * 
     */
    public SessionHeader createSessionHeader() {
        return new SessionHeader();
    }

    /**
     * Create an instance of {@link AtualizarTratativa }
     * 
     */
    public AtualizarTratativa createAtualizarTratativa() {
        return new AtualizarTratativa();
    }

    /**
     * Create an instance of {@link EntradaAtualizarTratativa }
     * 
     */
    public EntradaAtualizarTratativa createEntradaAtualizarTratativa() {
        return new EntradaAtualizarTratativa();
    }

    /**
     * Create an instance of {@link AtualizarTratativaResponse }
     * 
     */
    public AtualizarTratativaResponse createAtualizarTratativaResponse() {
        return new AtualizarTratativaResponse();
    }

    /**
     * Create an instance of {@link ConfirmarRecebimento }
     * 
     */
    public ConfirmarRecebimento createConfirmarRecebimento() {
        return new ConfirmarRecebimento();
    }

    /**
     * Create an instance of {@link ConfirmarRecebimentoResponse }
     * 
     */
    public ConfirmarRecebimentoResponse createConfirmarRecebimentoResponse() {
        return new ConfirmarRecebimentoResponse();
    }

    /**
     * Create an instance of {@link ConsultaStatusTratativa }
     * 
     */
    public ConsultaStatusTratativa createConsultaStatusTratativa() {
        return new ConsultaStatusTratativa();
    }

    /**
     * Create an instance of {@link ConsultaStatusTratativaResponse }
     * 
     */
    public ConsultaStatusTratativaResponse createConsultaStatusTratativaResponse() {
        return new ConsultaStatusTratativaResponse();
    }

    /**
     * Create an instance of {@link RetornoConsultarStatusTratativa }
     * 
     */
    public RetornoConsultarStatusTratativa createRetornoConsultarStatusTratativa() {
        return new RetornoConsultarStatusTratativa();
    }

    /**
     * Create an instance of {@link ConsultaTratativa }
     * 
     */
    public ConsultaTratativa createConsultaTratativa() {
        return new ConsultaTratativa();
    }

    /**
     * Create an instance of {@link ConsultaTratativaResponse }
     * 
     */
    public ConsultaTratativaResponse createConsultaTratativaResponse() {
        return new ConsultaTratativaResponse();
    }

    /**
     * Create an instance of {@link RetornoConsultaTratativa }
     * 
     */
    public RetornoConsultaTratativa createRetornoConsultaTratativa() {
        return new RetornoConsultaTratativa();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link Location }
     * 
     */
    public Location createLocation() {
        return new Location();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "assunto", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaAssunto(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaAssunto_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "carteira", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaCarteira(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaCarteira_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "codSegurado", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaCodSegurado(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaCodSegurado_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "cpfSegurado", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaCpfSegurado(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaCpfSegurado_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "dataIncioVPP", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaDataIncioVPP(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaDataIncioVPP_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "dataNascimento", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaDataNascimento(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaDataNascimento_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "dataSolicitacao", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaDataSolicitacao(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaDataSolicitacao_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "empresa", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaEmpresa(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaEmpresa_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "estado", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaEstado(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaEstado_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "mensagem", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaMensagem(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaMensagem_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "msgId", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaMsgId(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaMsgId_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "nomeSegurado", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaNomeSegurado(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaNomeSegurado_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "numVPP", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaNumVPP(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaNumVPP_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "prestador", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaPrestador(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaPrestador_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "prodSaude", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaProdSaude(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaProdSaude_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "sexo", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaSexo(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaSexo_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "slaTratativa", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaSlaTratativa(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaSlaTratativa_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "telCelular", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaTelCelular(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaTelCelular_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "telCobranca", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaTelCobranca(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaTelCobranca_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "telComercial", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaTelComercial(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaTelComercial_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "telContato", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaTelContato(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaTelContato_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "telContato2", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaTelContato2(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaTelContato2_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "telContato3", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaTelContato3(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaTelContato3_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "telPrestador", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaTelPrestador(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaTelPrestador_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "telResencial", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaTelResencial(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaTelResencial_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "tratativaId", scope = RetornoConsultaTratativa.class)
    public JAXBElement<String> createRetornoConsultaTratativaTratativaId(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaTratativaId_QNAME, String.class, RetornoConsultaTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "situacao", scope = RetornoConsultarStatusTratativa.class)
    public JAXBElement<String> createRetornoConsultarStatusTratativaSituacao(String value) {
        return new JAXBElement<String>(_RetornoConsultarStatusTratativaSituacao_QNAME, String.class, RetornoConsultarStatusTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "tratativaId", scope = RetornoConsultarStatusTratativa.class)
    public JAXBElement<String> createRetornoConsultarStatusTratativaTratativaId(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaTratativaId_QNAME, String.class, RetornoConsultarStatusTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "cid", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaCid(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaCid_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "condTriagem", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaCondTriagem(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaCondTriagem_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "dataInternacao", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaDataInternacao(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaDataInternacao_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "dataPrimContato", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaDataPrimContato(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaDataPrimContato_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "descCID", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaDescCID(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaDescCID_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "desfecho", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaDesfecho(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaDesfecho_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "emailResponsavel", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaEmailResponsavel(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaEmailResponsavel_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "eRespostaDefinitiva", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<Integer> createEntradaAtualizarTratativaERespostaDefinitiva(Integer value) {
        return new JAXBElement<Integer>(_EntradaAtualizarTratativaERespostaDefinitiva_QNAME, Integer.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "GrauParentesco", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaGrauParentesco(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaGrauParentesco_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "medico", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaMedico(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaMedico_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "mensagem", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaMensagem(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaMensagem_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "motivoAlta", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaMotivoAlta(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaMotivoAlta_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "motivoNContato", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaMotivoNContato(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaMotivoNContato_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "msgId", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaMsgId(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaMsgId_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "nomeContato", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaNomeContato(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaNomeContato_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "nomeResponsavel", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaNomeResponsavel(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaNomeResponsavel_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "nomeUsuario", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaNomeUsuario(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaNomeUsuario_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "realizouContato", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaRealizouContato(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaRealizouContato_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "telHospital", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaTelHospital(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaTelHospital_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "tipoAcomodacao", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaTipoAcomodacao(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaTipoAcomodacao_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "tipoAssistencia", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaTipoAssistencia(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaTipoAssistencia_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "tipoInternacao", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaTipoInternacao(String value) {
        return new JAXBElement<String>(_EntradaAtualizarTratativaTipoInternacao_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", name = "tratativaId", scope = EntradaAtualizarTratativa.class)
    public JAXBElement<String> createEntradaAtualizarTratativaTratativaId(String value) {
        return new JAXBElement<String>(_RetornoConsultaTratativaTratativaId_QNAME, String.class, EntradaAtualizarTratativa.class, value);
    }

}
