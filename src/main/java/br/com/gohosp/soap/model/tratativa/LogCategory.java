//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.11 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2021.02.22 às 07:56:12 PM BRT 
//


package br.com.gohosp.soap.model.tratativa;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de LogCategory.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="LogCategory"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Db"/&gt;
 *     &lt;enumeration value="Workflow"/&gt;
 *     &lt;enumeration value="Validation"/&gt;
 *     &lt;enumeration value="Callout"/&gt;
 *     &lt;enumeration value="Apex_code"/&gt;
 *     &lt;enumeration value="Apex_profiling"/&gt;
 *     &lt;enumeration value="Visualforce"/&gt;
 *     &lt;enumeration value="System"/&gt;
 *     &lt;enumeration value="Wave"/&gt;
 *     &lt;enumeration value="Nba"/&gt;
 *     &lt;enumeration value="All"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "LogCategory")
@XmlEnum
public enum LogCategory {

    @XmlEnumValue("Db")
    DB("Db"),
    @XmlEnumValue("Workflow")
    WORKFLOW("Workflow"),
    @XmlEnumValue("Validation")
    VALIDATION("Validation"),
    @XmlEnumValue("Callout")
    CALLOUT("Callout"),
    @XmlEnumValue("Apex_code")
    APEX_CODE("Apex_code"),
    @XmlEnumValue("Apex_profiling")
    APEX_PROFILING("Apex_profiling"),
    @XmlEnumValue("Visualforce")
    VISUALFORCE("Visualforce"),
    @XmlEnumValue("System")
    SYSTEM("System"),
    @XmlEnumValue("Wave")
    WAVE("Wave"),
    @XmlEnumValue("Nba")
    NBA("Nba"),
    @XmlEnumValue("All")
    ALL("All");
    private final String value;

    LogCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LogCategory fromValue(String v) {
        for (LogCategory c: LogCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
