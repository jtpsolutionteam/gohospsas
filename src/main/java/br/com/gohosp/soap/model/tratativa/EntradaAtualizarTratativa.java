//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.11 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2021.02.22 às 07:56:12 PM BRT 
//


package br.com.gohosp.soap.model.tratativa;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de EntradaAtualizarTratativa complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="EntradaAtualizarTratativa"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="condTriagem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dataInternacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dataPrimContato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="descCID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="desfecho" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="emailResponsavel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eRespostaDefinitiva" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="GrauParentesco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="medico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mensagem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="motivoAlta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="motivoNContato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="msgId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nomeContato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nomeResponsavel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nomeUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="realizouContato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="telHospital" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tipoAcomodacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tipoAssistencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tipoInternacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tratativaId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntradaAtualizarTratativa", propOrder = {
    "cid",
    "condTriagem",
    "dataInternacao",
    "dataPrimContato",
    "descCID",
    "desfecho",
    "emailResponsavel",
    "eRespostaDefinitiva",
    "grauParentesco",
    "medico",
    "mensagem",
    "motivoAlta",
    "motivoNContato",
    "msgId",
    "nomeContato",
    "nomeResponsavel",
    "nomeUsuario",
    "realizouContato",
    "telHospital",
    "tipoAcomodacao",
    "tipoAssistencia",
    "tipoInternacao",
    "tratativaId"
})
public class EntradaAtualizarTratativa {

    @XmlElementRef(name = "cid", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cid;
    @XmlElementRef(name = "condTriagem", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> condTriagem;
    @XmlElementRef(name = "dataInternacao", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataInternacao;
    @XmlElementRef(name = "dataPrimContato", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataPrimContato;
    @XmlElementRef(name = "descCID", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> descCID;
    @XmlElementRef(name = "desfecho", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> desfecho;
    @XmlElementRef(name = "emailResponsavel", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emailResponsavel;
    @XmlElementRef(name = "eRespostaDefinitiva", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> eRespostaDefinitiva;
    @XmlElementRef(name = "GrauParentesco", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> grauParentesco;
    @XmlElementRef(name = "medico", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> medico;
    @XmlElementRef(name = "mensagem", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mensagem;
    @XmlElementRef(name = "motivoAlta", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> motivoAlta;
    @XmlElementRef(name = "motivoNContato", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> motivoNContato;
    @XmlElementRef(name = "msgId", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> msgId;
    @XmlElementRef(name = "nomeContato", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nomeContato;
    @XmlElementRef(name = "nomeResponsavel", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nomeResponsavel;
    @XmlElementRef(name = "nomeUsuario", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nomeUsuario;
    @XmlElementRef(name = "realizouContato", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> realizouContato;
    @XmlElementRef(name = "telHospital", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> telHospital;
    @XmlElementRef(name = "tipoAcomodacao", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipoAcomodacao;
    @XmlElementRef(name = "tipoAssistencia", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipoAssistencia;
    @XmlElementRef(name = "tipoInternacao", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipoInternacao;
    @XmlElementRef(name = "tratativaId", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tratativaId;

    /**
     * Obtém o valor da propriedade cid.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCid() {
        return cid;
    }

    /**
     * Define o valor da propriedade cid.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCid(JAXBElement<String> value) {
        this.cid = value;
    }

    /**
     * Obtém o valor da propriedade condTriagem.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCondTriagem() {
        return condTriagem;
    }

    /**
     * Define o valor da propriedade condTriagem.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCondTriagem(JAXBElement<String> value) {
        this.condTriagem = value;
    }

    /**
     * Obtém o valor da propriedade dataInternacao.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDataInternacao() {
        return dataInternacao;
    }

    /**
     * Define o valor da propriedade dataInternacao.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDataInternacao(JAXBElement<String> value) {
        this.dataInternacao = value;
    }

    /**
     * Obtém o valor da propriedade dataPrimContato.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDataPrimContato() {
        return dataPrimContato;
    }

    /**
     * Define o valor da propriedade dataPrimContato.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDataPrimContato(JAXBElement<String> value) {
        this.dataPrimContato = value;
    }

    /**
     * Obtém o valor da propriedade descCID.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescCID() {
        return descCID;
    }

    /**
     * Define o valor da propriedade descCID.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescCID(JAXBElement<String> value) {
        this.descCID = value;
    }

    /**
     * Obtém o valor da propriedade desfecho.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDesfecho() {
        return desfecho;
    }

    /**
     * Define o valor da propriedade desfecho.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDesfecho(JAXBElement<String> value) {
        this.desfecho = value;
    }

    /**
     * Obtém o valor da propriedade emailResponsavel.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmailResponsavel() {
        return emailResponsavel;
    }

    /**
     * Define o valor da propriedade emailResponsavel.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmailResponsavel(JAXBElement<String> value) {
        this.emailResponsavel = value;
    }

    /**
     * Obtém o valor da propriedade eRespostaDefinitiva.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getERespostaDefinitiva() {
        return eRespostaDefinitiva;
    }

    /**
     * Define o valor da propriedade eRespostaDefinitiva.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setERespostaDefinitiva(JAXBElement<Integer> value) {
        this.eRespostaDefinitiva = value;
    }

    /**
     * Obtém o valor da propriedade grauParentesco.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGrauParentesco() {
        return grauParentesco;
    }

    /**
     * Define o valor da propriedade grauParentesco.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGrauParentesco(JAXBElement<String> value) {
        this.grauParentesco = value;
    }

    /**
     * Obtém o valor da propriedade medico.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMedico() {
        return medico;
    }

    /**
     * Define o valor da propriedade medico.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMedico(JAXBElement<String> value) {
        this.medico = value;
    }

    /**
     * Obtém o valor da propriedade mensagem.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMensagem() {
        return mensagem;
    }

    /**
     * Define o valor da propriedade mensagem.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMensagem(JAXBElement<String> value) {
        this.mensagem = value;
    }

    /**
     * Obtém o valor da propriedade motivoAlta.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMotivoAlta() {
        return motivoAlta;
    }

    /**
     * Define o valor da propriedade motivoAlta.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMotivoAlta(JAXBElement<String> value) {
        this.motivoAlta = value;
    }

    /**
     * Obtém o valor da propriedade motivoNContato.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMotivoNContato() {
        return motivoNContato;
    }

    /**
     * Define o valor da propriedade motivoNContato.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMotivoNContato(JAXBElement<String> value) {
        this.motivoNContato = value;
    }

    /**
     * Obtém o valor da propriedade msgId.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMsgId() {
        return msgId;
    }

    /**
     * Define o valor da propriedade msgId.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMsgId(JAXBElement<String> value) {
        this.msgId = value;
    }

    /**
     * Obtém o valor da propriedade nomeContato.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNomeContato() {
        return nomeContato;
    }

    /**
     * Define o valor da propriedade nomeContato.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNomeContato(JAXBElement<String> value) {
        this.nomeContato = value;
    }

    /**
     * Obtém o valor da propriedade nomeResponsavel.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNomeResponsavel() {
        return nomeResponsavel;
    }

    /**
     * Define o valor da propriedade nomeResponsavel.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNomeResponsavel(JAXBElement<String> value) {
        this.nomeResponsavel = value;
    }

    /**
     * Obtém o valor da propriedade nomeUsuario.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNomeUsuario() {
        return nomeUsuario;
    }

    /**
     * Define o valor da propriedade nomeUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNomeUsuario(JAXBElement<String> value) {
        this.nomeUsuario = value;
    }

    /**
     * Obtém o valor da propriedade realizouContato.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRealizouContato() {
        return realizouContato;
    }

    /**
     * Define o valor da propriedade realizouContato.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRealizouContato(JAXBElement<String> value) {
        this.realizouContato = value;
    }

    /**
     * Obtém o valor da propriedade telHospital.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTelHospital() {
        return telHospital;
    }

    /**
     * Define o valor da propriedade telHospital.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTelHospital(JAXBElement<String> value) {
        this.telHospital = value;
    }

    /**
     * Obtém o valor da propriedade tipoAcomodacao.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipoAcomodacao() {
        return tipoAcomodacao;
    }

    /**
     * Define o valor da propriedade tipoAcomodacao.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipoAcomodacao(JAXBElement<String> value) {
        this.tipoAcomodacao = value;
    }

    /**
     * Obtém o valor da propriedade tipoAssistencia.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipoAssistencia() {
        return tipoAssistencia;
    }

    /**
     * Define o valor da propriedade tipoAssistencia.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipoAssistencia(JAXBElement<String> value) {
        this.tipoAssistencia = value;
    }

    /**
     * Obtém o valor da propriedade tipoInternacao.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipoInternacao() {
        return tipoInternacao;
    }

    /**
     * Define o valor da propriedade tipoInternacao.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipoInternacao(JAXBElement<String> value) {
        this.tipoInternacao = value;
    }

    /**
     * Obtém o valor da propriedade tratativaId.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTratativaId() {
        return tratativaId;
    }

    /**
     * Define o valor da propriedade tratativaId.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTratativaId(JAXBElement<String> value) {
        this.tratativaId = value;
    }

}
