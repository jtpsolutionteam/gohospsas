//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.11 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2021.02.22 às 07:56:12 PM BRT 
//


package br.com.gohosp.soap.model.tratativa;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="categories" type="{http://soap.sforce.com/schemas/class/WebServiceTratativa}LogInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="debugLevel" type="{http://soap.sforce.com/schemas/class/WebServiceTratativa}LogType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "categories",
    "debugLevel"
})
@XmlRootElement(name = "DebuggingHeader")
public class DebuggingHeader {

    protected List<LogInfo> categories;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected LogType debugLevel;

    /**
     * Gets the value of the categories property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the categories property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCategories().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LogInfo }
     * 
     * 
     */
    public List<LogInfo> getCategories() {
        if (categories == null) {
            categories = new ArrayList<LogInfo>();
        }
        return this.categories;
    }

    /**
     * Obtém o valor da propriedade debugLevel.
     * 
     * @return
     *     possible object is
     *     {@link LogType }
     *     
     */
    public LogType getDebugLevel() {
        return debugLevel;
    }

    /**
     * Define o valor da propriedade debugLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link LogType }
     *     
     */
    public void setDebugLevel(LogType value) {
        this.debugLevel = value;
    }

}
