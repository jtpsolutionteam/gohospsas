//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.11 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2021.02.22 às 07:56:12 PM BRT 
//


package br.com.gohosp.soap.model.tratativa;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de RetornoConsultaTratativa complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RetornoConsultaTratativa"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="assunto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="carteira" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codSegurado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cpfSegurado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dataIncioVPP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dataNascimento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dataSolicitacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="empresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mensagem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="msgId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nomeSegurado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numVPP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="prestador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="prodSaude" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="slaTratativa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="telCelular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="telCobranca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="telComercial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="telContato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="telContato2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="telContato3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="telPrestador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="telResencial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tratativaId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetornoConsultaTratativa", propOrder = {
    "assunto",
    "carteira",
    "codSegurado",
    "cpfSegurado",
    "dataIncioVPP",
    "dataNascimento",
    "dataSolicitacao",
    "empresa",
    "estado",
    "mensagem",
    "msgId",
    "nomeSegurado",
    "numVPP",
    "prestador",
    "prodSaude",
    "sexo",
    "slaTratativa",
    "telCelular",
    "telCobranca",
    "telComercial",
    "telContato",
    "telContato2",
    "telContato3",
    "telPrestador",
    "telResencial",
    "tratativaId"
})
public class RetornoConsultaTratativa {

    @XmlElementRef(name = "assunto", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> assunto;
    @XmlElementRef(name = "carteira", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> carteira;
    @XmlElementRef(name = "codSegurado", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codSegurado;
    @XmlElementRef(name = "cpfSegurado", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cpfSegurado;
    @XmlElementRef(name = "dataIncioVPP", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataIncioVPP;
    @XmlElementRef(name = "dataNascimento", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataNascimento;
    @XmlElementRef(name = "dataSolicitacao", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataSolicitacao;
    @XmlElementRef(name = "empresa", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> empresa;
    @XmlElementRef(name = "estado", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> estado;
    @XmlElementRef(name = "mensagem", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mensagem;
    @XmlElementRef(name = "msgId", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> msgId;
    @XmlElementRef(name = "nomeSegurado", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nomeSegurado;
    @XmlElementRef(name = "numVPP", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numVPP;
    @XmlElementRef(name = "prestador", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prestador;
    @XmlElementRef(name = "prodSaude", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prodSaude;
    @XmlElementRef(name = "sexo", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sexo;
    @XmlElementRef(name = "slaTratativa", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> slaTratativa;
    @XmlElementRef(name = "telCelular", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> telCelular;
    @XmlElementRef(name = "telCobranca", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> telCobranca;
    @XmlElementRef(name = "telComercial", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> telComercial;
    @XmlElementRef(name = "telContato", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> telContato;
    @XmlElementRef(name = "telContato2", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> telContato2;
    @XmlElementRef(name = "telContato3", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> telContato3;
    @XmlElementRef(name = "telPrestador", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> telPrestador;
    @XmlElementRef(name = "telResencial", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> telResencial;
    @XmlElementRef(name = "tratativaId", namespace = "http://soap.sforce.com/schemas/class/WebServiceTratativa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tratativaId;

    /**
     * Obtém o valor da propriedade assunto.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAssunto() {
        return assunto;
    }

    /**
     * Define o valor da propriedade assunto.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAssunto(JAXBElement<String> value) {
        this.assunto = value;
    }

    /**
     * Obtém o valor da propriedade carteira.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCarteira() {
        return carteira;
    }

    /**
     * Define o valor da propriedade carteira.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCarteira(JAXBElement<String> value) {
        this.carteira = value;
    }

    /**
     * Obtém o valor da propriedade codSegurado.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodSegurado() {
        return codSegurado;
    }

    /**
     * Define o valor da propriedade codSegurado.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodSegurado(JAXBElement<String> value) {
        this.codSegurado = value;
    }

    /**
     * Obtém o valor da propriedade cpfSegurado.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCpfSegurado() {
        return cpfSegurado;
    }

    /**
     * Define o valor da propriedade cpfSegurado.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCpfSegurado(JAXBElement<String> value) {
        this.cpfSegurado = value;
    }

    /**
     * Obtém o valor da propriedade dataIncioVPP.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDataIncioVPP() {
        return dataIncioVPP;
    }

    /**
     * Define o valor da propriedade dataIncioVPP.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDataIncioVPP(JAXBElement<String> value) {
        this.dataIncioVPP = value;
    }

    /**
     * Obtém o valor da propriedade dataNascimento.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDataNascimento() {
        return dataNascimento;
    }

    /**
     * Define o valor da propriedade dataNascimento.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDataNascimento(JAXBElement<String> value) {
        this.dataNascimento = value;
    }

    /**
     * Obtém o valor da propriedade dataSolicitacao.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDataSolicitacao() {
        return dataSolicitacao;
    }

    /**
     * Define o valor da propriedade dataSolicitacao.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDataSolicitacao(JAXBElement<String> value) {
        this.dataSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade empresa.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmpresa() {
        return empresa;
    }

    /**
     * Define o valor da propriedade empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmpresa(JAXBElement<String> value) {
        this.empresa = value;
    }

    /**
     * Obtém o valor da propriedade estado.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEstado() {
        return estado;
    }

    /**
     * Define o valor da propriedade estado.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEstado(JAXBElement<String> value) {
        this.estado = value;
    }

    /**
     * Obtém o valor da propriedade mensagem.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMensagem() {
        return mensagem;
    }

    /**
     * Define o valor da propriedade mensagem.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMensagem(JAXBElement<String> value) {
        this.mensagem = value;
    }

    /**
     * Obtém o valor da propriedade msgId.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMsgId() {
        return msgId;
    }

    /**
     * Define o valor da propriedade msgId.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMsgId(JAXBElement<String> value) {
        this.msgId = value;
    }

    /**
     * Obtém o valor da propriedade nomeSegurado.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNomeSegurado() {
        return nomeSegurado;
    }

    /**
     * Define o valor da propriedade nomeSegurado.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNomeSegurado(JAXBElement<String> value) {
        this.nomeSegurado = value;
    }

    /**
     * Obtém o valor da propriedade numVPP.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumVPP() {
        return numVPP;
    }

    /**
     * Define o valor da propriedade numVPP.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumVPP(JAXBElement<String> value) {
        this.numVPP = value;
    }

    /**
     * Obtém o valor da propriedade prestador.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrestador() {
        return prestador;
    }

    /**
     * Define o valor da propriedade prestador.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrestador(JAXBElement<String> value) {
        this.prestador = value;
    }

    /**
     * Obtém o valor da propriedade prodSaude.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProdSaude() {
        return prodSaude;
    }

    /**
     * Define o valor da propriedade prodSaude.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProdSaude(JAXBElement<String> value) {
        this.prodSaude = value;
    }

    /**
     * Obtém o valor da propriedade sexo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSexo() {
        return sexo;
    }

    /**
     * Define o valor da propriedade sexo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSexo(JAXBElement<String> value) {
        this.sexo = value;
    }

    /**
     * Obtém o valor da propriedade slaTratativa.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSlaTratativa() {
        return slaTratativa;
    }

    /**
     * Define o valor da propriedade slaTratativa.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSlaTratativa(JAXBElement<String> value) {
        this.slaTratativa = value;
    }

    /**
     * Obtém o valor da propriedade telCelular.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTelCelular() {
        return telCelular;
    }

    /**
     * Define o valor da propriedade telCelular.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTelCelular(JAXBElement<String> value) {
        this.telCelular = value;
    }

    /**
     * Obtém o valor da propriedade telCobranca.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTelCobranca() {
        return telCobranca;
    }

    /**
     * Define o valor da propriedade telCobranca.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTelCobranca(JAXBElement<String> value) {
        this.telCobranca = value;
    }

    /**
     * Obtém o valor da propriedade telComercial.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTelComercial() {
        return telComercial;
    }

    /**
     * Define o valor da propriedade telComercial.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTelComercial(JAXBElement<String> value) {
        this.telComercial = value;
    }

    /**
     * Obtém o valor da propriedade telContato.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTelContato() {
        return telContato;
    }

    /**
     * Define o valor da propriedade telContato.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTelContato(JAXBElement<String> value) {
        this.telContato = value;
    }

    /**
     * Obtém o valor da propriedade telContato2.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTelContato2() {
        return telContato2;
    }

    /**
     * Define o valor da propriedade telContato2.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTelContato2(JAXBElement<String> value) {
        this.telContato2 = value;
    }

    /**
     * Obtém o valor da propriedade telContato3.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTelContato3() {
        return telContato3;
    }

    /**
     * Define o valor da propriedade telContato3.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTelContato3(JAXBElement<String> value) {
        this.telContato3 = value;
    }

    /**
     * Obtém o valor da propriedade telPrestador.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTelPrestador() {
        return telPrestador;
    }

    /**
     * Define o valor da propriedade telPrestador.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTelPrestador(JAXBElement<String> value) {
        this.telPrestador = value;
    }

    /**
     * Obtém o valor da propriedade telResencial.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTelResencial() {
        return telResencial;
    }

    /**
     * Define o valor da propriedade telResencial.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTelResencial(JAXBElement<String> value) {
        this.telResencial = value;
    }

    /**
     * Obtém o valor da propriedade tratativaId.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTratativaId() {
        return tratativaId;
    }

    /**
     * Define o valor da propriedade tratativaId.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTratativaId(JAXBElement<String> value) {
        this.tratativaId = value;
    }

}
