package br.com.gohosp.soap;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.gohosp.soap.model.tratativa.AtualizarTratativa;
import br.com.gohosp.soap.model.tratativa.AtualizarTratativaResponse;
import br.com.gohosp.soap.model.tratativa.ConfirmarRecebimento;
import br.com.gohosp.soap.model.tratativa.ConfirmarRecebimentoResponse;
import br.com.gohosp.soap.model.tratativa.ConsultaStatusTratativa;
import br.com.gohosp.soap.model.tratativa.ConsultaStatusTratativaResponse;
import br.com.gohosp.soap.model.tratativa.ConsultaTratativa;
import br.com.gohosp.soap.model.tratativa.ConsultaTratativaResponse;
import br.com.gohosp.soap.model.tratativa.EntradaAtualizarTratativa;
import br.com.gohosp.soap.model.tratativa.RetornoConsultaTratativa;
import br.com.gohosp.soap.model.tratativa.RetornoConsultarStatusTratativa;

public class Exemplo {
	
	@Autowired
	SulAmericaService sulAmericaService;

	private void exemplo() throws Exception {
		AtualizarTratativa atualizarTratativa = new AtualizarTratativa();
		AtualizarTratativaResponse atualizarTratativaResponse = sulAmericaService.atualizarTratativa(atualizarTratativa);
		
		ConfirmarRecebimento confirmarRecebimento = new ConfirmarRecebimento();
		ConfirmarRecebimentoResponse confirmarRecebimentoResponse = sulAmericaService.confirmarRecebimento(confirmarRecebimento);
		
		ConsultaStatusTratativa consultaStatusTratativa = new ConsultaStatusTratativa();		
		ConsultaStatusTratativaResponse consultaStatusTratativaResponse = sulAmericaService.consultaStatusTratativa(consultaStatusTratativa);
	
		List<RetornoConsultarStatusTratativa> t = consultaStatusTratativaResponse.getResult();
		
		for (RetornoConsultarStatusTratativa atual : t) {
			
			System.out.println(atual.getTratativaId().getValue());
			
		}
		
		
		ConsultaTratativa consultaTratativa = new ConsultaTratativa();		
		ConsultaTratativaResponse consultaTratativaResponse = sulAmericaService.consultaTratativa(consultaTratativa);
		
		List<RetornoConsultaTratativa> t2 = consultaTratativaResponse.getResult();
		
		for (RetornoConsultaTratativa atual : t2) {
			
			System.out.println(atual.getTratativaId().getValue());
			
		}
		
		
	}
	
	
}
