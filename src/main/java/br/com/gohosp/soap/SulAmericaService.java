package br.com.gohosp.soap;

import java.net.URI;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.gohosp.soap.config.SOAPConnector;
import br.com.gohosp.soap.config.SoapRequestHeaderModifier;
import br.com.gohosp.soap.model.tratativa.AtualizarTratativa;
import br.com.gohosp.soap.model.tratativa.AtualizarTratativaResponse;
import br.com.gohosp.soap.model.tratativa.ConfirmarRecebimento;
import br.com.gohosp.soap.model.tratativa.ConfirmarRecebimentoResponse;
import br.com.gohosp.soap.model.tratativa.ConsultaStatusTratativa;
import br.com.gohosp.soap.model.tratativa.ConsultaStatusTratativaResponse;
import br.com.gohosp.soap.model.tratativa.ConsultaTratativa;
import br.com.gohosp.soap.model.tratativa.ConsultaTratativaResponse;
import br.com.gohosp.soap.model.tratativa.SessionHeader;

@Service
public class SulAmericaService {
	
	@Autowired
	private SOAPConnector soapConnector;
	
	//private String url = "https://sulamerica--pjreembdev.cs92.my.salesforce.com/services/Soap/class/WebServiceTratativa";
	//private String url = "https://sulamerica--uat.cs93.my.salesforce.com/services/Soap/class/WebServiceTratativa";
	private String url = "https://sulamerica.my.salesforce.com/services/Soap/class/WebServiceTratativa";
	
	
	public ConsultaStatusTratativaResponse consultaStatusTratativa(ConsultaStatusTratativa consultaStatusTratativa) throws Exception {
		ConsultaStatusTratativaResponse response = (ConsultaStatusTratativaResponse) soapConnector.callWebService(this.url, consultaStatusTratativa, new SoapRequestHeaderModifier(this.getSession()));
		return response;
	}
	
	public ConfirmarRecebimentoResponse confirmarRecebimento(ConfirmarRecebimento confirmarRecebimento) throws Exception {
		ConfirmarRecebimentoResponse response = (ConfirmarRecebimentoResponse) soapConnector.callWebService(this.url, confirmarRecebimento, new SoapRequestHeaderModifier(this.getSession()));
		return response;
	}
	
	public AtualizarTratativaResponse atualizarTratativa(AtualizarTratativa atualizarTratativa) throws Exception {
		AtualizarTratativaResponse response = (AtualizarTratativaResponse) soapConnector.callWebService(this.url, atualizarTratativa, new SoapRequestHeaderModifier(this.getSession()));
		return response;
	}
	
	public ConsultaTratativaResponse consultaTratativa(ConsultaTratativa consultaTratativa) throws Exception {
		ConsultaTratativaResponse response = (ConsultaTratativaResponse) soapConnector.callWebService(this.url, consultaTratativa, new SoapRequestHeaderModifier(this.getSession()));
		return response;
	}
	
	private SessionHeader getSession() throws Exception {
		RestTemplate restTemplate =  new RestTemplate();
		//final String baseUrl = "https://test.salesforce.com/services/Soap/c/39.0";
		//final String baseUrl = "https://sulamerica--uat.cs93.my.salesforce.com/services/Soap/c/46.0";
		final String baseUrl = "https://sulamerica.my.salesforce.com/services/Soap/c/46.0";
	    URI uri = new URI(baseUrl);
	    
	    /*
	    String body = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:urn='urn:enterprise.soap.sforce.com'>" + 
	    		"<soapenv:Header/>" + 
	    		"<soapenv:Body>" + 
	    		"<urn:login>" + 
	    		"<urn:username>integrawelness@sulamerica.com.br.pjreembdev</urn:username>" + 
	    		"<urn:password>dev@1234561ay6mHpiS2VKzsi3PPg6SyDsQ</urn:password>" + 
	    		"</urn:login>" + 
	    		"</soapenv:Body>" + 
	    		"</soapenv:Envelope>";
	    */
	    
	    /*
	    String body = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:urn='urn:enterprise.soap.sforce.com'>" + 
	    		"<soapenv:Header/>" + 
	    		"<soapenv:Body>" + 
	    		"<urn:login>" + 
	    		"<urn:username>dnascimentosaudecare@sulamerica.com.br.uat</urn:username>" + 
	    		"<urn:password>abcd123456</urn:password>" + 
	    		"</urn:login>" + 
	    		"</soapenv:Body>" + 
	    		"</soapenv:Envelope>";
		*/
	    
	    
	    String body = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:urn='urn:enterprise.soap.sforce.com'>" + 
	    		"<soapenv:Header/>" + 
	    		"<soapenv:Body>" + 
	    		"<urn:login>" + 
	    		"<urn:username>sergioaugusto.jtpsolution@sulamerica.com.br</urn:username>" + 
	    		"<urn:password>saude#$2019</urn:password>" + 
	    		"</urn:login>" + 
	    		"</soapenv:Body>" + 
	    		"</soapenv:Envelope>";
		
	    
	    /*
	    String body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" + 
	    		" <soapenv:Body>" + 
	    		"     <login xmlns=\"urn:enterprise.soap.sforce.com\">" + 
	    		"         <username>sergioaugusto.jtpsolution@sulamerica.com.br</username>" + 
	    		"         <password>saude#$2019</password>" + 
	    		"     </login>" + 
	    		" </soapenv:Body>" + 
	    		"</soapenv:Envelope>";
		*/
	    
	    
	    HttpHeaders headers = new HttpHeaders();
	    //headers.add("SOAPAction", "https://test.salesforce.com/services/Soap/c/39.0");
	    //headers.add("SOAPAction", "https://sulamerica--uat.cs93.my.salesforce.com/services/Soap/c/46.0");
	    headers.add("SOAPAction", "https://sulamerica.my.salesforce.com/services/Soap/c/46.0");
	    headers.add("Content-type",MediaType.TEXT_XML_VALUE);
	    HttpEntity<String> request = new HttpEntity<String>(body, headers);
		
    	ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, request, String.class);
    	Document doc = Jsoup.parse(response.getBody());    	
    	SessionHeader sessionHeader = new SessionHeader();
		String sessionId = doc.select("sessionid").first().text();
		sessionHeader.setSessionId(sessionId);
		return sessionHeader;
	}

}
