package br.com.gohosp.ans;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.faturamento.VwTabFaturamentoV3Obj;
import br.com.gohosp.modulos.faturamento.service.FaturamentoService;
import br.com.gohosp.util.GeneralParser;
import br.gov.ans.CabecalhoTransacao;
import br.gov.ans.CabecalhoTransacao.Destino;
import br.gov.ans.CabecalhoTransacao.IdentificacaoTransacao;
import br.gov.ans.CabecalhoTransacao.Origem;
import br.gov.ans.CabecalhoTransacao.Origem.IdentificacaoPrestador;
import br.gov.ans.CtAutorizacaoInternacao;
import br.gov.ans.CtBeneficiarioDados;
import br.gov.ans.CtContratadoDados;
import br.gov.ans.CtGuiaCabecalho;
import br.gov.ans.CtGuiaValorTotal;
import br.gov.ans.CtProcedimentoDados;
import br.gov.ans.CtProcedimentoExecutadoInt;
import br.gov.ans.CtmGuiaLote;
import br.gov.ans.CtmInternacaoDados;
import br.gov.ans.CtmInternacaoDadosSaida;
import br.gov.ans.CtmInternacaoResumoGuia;
import br.gov.ans.CtmInternacaoResumoGuia.DadosExecutante;
import br.gov.ans.CtmInternacaoResumoGuia.ProcedimentosExecutados;
import br.gov.ans.DmSimNao;
import br.gov.ans.DmTipoTransacao;
import br.gov.ans.Epilogo;
import br.gov.ans.MensagemTISS;
import br.gov.ans.PrestadorOperadora;

@Service
public class AnsService {
	
	@Autowired
	private FaturamentoService faturamentoService;
	
	
	
	
	public static void main(String[] args) throws Exception {
		new AnsService().gerarXml(1);
	}
	
	public String gerarXml(Integer cdLoteInterno) throws Exception {
		
		Date dtSystem = new Date();
		
		MensagemTISS mensagem = new MensagemTISS();
		mensagem.setCabecalho(buildCabecalhoTransacao(cdLoteInterno, dtSystem));
		mensagem.setPrestadorParaOperadora(buildPrestadorOperadora(cdLoteInterno, dtSystem));
		mensagem.setEpilogo(buildEpilogo());

		return parseObjectToXml(mensagem);
	}

	private static String parseObjectToXml(MensagemTISS mensagem)throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(MensagemTISS.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "iso-8859-1");
		jaxbMarshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new ANSPrefixMapper());
		jaxbMarshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.TRUE);
		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.ans.gov.br/padroes/tiss/schemas http://www.ans.gov.br/padroes/tiss/schemas/tissV3_03_03.xsd");
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(mensagem, sw);

		String xmlContent = sw.toString();
		System.out.println( xmlContent );
		return xmlContent;
	}

	private static CabecalhoTransacao buildCabecalhoTransacao(Integer cdLoteInterno, Date dtSystem){

		
		
		IdentificacaoTransacao identificacaoTransacao = new IdentificacaoTransacao();
		identificacaoTransacao.setTipoTransacao(DmTipoTransacao.ENVIO_LOTE_GUIAS);
		identificacaoTransacao.setSequencialTransacao(String.valueOf(cdLoteInterno));   //Lote interno
		identificacaoTransacao.setDataRegistroTransacao(dateToXML(dtSystem));  //2020-02-06
		identificacaoTransacao.setHoraRegistroTransacao(timeToXML(dtSystem)); // 11:00:00

		Destino destino = new Destino();
		destino.setRegistroANS("400289");

		Origem origem = new Origem();
		IdentificacaoPrestador identificacaoPrestador = new IdentificacaoPrestador();
		identificacaoPrestador.setCodigoPrestadorNaOperadora("05094642000110"); 
		origem.setIdentificacaoPrestador(identificacaoPrestador);

		CabecalhoTransacao cabecalhoTransacao = new CabecalhoTransacao();
		cabecalhoTransacao.setIdentificacaoTransacao(identificacaoTransacao);
		cabecalhoTransacao.setDestino(destino);
		cabecalhoTransacao.setOrigem(origem);
		cabecalhoTransacao.setPadrao("3.03.03");
		return cabecalhoTransacao;
	}

	private static Epilogo buildEpilogo(){
		Epilogo epilogo = new Epilogo();
		epilogo.setHash("7617c317d1af87ea3b564229be9aaf6f");
		return epilogo;
	}


	private PrestadorOperadora buildPrestadorOperadora(Integer cdLoteInterno, Date dtSystem) {
		
		List<VwTabFaturamentoV3Obj> listTratativas = faturamentoService.consultarLoteInterno(cdLoteInterno);
		
		
		CtmGuiaLote.GuiasTISS guiasTISS = new CtmGuiaLote.GuiasTISS();
		
		for (VwTabFaturamentoV3Obj atual : listTratativas) {
		
		CtGuiaCabecalho ctGuiaCabecalho = new CtGuiaCabecalho();
		ctGuiaCabecalho.setRegistroANS("400289");
		ctGuiaCabecalho.setNumeroGuiaPrestador(String.valueOf(atual.getTxNumVpp())); //Lote interno + 001, 002
		
		CtAutorizacaoInternacao ctAutorizacaoInternacao = new CtAutorizacaoInternacao();
		ctAutorizacaoInternacao.setNumeroGuiaOperadora(String.valueOf(atual.getTxNumVpp())); //Lote interno + 001, 002
		ctAutorizacaoInternacao.setDataAutorizacao(dateToXML(dtSystem)); // Data do Lote interno
		ctAutorizacaoInternacao.setSenha(atual.getTxSenhaAutorizacao()); //Senha da Autorizacao
		ctAutorizacaoInternacao.setDataValidadeSenha(dateToXML(GeneralParser.retornaDataPosteriorDiasCorridos(new Date(),30))); //30 dias da Data de lote interno

		CtBeneficiarioDados ctBeneficiarioDados = new CtBeneficiarioDados();
		ctBeneficiarioDados.setNumeroCarteira(atual.getTxCodSegurado()); //Codigo segurado
		ctBeneficiarioDados.setAtendimentoRN(DmSimNao.S);
		ctBeneficiarioDados.setNomeBeneficiario(atual.getTxNomeSegurado()); //Nome segurado

		CtContratadoDados contratadoExecutante = new CtContratadoDados();
		contratadoExecutante.setCodigoPrestadorNaOperadora("05094642000110"); 
		contratadoExecutante.setNomeContratado("SAUDE CARE GEREN INFRAEST A ASSIST SAUDE");

		DadosExecutante dadosExecutante = new DadosExecutante();
		dadosExecutante.setContratadoExecutante(contratadoExecutante);
		dadosExecutante.setCNES("3527603");

		CtmInternacaoDados ctmInternacaoDados = new CtmInternacaoDados();
		ctmInternacaoDados.setCaraterAtendimento("1");
		ctmInternacaoDados.setTipoFaturamento("4");
		ctmInternacaoDados.setDataInicioFaturamento(dateToXML(atual.getDtPrimContato())); //Data do Primeiro Contato
		ctmInternacaoDados.setHoraInicioFaturamento(timeToXML(dtSystem));
		ctmInternacaoDados.setDataFinalFaturamento(dateToXML(dtSystem)); //Data do Lote interno
		ctmInternacaoDados.setHoraFinalFaturamento(timeToXML(dtSystem));
		ctmInternacaoDados.setTipoInternacao("1");
		ctmInternacaoDados.setRegimeInternacao("3");

		CtmInternacaoDadosSaida ctmInternacaoDadosSaida = new CtmInternacaoDadosSaida();
		ctmInternacaoDadosSaida.setIndicadorAcidente("9");
		ctmInternacaoDadosSaida.setMotivoEncerramento("18");

		CtProcedimentoDados ctProcedimentoDados = new CtProcedimentoDados();
		ctProcedimentoDados.setCodigoTabela("00");
		ctProcedimentoDados.setCodigoProcedimento(atual.getTxTipoOperacao()); //Códido do procedimento colocar na tabela de preços - 61602540 -Inicial / 61602558 - Continuidade
		if (atual.getCdTabelaPreco() == 1) {
			ctProcedimentoDados.setDescricaoProcedimento("TELEMONITORAMENTO INICIAL CASOS COMPLEXOS"); //Esse inicial / Esse - TELEMONITORAMENTO CONTINUIDADE CASOS COMPLEXOS
		}else {
			ctProcedimentoDados.setDescricaoProcedimento("TELEMONITORAMENTO CONTINUIDADE CASOS COMPLEXOS"); //Esse inicial / Esse - TELEMONITORAMENTO CONTINUIDADE CASOS COMPLEXOS
		}
		CtProcedimentoExecutadoInt ctProcedimentoExecutadoInt = new CtProcedimentoExecutadoInt();
		ctProcedimentoExecutadoInt.setDataExecucao(dateToXML(dtSystem));
		ctProcedimentoExecutadoInt.setProcedimento(ctProcedimentoDados);
		ctProcedimentoExecutadoInt.setQuantidadeExecutada(new BigInteger("1"));
		ctProcedimentoExecutadoInt.setReducaoAcrescimo(new BigDecimal(1.00));
		ctProcedimentoExecutadoInt.setValorUnitario(atual.getVlValor().setScale(2, BigDecimal.ROUND_FLOOR));
		ctProcedimentoExecutadoInt.setValorTotal(atual.getVlValor().setScale(2, BigDecimal.ROUND_FLOOR));

		ProcedimentosExecutados procedimentosExecutados = new ProcedimentosExecutados();
		procedimentosExecutados.getProcedimentoExecutado().add(ctProcedimentoExecutadoInt);
		
		CtGuiaValorTotal ctGuiaValorTotal = new CtGuiaValorTotal();
		ctGuiaValorTotal.setValorProcedimentos(atual.getVlValor().setScale(2, BigDecimal.ROUND_FLOOR));
		ctGuiaValorTotal.setValorTotalGeral(atual.getVlValor().setScale(2, BigDecimal.ROUND_FLOOR));

		CtmInternacaoResumoGuia ctmInternacaoResumoGuia = new CtmInternacaoResumoGuia();
		ctmInternacaoResumoGuia.setCabecalhoGuia(ctGuiaCabecalho);
		ctmInternacaoResumoGuia.setNumeroGuiaSolicitacaoInternacao(atual.getTxNumVpp()); //Lote interno
		ctmInternacaoResumoGuia.setDadosAutorizacao(ctAutorizacaoInternacao);
		ctmInternacaoResumoGuia.setDadosBeneficiario(ctBeneficiarioDados);
		ctmInternacaoResumoGuia.setDadosExecutante(dadosExecutante);
		ctmInternacaoResumoGuia.setDadosInternacao(ctmInternacaoDados);
		ctmInternacaoResumoGuia.setDadosSaidaInternacao(ctmInternacaoDadosSaida);
		ctmInternacaoResumoGuia.setProcedimentosExecutados(procedimentosExecutados);
		ctmInternacaoResumoGuia.setValorTotal(ctGuiaValorTotal);

		  guiasTISS.getGuiaResumoInternacao().add(ctmInternacaoResumoGuia);

		}
		
		CtmGuiaLote ctmGuiaLote = new CtmGuiaLote();
		ctmGuiaLote.setNumeroLote(String.valueOf(cdLoteInterno)); //Lote interno
		ctmGuiaLote.setGuiasTISS(guiasTISS);

	   
		PrestadorOperadora prestadorOperadora = new PrestadorOperadora();
		prestadorOperadora.setLoteGuias(ctmGuiaLote);
		return prestadorOperadora;
	}


	private static XMLGregorianCalendar dateToXML(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendarDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH)+1,c.get(Calendar.DAY_OF_MONTH),DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static XMLGregorianCalendar timeToXML(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendarTime(c.get(Calendar.HOUR), c.get(Calendar.MINUTE),c.get(Calendar.SECOND), DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
			return null;
		}
	}

}
