package br.com.gohosp.ans;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gohosp.modulos.faturamento.service.FaturamentoService;

@RestController
@RequestMapping("ans")
public class AnsController {
	@Autowired
	AnsService ansService;
	

	@RequestMapping("/xml/{cdLoteInterno}")
	@ResponseBody
	public ResponseEntity<?>  reportDeliveryStatusPendingExcel(@PathVariable Integer cdLoteInterno, HttpServletResponse response) throws Exception{

		
		
		String xml = ansService.gerarXml(cdLoteInterno);
		
		try {		
			
			InputStream myStream = new ByteArrayInputStream(xml.getBytes());
			response.addHeader("Content-disposition", "attachment;filename=ans.xml");
			response.setContentType("application/xml");
			
			IOUtils.copy(myStream, response.getOutputStream());
			response.flushBuffer();
			
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	} 
}
