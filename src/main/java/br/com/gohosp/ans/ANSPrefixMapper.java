package br.com.gohosp.ans;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class ANSPrefixMapper extends NamespacePrefixMapper {

    public static final String PREFIXO_ANS = "ans";
    
    @Override
    public String getPreferredPrefix(String arg0, String arg1, boolean arg2) {
        return PREFIXO_ANS;
    }
    
    @Override
    public String[] getPreDeclaredNamespaceUris2() {
    	
    	return  new String[] {"ns3", "http://www.w3.org/2001/XMLSchema-instance"};
    }
    
}