package br.com.gohosp;

import br.com.gohosp.util.GeneralUtil;

public class Constants {

	
	//Constantes relativas ao Quartz
	public static final boolean QUARTZ_LIBERADO = GeneralUtil.getAmbiente();
	
	//Constantes relativas ao numero de linhas do GRIS
    public static final int GRID_NUMERO_LINHAS = 15;
    
    //Constantes relativas a paginação
    public static final String URL_PAGINACAO = "www.jtptest.com.br";
	
	//Constantes relativas as Notificações
    public static final String TYPE_NOTIFY_SUCCESS = "success";
    public static final String TYPE_NOTIFY_DANGER = "error";
    public static final String TYPE_NOTIFY_WARNING = "warning";
    public static final String TYPE_NOTIFY_INFO = "info";
	
	//Constantes relativas aos PATHS Templates Web
    	public static final String TEMPLATE_PATH_ADM = "modulos/administracao";
	    public static final String TEMPLATE_PATH_UTIL = "modulos/util";
	    public static final String TEMPLATE_PATH_RELATORIOS = "modulos/relatorios";
	    public static final String TEMPLATE_PATH_TRATATIVAS = "modulos/tratativas";	    
	    public static final String TEMPLATE_PATH_CADASTROS = "modulos/cadastros";
	    public static final String TEMPLATE_PATH_FATURAMENTO = "modulos/faturamento";
	    public static final String TEMPLATE_PATH_GRAFICOS = "modulos/graficos";
	
		// Constantes relativas a Variavel de ultimo filtro	
		public static final String ULTIMO_FILTRO = "ULTIMO_FILTRO";	
	    
	    
	// Constantes relativas ao Schema de Banco de Dados	
		public static final String SCHEMA = "gohospsas";	
	
		// Constantes relativas as Mensagens de Retorno	
		public static final String REGISTRO_INCLUIDO_ALTERADO_SUCESSO = "Registro Incluído/Alterado com sucesso!";	
		public static final String LOGIN_OK = "Login efetuado com sucesso!";
		public static final String REGISTRO_EXCLUIDO_SUCESSO = "Registro excluído com sucesso!";	
		public static final String ERRO_AO_EXCLUIR = "Erro ao excluir o registro!";
		
		public static final String REGISTRO_NAO_EXISTE = "Registro não existe!";
		public static final String FILTRO_NAO_EXISTE = "Não existem dados para o filtro desejado!";
		public static final String HASH_INVALIDO = "Hash de segurança inválido!";
		public static final String ERRO_UPLOAD = "Erro no envio do Arquivo!";
		public static final String ERRO_UPLOAD_ARQUIVO_JA_EXISTE = "Arquivo já existe!";

		
		//Constantes relativas ao LogShow
		public static final boolean LOG_SHOW = !GeneralUtil.getAmbiente();
		
		//Constantes relativas ao path dos arquivos do Sistema
		public static final String NOME_SISTEMA = "Gohosp Sas";
		public static final String NOME_SISTEMA_LOCAL = "gohospsas";
		public static final String PATH_ARQS_JAVA_LOCAL = "/Sistemas/Spring/workspace/gohospsas/";
		//public static final String PATH_ARQS_JAVA_LOCAL = "/Sistemas/eclipse/workspaceSTS/gohospsas/";
		//public static final String PATH_ARQS_JAVA_LOCAL = "/Sistemas/SpringSTS/workspace/gohospsas/";
		//public static final String PATH_ARQS_JAVA_LOCAL = "/Projetos/JTP/gohospsas/";
		
		//Constantes relativas ao upload de documentos
		public static final String PATH_UPLOAD = "/gohospsas/jag/files/";
}
