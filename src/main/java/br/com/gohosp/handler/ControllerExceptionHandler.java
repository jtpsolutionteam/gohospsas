package br.com.gohosp.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.gohosp.exceptions.ErrosException;

@ControllerAdvice
public class ControllerExceptionHandler {

	@ExceptionHandler(ErrosException.class)
	public ResponseEntity<String> handleRegistroNaoExisteException(ErrosException e, HttpServletRequest request) {
		
		return ResponseEntity.badRequest().body(e.getMessage());
	}
	
	

	
}
