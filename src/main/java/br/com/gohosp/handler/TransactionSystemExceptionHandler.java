package br.com.gohosp.handler;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.gohosp.exceptions.ErrosTransactionSystemException;
import br.com.gohosp.mensagens.TabRetornoErrosObj;

@ControllerAdvice
public class TransactionSystemExceptionHandler {

	@ExceptionHandler(ErrosTransactionSystemException.class)
	public ResponseEntity<List<TabRetornoErrosObj>> handleException(ErrosTransactionSystemException e, HttpServletRequest request) {
		
		List<TabRetornoErrosObj> l = new ArrayList<TabRetornoErrosObj>();
		
		String[] lista = e.getMessage().split("\\|");
		
		for (int i = 0; i<lista.length; i++) {
		
			TabRetornoErrosObj ret = new TabRetornoErrosObj();
			ret.setCd_mensagem(2);
			ret.setTx_mensagem(lista[i]);
			ret.setDt_system(System.currentTimeMillis());
			l.add(ret);
		}
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(l);
	}
	
	

	
}
