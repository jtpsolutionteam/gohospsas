package br.com.gohosp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.cadastros.usuario.TabUsuarioObj;
import br.com.gohosp.dao.cadastros.usuario.repository.TabUsuarioRepository;

@Service
public class AppUserDetailsService implements UserDetailsService {

	@Autowired
	private TabUsuarioRepository tabUsuarioRepository;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		
		TabUsuarioObj tUsuario = tabUsuarioRepository.findByTxEmailAndCkAtivo(email, 1);
		
		if (tUsuario == null) {
			throw new UsernameNotFoundException("Usuário e/ou senha inválidos!");
		}
		
		return new DadosUser(tUsuario.getTxEmail(), 
				tUsuario.getTxSenha(), 
				AuthorityUtils.createAuthorityList(), 
				tUsuario.getCdUsuario(), 				 
				tUsuario.getCdGrupoVisao(), 
				tUsuario.getTxNome(), 
				tUsuario.getTxApelido());
	}

}
