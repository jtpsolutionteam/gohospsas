package br.com.gohosp.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class DadosUser extends User{
	private Integer cdUsuario;
	private Integer cdGrupoVisao;
	private String txApelido;
	private String txEmail;
	
	private static final long serialVersionUID = -8062200468272967425L;
	
	public DadosUser(String username, String password, Collection<? extends GrantedAuthority> authorities, Integer cdUsuario, Integer cdGrupoVisao, String txApelido, String txEmail) {
		super(username, password, authorities);
		this.cdUsuario = cdUsuario;
		this.cdGrupoVisao = cdGrupoVisao;
		this.txApelido = txApelido;
		this.txEmail = txEmail;
	}
	
	public Integer getCdUsuario() {
		return this.cdUsuario;
	}

	public String getTxApelido() {
		return txApelido;
	}

	public String getTxEmail() {
		return txEmail;
	}

	public Integer getCdGrupoVisao() {
		return cdGrupoVisao;
	}

	
}
