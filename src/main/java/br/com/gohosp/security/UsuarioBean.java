package br.com.gohosp.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioBean {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public DadosUser DadosUsuario() {
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		DadosUser userDetails = (DadosUser) authentication.getPrincipal();
		
		return userDetails;
		
	}

}
