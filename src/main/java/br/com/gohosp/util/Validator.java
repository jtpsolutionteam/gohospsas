package br.com.gohosp.util;

import java.lang.reflect.Field;
import java.util.Date;

import org.json.JSONObject;

import br.com.gohosp.modulos.administrador.bean.TabFiltroCamposObj;

public class Validator {
	
	public static void main(String[] args) {
		
		TabFiltroCamposObj Tab = new TabFiltroCamposObj();
		
		System.out.println(new Validator().isObjetoNull(Tab));
		
	}
	
	
	public static boolean isObjetoNull(Object obj) {
		
		Field[] campos = obj.getClass().getDeclaredFields();
		
		boolean ckNull = true;
		
		try {
		
		 for (int i = 0; i < campos.length; i++) {
			 
			 campos[i].setAccessible(true);
			 
			 //String fieldName = campos[i].getName().toString().toLowerCase();
			 //String fieldType = campos[i].getType().toString().toLowerCase();
			 
			 
			 if (campos[i].get(obj) != null) {
				 ckNull = false;
			 }
		 }	
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return ckNull;
	}
	
	public static boolean jsonIsNull(JSONObject jsonObj, String field) {
		
		if (jsonObj.isNull(field)) {
			return true;
		}
		
		return false;
	}
	
	public static boolean isBlankOrNullUndefined(String value) {
		
		if (value == null || value.equals("") || value.equals("undefined")) {
			return true;
		}else {
			return false;
		}		
	}

	public static boolean isBlankOrNull(Date value) {
		return (value == null) ? true : "".equals(value);
	}
	
	public static boolean isBlankOrNull(Integer value) {
		return (value == null) ? true : "".equals(value);
	}
	
	public static boolean isBlankOrNull(String value) {
		return (value == null) ? true : "".equals(value.trim());
	}

	public static boolean isBlankOrNullOr0(String value) {
		boolean ck = (value == null) ? true : "".equals(value.trim());
		
		if (!ck && value.equals("0")) {
			ck = true;
		}
		
		return ck;
	}
	
	
	
	public static boolean isValidDate(String value) {
		return GeneralParser.parseDate(value) != null;
	}

	public static boolean isValidDate(String formato, String value) {
		return GeneralParser.parseDate(formato,value) != null;
	}
	
	
	public static boolean isValidInteger(String value) {
		boolean result = true;

		try {
			Integer.parseInt(value);
		} catch (NumberFormatException e) {
			result = false;
		}

		return result;
	}

	public static boolean isBlankOrNullorZero(Object value){
		boolean ck = false;	
		
		if (value == null){ 
			ck = true;
		}else{ 
			ck = isBlankOrNullorZero(value.toString());
		}	 	 
		
		return ck;
	}
	
	public static boolean isBlankOrNullorZero(String value) {
		
		 boolean ck = false;	
			
		 if (value == null){ 
			ck = true;
		 }else if ("".equals(value.trim())){ 
			ck = true;
		 }else  if ("0".equals(value.trim())){ 
			ck = true;
		 }else  if ("0.0".equals(value.trim())){ 
				ck = true;
		 }else  if ("0.00".equals(value.trim())){ 
				ck = true;
		 }else  if ("0,00".equals(value.trim())){ 
				ck = true;		
		 }else  if ("NULL".equals(value.trim())){
			ck = true; 
		 }else  if ("null".equals(value.trim())){
				ck = true; 
		 }	 	 
		 
	      return ck;
		}
	
	
	public static boolean isValidDouble(String value) {
		boolean result = true;

		try {
			GeneralParser.parseDouble(value);
		} catch (NumberFormatException e) {
			result = false;
		}

		return result;
	}

	public static boolean isValidFloat(String value) {
		boolean result = true;

		try {
			Float.parseFloat(value);
		} catch (NumberFormatException e) {
			result = false;
		}

		return result;
	}
	
	public static boolean isValidCPF(String value) {
		int d1, d2;
		int digito1, digito2, resto;
		int digitoCPF;
		String nDigResult;
		String digitoVerificador;
		boolean ck_ok;
		try {
		d1 = d2 = 0;
		digito1 = digito2 = resto = 0;

		for (int nCount = 1; nCount < value.length() - 1; nCount++) {
			digitoCPF = Integer.valueOf(value.substring(nCount - 1, nCount))
					.intValue();

			// multiplique a ultima casa por 2 a seguinte por 3 a seguinte por 4
			// e assim por diante.
			d1 = d1 + (11 - nCount) * digitoCPF;

			// para o segundo digito repita o procedimento incluindo o primeiro
			// digito calculado no passo anterior.
			d2 = d2 + (12 - nCount) * digitoCPF;
		}
		;

		// Primeiro resto da divis�o por 11.
		resto = (d1 % 11);

		// Se o resultado for 0 ou 1 o digito � 0 caso contr�rio o digito
		// � 11 menos o resultado anterior.
		if (resto < 2)
			digito1 = 0;
		else
			digito1 = 11 - resto;

		d2 += 2 * digito1;

		// Segundo resto da divis�o por 11.
		resto = (d2 % 11);

		// Se o resultado for 0 ou 1 o digito � 0 caso contr�rio o digito
		// � 11 menos o resultado anterior.
		if (resto < 2)
			digito2 = 0;
		else
			digito2 = 11 - resto;

		// Digito verificador do CPF que est� sendo validado.
		digitoVerificador = value.substring(value.length() - 2, value
				.length());

		// Concatenando o primeiro resto com o segundo.
		nDigResult = String.valueOf(digito1) + String.valueOf(digito2);
		
		ck_ok = digitoVerificador.equals(nDigResult);
		}catch (Exception ex) {
			ck_ok = false;
		}
		// comparar o digito verificador do cpf com o primeiro resto + o segundo
		// resto.
		return ck_ok;
	}

	public static boolean isValidCNPJ(String value) {
		int soma = 0, dig;
		String cnpj_calc;

		if (value.length() != 14)
			return false;

		cnpj_calc = value.substring(0, 12);

		char[] chr_cnpj = value.toCharArray();

		/* Primeira parte */
		for (int i = 0; i < 4; i++)
			if (chr_cnpj[i] - 48 >= 0 && chr_cnpj[i] - 48 <= 9)
				soma += (chr_cnpj[i] - 48) * (6 - (i + 1));
		for (int i = 0; i < 8; i++)
			if (chr_cnpj[i + 4] - 48 >= 0 && chr_cnpj[i + 4] - 48 <= 9)
				soma += (chr_cnpj[i + 4] - 48) * (10 - (i + 1));
		dig = 11 - (soma % 11);

		cnpj_calc += (dig == 10 || dig == 11) ? "0" : Integer.toString(dig);

		/* Segunda parte */
		soma = 0;
		for (int i = 0; i < 5; i++)
			if (chr_cnpj[i] - 48 >= 0 && chr_cnpj[i] - 48 <= 9)
				soma += (chr_cnpj[i] - 48) * (7 - (i + 1));
		for (int i = 0; i < 8; i++)
			if (chr_cnpj[i + 5] - 48 >= 0 && chr_cnpj[i + 5] - 48 <= 9)
				soma += (chr_cnpj[i + 5] - 48) * (10 - (i + 1));
		dig = 11 - (soma % 11);
		cnpj_calc += (dig == 10 || dig == 11) ? "0" : Integer.toString(dig);

		return value.equals(cnpj_calc);
	}

}
