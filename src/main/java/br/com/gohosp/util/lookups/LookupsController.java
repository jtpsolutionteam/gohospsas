package br.com.gohosp.util.lookups;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.gohosp.dao.cadastros.cid.TabCidSubcategoriaObj;
import br.com.gohosp.dao.cadastros.cid.repository.TabCidSubcategoriaRepository;
import br.com.gohosp.security.UsuarioBean;

@Controller
@RequestMapping("/lookups")
public class LookupsController {

	@Autowired
	private UsuarioBean tabDadosUsuariosBean;

	
	@Autowired
	private TabCidSubcategoriaRepository tabCidSubcategoriaRepository;
	
	@RequestMapping(value = "/lkcid/{txDescricaoAbreviada}")
	public @ResponseBody List<TabCidSubcategoriaObj> selecttxcid(@PathVariable String txDescricaoAbreviada) {
		return tabCidSubcategoriaRepository.findByTxDescricaoAbreviadaContaining(txDescricaoAbreviada);
	}
	
	

}
