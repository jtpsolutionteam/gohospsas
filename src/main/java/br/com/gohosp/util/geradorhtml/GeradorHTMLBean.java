package br.com.gohosp.util.geradorhtml;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.reflect.Field;

import br.com.gohosp.Constants;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.geradorclasse.GeradorClasseBean;

public class GeradorHTMLBean {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		new GeneralUtil().ApagarArquivosDiretorioGeradores();
		// 1 - Normal; 2 - Filho; 3 - Pesquisa; 4 - FilhoModal; 5 - Pequisa/Gravação; 6 - Pesquisa Relatório
		new GeradorHTMLBean().GeraListaHTML("TabRetornoConsultaTratativaPesquisaFiltroObj", "tratativapesquisa", 3);
		
	}
	
	private void GerarHTMLPackage(String txObj, String txPathForm) {
		try {
		    String pathObj = new GeneralUtil().LocalizaArquivo(Constants.NOME_SISTEMA_LOCAL,"java",Constants.PATH_ARQS_JAVA_LOCAL,txObj+".java").replaceAll("\\.", "/").replace(txObj, "");
    	
	        File[] files = new File(Constants.PATH_ARQS_JAVA_LOCAL+"src/main/java/"+pathObj).listFiles();  
	        for (int i = 0; i < files.length; ++i) {  
	            File pathname = files[i];  
	            String nm = pathname.getName();  
	            
	            if (nm.contains(".java")) {
	            	nm = pathname.getName().replace(".java", "");
	            	GeradorHTML(nm,txPathForm);
					
	            }
	            
	            Thread.sleep(1000);  
	        } 
	        
	        System.out.println("Geração do Lote de Services terminado!");
		}catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	// 1 - Normal; 2 - Filho
	public void GeraListaHTML(String lista, String txPathForm, Integer cd_tipo_tela_html) {
		
		  try {	
			String[] l = lista.split(";");
			
			for (int i=0; i<=l.length-1; i++) {
				
				if (cd_tipo_tela_html == 1) {
				 GeradorHTML(l[i], txPathForm);
				}else if (cd_tipo_tela_html == 2) {
				 GeradorHTMLFilho(l[i], txPathForm);	
				}else if (cd_tipo_tela_html == 3) {
				  GeradorHTMLPesquisa(l[i], txPathForm);	
				}else if (cd_tipo_tela_html == 4) {
					GeradorHTMLFilhoModal(l[i], txPathForm);  					
				}else if (cd_tipo_tela_html == 5) {
				  GeradorHTMLPesquisaGravacao(l[i], txPathForm);	
				}else if (cd_tipo_tela_html == 6) {
				  GeradorHTMLPesquisaRelatorio(l[i], txPathForm);	
				}

				Thread.sleep(1000);
			}
			
			System.out.println("Geração do Lote de HTML terminado!");
		  }catch (Exception ex) {
			  ex.printStackTrace();
		  }
			
		}

	
	//private void GeradorService(String txObjTabela, String pkTabela, String txTipoField) {
	private void GeradorHTML(String txObj, String txPathForm) {	 
		try {
			
			String pathObj = new GeneralUtil().LocalizaArquivo(Constants.NOME_SISTEMA_LOCAL,"java",Constants.PATH_ARQS_JAVA_LOCAL,txObj+".java");
			
			Object tabObj = null;
			
			tabObj = Class.forName(pathObj).newInstance();
			
			Field[] campos = tabObj.getClass().getDeclaredFields();
			
			StringBuilder str = new StringBuilder();
			
			GeradorClasseBean g = new GeradorClasseBean();
			String tabObjMinusculo = g.arrumaCampoMinusculaObj(txObj);
			
			str.append("<!DOCTYPE html>\n");
			str.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" \n"); 
			str.append("  xmlns:th=\"http://www.thymeleaf.org\"\n");
			str.append("  xmlns:layout=\"http://www.ultraq.net.nz/thymeleaf/layout\"\n"); 
			str.append("  layout:decorator=\"/layout/LayoutPadrao\">\n\n");
			 
			str.append(" <section layout:fragment=\"conteudo\" >\n");
			str.append("\n\n");
			
            str.append("<div class=\"page-inner\">\n");              
            str.append("<div class=\"page-section\">\n");
            str.append("<section id=\"base-style\" class=\"card\">\n");
            str.append("    <div class=\"card-body\">\n");
            str.append("      <form id=\""+tabObjMinusculo+"\" method=\"post\" th:action=\"@{/"+txPathForm+"/gravar}\" th:object=\"${"+tabObjMinusculo+"}\" >\n");
            str.append("        <fieldset>\n");
            
            str.append("<div class=\"row\"> \n");
            str.append(" <div class=\"col-sm-10\"> \n");
            str.append("  <header class=\"page-title-bar\"> \n");
            str.append("	<h5 class=\"page-title\" th:text=\"${pathtela}\">Nome da Tela</h5> \n");
            str.append("  </header> \n");
            str.append("  </div> \n");
            str.append("  <div class=\"col-sm-2\"> \n");
            str.append("  <button type=\"button\" class=\"btn btn-primary col-sm-5 float-right\" th:onclick=\"|javascript: abrirtela('@{/TelaPrincipal/novo}');|\" > \n");
            str.append("		<strong>Voltar</strong> ");
            str.append("  </button> \n");
            str.append("  </div> \n");
            str.append("</div> \n");
            
            
            str.append("          <nav class=\"navbar bg-primary main-menu-jtp\">\n");
            str.append("          	<ul>\n");
            str.append("          		<a th:href=\"@{/"+txPathForm+"/novo}\" id=\"linkNovo\">\n");
            str.append("          			<li><span class=\"fas fa-plus-circle\"></span> Novo</li>\n");
            str.append("          		</a>\n");
            str.append("          		<a href=\"javascript: gravar('"+tabObjMinusculo+"');\" id=\"linkGravar\">\n");
            str.append("          			<li><span class=\"fas fa-save\"></span> Gravar</li>\n");
            str.append("          		</a>\n");                  		
            str.append("          	</ul>\n");
            str.append("          	<div class=\"form-group pesquisar-main-menu\">\n");
            str.append("                	<input type=\"text\" class=\"form-control pesquisar\" placeholder=\"Pesquisar\" id=\"txPesquisar\"/>\n");
            str.append("          	</div>\n");
            str.append("          </nav>\n");
            str.append("\n");
			
            String tx_classe = tabObj.getClass().getSimpleName().replace("Obj", "Service");

            String strJS = "\n\n";
            
			 for (int i = 0; i < campos.length; i++) {
				 
				 campos[i].setAccessible(true);
				 
				 String fieldName = campos[i].getName();
				 
				 String fieldType = campos[i].getType().toString().toLowerCase();
				 
				 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
				 
				  if (fieldType.contains("integer") || fieldType.contains("int") || fieldName.contains("cd")) {
					  str.append("<!-- INPUT TEXT -->\n"); 
		                 str.append("<div class=\"col-sm-6\">\n");
		                 str.append("<div class=\"form-group\">\n");
		                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
		                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
		                 str.append("</div>\n");
		                 str.append("</div>\n");
		                 str.append("\n");
		                 
					  str.append("<!-- SELECT -->\n");
					  str.append("<div class=\"col-sm-2\">\n");
					  str.append(" <div class=\"form-group\">\n");
					  str.append("	<label for=\""+fieldName+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
					  str.append("	 <select class=\"custom-select\" id=\""+fieldName+"\" th:field=\"*{"+fieldName+"}\">\n");
					  str.append("    <option value=\"\">...</option>\n");
					  str.append("    <option th:each=\"s"+fieldName.toLowerCase()+" : ${select"+fieldName.toLowerCase()+"}\" th:value=\"${s"+fieldName.toLowerCase()+"."+fieldName+"}\" th:text=\"${s"+fieldName.toLowerCase()+"."+fieldName.replace("cd", "tx")+"}\"></option>\n");
					  str.append("  </select>\n");
					  str.append(" </div>\n");
					  str.append("</div>\n");
					  
						if (!fieldName.contains("ck")) {
						  str.append("<!-- LOOKUP -->\n");
						  str.append("<div class=\"col-sm-4\">\n");
						  str.append("	 <div class=\"form-group\">\n");
						  str.append("    <label for=\""+fieldName.replace("cd", "tx")+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");						 
						  str.append("	    <div class=\"input-group\">\n");							 
						  str.append("     <label class=\"input-group-prepend\" for=\""+fieldName.replace("cd", "tx")+"\">\n");
						  str.append("     <span class=\"input-group-text\">\n");
						  str.append("       <span class=\"fas fa-search\"></span>\n");
						  str.append("     </span>\n");
						  str.append("    </label>\n");
						  str.append("     <input type=\"text\" class=\"form-control\" id=\""+fieldName.replace("cd", "tx")+"\" name=\""+fieldName.replace("cd", "tx")+"\" th:field=\"*{"+fieldName.replace("cd", "tx")+"}\" onchange=\"javascript: limpalookup('"+fieldName.replace("cd", "tx")+"','"+fieldName+"');\" />\n");
						  str.append("     <input type=\"hidden\" class=\"form-control\" id=\""+fieldName+"\" name=\""+fieldName+"\" th:field=\"*{"+fieldName+"}\" /> \n");
						  str.append("  </div>\n");
						  str.append("	 </div>\n");
						  str.append("	</div>\n");
						  
						   //Gera JS Lookup
						  	strJS += gerarJS("lk"+descricaoCampo.toLowerCase(), fieldName.replace("cd", "tx"), fieldName);
					    }
					  str.append("\n");
				  }else {
					  if (fieldType.contains("big")) {  
							 str.append("<!-- INPUT TEXT BIGDECIMAL -->\n"); 
			                 str.append("<div class=\"col-sm-6\">\n");
			                 str.append("<div class=\"form-group\">\n");
			                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
			                 str.append("  <input type=\"text\" class=\"form-control textFloat\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
			                 str.append("</div>\n");
			                 str.append("</div>\n");
			                 str.append("\n");
							}else if (fieldType.contains("date")) {
								 str.append("<!-- INPUT TEXT -->\n"); 
				                 str.append("<div class=\"col-sm-6\">\n");
				                 str.append("<div class=\"form-group\">\n");
				                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
				                 str.append("  <input type=\"text\" class=\"form-control jtpDate\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\" />\n");
				                 str.append("</div>\n");
				                 str.append("</div>\n");
				                 str.append("\n");						

							}else {	
								 str.append("<!-- INPUT TEXT -->\n"); 
				                 str.append("<div class=\"col-sm-6\">\n");
				                 str.append("<div class=\"form-group\">\n");
				                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
				                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
				                 str.append("</div>\n");
				                 str.append("</div>\n");
				                 str.append("\n");						
						}
				 }
			 }
	         
			 
			 str.append("      	</fieldset>\n");            
			 str.append("      </form>\n");
			 str.append("    </div>\n");
			 str.append("  	</section>\n");
			 str.append(" </div>\n");
			 str.append("</div>\n\n");

			 str.append("<!-- Script JS --> \n");
			 str.append("<script th:inline=\"javascript\"> \n");	
			 str.append(" /*<![CDATA[*/	");	   
			 str.append("  $(document).ready(function() {	\n");
			 str.append(" var cdTipo = /*[[${tipoMensagem}]]*/ \n jtpNotifySimple(/*[[${txMensagem}]]*/,cdTipo);\n");	
			 str.append("	jtpNotify(/*[[${listaErros}]]*/);\n");			 
			 str.append("    var cdchave = /*[[${CampoChave}]]*/; \n");
			 str.append("    document.getElementById('CampoChave').value = cdchave; \n\n");
			 
			 str.append("//Permissões \n");
			 str.append("	var listPermissaoBotao = /*[[${listPermissaoBotao}]]*/; \n");
			 str.append("	checkPermissaoBotao(listPermissaoBotao);\n\n");
				
			 str.append("	var listPermissaoCampos = /*[[${listPermissaoCampos}]]*/; \n");
			 str.append("	checkPermissaoCampos(listPermissaoCampos); \n\n");

			 str.append("	//Service \n");
			 str.append("	var txService = /*[[${txService}]]*/; \n");
			 str.append("	analistaVirtual(txService); \n\n");
			 str.append("	var pathUrl = /*[[@{/}]]*/; \n\n");

			 str.append(strJS);
			 
			 str.append("	var lkPesquisar = /*[[@{/"+txPathForm+"/lkpesquisar}]]*/;\n");
			 str.append("	var autocomplete = new JTPAutocompleteSimple.AutocompleteSimple('#txPesquisar');\n");
			 str.append("	autocomplete.iniciar(lkPesquisar,'txUrlPesquisar','txPesquisar');");
			 
			 str.append(" });\n");	
			 str.append(" /*]]>*/ \n");
			 str.append("</script> \n"); 
			 
			 str.append("</section>\n\n");
			 
			 str.append("</html>\n");
	    
			 
			 
	         File f = new File("/temp/obj/html");
		        if (!f.exists()) {
		        	f.mkdirs();
		        }
	         
	          FileWriter arq = new FileWriter("/temp/obj/html/"+txPathForm+".html");
		      PrintWriter gravarArq = new PrintWriter(arq);
		      gravarArq.printf(str.toString());
		      gravarArq.close();
		      
		      System.out.println("Service "+txObj+".html criado com sucesso!");
		      
		}catch (Exception ex) {
			ex.printStackTrace();
		}		
	}
	
	private void GeradorHTMLFilhoModal(String txObj, String txPathForm) {	 
		try {
			
			String pathObj = new GeneralUtil().LocalizaArquivo(Constants.NOME_SISTEMA_LOCAL,"java",Constants.PATH_ARQS_JAVA_LOCAL,txObj+".java");
			
			Object tabObj = null;
			
			tabObj = Class.forName(pathObj).newInstance();
			
			Field[] campos = tabObj.getClass().getDeclaredFields();
			
			StringBuilder str = new StringBuilder();
			
			GeradorClasseBean g = new GeradorClasseBean();
			String tabObjMinusculo = g.arrumaCampoMinusculaObj(txObj);
			
			str.append("<!DOCTYPE html>\n");
			str.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" \n"); 
			str.append("  xmlns:th=\"http://www.thymeleaf.org\"\n");
			str.append("  xmlns:layout=\"http://www.ultraq.net.nz/thymeleaf/layout\"\n"); 
			str.append("  layout:decorator=\"/layout/LayoutPadrao\">\n\n");
			 
			str.append(" <section layout:fragment=\"conteudo\" >\n");
			str.append("\n\n");
			
            str.append("<div class=\"page-inner\">\n");              
            str.append("<div class=\"page-section\">\n");
            str.append("<section id=\"base-style\" class=\"card\">\n");
            str.append("    <div class=\"card-body\">\n");
            str.append("      <form id=\""+tabObjMinusculo+"\" method=\"post\" th:action=\"@{/"+txPathForm+"/gravar}\" th:object=\"${"+tabObjMinusculo+"}\" >\n");
            str.append("        <fieldset>\n");
            str.append("         <header class=\"page-title-bar\">\n");                
            str.append("          <h5 class=\"page-title\" th:text=\"${pathtela}\"> Nome da Tela </h5>\n");
            str.append("		   </header> \n");                      
            str.append("          <nav class=\"navbar bg-primary main-menu-jtp\">\n");
            str.append("          	<ul>\n");
            str.append("          		<a th:href=\"@{${hrefNovo}}\" id=\"linkNovo\">\n");
            str.append("          			<li><span class=\"fas fa-plus-circle\"></span> Novo</li>\n");
            str.append("          		</a>\n");                            		
            str.append("          	</ul>\n");
            str.append("          	<div class=\"form-group pesquisar-main-menu\">\n");
            str.append("              <ul><a th:href=\"@{${hrefBack}}\"><li><span class=\"fas fa-arrow-left\"></span> Voltar</li></a></ul>\n");
            str.append("          	</div>\n");
            str.append("          </nav>\n");
            str.append("\n");
			
            String tx_classe = tabObj.getClass().getSimpleName().replace("Obj", "Service");

            
            str.append("<div class=\"col-sm-12\"> \n");
            str.append("       <section class=\"card card-fluid\"> \n");
            str.append("         <div class=\"table-responsive\"> \n");
            str.append("           <table class=\"table table-hover\"> \n");
            str.append("             <thead class=\"jtp-bg-primary\"> \n");
            str.append("               <tr> \n");
            
            for (int i = 0; i < campos.length; i++) {
				 
				 campos[i].setAccessible(true);
				 
				 String fieldName = campos[i].getName();
				 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
				 
				 str.append("                 <th style=\"min-width:100px\"> "+descricaoCampo+" </th> \n");
            
            }
            
            str.append("                 <th style=\"width:100px; min-width:100px;\"> &nbsp; </th> \n");
            str.append("               </tr> \n");
            str.append("             </thead> \n");
            str.append("             <tbody> \n");
            
             String txLista = "";
			 for (int i = 0; i < campos.length; i++) {
				 
				 campos[i].setAccessible(true);
				 
				 String fieldName = campos[i].getName();
				 
				 String fieldType = campos[i].getType().toString().toLowerCase();
				 
				 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
				 if (i == 0) {
				  str.append("               <tr th:each=\"listdados : ${listadados}\" th:onclick=\"|javascript: abrirtela('@{/"+txPathForm+"/consultar}/${listdados.cdCodigo}/${txNref}');|\"> \n");
				  txLista = descricaoCampo;
				 }
				 if (fieldType.contains("big") || fieldType.contains("date")) {
				   str.append("                 <td class=\"jtp-grid-td\" th:text=\"${{listdados."+fieldName+"}}\"> "+fieldName+" </td> \n");			 
				 }else {
				   str.append("                 <td class=\"jtp-grid-td\" th:text=\"${listdados."+fieldName+"}\"> "+fieldName+" </td> \n");			 
				 }
			 }
				 
            
            //str.append("               <tr th:each=\"list : ${listagrupoacesso}\"> \n");
            
            str.append("                <td class=\"align-middle text-right\"> \n");
            str.append("                 <a th:href=\"${listdados.txUrlLinhaGrid}\" class=\"btn btn-sm btn-secondary\"> \n");
            str.append("                   <i class=\"fa fa-pencil-alt\"></i> \n");
            str.append("                   <span class=\"sr-only\">Edit</span> \n");
            str.append("                 </a> \n");
            str.append("                 <a href=\"#\" class=\"btn btn-sm btn-secondary\"> \n");
            str.append("                   <i class=\"far fa-trash-alt\"></i> \n");
            str.append("                   <span class=\"sr-only\">Remove</span> \n");
            str.append("                 </a> \n");
            str.append("               </td> \n");
            str.append("               </tr> \n");
            str.append("             </tbody> \n");
            str.append("           </table> \n");
            str.append("         </div> \n");
            str.append("       </section> \n");                
            str.append("</div> \n");
            
            
            //Modal - Inicio
            str.append("<div class=\"modal fade\" id=\""+txPathForm+"\" tabindex=\"-1\" \n");
            str.append("		role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\"> \n");
            str.append("		<div class=\"vertical-alignment-helper\"> \n");
            str.append("			<div class=\"modal-dialog vertical-align-center\"> \n");
            str.append("				<div class=\"modal-content\"> \n");
            str.append("					<div class=\"modal-header\"> \n");
            str.append("						<h4 class=\"modal-title\" id=\"myModalLabel\" th:text=\"${pathtela}\"></h4> \n");            
            str.append("						<button type=\"button\" class=\"close\" data-dismiss=\"modal\">\n");
            str.append("							<span aria-hidden=\"true\">&times;</span><span \n");
            str.append("								class=\"sr-only\">Close</span> \n");
            str.append("						</button> \n");
            str.append("					</div> \n");
            str.append("					<div class=\"modal-body\"> \n");
            
            
            
            String strJS = "\n\n";
            
			 for (int i = 0; i < campos.length; i++) {
				 
				 campos[i].setAccessible(true);
				 
				 String fieldName = campos[i].getName();
				 
				 String fieldType = campos[i].getType().toString().toLowerCase();
				 
				 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
				 
				  if (fieldType.contains("integer") || fieldType.contains("int")  || fieldName.contains("cd")) {
					  str.append("<!-- INPUT TEXT -->\n"); 
		                 str.append("<div class=\"col-sm-6\">\n");
		                 str.append("<div class=\"form-group\">\n");
		                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
		                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
		                 str.append("</div>\n");
		                 str.append("</div>\n");
		                 str.append("\n");
		                 
					  str.append("<!-- SELECT -->\n");
					  str.append("<div class=\"col-sm-6\">\n");
					  str.append(" <div class=\"form-group\">\n");
					  str.append("	<label for=\""+fieldName+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
					  str.append("	 <select class=\"custom-select\" id=\""+fieldName+"\" th:field=\"*{"+fieldName+"}\">\n");
					  str.append("    <option value=\"\">...</option>\n");
					  str.append("    <option th:each=\"s"+fieldName.toLowerCase()+" : ${select"+fieldName.toLowerCase()+"}\" th:value=\"${s"+fieldName.toLowerCase()+"."+fieldName+"}\" th:text=\"${s"+fieldName.toLowerCase()+"."+fieldName.replace("cd", "tx")+"}\"></option>\n");
					  str.append("  </select>\n");
					  str.append(" </div>\n");
					  str.append("</div>\n");
					  
						if (!fieldName.contains("ck")) {
						  str.append("<!-- LOOKUP -->\n");		
						  str.append("<div class=\"col-sm-6\">\n");
						  str.append("	 <div class=\"form-group\">\n");
						  str.append("    <label for=\""+fieldName.replace("cd", "tx")+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");						 
						  str.append("	    <div class=\"input-group\">\n");							 
						  str.append("     <label class=\"input-group-prepend\" for=\""+fieldName.replace("cd", "tx")+"\">\n");
						  str.append("     <span class=\"input-group-text\">\n");
						  str.append("       <span class=\"fas fa-search\"></span>\n");
						  str.append("     </span>\n");
						  str.append("    </label>\n");
						  str.append("     <input type=\"text\" class=\"form-control\" id=\""+fieldName.replace("cd", "tx")+"\" name=\""+fieldName.replace("cd", "tx")+"\" th:field=\"*{"+fieldName.replace("cd", "tx")+"}\" onchange=\"javascript: limpalookup('"+fieldName.replace("cd", "tx")+"','"+fieldName+"');\" />\n");
						  str.append("     <input type=\"hidden\" class=\"form-control\" id=\""+fieldName+"\" name=\""+fieldName+"\" th:field=\"*{"+fieldName+"}\" /> \n");
						  str.append("  </div>\n");
						  str.append("	 </div>\n");
						  str.append("	</div>\n");
						  
						   //Gera JS Lookup
						  	strJS += gerarJS("lk"+descricaoCampo.toLowerCase(), fieldName.replace("cd", "tx"), fieldName);
					    }
					  str.append("\n");
				  }else {
					  
					if (fieldType.contains("big")) {  
					 str.append("<!-- INPUT TEXT BIGDECIMAL -->\n"); 
	                 str.append("<div class=\"col-sm-6\">\n");
	                 str.append("<div class=\"form-group\">\n");
	                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
	                 str.append("  <input type=\"text\" class=\"form-control textFloat\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
	                 str.append("</div>\n");
	                 str.append("</div>\n");
	                 str.append("\n");
					}else if (fieldType.contains("date")) {
						 str.append("<!-- INPUT TEXT -->\n"); 
		                 str.append("<div class=\"col-sm-6\">\n");
		                 str.append("<div class=\"form-group\">\n");
		                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");		                 
		                 /*
		                 str.append("<div class=\"input-group date form_date col-sm-12\" \n");
		                 str.append("			data-date=\"\" data-date-format=\"dd/mm/yyyy\" \n");
		                 str.append("			data-link-field=\""+campos[i].getName()+"\" \n");
		                 str.append("			data-link-format=\"dd/mm/yyyy\"> \n");
		                 str.append("			<input class=\"form-control\" type=\"text\" \n");
		                 str.append("				id=\""+campos[i].getName()+"ReadOnly\" th:field=\"*{"+campos[i].getName()+"}\" /> \n");
		                 str.append("			<span class=\"input-group-addon\"><span \n");
		                 str.append("				class=\"glyphicon glyphicon-calendar\"></span></span> \n");
		                 str.append("		</div> \n");		                 
		                 */
		                 str.append("  <input type=\"text\" class=\"form-control jtpDate\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\" />\n");
		                 str.append("</div>\n");
		                 str.append("</div>\n");
		                 str.append("\n");						

					}else {	
						 str.append("<!-- INPUT TEXT -->\n"); 
		                 str.append("<div class=\"col-sm-6\">\n");
		                 str.append("<div class=\"form-group\">\n");
		                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
		                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
		                 str.append("</div>\n");
		                 str.append("</div>\n");
		                 str.append("\n");						
					}

				 }
			 }
	         
			 str.append("<!-- Campos Hidden --> \n");
			 str.append("	<input type=\"hidden\" class=\"form-control\" id=\"NomeCampoRelacionamento\" name=\"NomeCampoRelacionamento\"  th:field=\"*{cdCliente}\"/> \n");
			 str.append("	<input type=\"hidden\" class=\"form-control\" id=\"NomeCampoChave\" name=\"NomeCampoChave\"  th:field=\"*{NomeCampoChave}\"/> \n");
			 
			 
			 str.append("</div> \n");
			 str.append("	<div class=\"modal-footer\"> \n");
			 str.append("		<button type=\"button\" id=\"btnGravarModal\" class=\"btn btn-primary col-sm-2\"  \n");
			 str.append("			th:onclick=\"|javascript: gravarModal('@{/"+txPathForm+"/gravarmodal}', '@{/"+txPathForm+"}/${CampoRelacionamento}/${Camposeprecisarparalookup}', '#"+txPathForm+"');|\">  \n");
			 str.append("			<strong>Gravar</strong> \n");
			 str.append("		</button>  \n");
			 str.append("		<button type=\"button\" class=\"btn btn-default\"  \n");
			 str.append("			data-dismiss=\"modal\">  \n");
			 str.append("			<strong>Sair</strong>  \n");
			 str.append("		</button>  \n");
			 str.append("	</div>  \n");
			 str.append("</div> \n");
			 str.append("</div>  \n");
			 str.append("</div>  \n");
			 str.append("</div>  \n");
			 str.append("</fieldset> \n");
			 str.append("</form> \n");
			 str.append("</div> \n");
			 str.append("</section> \n");
			 str.append("</div> \n");
			 str.append("</div> \n");


			 str.append("<!-- Script JS --> \n");
			 
			 str.append("<script th:inline=\"javascript\"> \n");	
			 str.append(" /*<![CDATA[*/	");	   
			 str.append("  $(document).ready(function() {	\n");
			 str.append(" var cdTipo = /*[[${tipoMensagem}]]*/ \n jtpNotifySimple(/*[[${txMensagem}]]*/,cdTipo);\n");	
			 str.append("	jtpNotify(/*[[${listaErros}]]*/);\n");			 
			 str.append("    var cdchave = /*[[${CampoChave}]]*/; \n");
			 str.append("    document.getElementById('CampoChave').value = cdchave; \n");
			 
			 str.append(" var txEdit = /*[[${txEdit}]]*/; \n");
			 str.append(" if (txEdit != null) { \n");
			 str.append("	 $('#"+txPathForm+"').modal('show'); \n");	
			 str.append(" } \n");
			 
			 str.append("//Permissões \n");
			 str.append("	var listPermissaoBotao = /*[[${listPermissaoBotao}]]*/; \n");
			 str.append("	checkPermissaoBotao(listPermissaoBotao);\n\n");
				
			 str.append("	var listPermissaoCampos = /*[[${listPermissaoCampos}]]*/; \n");
			 str.append("	checkPermissaoCampos(listPermissaoCampos); \n\n");

			 str.append("	//Service \n");
			 str.append("	var txService = /*[[${txService}]]*/; \n");
			 str.append("	analistaVirtual(txService); \n\n");
			 str.append("	var pathUrl = /*[[@{/}]]*/; \n\n");

			 str.append(strJS);
			 
			 str.append("	var lkPesquisar = /*[[@{/"+txPathForm+"/lkpesquisar}]]*/;\n");
			 str.append("	var autocomplete = new JTPAutocompleteSimple.AutocompleteSimple('#txPesquisar');\n");
			 str.append("	autocomplete.iniciar(lkPesquisar,'txUrlPesquisar','txPesquisar');");

			 
			 str.append(" });\n");	
			 str.append(" /*]]>*/ \n");
			 str.append("</script> \n"); 
			 
			 str.append("</section>\n\n");
			 
			 str.append("</html>\n");
	    
			 
			 
	         File f = new File("/temp/obj/html");
		        if (!f.exists()) {
		        	f.mkdirs();
		        }
	         
	          FileWriter arq = new FileWriter("/temp/obj/html/"+txPathForm+".html");
		      PrintWriter gravarArq = new PrintWriter(arq);
		      gravarArq.printf(str.toString());
		      gravarArq.close();
		      
		      System.out.println("Service "+txObj+".html criado com sucesso!");
		      
		}catch (Exception ex) {
			ex.printStackTrace();
		}		
	}

	
	private void GeradorHTMLFilho(String txObj, String txPathForm) {	 
		try {
			
			String pathObj = new GeneralUtil().LocalizaArquivo(Constants.NOME_SISTEMA_LOCAL,"java",Constants.PATH_ARQS_JAVA_LOCAL,txObj+".java");
			
			Object tabObj = null;
			
			tabObj = Class.forName(pathObj).newInstance();
			
			Field[] campos = tabObj.getClass().getDeclaredFields();
			
			StringBuilder str = new StringBuilder();
			
			GeradorClasseBean g = new GeradorClasseBean();
			String tabObjMinusculo = g.arrumaCampoMinusculaObj(txObj);
			
			str.append("<!DOCTYPE html>\n");
			str.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" \n"); 
			str.append("  xmlns:th=\"http://www.thymeleaf.org\"\n");
			str.append("  xmlns:layout=\"http://www.ultraq.net.nz/thymeleaf/layout\"\n"); 
			str.append("  layout:decorator=\"/layout/LayoutPadrao\">\n\n");
			 
			str.append(" <section layout:fragment=\"conteudo\" >\n");
			str.append("\n\n");
			
            str.append("<div class=\"page-inner\">\n");              
            str.append("<div class=\"page-section\">\n");
            str.append("<section id=\"base-style\" class=\"card\">\n");
            str.append("    <div class=\"card-body\">\n");
            str.append("      <form id=\""+tabObjMinusculo+"\" method=\"post\" th:action=\"@{/"+txPathForm+"/gravar}\" th:object=\"${"+tabObjMinusculo+"}\" >\n");
            str.append("        <fieldset>\n");
            str.append("         <header class=\"page-title-bar\">\n");                
            str.append("          <h5 class=\"page-title\" th:text=\"${pathtela}\"> Nome da Tela </h5>\n");
            str.append("		   </header> \n");                      
            str.append("          <nav class=\"navbar bg-primary main-menu-jtp\">\n");
            str.append("          	<ul>\n");
            str.append("          		<a th:href=\"@{${hrefNovo}}\" id=\"linkNovo\">\n");
            str.append("          			<li><span class=\"fas fa-plus-circle\"></span> Novo</li>\n");
            str.append("          		</a>\n");
            str.append("          		<a href=\"javascript: gravar('"+tabObjMinusculo+"');\" id=\"linkGravar\">\n");
            str.append("          			<li><span class=\"fas fa-save\"></span> Gravar</li>\n");
            str.append("          		</a>\n");                  		
            str.append("          	</ul>\n");
            str.append("          	<div class=\"form-group pesquisar-main-menu\">\n");
            str.append("              <ul><a th:href=\"@{${hrefBack}}\"><li><span class=\"fas fa-arrow-left\"></span> Voltar</li></a></ul>\n");
            str.append("          	</div>\n");
            str.append("          </nav>\n");
            str.append("\n");
			
            String tx_classe = tabObj.getClass().getSimpleName().replace("Obj", "Service");

            str.append("<div class=\"row\" > \n");
            str.append("<div th:class=\"((${txEdit} == 'edit')? 'col-sm-8' : 'col-sm-12')\"> \n");
            str.append("       <section class=\"card card-fluid\"> \n");
            str.append("         <div class=\"table-responsive\"> \n");
            str.append("           <table class=\"table table-hover\"> \n");
            str.append("             <thead class=\"jtp-bg-primary\"> \n");
            str.append("               <tr> \n");
            
            for (int i = 0; i < campos.length; i++) {
				 
				 campos[i].setAccessible(true);
				 
				 String fieldName = campos[i].getName();
				 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
				 
				 str.append("                 <th style=\"min-width:100px\"> "+descricaoCampo+" </th> \n");
            
            }
            
            str.append("                 <th style=\"width:100px; min-width:100px;\"> &nbsp; </th> \n");
            str.append("               </tr> \n");
            str.append("             </thead> \n");
            str.append("             <tbody> \n");
            
             String txLista = "";
			 for (int i = 0; i < campos.length; i++) {
				 
				 campos[i].setAccessible(true);
				 
				 String fieldName = campos[i].getName();
				 
				 String fieldType = campos[i].getType().toString().toLowerCase();
				 
				 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
				 if (i == 0) {
				  str.append("               <tr th:each=\"listdados : ${listadados}\" th:onclick=\"|javascript: abrirtela('@{/"+txPathForm+"/consultar}/${listdados.cdCodigo}/${txNref}');|\"> \n");
				  txLista = descricaoCampo;
				 }
				 if (fieldType.contains("big") || fieldType.contains("date")) {
				   str.append("                 <td class=\"jtp-grid-td\" th:text=\"${{listdados."+fieldName+"}}\"> "+fieldName+" </td> \n");			 
				 }else {
				   str.append("                 <td class=\"jtp-grid-td\" th:text=\"${listdados."+fieldName+"}\"> "+fieldName+" </td> \n");			 
				 }
			 }
				 
            
            //str.append("               <tr th:each=\"list : ${listagrupoacesso}\"> \n");
            
            str.append("                <td class=\"align-middle text-right\"> \n");
            str.append("                 <a th:href=\"${listdados.txUrlLinhaGrid}\" class=\"btn btn-sm btn-secondary\"> \n");
            str.append("                   <i class=\"fa fa-pencil-alt\"></i> \n");
            str.append("                   <span class=\"sr-only\">Edit</span> \n");
            str.append("                 </a> \n");
            str.append("                 <a href=\"#\" class=\"btn btn-sm btn-secondary\"> \n");
            str.append("                   <i class=\"far fa-trash-alt\"></i> \n");
            str.append("                   <span class=\"sr-only\">Remove</span> \n");
            str.append("                 </a> \n");
            str.append("               </td> \n");
            str.append("               </tr> \n");
            str.append("             </tbody> \n");
            str.append("           </table> \n");
            str.append("         </div> \n");
            str.append("       </section> \n");                
            str.append("</div> \n");
            str.append("<div class=\"col-sm-4\" th:if=\"${!#strings.isEmpty(txEdit)}\"> \n");
            str.append("<table class=\"table table-hover\"> \n");
            str.append("  <thead class=\"jtp-bg-primary\"> \n");
            str.append("    <tr> \n");
            str.append("      <th style=\"min-width:200px\"> Novo/Alterar </th>  \n");                          
            str.append("    </tr> \n");
            str.append("  </thead> \n");
            str.append("  <tbody> \n");
            str.append("   <tr> \n");
            str.append("    <td> \n");

            
            String strJS = "\n\n";
            
			 for (int i = 0; i < campos.length; i++) {
				 
				 campos[i].setAccessible(true);
				 
				 String fieldName = campos[i].getName();
				 
				 String fieldType = campos[i].getType().toString().toLowerCase();
				 
				 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
				 
				  if (fieldType.contains("integer") || fieldType.contains("int")  || fieldName.contains("cd")) {
					  str.append("<!-- INPUT TEXT -->\n"); 
		                 str.append("<div class=\"col-sm-6\">\n");
		                 str.append("<div class=\"form-group\">\n");
		                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
		                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
		                 str.append("</div>\n");
		                 str.append("</div>\n");
		                 str.append("\n");
					  
					  
					  str.append("<!-- SELECT -->\n");
					  str.append("<div class=\"col-sm-6\">\n");
					  str.append(" <div class=\"form-group\">\n");
					  str.append("	<label for=\""+fieldName+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
					  str.append("	 <select class=\"custom-select\" id=\""+fieldName+"\" th:field=\"*{"+fieldName+"}\">\n");
					  str.append("    <option value=\"\">...</option>\n");
					  str.append("    <option th:each=\"s"+fieldName.toLowerCase()+" : ${select"+fieldName.toLowerCase()+"}\" th:value=\"${s"+fieldName.toLowerCase()+"."+fieldName+"}\" th:text=\"${s"+fieldName.toLowerCase()+"."+fieldName.replace("cd", "tx")+"}\"></option>\n");
					  str.append("  </select>\n");
					  str.append(" </div>\n");
					  str.append("</div>\n");
					  
						if (!fieldName.contains("ck")) {
						  str.append("<!-- LOOKUP -->\n");		
						  str.append("<div class=\"col-sm-6\">\n");
						  str.append("	 <div class=\"form-group\">\n");
						  str.append("    <label for=\""+fieldName.replace("cd", "tx")+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");						 
						  str.append("	    <div class=\"input-group\">\n");							 
						  str.append("     <label class=\"input-group-prepend\" for=\""+fieldName.replace("cd", "tx")+"\">\n");
						  str.append("     <span class=\"input-group-text\">\n");
						  str.append("       <span class=\"fas fa-search\"></span>\n");
						  str.append("     </span>\n");
						  str.append("    </label>\n");
						  str.append("     <input type=\"text\" class=\"form-control\" id=\""+fieldName.replace("cd", "tx")+"\" name=\""+fieldName.replace("cd", "tx")+"\" th:field=\"*{"+fieldName.replace("cd", "tx")+"}\" onchange=\"javascript: limpalookup('"+fieldName.replace("cd", "tx")+"','"+fieldName+"');\" />\n");
						  str.append("     <input type=\"hidden\" class=\"form-control\" id=\""+fieldName+"\" name=\""+fieldName+"\" th:field=\"*{"+fieldName+"}\" /> \n");
						  str.append("  </div>\n");
						  str.append("	 </div>\n");
						  str.append("	</div>\n");
						  
						   //Gera JS Lookup
						  	strJS += gerarJS("lk"+descricaoCampo.toLowerCase(), fieldName.replace("cd", "tx"), fieldName);
					    }
					  str.append("\n");
				  }else {
					  
					if (fieldType.contains("big")) {  
					 str.append("<!-- INPUT TEXT BIGDECIMAL -->\n"); 
	                 str.append("<div class=\"col-sm-6\">\n");
	                 str.append("<div class=\"form-group\">\n");
	                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
	                 str.append("  <input type=\"text\" class=\"form-control textFloat\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
	                 str.append("</div>\n");
	                 str.append("</div>\n");
	                 str.append("\n");
					}else if (fieldType.contains("date")) {
						 str.append("<!-- INPUT TEXT -->\n"); 
		                 str.append("<div class=\"col-sm-6\">\n");
		                 str.append("<div class=\"form-group\">\n");
		                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
		                 str.append("  <input type=\"text\" class=\"form-control jtpDate\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\" />\n");
		                 str.append("</div>\n");
		                 str.append("</div>\n");
		                 str.append("\n");						

					}else {	
						 str.append("<!-- INPUT TEXT -->\n"); 
		                 str.append("<div class=\"col-sm-6\">\n");
		                 str.append("<div class=\"form-group\">\n");
		                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
		                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
		                 str.append("</div>\n");
		                 str.append("</div>\n");
		                 str.append("\n");						
					}

				 }
			 }
	         
			 str.append("<!-- Campos Hidden --> \n");
			 str.append("	<input type=\"hidden\" class=\"form-control\" id=\"NomeCampoRelacionamento\" name=\"NomeCampoRelacionamento\"  th:field=\"*{cdCliente}\"/> \n");
			 str.append("	<input type=\"hidden\" class=\"form-control\" id=\"NomeCampoChave\" name=\"NomeCampoChave\"  th:field=\"*{NomeCampoChave}\"/> \n");
			 
			 
			 str.append("</td> \n");
			 str.append("   </tr> \n");
			 str.append("  </tbody> \n");
			 str.append(" </table> \n");			
			 str.append("</div> \n");
			 str.append("</div> \n");
			 str.append("</fieldset> \n");
			 str.append("</form> \n");
			 str.append("</div> \n");
			 str.append("</section> \n");
			 str.append("</div> \n");
			 str.append("</div> \n");


			 str.append("<!-- Script JS --> \n");
			 
			 str.append("<script th:inline=\"javascript\"> \n");	
			 str.append(" /*<![CDATA[*/	");	   
			 str.append("  $(document).ready(function() {	\n");
			 str.append(" var cdTipo = /*[[${tipoMensagem}]]*/ \n jtpNotifySimple(/*[[${txMensagem}]]*/,cdTipo);\n");	
			 str.append("	jtpNotify(/*[[${listaErros}]]*/);\n");			 
			 str.append("    var cdchave = /*[[${CampoChave}]]*/; \n");
			 str.append("    document.getElementById('CampoChave').value = cdchave; \n");
			 
			 str.append(" var txEdit = /*[[${txEdit}]]*/; \n");
			 str.append(" if (txEdit != null) { \n");
			 str.append("	 $('#NomeModal').modal('show'); \n");	
			 str.append(" } \n");
			 
			 str.append("//Permissões \n");
			 str.append("	var listPermissaoBotao = /*[[${listPermissaoBotao}]]*/; \n");
			 str.append("	checkPermissaoBotao(listPermissaoBotao);\n\n");
				
			 str.append("	var listPermissaoCampos = /*[[${listPermissaoCampos}]]*/; \n");
			 str.append("	checkPermissaoCampos(listPermissaoCampos); \n\n");

			 str.append("	//Service \n");
			 str.append("	var txService = /*[[${txService}]]*/; \n");
			 str.append("	analistaVirtual(txService); \n");
			 
			 str.append("	var pathUrl = /*[[@{/}]]*/; \n\n");

			 str.append(strJS);

			 
			 str.append("	var lkPesquisar = /*[[@{/"+txPathForm+"/lkpesquisar}]]*/;\n");
			 str.append("	var autocomplete = new JTPAutocompleteSimple.AutocompleteSimple('#txPesquisar');\n");
			 str.append("	autocomplete.iniciar(lkPesquisar,'txUrlPesquisar','txPesquisar');\n\n");
			 
			 str.append(" });\n");	
			 str.append(" /*]]>*/ \n");
			 str.append("</script> \n"); 
			 
			 str.append("</section>\n\n");
			 
			 str.append("</html>\n");
	    
			 
			 
	         File f = new File("/temp/obj/html");
		        if (!f.exists()) {
		        	f.mkdirs();
		        }
	         
	          FileWriter arq = new FileWriter("/temp/obj/html/"+txPathForm+".html");
		      PrintWriter gravarArq = new PrintWriter(arq);
		      gravarArq.printf(str.toString());
		      gravarArq.close();
		      
		      System.out.println("Service "+txObj+".html criado com sucesso!");
		      
		}catch (Exception ex) {
			ex.printStackTrace();
		}		
	}

	
	private void GeradorHTMLPesquisaOld(String txObj, String txPathForm) {	 
		try {
			
			String pathObj = new GeneralUtil().LocalizaArquivo(Constants.NOME_SISTEMA_LOCAL,"java",Constants.PATH_ARQS_JAVA_LOCAL,txObj+".java");
			
			Object tabObj = null;
			
			tabObj = Class.forName(pathObj).newInstance();
			
			Field[] campos = tabObj.getClass().getDeclaredFields();
			
			StringBuilder str = new StringBuilder();
			
			GeradorClasseBean g = new GeradorClasseBean();
			String tabObjMinusculo = g.arrumaCampoMinusculaObj(txObj);
			
			str.append("<!DOCTYPE html>\n");
			str.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" \n"); 
			str.append("  xmlns:th=\"http://www.thymeleaf.org\"\n");
			str.append("  xmlns:layout=\"http://www.ultraq.net.nz/thymeleaf/layout\"\n"); 
			str.append("  layout:decorator=\"/layout/LayoutPadrao\">\n\n");
			 
			str.append(" <section layout:fragment=\"conteudo\" >\n");
			str.append("\n\n");
			
            str.append("<div class=\"page-inner\">\n");              
            str.append("<div class=\"page-section\">\n");
            str.append("<section id=\"base-style\" class=\"card\">\n");
            str.append("    <div class=\"card-body\">\n");
            str.append("      <form id=\""+tabObjMinusculo+"\" method=\"post\" th:action=\"@{/"+txPathForm+"/pesquisar}\" th:object=\"${"+tabObjMinusculo+"}\" >\n");
            str.append("        <fieldset>\n");
            str.append("<div class=\"row\"> \n");
            str.append(" <div class=\"col-sm-10\"> \n");
            str.append("  <header class=\"page-title-bar\"> \n");
            str.append("	<h5 class=\"page-title\" th:text=\"${pathtela}\">Nome da Tela</h5> \n");
            str.append("  </header> \n");
            str.append("  </div> \n");
            str.append("  <div class=\"col-sm-2\"> \n");
            str.append("  <button type=\"button\" class=\"btn btn-primary col-sm-5 float-right\" th:onclick=\"|javascript: abrirtela('@{/TelaPrincipal/novo}');|\" > \n");
            str.append("		<strong>Novo</strong> ");
            str.append("  </button> \n");
            str.append("  </div> \n");
            str.append("</div> \n");
            
            str.append("<nav class=\"navbar bg-primary main-menu-jtp\">\n");

            str.append("<fieldset class=\"col-sm-12 jtp-fieldset\">\n");
            str.append("	<legend class=\"jtp-legend\">Filtros</legend>\n");

            str.append("	<div class=\"panel panel-default\">\n");
            str.append("		<div class=\"panel-body\">\n");
            str.append("			<div class=\"row\">\n");
            
            String tx_classe = tabObj.getClass().getSimpleName().replace("Obj", "Service");
 
            String strJS = "\n\n";
    
				 for (int i = 0; i < campos.length; i++) {
					 
					 campos[i].setAccessible(true);
					 
					 String fieldName = campos[i].getName();
					 
					 String fieldType = campos[i].getType().toString().toLowerCase();
					 
					 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
					 
					  if (fieldType.contains("integer") || fieldType.contains("int") || fieldName.contains("cd")) {
						  str.append("<!-- INPUT TEXT -->\n"); 
			                 str.append("<div class=\"col-sm-6\">\n");
			                 str.append("<div class=\"form-group\">\n");
			                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
			                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
			                 str.append("</div>\n");
			                 str.append("</div>\n");
			                 str.append("\n");
						  
						  str.append("<!-- SELECT -->\n");
						  str.append("<div class=\"col-sm-6\">\n");
						  str.append(" <div class=\"form-group\">\n");
						  str.append("	<label for=\""+fieldName+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
						  str.append("	 <select class=\"custom-select\" id=\""+fieldName+"\" th:field=\"*{"+fieldName+"}\">\n");
						  str.append("    <option value=\"\">...</option>\n");
						  str.append("    <option th:each=\"s"+fieldName.toLowerCase()+" : ${select"+fieldName.toLowerCase()+"}\" th:value=\"${s"+fieldName.toLowerCase()+"."+fieldName+"}\" th:text=\"${s"+fieldName.toLowerCase()+"."+fieldName.replace("cd", "tx")+"}\"></option>\n");
						  str.append("  </select>\n");
						  str.append(" </div>\n");
						  str.append("</div>\n");
						  
							if (!fieldName.contains("ck")) {
							  str.append("<!-- LOOKUP -->\n");
							  str.append("<div class=\"col-sm-6\">\n");
							  str.append("	 <div class=\"form-group\">\n");
							  str.append("    <label for=\""+fieldName.replace("cd", "tx")+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");						 
							  str.append("	    <div class=\"input-group\">\n");							 
							  str.append("     <label class=\"input-group-prepend\" for=\""+fieldName.replace("cd", "tx")+"\">\n");
							  str.append("     <span class=\"input-group-text\">\n");
							  str.append("       <span class=\"fas fa-search\"></span>\n");
							  str.append("     </span>\n");
							  str.append("    </label>\n");
							  str.append("     <input type=\"text\" class=\"form-control\" id=\""+fieldName.replace("cd", "tx")+"\" name=\""+fieldName.replace("cd", "tx")+"\" th:field=\"*{"+fieldName.replace("cd", "tx")+"}\" onchange=\"javascript: limpalookup('"+fieldName.replace("cd", "tx")+"','"+fieldName+"');\" />\n");
							  str.append("     <input type=\"hidden\" class=\"form-control\" id=\""+fieldName+"\" name=\""+fieldName+"\" th:field=\"*{"+fieldName+"}\" /> \n");
							  str.append("  </div>\n");
							  str.append("	 </div>\n");
							  str.append("	</div>\n");
							  
							   //Gera JS Lookup
							  	strJS += gerarJS("lk"+descricaoCampo.toLowerCase(), fieldName.replace("cd", "tx"), fieldName);
						    }
						  str.append("\n");
					  }else {
						  
						if (fieldType.contains("big")) {  
						 str.append("<!-- INPUT TEXT BIGDECIMAL -->\n");
			             str.append("<div class=\"col-sm-6\">\n");
			             str.append("<div class=\"form-group\">\n");
			             str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
			             str.append("  <input type=\"text\" class=\"form-control textFloat\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
			             str.append("</div>\n");
			             str.append("</div>\n");
			             str.append("\n");
						}else if (fieldType.contains("date")) {
							 str.append("<!-- INPUT TEXT -->\n"); 
			                 str.append("<div class=\"col-sm-6\">\n");
			                 str.append("<div class=\"form-group\">\n");
			                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
			                 str.append("  <input type=\"text\" class=\"form-control jtpDate\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\" />\n");
			                 str.append("</div>\n");
			                 str.append("</div>\n");
			                 str.append("\n");						

						}else {	
							 str.append("<!-- INPUT TEXT -->\n"); 
			                 str.append("<div class=\"col-sm-6\">\n");
			                 str.append("<div class=\"form-group\">\n");
			                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
			                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
			                 str.append("</div>\n");
			                 str.append("</div>\n");
			                 str.append("\n");						
						}

					 }
				 }            
            
            str.append("			</div>\n");
            str.append("				<div class=\"col-sm-12\">\n");
            str.append("					<button type=\"submit\" class=\"btn btn-primary col-sm-12\">\n");
            str.append("						<strong>Pesquisar</strong>\n");
            str.append("					</button>\n");
            str.append("				</div>\n");
            str.append("		</div>\n");
            str.append("	</div>\n");
            str.append("</fieldset>\n");
            str.append("</nav>\n");
            
            str.append("\n");
			
 
            str.append("<div class=\"row\" > \n");
            str.append("<div class=\"col-sm-12\"> \n");
            str.append("       <section class=\"card card-fluid\"> \n");
            str.append("         <div class=\"table-responsive\"> \n");
            str.append("           <table class=\"table table-hover\"> \n");
            str.append("             <thead class=\"jtp-bg-primary\"> \n");
            str.append("               <tr> \n");
            
            for (int i = 0; i < campos.length; i++) {
				 
				 campos[i].setAccessible(true);
				 
				 String fieldName = campos[i].getName();
				 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
				 
				 str.append("                 <th style=\"min-width:100px\"> "+descricaoCampo+" </th> \n");
            
            }
            
            str.append("                 <th style=\"width:100px; min-width:100px;\"> &nbsp; </th> \n");
            str.append("               </tr> \n");
            str.append("             </thead> \n");
            str.append("             <tbody th:if=\"${#strings.isEmpty(mensagem)}\"> \n");
            
             String txLista = "";
			 for (int i = 0; i < campos.length; i++) {
				 
				 campos[i].setAccessible(true);
				 
				 String fieldName = campos[i].getName();
				 
				 String fieldType = campos[i].getType().toString().toLowerCase();
				 
				 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
				 if (i == 0) {
				  str.append("               <tr th:each=\"list"+descricaoCampo.toLowerCase()+" : ${listadados.conteudo}\"> \n");
				  txLista = descricaoCampo;
				 }
				 if (fieldType.contains("big") || fieldType.contains("date")) {
				   str.append("                 <td class=\"jtp-grid-td\" th:text=\"${{list"+txLista.toLowerCase()+"."+fieldName+"}}\"> "+fieldName+" </td> \n");			 
				 }else {
				   str.append("                 <td class=\"jtp-grid-td\" th:text=\"${list"+txLista.toLowerCase()+"."+fieldName+"}\"> "+fieldName+" </td> \n");			 
				 }
			 }
            
            str.append("                <td class=\"align-middle text-right\"> \n");
            str.append("                 <a th:href=\"${listclassificacao.txUrlLinhaGrid}\" class=\"btn btn-sm btn-secondary\"> \n");
            str.append("                   <i class=\"fa fa-pencil-alt\"></i> \n");
            str.append("                   <span class=\"sr-only\">Edit</span> \n");
            str.append("                 </a> \n");
            str.append("                 <a href=\"#\" class=\"btn btn-sm btn-secondary\"> \n");
            str.append("                   <i class=\"far fa-trash-alt\"></i> \n");
            str.append("                   <span class=\"sr-only\">Remove</span> \n");
            str.append("                 </a> \n");
            str.append("               </td> \n");
            str.append("               </tr> \n");
            str.append("             </tbody> \n");
            str.append("           </table> \n");
            str.append("			<div class=\"col-lg-4\" th:if=\"${!#strings.isEmpty(mensagem)}\">\n");
            str.append("			 <div class=\"form-group\">\n");
            str.append("				<span class=\"jtp-span\" th:text=\"${mensagem}\"><strong>Não existe!</strong></span>\n");
            str.append("			 </div>\n");
            str.append("			</div>\n");
            str.append("         </div> \n");
            str.append("		<ul class=\"pagination justify-content-center mt-4\"\n");
            str.append("		 th:if=\"not ${!#strings.isEmpty(mensagem)}\" th:with=\"paginaAtual=${listadados.atual}\">\n");
            str.append("		  <li class=\"page-item\" th:classappend=\"${listadados.primeira} ? disabled\"><a class=\"page-link\"\n");
            str.append("			th:href=\"${listadados.urlParaPagina(paginaAtual - 1)}\" tabindex=\"-1\"> <i\n");
            str.append("				class=\"fa fa-lg fa-angle-left\"></i>\n");
            str.append("		    </a></li>\n");
            str.append("		  <th:block\n");
            str.append("			th:each=\"p : ${#numbers.sequence(1, listadados.total)}\"\n");
            str.append("			th:with=\"numeroPagina=${p - 1}\">\n");
            str.append("			<li class=\"page-item\" th:classappend=\"${paginaAtual == numeroPagina} ? active\">\n");
            str.append("				<a class=\"page-link\" th:href=\"${listadados.urlParaPagina(numeroPagina)}\" th:text=\"${numeroPagina + 1}\">1</a>\n");
            str.append("			</li>\n");
            str.append("		  </th:block>\n");
            str.append("		  <li class=\"page-item\" th:classappend=\"${listadados.ultima} ? disabled\"><a class=\"page-link\" th:href=\"${listadados.urlParaPagina(paginaAtual + 1)}\">\n");
            str.append("				<i class=\"fa fa-lg fa-angle-right\"></i>\n");
            str.append("		</a></li>\n");
            str.append("	</ul>\n");
            str.append("       </section> \n");                
            str.append("</div> \n");
            
            
			 str.append("</div> \n");
			 str.append("</fieldset> \n");
			 str.append("</form> \n");
			 str.append("</div> \n");
			 str.append("</section> \n");
			 str.append("</div> \n");
			 str.append("</div> \n");


			 str.append("<!-- Script JS --> \n");
			 str.append("<script th:inline=\"javascript\"> \n");	
			 str.append(" /*<![CDATA[*/	");	   
			 str.append("  $(document).ready(function() {	\n");			 
			 str.append(" var cdTipo = /*[[${tipoMensagem}]]*/ \n jtpNotifySimple(/*[[${txMensagem}]]*/,cdTipo);\n");	
			 str.append("	jtpNotify(/*[[${listaErros}]]*/);\n");			 
			 str.append("    var cdchave = /*[[${CampoChave}]]*/; \n");
			 str.append("    document.getElementById('CampoChave').value = cdchave; \n");
			 
			 str.append("//Permissões \n");
			 str.append("	var listPermissaoBotao = /*[[${listPermissaoBotao}]]*/; \n");
			 str.append("	checkPermissaoBotao(listPermissaoBotao);\n\n");
				
			 str.append("	var listPermissaoCampos = /*[[${listPermissaoCampos}]]*/; \n");
			 str.append("	checkPermissaoCampos(listPermissaoCampos); \n\n");

			 str.append("	//Service \n");
			 str.append("	var txService = /*[[${txService}]]*/; \n");
			 str.append("	analistaVirtual(txService); \n");
			 str.append("	var pathUrl = /*[[@{/}]]*/; \n\n");

			 str.append(strJS);

			 
			 str.append(" });\n");	
			 str.append(" /*]]>*/ \n");
			 str.append("</script> \n"); 
			 
			 str.append("</section>\n\n");
			 
			 str.append("</html>\n");
			 
	         File f = new File("/temp/obj/html");
		        if (!f.exists()) {
		        	f.mkdirs();
		        }
	         
	          FileWriter arq = new FileWriter("/temp/obj/html/"+txPathForm+".html");
		      PrintWriter gravarArq = new PrintWriter(arq);
		      gravarArq.printf(str.toString());
		      gravarArq.close();
		      
		      System.out.println("HTML "+txObj+".html criado com sucesso!");
		      
		}catch (Exception ex) {
			ex.printStackTrace();
		}		
	}
	
	private void GeradorHTMLPesquisaGravacaoOld(String txObj, String txPathForm) {	 
		try {
			
			String pathObj = new GeneralUtil().LocalizaArquivo(Constants.NOME_SISTEMA_LOCAL,"java",Constants.PATH_ARQS_JAVA_LOCAL,txObj+".java");
			
			Object tabObj = null;
			
			tabObj = Class.forName(pathObj).newInstance();
			
			Field[] campos = tabObj.getClass().getDeclaredFields();
			
			StringBuilder str = new StringBuilder();
			
			GeradorClasseBean g = new GeradorClasseBean();
			String tabObjMinusculo = g.arrumaCampoMinusculaObj(txObj);			
			
			str.append("<!DOCTYPE html>\n");
			str.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" \n"); 
			str.append("  xmlns:th=\"http://www.thymeleaf.org\"\n");
			str.append("  xmlns:layout=\"http://www.ultraq.net.nz/thymeleaf/layout\"\n"); 
			str.append("  layout:decorator=\"/layout/LayoutPadrao\">\n\n");
			 
			str.append(" <section layout:fragment=\"conteudo\" >\n");
			str.append("\n\n");
			
            str.append("<div class=\"page-inner\">\n");              
            str.append("<div class=\"page-section\">\n");
            str.append("<section id=\"base-style\" class=\"card\">\n");
            str.append("    <div class=\"card-body\">\n");
            str.append("      <form id=\""+tabObjMinusculo.replace("tab", "tabFiltro")+"\" method=\"get\" th:action=\"@{/"+txPathForm+"}\" th:object=\"${"+tabObjMinusculo.replace("tab", "tabFiltro")+"}\" >\n");
            str.append("        <fieldset>\n");
            str.append("<div class=\"row\"> \n");
            str.append(" <div class=\"col-sm-10\"> \n");
            str.append("  <header class=\"page-title-bar\"> \n");
            str.append("	<h5 class=\"page-title\" th:text=\"${pathtela}\">Nome da Tela</h5> \n");
            str.append("  </header> \n");
            str.append("  </div> \n");
            str.append("  <div class=\"col-sm-2\"> \n");
            str.append("  <button type=\"button\" class=\"btn btn-primary col-sm-5 float-right\" th:onclick=\"|javascript: abrirtela('@{/"+txPathForm+"/novo}');|\" > \n");
            str.append("		<strong>Novo</strong> ");
            str.append("  </button> \n");
            str.append("  </div> \n");
            str.append("</div> \n");
            
            str.append("<nav class=\"navbar bg-primary main-menu-jtp\">\n");

            str.append("<fieldset class=\"col-sm-12 jtp-fieldset\">\n");
            str.append("	<legend class=\"jtp-legend\">Filtros</legend>\n");

            str.append("	<div class=\"panel panel-default\">\n");
            str.append("		<div class=\"panel-body\">\n");
            str.append("			<div class=\"row\">\n");
            
            String tx_classe = tabObj.getClass().getSimpleName().replace("Obj", "Service");
 
            String strJS = "\n\n";
    
				 for (int i = 0; i < campos.length; i++) {
					 
					 campos[i].setAccessible(true);
					 
					 String fieldName = campos[i].getName();
					 
					 String fieldType = campos[i].getType().toString().toLowerCase();
					 
					 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
					 
					  if (fieldType.contains("integer") || fieldType.contains("int") || fieldName.contains("cd")) {
						  str.append("<!-- INPUT TEXT -->\n"); 
			                 str.append("<div class=\"col-sm-6\">\n");
			                 str.append("<div class=\"form-group\">\n");
			                 str.append("  <label for=\""+campos[i].getName()+"Filtro\"><strong>"+descricaoCampo+"</strong></label>\n");
			                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"Filtro\" name=\""+campos[i].getName()+"Filtro\"  th:field=\"*{"+fieldName+"Filtro}\"/>\n");
			                 str.append("</div>\n");
			                 str.append("</div>\n");
			                 str.append("\n");
						  
						  str.append("<!-- SELECT -->\n");
						  str.append("<div class=\"col-sm-6\">\n");
						  str.append(" <div class=\"form-group\">\n");
						  str.append("	<label for=\""+fieldName+"Filtro\"><strong>"+descricaoCampo+"</strong></label>\n");
						  str.append("	 <select class=\"custom-select\" id=\""+fieldName+"Filtro\" th:field=\"*{"+fieldName+"Filtro}\">\n");
						  str.append("    <option value=\"\">...</option>\n");
						  str.append("    <option th:each=\"s"+fieldName.toLowerCase()+" : ${select"+fieldName.toLowerCase()+"}\" th:value=\"${s"+fieldName.toLowerCase()+"."+fieldName+"}\" th:text=\"${s"+fieldName.toLowerCase()+"."+fieldName.replace("cd", "tx")+"}\"></option>\n");
						  str.append("  </select>\n");
						  str.append(" </div>\n");
						  str.append("</div>\n");
						  
							if (!fieldName.contains("ck")) {
							  str.append("<!-- LOOKUP -->\n");
							  str.append("<div class=\"col-sm-6\">\n");
							  str.append("	 <div class=\"form-group\">\n");
							  str.append("    <label for=\""+fieldName.replace("cd", "tx")+"Filtro\"><strong>"+descricaoCampo+"</strong></label>\n");						 
							  str.append("	    <div class=\"input-group\">\n");							 
							  str.append("     <label class=\"input-group-prepend\" for=\""+fieldName.replace("cd", "tx")+"Filtro\">\n");
							  str.append("     <span class=\"input-group-text\">\n");
							  str.append("       <span class=\"fas fa-search\"></span>\n");
							  str.append("     </span>\n");
							  str.append("    </label>\n");
							  str.append("     <input type=\"text\" class=\"form-control\" id=\""+fieldName.replace("cd", "tx")+"Filtro\" name=\""+fieldName.replace("cd", "tx")+"Filtro\" th:field=\"*{"+fieldName.replace("cd", "tx")+"Filtro}\" onchange=\"javascript: limpalookup('"+fieldName.replace("cd", "tx")+"Filtro','"+fieldName+"Filtro');\" />\n");
							  str.append("     <input type=\"hidden\" class=\"form-control\" id=\""+fieldName+"Filtro\" name=\""+fieldName+"Filtro\" th:field=\"*{"+fieldName+"Filtro}\" /> \n");
							  str.append("  </div>\n");
							  str.append("	 </div>\n");
							  str.append("	</div>\n");
							  
							   //Gera JS Lookup
							  	strJS += gerarJS("lk"+descricaoCampo.toLowerCase(), fieldName.replace("cd", "tx"), fieldName);
						    }
						  str.append("\n");
					  }else {
						  
						if (fieldType.contains("big")) {  
						 str.append("<!-- INPUT TEXT BIGDECIMAL -->\n");
			             str.append("<div class=\"col-sm-6\">\n");
			             str.append("<div class=\"form-group\">\n");
			             str.append("  <label for=\""+campos[i].getName()+"Filtro\"><strong>"+descricaoCampo+"</strong></label>\n");
			             str.append("  <input type=\"text\" class=\"form-control textFloat\" id=\""+campos[i].getName()+"Filtro\" name=\""+campos[i].getName()+"Filtro\"  th:field=\"*{"+fieldName+"Filtro}\"/>\n");
			             str.append("</div>\n");
			             str.append("</div>\n");
			             str.append("\n");
						}else if (fieldType.contains("date")) {
							 str.append("<!-- INPUT TEXT -->\n"); 
			                 str.append("<div class=\"col-sm-6\">\n");
			                 str.append("<div class=\"form-group\">\n");
			                 str.append("  <label for=\""+campos[i].getName()+"Filtro\"><strong>"+descricaoCampo+"</strong></label>\n");
			                 str.append("  <input type=\"text\" class=\"form-control jtpDate\" id=\""+campos[i].getName()+"Filtro\" name=\""+campos[i].getName()+"Filtro\"  th:field=\"*{"+fieldName+"Filtro}\" />\n");
			                 str.append("</div>\n");
			                 str.append("</div>\n");
			                 str.append("\n");						

						}else {	
							 str.append("<!-- INPUT TEXT -->\n"); 
			                 str.append("<div class=\"col-sm-6\">\n");
			                 str.append("<div class=\"form-group\">\n");
			                 str.append("  <label for=\""+campos[i].getName()+"Filtro\"><strong>"+descricaoCampo+"</strong></label>\n");
			                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"Filtro\" name=\""+campos[i].getName()+"Filtro\"  th:field=\"*{"+fieldName+"Filtro}\"/>\n");
			                 str.append("</div>\n");
			                 str.append("</div>\n");
			                 str.append("\n");						
						}

					 }
				 }            
            
            str.append("			</div>\n");
            str.append("				<div class=\"col-sm-12\">\n");
            str.append("					<button type=\"submit\" class=\"btn btn-primary col-sm-12\">\n");
            str.append("						<strong>Pesquisar</strong>\n");
            str.append("					</button>\n");
            str.append("				</div>\n");
            str.append("		</div>\n");
            str.append("	</div>\n");
            str.append("</fieldset>\n");
            str.append("</nav>\n");
            
            str.append("\n");
			
 
            str.append("<div class=\"row\" > \n");
            str.append("<div class=\"col-sm-12\"> \n");
            str.append("       <section class=\"card card-fluid\"> \n");
            str.append("         <div class=\"table-responsive\"> \n");
            str.append("           <table class=\"table table-hover\"> \n");
            str.append("             <thead class=\"jtp-bg-primary\"> \n");
            str.append("               <tr> \n");
            
            for (int i = 0; i < campos.length; i++) {
				 
				 campos[i].setAccessible(true);
				 
				 String fieldName = campos[i].getName();
				 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
				 
				 str.append("                 <th style=\"min-width:100px\"> "+descricaoCampo+" </th> \n");
            
            }
            
            str.append("                 <th style=\"width:100px; min-width:100px;\"> &nbsp; </th> \n");
            str.append("               </tr> \n");
            str.append("             </thead> \n");
            str.append("             <tbody th:if=\"${#strings.isEmpty(mensagem)}\"> \n");
            
             String txLista = "";
			 for (int i = 0; i < campos.length; i++) {
				 
				 campos[i].setAccessible(true);
				 
				 String fieldName = campos[i].getName();
				 
				 String fieldType = campos[i].getType().toString().toLowerCase();
				 
				 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
				 if (i == 0) {
				  //str.append("               <tr th:each=\"list"+descricaoCampo.toLowerCase()+" : ${listadados.conteudo}\"> \n");
				  str.append("				 <tr th:each=\"listdados : ${listadados.conteudo}\" th:onclick=\"|javascript: abrirtela('@{/"+txPathForm+"/consultar}/${listdados."+fieldName+"}?page=${listadados.atual}');|\"> \n");
				  txLista = descricaoCampo;
				 }
				 if (fieldType.contains("big") || fieldType.contains("date")) {
				   str.append("                 <td class=\"jtp-grid-td\" th:text=\"${{listdados."+fieldName+"}}\"> "+fieldName+" </td> \n");			 
				 }else {
				   str.append("                 <td class=\"jtp-grid-td\" th:text=\"${listdados."+fieldName+"}\"> "+fieldName+" </td> \n");			 
				 }
			 }
            
            str.append("                <td class=\"align-middle text-right\"> \n");
            str.append("                 <a th:href=\"${listclassificacao.txUrlLinhaGrid}\" class=\"btn btn-sm btn-secondary\"> \n");
            str.append("                   <i class=\"fa fa-pencil-alt\"></i> \n");
            str.append("                   <span class=\"sr-only\">Edit</span> \n");
            str.append("                 </a> \n");
            str.append("                 <a href=\"#\" class=\"btn btn-sm btn-secondary\"> \n");
            str.append("                   <i class=\"far fa-trash-alt\"></i> \n");
            str.append("                   <span class=\"sr-only\">Remove</span> \n");
            str.append("                 </a> \n");
            str.append("               </td> \n");
            str.append("               </tr> \n");
            str.append("             </tbody> \n");
            str.append("           </table> \n");
            str.append("			<div class=\"col-lg-4\" th:if=\"${!#strings.isEmpty(mensagem)}\">\n");
            str.append("			 <div class=\"form-group\">\n");
            str.append("				<span class=\"jtp-span\" th:text=\"${mensagem}\"><strong>Não existe!</strong></span>\n");
            str.append("			 </div>\n");
            str.append("			</div>\n");
            str.append("         </div> \n");
            str.append("		<ul class=\"pagination justify-content-center mt-4\"\n");
            str.append("		 th:if=\"not ${!#strings.isEmpty(mensagem)}\" th:with=\"paginaAtual=${listadados.atual}\">\n");
            str.append("		  <li class=\"page-item\" th:classappend=\"${listadados.primeira} ? disabled\"><a class=\"page-link\"\n");
            str.append("			th:href=\"${listadados.urlParaPagina(paginaAtual - 1)}\" tabindex=\"-1\"> <i\n");
            str.append("				class=\"fa fa-lg fa-angle-left\"></i>\n");
            str.append("		    </a></li>\n");
            str.append("		  <th:block\n");
            str.append("			th:each=\"p : ${#numbers.sequence(1, listadados.total)}\"\n");
            str.append("			th:with=\"numeroPagina=${p - 1}\">\n");
            str.append("			<li class=\"page-item\" th:classappend=\"${paginaAtual == numeroPagina} ? active\">\n");
            str.append("				<a class=\"page-link\" th:href=\"${listadados.urlParaPagina(numeroPagina)}\" th:text=\"${numeroPagina + 1}\">1</a>\n");
            str.append("			</li>\n");
            str.append("		  </th:block>\n");
            str.append("		  <li class=\"page-item\" th:classappend=\"${listadados.ultima} ? disabled\"><a class=\"page-link\" th:href=\"${listadados.urlParaPagina(paginaAtual + 1)}\">\n");
            str.append("				<i class=\"fa fa-lg fa-angle-right\"></i>\n");
            str.append("		</a></li>\n");
            str.append("	</ul>\n");
            str.append("       </section> \n");                
            str.append("</div> \n");
		    str.append("</div> \n");
			str.append("</fieldset> \n");
			str.append("</form> \n");
			 
	           //Modal - Inicio
			str.append("<form id=\""+tabObjMinusculo+"\" method=\"post\" th:action=\"@{/"+txPathForm+"}\" th:object=\"${"+tabObjMinusculo+"}\">\n");
			str.append("<div class=\"modal fade\" id=\""+txPathForm+"\" tabindex=\"-1\" \n");
            str.append("		role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\"> \n");
            str.append("		<div class=\"vertical-alignment-helper\"> \n");
            str.append("			<div class=\"modal-dialog vertical-align-center\"> \n");
            str.append("				<div class=\"modal-content\"> \n");
            str.append("					<div class=\"modal-header\"> \n");
            str.append("						<h4 class=\"modal-title\" id=\"myModalLabel\" th:text=\"${pathtela}\"></h4> \n");            
            str.append("						<button type=\"button\" class=\"close\" data-dismiss=\"modal\">\n");
            str.append("							<span aria-hidden=\"true\">&times;</span><span \n");
            str.append("								class=\"sr-only\">Close</span> \n");
            str.append("						</button> \n");
            str.append("					</div> \n");
            str.append("					<div class=\"modal-body\"> \n");
            str.append("					<div class=\"row\"> \n");
            
            
			 for (int i = 0; i < campos.length; i++) {
				 
				 campos[i].setAccessible(true);
				 
				 String fieldName = campos[i].getName();
				 
				 String fieldType = campos[i].getType().toString().toLowerCase();
				 
				 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
				 
				  if (fieldType.contains("integer") || fieldType.contains("int")  || fieldName.contains("cd")) {
					  str.append("<!-- INPUT TEXT -->\n"); 
		                 str.append("<div class=\"col-sm-6\">\n");
		                 str.append("<div class=\"form-group\">\n");
		                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
		                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
		                 str.append("</div>\n");
		                 str.append("</div>\n");
		                 str.append("\n");
		                 
					  str.append("<!-- SELECT -->\n");
					  str.append("<div class=\"col-sm-2\">\n");
					  str.append(" <div class=\"form-group\">\n");
					  str.append("	<label for=\""+fieldName+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
					  str.append("	 <select class=\"custom-select\" id=\""+fieldName+"\" th:field=\"*{"+fieldName+"}\">\n");
					  str.append("    <option value=\"\">...</option>\n");
					  str.append("    <option th:each=\"s"+fieldName.toLowerCase()+" : ${select"+fieldName.toLowerCase()+"}\" th:value=\"${s"+fieldName.toLowerCase()+"."+fieldName+"}\" th:text=\"${s"+fieldName.toLowerCase()+"."+fieldName.replace("cd", "tx")+"}\"></option>\n");
					  str.append("  </select>\n");
					  str.append(" </div>\n");
					  str.append("</div>\n");
					  
						if (!fieldName.contains("ck")) {
						  str.append("<!-- LOOKUP -->\n");		
						  str.append("<div class=\"col-sm-6\">\n");
						  str.append("	 <div class=\"form-group\">\n");
						  str.append("    <label for=\""+fieldName.replace("cd", "tx")+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");						 
						  str.append("	    <div class=\"input-group\">\n");							 
						  str.append("     <label class=\"input-group-prepend\" for=\""+fieldName.replace("cd", "tx")+"\">\n");
						  str.append("     <span class=\"input-group-text\">\n");
						  str.append("       <span class=\"fas fa-search\"></span>\n");
						  str.append("     </span>\n");
						  str.append("    </label>\n");
						  str.append("     <input type=\"text\" class=\"form-control\" id=\""+fieldName.replace("cd", "tx")+"\" name=\""+fieldName.replace("cd", "tx")+"\" th:field=\"*{"+fieldName.replace("cd", "tx")+"}\" onchange=\"javascript: limpalookup('"+fieldName.replace("cd", "tx")+"','"+fieldName+"');\" />\n");
						  str.append("     <input type=\"hidden\" class=\"form-control\" id=\""+fieldName+"\" name=\""+fieldName+"\" th:field=\"*{"+fieldName+"}\" /> \n");
						  str.append("  </div>\n");
						  str.append("	 </div>\n");
						  str.append("	</div>\n");
						  
						   //Gera JS Lookup
						  	strJS += gerarJS("lk"+descricaoCampo.toLowerCase(), fieldName.replace("cd", "tx"), fieldName);
					    }
					  str.append("\n");
				  }else {
					  
					if (fieldType.contains("big")) {  
					 str.append("<!-- INPUT TEXT BIGDECIMAL -->\n"); 
	                 str.append("<div class=\"col-sm-2\">\n");
	                 str.append("<div class=\"form-group\">\n");
	                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
	                 str.append("  <input type=\"text\" class=\"form-control textFloat\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
	                 str.append("</div>\n");
	                 str.append("</div>\n");
	                 str.append("\n");
					}else if (fieldType.contains("date")) {
						 str.append("<!-- INPUT TEXT -->\n"); 
		                 str.append("<div class=\"col-sm-2\">\n");
		                 str.append("<div class=\"form-group\">\n");
		                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");		                 
		                 /*
		                 str.append("<div class=\"input-group date form_date col-sm-12\" \n");
		                 str.append("			data-date=\"\" data-date-format=\"dd/mm/yyyy\" \n");
		                 str.append("			data-link-field=\""+campos[i].getName()+"\" \n");
		                 str.append("			data-link-format=\"dd/mm/yyyy\"> \n");
		                 str.append("			<input class=\"form-control\" type=\"text\" \n");
		                 str.append("				id=\""+campos[i].getName()+"ReadOnly\" th:field=\"*{"+campos[i].getName()+"}\" /> \n");
		                 str.append("			<span class=\"input-group-addon\"><span \n");
		                 str.append("				class=\"glyphicon glyphicon-calendar\"></span></span> \n");
		                 str.append("		</div> \n");		                 
		                 */
		                 str.append("  <input type=\"text\" class=\"form-control jtpDate\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\" />\n");
		                 str.append("</div>\n");
		                 str.append("</div>\n");
		                 str.append("\n");						

					}else {	
						 str.append("<!-- INPUT TEXT -->\n"); 
		                 str.append("<div class=\"col-sm-6\">\n");
		                 str.append("<div class=\"form-group\">\n");
		                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
		                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
		                 str.append("</div>\n");
		                 str.append("</div>\n");
		                 str.append("\n");						
					}

				 }
			 }
			 str.append("</div> \n");
			 str.append("<!-- Campos Hidden --> \n");
			 str.append("	<input type=\"hidden\" class=\"form-control\" id=\"NomeCampoRelacionamento\" name=\"NomeCampoRelacionamento\"  th:field=\"*{cdCliente}\"/> \n");
			 str.append("	<input type=\"hidden\" class=\"form-control\" id=\"NomeCampoChave\" name=\"NomeCampoChave\"  th:field=\"*{NomeCampoChave}\"/> \n");
			 
			 str.append("</div> \n");
			 str.append("	<div class=\"modal-footer\" th:with=\"varTxCampo=*{txCampo} == null ? '' : *{txCampo}\"> \n");
			 str.append("		<button type=\"button\" id=\"btnGravarModal\" class=\"btn btn-primary col-sm-2\"  \n");
			 str.append("			th:onclick=\"|javascript: gravarModal('@{/"+txPathForm+"/gravarmodal}', '@{/"+txPathForm+"}/?txCampoFiltro=${varTxCampo}', '#"+txPathForm+"', '#"+tabObjMinusculo+"');|\">  \n");
			 str.append("			<strong>Gravar</strong> \n");
			 str.append("		</button>  \n");
			 str.append("		<button type=\"button\" class=\"btn btn-default\"  \n");
			 str.append("			data-dismiss=\"modal\">  \n");
			 str.append("			<strong>Sair</strong>  \n");
			 str.append("		</button>  \n");
			 str.append("	</div>  \n");
			 str.append("</div> \n");
			 str.append("</div>  \n");
			 str.append("</div>  \n");
			 str.append("</div>  \n");
			 //str.append("</div>  \n");
			 str.append("</form>  \n");
			 
			 str.append("</div> \n");
			 str.append("</section> \n");
			 str.append("</div> \n");
			 str.append("</div> \n");


			 str.append("<!-- Script JS --> \n");
			 str.append("<script th:inline=\"javascript\"> \n");	
			 str.append(" /*<![CDATA[*/	\n");	   
			 str.append("  $(document).ready(function() {	\n");			 
			 str.append("    var cdTipo = /*[[${tipoMensagem}]]*/ \n jtpNotifySimple(/*[[${txMensagem}]]*/,cdTipo);\n");	
			 str.append("	jtpNotify(/*[[${listaErros}]]*/);\n");			 
			 str.append("    var cdchave = /*[[${CampoChave}]]*/; \n");
			 str.append("    document.getElementById('CampoChave').value = cdchave; \n");
			 
			 str.append(" var txEdit = /*[[${txEdit}]]*/; \n");
			 str.append(" if (txEdit != null) { \n");
			 str.append("	 $('#"+txPathForm+"').modal('show'); \n");	
			 str.append(" } \n");
			 
			 str.append("//Permissões \n");
			 str.append("	var listPermissaoBotao = /*[[${listPermissaoBotao}]]*/; \n");
			 str.append("	checkPermissaoBotao(listPermissaoBotao);\n\n");
				
			 str.append("	var listPermissaoCampos = /*[[${listPermissaoCampos}]]*/; \n");
			 str.append("	checkPermissaoCampos(listPermissaoCampos); \n\n");

			 str.append("	//Service \n");
			 str.append("	var txService = /*[[${txService}]]*/; \n");
			 str.append("	analistaVirtual(txService); \n");			 
			 str.append("	var pathUrl = /*[[@{/}]]*/; \n\n");

			 str.append(strJS);
			 
			 str.append(" });\n");	
			 str.append(" /*]]>*/ \n");
			 str.append("</script> \n"); 

			 str.append("</section>\n\n");
			 
			 str.append("</html>\n");
			 
	         File f = new File("/temp/obj/html");
		        if (!f.exists()) {
		        	f.mkdirs();
		        }
	         
	          FileWriter arq = new FileWriter("/temp/obj/html/"+txPathForm+".html");
		      PrintWriter gravarArq = new PrintWriter(arq);
		      gravarArq.printf(str.toString());
		      gravarArq.close();
		      
		      System.out.println("HTML "+txObj+".html criado com sucesso!");
		      
		}catch (Exception ex) {
			ex.printStackTrace();
		}		
	}
	

	private void GeradorHTMLPesquisaRelatorio(String txObj, String txPathForm) {	 
		try {
			
			String pathObj = new GeneralUtil().LocalizaArquivo(Constants.NOME_SISTEMA_LOCAL,"java",Constants.PATH_ARQS_JAVA_LOCAL,txObj+".java");
			
			Object tabObj = null;
			
			tabObj = Class.forName(pathObj).newInstance();
			
			Field[] campos = tabObj.getClass().getDeclaredFields();
			
			StringBuilder str = new StringBuilder();
			
			GeradorClasseBean g = new GeradorClasseBean();
			String tabObjMinusculo = g.arrumaCampoMinusculaObj(txObj);
			
			str.append("<!DOCTYPE html>\n");
			str.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" \n"); 
			str.append("  xmlns:th=\"http://www.thymeleaf.org\"\n");
			str.append("  xmlns:layout=\"http://www.ultraq.net.nz/thymeleaf/layout\"\n"); 
			str.append("  layout:decorator=\"/layout/LayoutPadrao\">\n\n");
			 
			str.append(" <section layout:fragment=\"conteudo\" >\n");
			str.append("\n\n");
			
            str.append("<div class=\"page-inner\">\n");              
            str.append("<div class=\"page-section\">\n");
            str.append("<section id=\"base-style\" class=\"card\">\n");
            str.append("    <div class=\"card-body\">\n");
            str.append("      <form id=\""+tabObjMinusculo+"\" method=\"post\" th:action=\"@{/"+txPathForm+"/pesquisar}\" th:object=\"${"+tabObjMinusculo+"}\" >\n");
            str.append("        <fieldset>\n");
            str.append("<div class=\"row\"> \n");
            str.append(" <div class=\"col-sm-10\"> \n");
            str.append("  <header class=\"page-title-bar\"> \n");
            str.append("	<h5 class=\"page-title\" th:text=\"${pathtela}\">Nome da Tela</h5> \n");
            str.append("  </header> \n");
            str.append("  </div> \n");
            str.append("</div> \n");
            
            str.append("<nav class=\"navbar bg-primary main-menu-jtp\">\n");

            str.append("<fieldset class=\"col-sm-12 jtp-fieldset\">\n");
            str.append("	<legend class=\"jtp-legend\">Filtros</legend>\n");

            str.append("	<div class=\"panel panel-default\">\n");
            str.append("		<div class=\"panel-body\">\n");
            str.append("			<div class=\"row\">\n");
            
            String tx_classe = tabObj.getClass().getSimpleName().replace("Obj", "Service");
 
            String strJS = "\n\n";
    
				 for (int i = 0; i < campos.length; i++) {
					 
					 campos[i].setAccessible(true);
					 
					 String fieldName = campos[i].getName();
					 
					 String fieldType = campos[i].getType().toString().toLowerCase();
					 
					 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
					 
					  if (fieldType.contains("integer") || fieldType.contains("int") || fieldName.contains("cd")) {
						  str.append("<!-- INPUT TEXT -->\n"); 
			                 str.append("<div class=\"col-sm-6\">\n");
			                 str.append("<div class=\"form-group\">\n");
			                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
			                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
			                 str.append("</div>\n");
			                 str.append("</div>\n");
			                 str.append("\n");
						  
						  str.append("<!-- SELECT -->\n");
						  str.append("<div class=\"col-sm-6\">\n");
						  str.append(" <div class=\"form-group\">\n");
						  str.append("	<label for=\""+fieldName+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
						  str.append("	 <select class=\"custom-select\" id=\""+fieldName+"\" th:field=\"*{"+fieldName+"}\">\n");
						  str.append("    <option value=\"\">...</option>\n");
						  str.append("    <option th:each=\"s"+fieldName.toLowerCase()+" : ${select"+fieldName.toLowerCase()+"}\" th:value=\"${s"+fieldName.toLowerCase()+"."+fieldName+"}\" th:text=\"${s"+fieldName.toLowerCase()+"."+fieldName.replace("cd", "tx")+"}\"></option>\n");
						  str.append("  </select>\n");
						  str.append(" </div>\n");
						  str.append("</div>\n");
						  
							if (!fieldName.contains("ck")) {
							  str.append("<!-- LOOKUP -->\n");
							  str.append("<div class=\"col-sm-6\">\n");
							  str.append("	 <div class=\"form-group\">\n");
							  str.append("    <label for=\""+fieldName.replace("cd", "tx")+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");						 
							  str.append("	    <div class=\"input-group\">\n");							 
							  str.append("     <label class=\"input-group-prepend\" for=\""+fieldName.replace("cd", "tx")+"\">\n");
							  str.append("     <span class=\"input-group-text\">\n");
							  str.append("       <span class=\"fas fa-search\"></span>\n");
							  str.append("     </span>\n");
							  str.append("    </label>\n");
							  str.append("     <input type=\"text\" class=\"form-control\" id=\""+fieldName.replace("cd", "tx")+"\" name=\""+fieldName.replace("cd", "tx")+"\" th:field=\"*{"+fieldName.replace("cd", "tx")+"}\" onchange=\"javascript: limpalookup('"+fieldName.replace("cd", "tx")+"','"+fieldName+"');\" />\n");
							  str.append("     <input type=\"hidden\" class=\"form-control\" id=\""+fieldName+"\" name=\""+fieldName+"\" th:field=\"*{"+fieldName+"}\" /> \n");
							  str.append("  </div>\n");
							  str.append("	 </div>\n");
							  str.append("	</div>\n");
							  
							   //Gera JS Lookup
							  	strJS += gerarJS("lk"+descricaoCampo.toLowerCase(), fieldName.replace("cd", "tx"), fieldName);
						    }
						  str.append("\n");
					  }else {
						  
						if (fieldType.contains("big")) {  
						 str.append("<!-- INPUT TEXT BIGDECIMAL -->\n");
			             str.append("<div class=\"col-sm-6\">\n");
			             str.append("<div class=\"form-group\">\n");
			             str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
			             str.append("  <input type=\"text\" class=\"form-control textFloat\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
			             str.append("</div>\n");
			             str.append("</div>\n");
			             str.append("\n");
						}else if (fieldType.contains("date")) {
							 str.append("<!-- INPUT TEXT -->\n"); 
			                 str.append("<div class=\"col-sm-6\">\n");
			                 str.append("<div class=\"form-group\">\n");
			                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
			                 str.append("  <input type=\"text\" class=\"form-control jtpDate\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\" />\n");
			                 str.append("</div>\n");
			                 str.append("</div>\n");
			                 str.append("\n");						

						}else {	
							 str.append("<!-- INPUT TEXT -->\n"); 
			                 str.append("<div class=\"col-sm-6\">\n");
			                 str.append("<div class=\"form-group\">\n");
			                 str.append("  <label for=\""+campos[i].getName()+"\"><a href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
			                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
			                 str.append("</div>\n");
			                 str.append("</div>\n");
			                 str.append("\n");						
						}

					 }
				 }            
				 
			str.append("	<!-- SELECT --> \n");
			str.append("	<div class=\"col-sm-6\"> \n");
			str.append("		<div class=\"form-group\"> \n");
			str.append("			<label for=\"cdRelatorio\"><strong>Escolha um Relatorio</strong></label> \n");
			str.append("			<select class=\"custom-select\" id=\"cdRelatorio\" \n");
			str.append("				th:field=\"*{cdRelatorio}\"> \n");
			str.append("				<option value=\"\">...</option> \n");
			str.append("				<option th:each=\"scdrelatorio : ${selectcdrelatorio}\" \n");
			str.append("					th:value=\"${scdrelatorio.cdRelatorio}\" \n");
			str.append("					th:text=\"${scdrelatorio.txTitulo}\"></option> \n");
			str.append("			</select> \n");
			str.append("		</div> \n");
			str.append("	</div> \n");
            str.append("</div>\n");
            
            str.append("<div class=\"row\"> \n");
            str.append("<div class=\"col-sm-4\"> \n");
            str.append("	<button type=\"button\" class=\"btn btn-primary col-sm-12\" th:onclick=\"|javascript: gerarRelatorio('@{/"+txPathForm+"/filtrarrelatorio}', '@{/relmodelo1}');|\"> \n");
            str.append("		<strong>Pesquisar</strong> \n");
            str.append("	</button> \n");
            str.append("	</div> \n");
            str.append("	<div class=\"col-sm-4\"> \n");
            str.append("	<button type=\"button\" class=\"btn btn-primary col-sm-12\" th:onclick=\"|javascript: openWindowSimple('@{/cadastrorelatorio}/novo/"+tx_classe+"','_blank');|\"> \n");
            str.append("		<strong>Monte seu relatório</strong> \n");
            str.append("	</button> \n");
            str.append("</div> \n");
            str.append("<div class=\"col-sm-4\"> \n");
            str.append("	<button type=\"button\" class=\"btn btn-primary col-sm-12\" th:onclick=\"|javascript: gerarRelatorio('@{/"+txPathForm+"/filtrarrelatorio}', '@{/relmodelo1}');|\"> \n");
            str.append("		<strong>Agendamento</strong> \n");
            str.append("	</button> \n");
            str.append("</div> \n");
            str.append("</div> \n");

            
            str.append("		</div>\n");
            str.append("	</div>\n");
            str.append("</fieldset>\n");
            str.append("</nav>\n");
			 str.append("</fieldset> \n");
			 str.append("</form> \n");
			 str.append("</div> \n");
			 str.append("</section> \n");
			 str.append("</div> \n");
			 str.append("</div> \n");


			 str.append("<!-- Script JS --> \n");
			 str.append("<script th:inline=\"javascript\"> \n");	
			 str.append(" /*<![CDATA[*/	");	   
			 str.append("  $(document).ready(function() {	\n");			 
			 str.append(" var cdTipo = /*[[${tipoMensagem}]]*/ \n jtpNotifySimple(/*[[${txMensagem}]]*/,cdTipo);\n");	
			 str.append("	jtpNotify(/*[[${listaErros}]]*/);\n");			 
			 str.append("    var cdchave = /*[[${CampoChave}]]*/; \n");
			 str.append("    document.getElementById('CampoChave').value = cdchave; \n");
			 
			 str.append("//Permissões \n");
			 str.append("	var listPermissaoBotao = /*[[${listPermissaoBotao}]]*/; \n");
			 str.append("	checkPermissaoBotao(listPermissaoBotao);\n\n");
				
			 str.append("	var listPermissaoCampos = /*[[${listPermissaoCampos}]]*/; \n");
			 str.append("	checkPermissaoCampos(listPermissaoCampos); \n\n");

			 str.append("	//Service \n");
			 str.append("	var txService = /*[[${txService}]]*/; \n");
			 str.append("	analistaVirtual(txService); \n");
			 str.append("	var pathUrl = /*[[@{/}]]*/; \n\n");

			 str.append(strJS);

			 
			 str.append(" });\n");	
			 str.append(" /*]]>*/ \n");
			 str.append("</script> \n"); 
			 
			 str.append("</section>\n\n");
			 
			 str.append("</html>\n");
			 
	         File f = new File("/temp/obj/html");
		        if (!f.exists()) {
		        	f.mkdirs();
		        }
	         
	          FileWriter arq = new FileWriter("/temp/obj/html/"+txPathForm+".html");
		      PrintWriter gravarArq = new PrintWriter(arq);
		      gravarArq.printf(str.toString());
		      gravarArq.close();
		      
		      System.out.println("HTML "+txObj+".html criado com sucesso!");
		      
		}catch (Exception ex) {
			ex.printStackTrace();
		}		
	}
	
	
	
	private String gerarJS(String txNomeLookup, String txFieldDescricao, String txField) {
		
		StringBuffer str = new StringBuffer();
		
		str.append("// Lookup "+txNomeLookup+"  \n");
		str.append("var "+txNomeLookup+" = { \n");
		str.append("		 url: function(value) { \n");
		str.append("				return pathUrl+'lookups/"+txNomeLookup+"?"+txFieldDescricao+"=' + value; \n");
		str.append("			}, \n");
		str.append("			minCharNumber: 3, \n");
		str.append("			requestDelay: 300, \n");
		str.append("			ajaxSettings: { \n");
		str.append("				contentType: 'application/json' \n");
		str.append("			},	\n");
		str.append("		    getValue: function(element) { \n");
		str.append("		        return element."+txFieldDescricao+"; \n");
		str.append("		    }, \n");
		str.append("		    list: { \n");
		str.append("		        onSelectItemEvent: function() { \n");
		str.append("		            var selectedItemValue = $(\"#"+txFieldDescricao+"\").getSelectedItemData()."+txField+"; \n");	
		str.append("		            $(\"#"+txField+"\").val(selectedItemValue).trigger(\"change\"); \n");
		str.append("		        } \n");
		str.append("		    }, \n");
		str.append("		    theme: \"plate-dark\" \n");
		str.append("		}; \n");
		str.append("		$(\"#"+txFieldDescricao+"\").easyAutocomplete("+txNomeLookup+"); \n\n");
		
		return str.toString();
	}

	private void GeradorHTMLPesquisa(String txObj, String txPathForm) {	 
		try {
			
			String pathObj = new GeneralUtil().LocalizaArquivo(Constants.NOME_SISTEMA_LOCAL,"java",Constants.PATH_ARQS_JAVA_LOCAL,txObj+".java");
			
			Object tabObj = null;
			
			tabObj = Class.forName(pathObj).newInstance();
			
			Field[] campos = tabObj.getClass().getDeclaredFields();
			
			StringBuilder str = new StringBuilder();
			
			GeradorClasseBean g = new GeradorClasseBean();
			String tabObjMinusculo = g.arrumaCampoMinusculaObj(txObj);	
			
			str.append("<!DOCTYPE html>\n");
			str.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" \n"); 
			str.append("  xmlns:th=\"http://www.thymeleaf.org\"\n");
			str.append("  xmlns:layout=\"http://www.ultraq.net.nz/thymeleaf/layout\"\n"); 
			str.append("  layout:decorator=\"layoutdefault/layoutdefault\">\n\n");
			
			str.append("<section layout:fragment=\"pagecontent\"> \n");
		    

			str.append("<div class=\"content\" style=\"min-height: 530px\">	 \n");
			   
			str.append("   <div class=\"row\">  \n");
			str.append("      <div class=\"col-md-12\" style=\"background-color: white;\">  \n");

			str.append("	<nav class=\"navbar blue\">  \n");
			str.append("<fieldset class=\"col-sm-12 jtp-fieldset\">  \n");
			str.append("<legend class=\"blue\">Filtros</legend>  \n");
			str.append("<div class=\"panel panel-default\">  \n");
			str.append("	<div class=\"panel-body\">  \n");
			str.append("      <form id=\""+tabObjMinusculo.replace("tab", "tabFiltro")+"\" method=\"get\" th:action=\"@{/"+txPathForm+"\" th:object=\"${"+tabObjMinusculo.replace("tab", "tabFiltro")+"}\" >\n");
			str.append("		<div class=\"row\"> \n"); 

		            String tx_classe = tabObj.getClass().getSimpleName().replace("Obj", "Service");
			 
			            String strJS = "\n\n";
			    
							 for (int i = 0; i < campos.length; i++) {
								 
								 campos[i].setAccessible(true);
								 
								 String fieldName = campos[i].getName();
								 
								 String fieldType = campos[i].getType().toString().toLowerCase();
								 
								 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
								 
								  if (fieldType.contains("integer") || fieldType.contains("int") || fieldName.contains("cd")) {
									  str.append("<!-- INPUT TEXT -->\n"); 
						                 str.append("<div class=\"col-sm-6\">\n");
						                 str.append("<div class=\"form-group\">\n");
						                 str.append("  <label for=\""+campos[i].getName()+"Filtro\" class=\"blue\"><strong>"+descricaoCampo+"</strong></label>\n");
						                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"Filtro\" name=\""+campos[i].getName()+"Filtro\"  th:field=\"*{"+fieldName+"Filtro}\"/>\n");
						                 str.append("</div>\n");
						                 str.append("</div>\n");
						                 str.append("\n");
									  
									  str.append("<!-- SELECT -->\n");
									  str.append("<div class=\"col-sm-6\">\n");
									  str.append(" <div class=\"form-group\">\n");
									  str.append("	<label for=\""+fieldName+"Filtro\" class=\"blue\"><strong>"+descricaoCampo+"</strong></label>\n");
									  str.append("	 <select class=\"custom-select blue\" id=\""+fieldName+"Filtro\" th:field=\"*{"+fieldName+"Filtro}\">\n");
									  str.append("    <option value=\"\">...</option>\n");
									  str.append("    <option th:each=\"s"+fieldName.toLowerCase()+" : ${select"+fieldName.toLowerCase()+"}\" th:value=\"${s"+fieldName.toLowerCase()+"."+fieldName+"}\" th:text=\"${s"+fieldName.toLowerCase()+"."+fieldName.replace("cd", "tx")+"}\"></option>\n");
									  str.append("  </select>\n");
									  str.append(" </div>\n");
									  str.append("</div>\n");
									  
										if (!fieldName.contains("ck")) {
										  str.append("<!-- LOOKUP -->\n");
										  str.append("<div class=\"col-sm-6\">\n");
										  str.append("	 <div class=\"form-group\">\n");
										  str.append("    <label for=\""+fieldName.replace("cd", "tx")+"Filtro\" class=\"blue\"><strong>"+descricaoCampo+"</strong></label>\n");						 
										  str.append("	    <div class=\"input-group\">\n");							 
										  str.append("     <label class=\"input-group-prepend\" for=\""+fieldName.replace("cd", "tx")+"Filtro\" class=\"blue\">\n");
										  str.append("     <span class=\"input-group-text\">\n");
										  str.append("       <span class=\"fas fa-search\"></span>\n");
										  str.append("     </span>\n");
										  str.append("    </label>\n");
										  str.append("     <input type=\"text\" class=\"form-control\" id=\""+fieldName.replace("cd", "tx")+"Filtro\" name=\""+fieldName.replace("cd", "tx")+"Filtro\" th:field=\"*{"+fieldName.replace("cd", "tx")+"Filtro}\" onchange=\"javascript: limpalookup('"+fieldName.replace("cd", "tx")+"Filtro','"+fieldName+"Filtro');\" />\n");
										  str.append("     <input type=\"hidden\" class=\"form-control\" id=\""+fieldName+"Filtro\" name=\""+fieldName+"Filtro\" th:field=\"*{"+fieldName+"Filtro}\" /> \n");
										  str.append("  </div>\n");
										  str.append("	 </div>\n");
										  str.append("	</div>\n");
										  
										   //Gera JS Lookup
										  	strJS += gerarJS("lk"+descricaoCampo.toLowerCase(), fieldName.replace("cd", "tx"), fieldName);
									    }
									  str.append("\n");
								  }else {
									  
									if (fieldType.contains("big")) {  
									 str.append("<!-- INPUT TEXT BIGDECIMAL -->\n");
						             str.append("<div class=\"col-sm-6\">\n");
						             str.append("<div class=\"form-group\">\n");
						             str.append("  <label for=\""+campos[i].getName()+"Filtro\" class=\"blue\"><strong>"+descricaoCampo+"</strong></label>\n");
						             str.append("  <input type=\"text\" class=\"form-control textFloat\" id=\""+campos[i].getName()+"Filtro\" name=\""+campos[i].getName()+"Filtro\"  th:field=\"*{"+fieldName+"Filtro}\"/>\n");
						             str.append("</div>\n");
						             str.append("</div>\n");
						             str.append("\n");
									}else if (fieldType.contains("date")) {
										 str.append("<!-- INPUT TEXT -->\n"); 
						                 str.append("<div class=\"col-sm-6\">\n");
						                 str.append("<div class=\"form-group\">\n");
						                 str.append("  <label for=\""+campos[i].getName()+"Filtro\" class=\"blue\"><strong>"+descricaoCampo+"</strong></label>\n");
						                 str.append("  <input type=\"text\" class=\"form-control jtpDate\" id=\""+campos[i].getName()+"Filtro\" name=\""+campos[i].getName()+"Filtro\"  th:field=\"*{"+fieldName+"Filtro}\" />\n");
						                 str.append("</div>\n");
						                 str.append("</div>\n");
						                 str.append("\n");						

									}else {	
										 str.append("<!-- INPUT TEXT -->\n"); 
						                 str.append("<div class=\"col-sm-6\">\n");
						                 str.append("<div class=\"form-group\">\n");
						                 str.append("  <label for=\""+campos[i].getName()+"Filtro\" class=\"blue\"><strong>"+descricaoCampo+"</strong></label>\n");
						                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"Filtro\" name=\""+campos[i].getName()+"Filtro\"  th:field=\"*{"+fieldName+"Filtro}\"/>\n");
						                 str.append("</div>\n");
						                 str.append("</div>\n");
						                 str.append("\n");						
									}

								 }
							 }     

				 str.append("<button type=\"submit\" class=\"btn btn-success float-right\" style=\"margin: 25px 0px 0px; max-height: 37px;\"> \n");
				 str.append("	<strong>Pesquisar</strong> \n");
				 str.append("</button> \n");

				 str.append("</div> \n");
				 str.append("</form> \n");
				 str.append("</div> \n");
				 str.append("</div> \n");
				 str.append("</fieldset> \n");
				 str.append("</nav> \n");

				 str.append(" </div> \n");
				 str.append("</div> \n");
		  
				 str.append("<div class=\"row\"> \n");
				 str.append(" <div class=\"col-md-12\"> \n");
				 str.append("   <div class=\"card\"> \n");
				 
				 str.append("<div class=\"row\"> \n");
				 str.append("<div class=\"card-header\"> \n");
				 str.append("   <h4 class=\"card-title blue bold\" style=\"padding-left: 10px\">Titulo GRID</h4>  \n");
				 str.append("</div>  \n");				 
				str.append("</div>  \n");
				
				 str.append("     <div class=\"card-body\"> \n");
				 str.append("       <div class=\"toolbar\"> \n");
		                               
				 str.append("      </div> \n");
				 str.append("       <table id=\"datatable\" class=\"table table-striped table-bordered\" cellspacing=\"0\" width=\"100colocarporcent\">  \n");
				 str.append("        <thead>  \n");
				 
				 str.append("          <tr> \n");
				 
				 for (int i = 0; i < campos.length; i++) {
					 
					 campos[i].setAccessible(true);
					 
					 String fieldName = campos[i].getName();
					 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
					 
					 str.append("                 <th class=\"bold disabled-sorting\"> "+descricaoCampo+" </th> \n");
	            
	            }
				 str.append(" </tr> \n");
				 str.append("  </thead> \n");                  
				 str.append("  <tbody> \n");
				            String txLista = "";
							 for (int i = 0; i < campos.length; i++) {
								 
								 campos[i].setAccessible(true);
								 
								 String fieldName = campos[i].getName();
								 
								 String fieldType = campos[i].getType().toString().toLowerCase();
								 
								 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
								 if (i == 0) {
								  //str.append("               <tr th:each=\"list"+descricaoCampo.toLowerCase()+" : ${listadados.conteudo}\"> \n");
								  str.append("				 <tr th:each=\"listdados : ${listadados}\" th:onclick=\"|javascript: abrirtela('@{/"+txPathForm+"/consultar}/${listdados."+fieldName+"}');|\"> \n");
								  txLista = descricaoCampo;
								 }
								 if (fieldType.contains("big") || fieldType.contains("date")) {
								   str.append("                 <td class=\"blue\" th:text=\"${{listdados."+fieldName+"}}\"> "+fieldName+" </td> \n");			 
								 }else {
								   str.append("                 <td class=\"blue\" th:text=\"${listdados."+fieldName+"}\"> "+fieldName+" </td> \n");			 
								 }
							 }
				            
				            str.append("               </tr> \n");
				            str.append("             </tbody> \n");		                    
				            str.append("</table>\n");
				            str.append("</div> \n");
				            str.append("</div> \n");
				            str.append("</div> \n");
				            str.append("</div> \n");
				            str.append("</div> \n");
				            
					  

				str.append("<script> \n");
				str.append("$(document).ready(function() { \n");

				str.append(" var printCounter = 0;  \n");
			     
				str.append("  $('#datatable').DataTable({  \n");
				str.append("    \"pagingType\": \"full_numbers\", \n");
				str.append("    \"ordering\": false, \n");
				str.append("    \"scrollX\": true, \n");
				str.append("    \"lengthMenu\": [ \n");
				str.append("      [100, 200, 300, -1], \n");
				str.append("      [100, 200, 300, \"All\"] \n");
				str.append("    ], \n");
				str.append("    dom: 'Bfrtip', \n");
				str.append("    buttons:  [ \n");
				str.append("        'copy', \n");
				str.append("        { \n");
				str.append("            extend: 'excel', \n");
				str.append("            messageTop: 'Titulo Excel.' \n");
				str.append("        }, \n");
				str.append("        { \n");
				str.append("            extend: 'pdf', \n");
				str.append("            messageBottom: null \n");
				str.append("        }, \n");
				str.append("        { \n");
				str.append("            extend: 'print', \n");
				str.append("            messageTop: function () { \n");
				str.append("                printCounter++;	\n"); 
				str.append("         if ( printCounter === 1 ) { \n");
				str.append("                    return 'Esta é a primeira vez que você imprime este documento.'; \n");
				str.append("                } \n");
				str.append("                else { \n");
				str.append("                    return 'Você imprimiu este documento '+printCounter+' vezes'; \n");
				str.append("			          } \n");
				str.append("            }, \n");
				str.append("            messageBottom: null \n");
				str.append("        } \n");
				str.append("			        ], \n");
				str.append("    responsive: true, \n");
				str.append("			        language: {  \n");
				str.append("      search: \"_INPUT_\", \n");
				str.append("      searchPlaceholder: \"Localizar\", \n");
				str.append("    } \n");
				str.append("}); \n");
				str.append(" var table = $('#datatable').DataTable(); \n");
				str.append("}); \n");
				str.append("</script> \n");
			
				 str.append("<!-- Script JS --> \n");
				 str.append("<script th:inline=\"javascript\"> \n");	
				 str.append(" /*<![CDATA[*/	\n");	   
				 str.append("  $(document).ready(function() {	\n");			 
				 str.append("    var cdTipo = /*[[${tipoMensagem}]]*/ \n jtpNotifySimple(/*[[${txMensagem}]]*/,cdTipo);\n");	
				 str.append("	jtpNotify(/*[[${listaErros}]]*/);\n");			 
				 str.append("    var cdchave = /*[[${CampoChave}]]*/; \n");
				 str.append("    document.getElementById('CampoChave').value = cdchave; \n");
				 
				 str.append(" var txEdit = /*[[${txEdit}]]*/; \n");
				 str.append(" if (txEdit != null) { \n");
				 str.append("	 $('#"+txPathForm+"').modal('show'); \n");	
				 str.append(" } \n");
				 
				 str.append("//Permissões \n");
				 str.append("	var listPermissaoBotao = /*[[${listPermissaoBotao}]]*/; \n");
				 str.append("	checkPermissaoBotao(listPermissaoBotao);\n\n");
					
				 str.append("	var listPermissaoCampos = /*[[${listPermissaoCampos}]]*/; \n");
				 str.append("	checkPermissaoCampos(listPermissaoCampos); \n\n");

				 str.append("	//Service \n");
				 str.append("	var txService = /*[[${txService}]]*/; \n");
				 str.append("	analistaVirtual(txService); \n");			 
				 str.append("	var pathUrl = /*[[@{/}]]*/; \n\n");

				 str.append(strJS);
				 
				 str.append(" });\n");	
				 str.append(" /*]]>*/ \n");
				 str.append("</script> \n"); 
			
			
				str.append("</section>\n");

				str.append("</html>\n");
		
		
	         File f = new File("/temp/obj/html");
		        if (!f.exists()) {
		        	f.mkdirs();
		        }
	         
	          FileWriter arq = new FileWriter("/temp/obj/html/"+txPathForm+".html");
		      PrintWriter gravarArq = new PrintWriter(arq);
		      gravarArq.printf(str.toString());
		      gravarArq.close();
		      
		      System.out.println("HTML "+txObj+".html criado com sucesso!");
		      
		}catch (Exception ex) {
			ex.printStackTrace();
		}		
	}



	private void GeradorHTMLPesquisaGravacao(String txObj, String txPathForm) {	 
		try {
			
			String pathObj = new GeneralUtil().LocalizaArquivo(Constants.NOME_SISTEMA_LOCAL,"java",Constants.PATH_ARQS_JAVA_LOCAL,txObj+".java");
			
			Object tabObj = null;
			
			tabObj = Class.forName(pathObj).newInstance();
			
			Field[] campos = tabObj.getClass().getDeclaredFields();
			
			StringBuilder str = new StringBuilder();
			
			GeradorClasseBean g = new GeradorClasseBean();
			String tabObjMinusculo = g.arrumaCampoMinusculaObj(txObj);	
			
			str.append("<!DOCTYPE html>\n");
			str.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" \n"); 
			str.append("  xmlns:th=\"http://www.thymeleaf.org\"\n");
			str.append("  xmlns:layout=\"http://www.ultraq.net.nz/thymeleaf/layout\"\n"); 
			str.append("  layout:decorator=\"layoutdefault/layoutdefault\">\n\n");
			
			str.append("<section layout:fragment=\"pagecontent\"> \n");
		    

			str.append("<div class=\"content\" style=\"min-height: 530px\">	 \n");
			   
			str.append("   <div class=\"row\">  \n");
			str.append("      <div class=\"col-md-12\" style=\"background-color: white;\">  \n");

			str.append("	<nav class=\"navbar blue\">  \n");
			str.append("<fieldset class=\"col-sm-12 jtp-fieldset\">  \n");
			str.append("<legend class=\"blue\">Filtros</legend>  \n");
			str.append("<div class=\"panel panel-default\">  \n");
			str.append("	<div class=\"panel-body\">  \n");
			str.append("      <form id=\""+tabObjMinusculo.replace("tab", "tabFiltro")+"\" method=\"get\" th:action=\"@{/"+txPathForm+"}\" th:object=\"${"+tabObjMinusculo.replace("tab", "tabFiltro")+"}\" >\n");
			str.append("		<div class=\"row\"> \n"); 

		            String tx_classe = tabObj.getClass().getSimpleName().replace("Obj", "Service");
			 
			            String strJS = "\n\n";
			    
							 for (int i = 0; i < campos.length; i++) {
								 
								 campos[i].setAccessible(true);
								 
								 String fieldName = campos[i].getName();
								 
								 String fieldType = campos[i].getType().toString().toLowerCase();
								 
								 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
								 
								  if (fieldType.contains("integer") || fieldType.contains("int") || fieldName.contains("cd")) {
									  str.append("<!-- INPUT TEXT -->\n"); 
						                 str.append("<div class=\"col-sm-6\">\n");
						                 str.append("<div class=\"form-group\">\n");
						                 str.append("  <label for=\""+campos[i].getName()+"Filtro\" class=\"blue\"><strong>"+descricaoCampo+"</strong></label>\n");
						                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"Filtro\" name=\""+campos[i].getName()+"Filtro\"  th:field=\"*{"+fieldName+"Filtro}\"/>\n");
						                 str.append("</div>\n");
						                 str.append("</div>\n");
						                 str.append("\n");
									  
									  str.append("<!-- SELECT -->\n");
									  str.append("<div class=\"col-sm-6\">\n");
									  str.append(" <div class=\"form-group\">\n");
									  str.append("	<label for=\""+fieldName+"Filtro\" class=\"blue\"><strong>"+descricaoCampo+"</strong></label>\n");
									  str.append("	 <select class=\"custom-select blue\" id=\""+fieldName+"Filtro\" th:field=\"*{"+fieldName+"Filtro}\">\n");
									  str.append("    <option value=\"\">...</option>\n");
									  str.append("    <option th:each=\"s"+fieldName.toLowerCase()+" : ${select"+fieldName.toLowerCase()+"}\" th:value=\"${s"+fieldName.toLowerCase()+"."+fieldName+"}\" th:text=\"${s"+fieldName.toLowerCase()+"."+fieldName.replace("cd", "tx")+"}\"></option>\n");
									  str.append("  </select>\n");
									  str.append(" </div>\n");
									  str.append("</div>\n");
									  
										if (!fieldName.contains("ck")) {
										  str.append("<!-- LOOKUP -->\n");
										  str.append("<div class=\"col-sm-6\">\n");
										  str.append("	 <div class=\"form-group\">\n");
										  str.append("    <label for=\""+fieldName.replace("cd", "tx")+"Filtro\" class=\"blue\"><strong>"+descricaoCampo+"</strong></label>\n");						 
										  str.append("	    <div class=\"input-group\">\n");							 
										  str.append("     <label class=\"input-group-prepend\" for=\""+fieldName.replace("cd", "tx")+"Filtro\" class=\"blue\">\n");
										  str.append("     <span class=\"input-group-text\">\n");
										  str.append("       <span class=\"fas fa-search\"></span>\n");
										  str.append("     </span>\n");
										  str.append("    </label>\n");
										  str.append("     <input type=\"text\" class=\"form-control\" id=\""+fieldName.replace("cd", "tx")+"Filtro\" name=\""+fieldName.replace("cd", "tx")+"Filtro\" th:field=\"*{"+fieldName.replace("cd", "tx")+"Filtro}\" onchange=\"javascript: limpalookup('"+fieldName.replace("cd", "tx")+"Filtro','"+fieldName+"Filtro');\" />\n");
										  str.append("     <input type=\"hidden\" class=\"form-control\" id=\""+fieldName+"Filtro\" name=\""+fieldName+"Filtro\" th:field=\"*{"+fieldName+"Filtro}\" /> \n");
										  str.append("  </div>\n");
										  str.append("	 </div>\n");
										  str.append("	</div>\n");
										  
										   //Gera JS Lookup
										  	strJS += gerarJS("lk"+descricaoCampo.toLowerCase(), fieldName.replace("cd", "tx"), fieldName);
									    }
									  str.append("\n");
								  }else {
									  
									if (fieldType.contains("big")) {  
									 str.append("<!-- INPUT TEXT BIGDECIMAL -->\n");
						             str.append("<div class=\"col-sm-6\">\n");
						             str.append("<div class=\"form-group\">\n");
						             str.append("  <label for=\""+campos[i].getName()+"Filtro\" class=\"blue\"><strong>"+descricaoCampo+"</strong></label>\n");
						             str.append("  <input type=\"text\" class=\"form-control textFloat\" id=\""+campos[i].getName()+"Filtro\" name=\""+campos[i].getName()+"Filtro\"  th:field=\"*{"+fieldName+"Filtro}\"/>\n");
						             str.append("</div>\n");
						             str.append("</div>\n");
						             str.append("\n");
									}else if (fieldType.contains("date")) {
										 str.append("<!-- INPUT TEXT -->\n"); 
						                 str.append("<div class=\"col-sm-6\">\n");
						                 str.append("<div class=\"form-group\">\n");
						                 str.append("  <label for=\""+campos[i].getName()+"Filtro\" class=\"blue\"><strong>"+descricaoCampo+"</strong></label>\n");
						                 str.append("  <input type=\"text\" class=\"form-control jtpDate\" id=\""+campos[i].getName()+"Filtro\" name=\""+campos[i].getName()+"Filtro\"  th:field=\"*{"+fieldName+"Filtro}\" />\n");
						                 str.append("</div>\n");
						                 str.append("</div>\n");
						                 str.append("\n");						

									}else {	
										 str.append("<!-- INPUT TEXT -->\n"); 
						                 str.append("<div class=\"col-sm-6\">\n");
						                 str.append("<div class=\"form-group\">\n");
						                 str.append("  <label for=\""+campos[i].getName()+"Filtro\" class=\"blue\"><strong>"+descricaoCampo+"</strong></label>\n");
						                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"Filtro\" name=\""+campos[i].getName()+"Filtro\"  th:field=\"*{"+fieldName+"Filtro}\"/>\n");
						                 str.append("</div>\n");
						                 str.append("</div>\n");
						                 str.append("\n");						
									}

								 }
							 }     

				 str.append("<button type=\"submit\" class=\"btn btn-success float-right\" style=\"margin: 25px 0px 0px; max-height: 37px;\"> \n");
				 str.append("	<strong>Pesquisar</strong> \n");
				 str.append("</button> \n");

				 str.append("</div> \n");
				 str.append("</form> \n");
				 str.append("</div> \n");
				 str.append("</div> \n");
				 str.append("</fieldset> \n");
				 str.append("</nav> \n");

				 str.append(" </div> \n");
				 str.append("</div> \n");
		  
				 str.append("<div class=\"row\"> \n");
				 str.append(" <div class=\"col-md-12\"> \n");
				 str.append("   <div class=\"card\"> \n");
				 
				 str.append("<div class=\"row\"> \n");
				 str.append("<div class=\"card-header\"> \n");
				 str.append("   <h4 class=\"card-title blue bold\" style=\"padding-left: 10px\">Titulo GRID</h4>  \n");
				 str.append("</div>  \n");
				 str.append("<a href=\"/"+txPathForm+"/novo\" id=\"idNovo\">  \n");
				 str.append("<button type=\"button\"  class=\"btn btn-default float-right\" style=\"margin: 25px 0px 0px; max-height: 37px; float: right;\" >  \n"); 
				 str.append("<strong>Novo</strong> \n"); 
				 str.append("</button> \n");
				 str.append("</a> \n");
				str.append("</div>  \n");
				
				 str.append("     <div class=\"card-body\"> \n");
				 str.append("       <div class=\"toolbar\"> \n");
		                               
				 str.append("      </div> \n");
				 str.append("       <table id=\"datatable\" class=\"table table-striped table-bordered\" cellspacing=\"0\" width=\"100colocarporcent\">  \n");
				 str.append("        <thead>  \n");
				 
				 str.append("          <tr> \n");
				 
				 for (int i = 0; i < campos.length; i++) {
					 
					 campos[i].setAccessible(true);
					 
					 String fieldName = campos[i].getName();
					 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
					 
					 str.append("                 <th class=\"bold disabled-sorting\"> "+descricaoCampo+" </th> \n");
	            
	            }
				 str.append(" </tr> \n");
				 str.append("  </thead> \n");                  
				 str.append("  <tbody> \n");
				            String txLista = "";
							 for (int i = 0; i < campos.length; i++) {
								 
								 campos[i].setAccessible(true);
								 
								 String fieldName = campos[i].getName();
								 
								 String fieldType = campos[i].getType().toString().toLowerCase();
								 
								 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
								 if (i == 0) {
								  //str.append("               <tr th:each=\"list"+descricaoCampo.toLowerCase()+" : ${listadados.conteudo}\"> \n");
								  str.append("				 <tr th:each=\"listdados : ${listadados}\" th:onclick=\"|javascript: abrirtela('@{/"+txPathForm+"/consultar}/${listdados."+fieldName+"}');|\"> \n");
								  txLista = descricaoCampo;
								 }
								 if (fieldType.contains("big") || fieldType.contains("date")) {
								   str.append("                 <td class=\"blue\" th:text=\"${{listdados."+fieldName+"}}\"> "+fieldName+" </td> \n");			 
								 }else {
								   str.append("                 <td class=\"blue\" th:text=\"${listdados."+fieldName+"}\"> "+fieldName+" </td> \n");			 
								 }
							 }
				            
				            str.append("               </tr> \n");
				            str.append("             </tbody> \n");		                    
				            str.append("</table>\n");
				            str.append("</div> \n");
				            str.append("</div> \n");
				            str.append("</div> \n");
				            str.append("</div> \n");
				            str.append("</div> \n");
				            
					           //Modal - Inicio
							str.append("<form id=\""+tabObjMinusculo+"\" method=\"post\" th:action=\"@{/"+txPathForm+"}\" th:object=\"${"+tabObjMinusculo+"}\">\n");
							str.append("<div class=\"modal fade\" id=\""+txPathForm+"\" tabindex=\"-1\" \n");
				            str.append("		role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\"> \n");
				            str.append("		<div class=\"vertical-alignment-helper\"> \n");
				            str.append("			<div class=\"modal-dialog vertical-align-center\"> \n");
				            str.append("				<div class=\"modal-content\"> \n");
				            str.append("					<div class=\"modal-header\"> \n");
				            str.append("						<h4 class=\"modal-title\" id=\"myModalLabel\" th:text=\"${pathtela}\"></h4> \n");            
				            str.append("						<button type=\"button\" class=\"close\" data-dismiss=\"modal\">\n");
				            str.append("							<span aria-hidden=\"true\">&times;</span><span \n");
				            str.append("								class=\"sr-only\">Close</span> \n");
				            str.append("						</button> \n");
				            str.append("					</div> \n");
				            str.append("					<div class=\"modal-body\"> \n");
				            str.append("					<div class=\"row\"> \n");
				            
				            
							 for (int i = 0; i < campos.length; i++) {
								 
								 campos[i].setAccessible(true);
								 
								 String fieldName = campos[i].getName();
								 
								 String fieldType = campos[i].getType().toString().toLowerCase();
								 
								 String descricaoCampo = g.arrumaCampo(fieldName.replace("cd", "").replace("tx", "").replace("ck", "").replace("vl", ""));
								 
								  if (fieldType.contains("integer") || fieldType.contains("int")  || fieldName.contains("cd")) {
									  str.append("<!-- INPUT TEXT -->\n"); 
						                 str.append("<div class=\"col-sm-6\">\n");
						                 str.append("<div class=\"form-group\">\n");
						                 str.append("  <label for=\""+campos[i].getName()+"\"><a class=\"blue\" href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
						                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
						                 str.append("</div>\n");
						                 str.append("</div>\n");
						                 str.append("\n");
						                 
									  str.append("<!-- SELECT -->\n");
									  str.append("<div class=\"col-sm-2\">\n");
									  str.append(" <div class=\"form-group\">\n");
									  str.append("	<label for=\""+fieldName+"\"><a class=\"blue\" href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
									  str.append("	 <select class=\"custom-select\" id=\""+fieldName+"\" th:field=\"*{"+fieldName+"}\">\n");
									  str.append("    <option value=\"\">...</option>\n");
									  str.append("    <option th:each=\"s"+fieldName.toLowerCase()+" : ${select"+fieldName.toLowerCase()+"}\" th:value=\"${s"+fieldName.toLowerCase()+"."+fieldName+"}\" th:text=\"${s"+fieldName.toLowerCase()+"."+fieldName.replace("cd", "tx")+"}\"></option>\n");
									  str.append("  </select>\n");
									  str.append(" </div>\n");
									  str.append("</div>\n");
									  
										if (!fieldName.contains("ck")) {
										  str.append("<!-- LOOKUP -->\n");		
										  str.append("<div class=\"col-sm-6\">\n");
										  str.append("	 <div class=\"form-group\">\n");
										  str.append("    <label for=\""+fieldName.replace("cd", "tx")+"\"><a class=\"blue\" href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");						 
										  str.append("	    <div class=\"input-group\">\n");							 
										  str.append("     <label class=\"input-group-prepend\" for=\""+fieldName.replace("cd", "tx")+"\">\n");
										  str.append("     <span class=\"input-group-text\">\n");
										  str.append("       <span class=\"fas fa-search\"></span>\n");
										  str.append("     </span>\n");
										  str.append("    </label>\n");
										  str.append("     <input type=\"text\" class=\"form-control\" id=\""+fieldName.replace("cd", "tx")+"\" name=\""+fieldName.replace("cd", "tx")+"\" th:field=\"*{"+fieldName.replace("cd", "tx")+"}\" onchange=\"javascript: limpalookup('"+fieldName.replace("cd", "tx")+"','"+fieldName+"');\" />\n");
										  str.append("     <input type=\"hidden\" class=\"form-control\" id=\""+fieldName+"\" name=\""+fieldName+"\" th:field=\"*{"+fieldName+"}\" /> \n");
										  str.append("  </div>\n");
										  str.append("	 </div>\n");
										  str.append("	</div>\n");
										  
										   //Gera JS Lookup
										  	strJS += gerarJS("lk"+descricaoCampo.toLowerCase(), fieldName.replace("cd", "tx"), fieldName);
									    }
									  str.append("\n");
								  }else {
									  
									if (fieldType.contains("big")) {  
									 str.append("<!-- INPUT TEXT BIGDECIMAL -->\n"); 
					                 str.append("<div class=\"col-sm-2\">\n");
					                 str.append("<div class=\"form-group\">\n");
					                 str.append("  <label for=\""+campos[i].getName()+"\"><a class=\"blue\" href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
					                 str.append("  <input type=\"text\" class=\"form-control textFloat\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
					                 str.append("</div>\n");
					                 str.append("</div>\n");
					                 str.append("\n");
									}else if (fieldType.contains("date")) {
										 str.append("<!-- INPUT TEXT -->\n"); 
						                 str.append("<div class=\"col-sm-2\">\n");
						                 str.append("<div class=\"form-group\">\n");
						                 str.append("  <label for=\""+campos[i].getName()+"\"><a class=\"blue\" href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");		                 
						                 str.append("  <input type=\"text\" class=\"form-control jtpDate\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\" />\n");
						                 str.append("</div>\n");
						                 str.append("</div>\n");
						                 str.append("\n");						

									}else {	
										 str.append("<!-- INPUT TEXT -->\n"); 
						                 str.append("<div class=\"col-sm-6\">\n");
						                 str.append("<div class=\"form-group\">\n");
						                 str.append("  <label for=\""+campos[i].getName()+"\"><a class=\"blue\" href=\"javascript: logField('"+tx_classe+"','"+fieldName+"');\"><strong>"+descricaoCampo+"</strong></a></label>\n");
						                 str.append("  <input type=\"text\" class=\"form-control\" id=\""+campos[i].getName()+"\" name=\""+campos[i].getName()+"\"  th:field=\"*{"+fieldName+"}\"/>\n");
						                 str.append("</div>\n");
						                 str.append("</div>\n");
						                 str.append("\n");						
									}

								 }
							 }
							 str.append("</div> \n");
							 str.append("<!-- Campos Hidden --> \n");
							 str.append("	<input type=\"hidden\" class=\"form-control\" id=\"NomeCampoRelacionamento\" name=\"NomeCampoRelacionamento\"  th:field=\"*{cdCliente}\"/> \n");
							 str.append("	<input type=\"hidden\" class=\"form-control\" id=\"NomeCampoChave\" name=\"NomeCampoChave\"  th:field=\"*{NomeCampoChave}\"/> \n");
							 
							 str.append("</div> \n");
							 str.append("	<div class=\"modal-footer\" th:with=\"varTxCampo=*{txCampo} == null ? '' : *{txCampo}\"> \n");
							 str.append("		<button type=\"button\" id=\"btnGravarModal\" class=\"btn btn-primary col-sm-2\"  \n");
							 str.append("			th:onclick=\"|javascript: gravarModal('@{/"+txPathForm+"/gravarmodal}', '@{/"+txPathForm+"}/?txCampoFiltro=${varTxCampo}', '#"+txPathForm+"', '#"+tabObjMinusculo+"');|\">  \n");
							 str.append("			<strong>Gravar</strong> \n");
							 str.append("		</button>  \n");
							 str.append("		<button type=\"button\" class=\"btn btn-default\"  \n");
							 str.append("			data-dismiss=\"modal\">  \n");
							 str.append("			<strong>Sair</strong>  \n");
							 str.append("		</button>  \n");
							 str.append("	</div>  \n");
							 str.append("</div> \n");
							 str.append("</div>  \n");
							 str.append("</div>  \n");
							 str.append("</div>  \n");
							 //str.append("</div>  \n");
							 str.append("</form>  \n");

				str.append("<script> \n");
				str.append("$(document).ready(function() { \n");

				str.append(" var printCounter = 0;  \n");
			     
				str.append("  $('#datatable').DataTable({  \n");
				str.append("    \"pagingType\": \"full_numbers\", \n");
				str.append("    \"ordering\": false, \n");
				str.append("    \"scrollX\": true, \n");
				str.append("    \"lengthMenu\": [ \n");
				str.append("      [100, 200, 300, -1], \n");
				str.append("      [100, 200, 300, \"All\"] \n");
				str.append("    ], \n");
				str.append("    dom: 'Bfrtip', \n");
				str.append("    buttons:  [ \n");
				str.append("        'copy', \n");
				str.append("        { \n");
				str.append("            extend: 'excel', \n");
				str.append("            messageTop: 'Titulo Excel.' \n");
				str.append("        }, \n");
				str.append("        { \n");
				str.append("            extend: 'pdf', \n");
				str.append("            messageBottom: null \n");
				str.append("        }, \n");
				str.append("        { \n");
				str.append("            extend: 'print', \n");
				str.append("            messageTop: function () { \n");
				str.append("                printCounter++;	\n"); 
				str.append("         if ( printCounter === 1 ) { \n");
				str.append("                    return 'Esta é a primeira vez que você imprime este documento.'; \n");
				str.append("                } \n");
				str.append("                else { \n");
				str.append("                    return 'Você imprimiu este documento '+printCounter+' vezes'; \n");
				str.append("			          } \n");
				str.append("            }, \n");
				str.append("            messageBottom: null \n");
				str.append("        } \n");
				str.append("			        ], \n");
				str.append("    responsive: true, \n");
				str.append("			        language: {  \n");
				str.append("      search: \"_INPUT_\", \n");
				str.append("      searchPlaceholder: \"Localizar\", \n");
				str.append("    } \n");
				str.append("}); \n");
				str.append(" var table = $('#datatable').DataTable(); \n");
				str.append("}); \n");
				str.append("</script> \n");
			
				 str.append("<!-- Script JS --> \n");
				 str.append("<script th:inline=\"javascript\"> \n");	
				 str.append(" /*<![CDATA[*/	\n");	   
				 str.append("  $(document).ready(function() {	\n");			 
				 str.append("    var cdTipo = /*[[${tipoMensagem}]]*/ \n jtpNotifySimple(/*[[${txMensagem}]]*/,cdTipo);\n");	
				 str.append("	jtpNotify(/*[[${listaErros}]]*/);\n");			 
				 str.append("    var cdchave = /*[[${CampoChave}]]*/; \n");
				 str.append("    document.getElementById('CampoChave').value = cdchave; \n");
				 
				 str.append(" var txEdit = /*[[${txEdit}]]*/; \n");
				 str.append(" if (txEdit != null) { \n");
				 str.append("	 $('#"+txPathForm+"').modal('show'); \n");	
				 str.append(" } \n");
				 
				 str.append("//Permissões \n");
				 str.append("	var listPermissaoBotao = /*[[${listPermissaoBotao}]]*/; \n");
				 str.append("	checkPermissaoBotao(listPermissaoBotao);\n\n");
					
				 str.append("	var listPermissaoCampos = /*[[${listPermissaoCampos}]]*/; \n");
				 str.append("	checkPermissaoCampos(listPermissaoCampos); \n\n");

				 str.append("	//Service \n");
				 str.append("	var txService = /*[[${txService}]]*/; \n");
				 str.append("	analistaVirtual(txService); \n");			 
				 str.append("	var pathUrl = /*[[@{/}]]*/; \n\n");

				 str.append(strJS);
				 
				 str.append(" });\n");	
				 str.append(" /*]]>*/ \n");
				 str.append("</script> \n"); 
			
			
				str.append("</section>\n");

				str.append("</html>\n");
		
		
	         File f = new File("/temp/obj/html");
		        if (!f.exists()) {
		        	f.mkdirs();
		        }
	         
	          FileWriter arq = new FileWriter("/temp/obj/html/"+txPathForm+".html");
		      PrintWriter gravarArq = new PrintWriter(arq);
		      gravarArq.printf(str.toString());
		      gravarArq.close();
		      
		      System.out.println("HTML "+txObj+".html criado com sucesso!");
		      
		}catch (Exception ex) {
			ex.printStackTrace();
		}		
	}

	
	
}
