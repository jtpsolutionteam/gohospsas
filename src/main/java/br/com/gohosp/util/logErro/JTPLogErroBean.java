package br.com.gohosp.util.logErro;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.util.logerro.TabLogErroObj;
import br.com.gohosp.dao.util.logerro.repository.TabLogErroRepository;
import br.com.gohosp.util.GeneralParser;

@Service
public class JTPLogErroBean {
	
	@Autowired
	private TabLogErroRepository tabLogErroRepository;

	private boolean ck_status;
	private String tx_erro;
	private boolean ck_log_ok;

	public boolean isCk_log_ok() {
		return ck_log_ok;
	}
	
	public void setCk_log_ok(boolean ck_log_ok) {
		this.ck_log_ok = ck_log_ok;
	}
	
	public boolean isCk_status() {
		return ck_status;
	}
	
	public void setCk_status(boolean ck_status) {
		this.ck_status = ck_status;
	}
	
	public String getTx_erro() {
		return tx_erro;
	}
	
	public void setTx_erro(String tx_erro) {
		this.tx_erro = tx_erro;
	}
	
	/*public void addLog1 (String tx_caminho,String tx_nomearquivo,String tx_servico, String tx_mensagem) {
		try{ 
			FileWriter writer = new FileWriter(tx_caminho+tx_nomearquivo+".xml",true);			
			PrintWriter saida = new PrintWriter(writer,true);
			String XML = "";	        
			
			tx_mensagem = tx_mensagem.replaceAll("<", " ");
			tx_mensagem = tx_mensagem.replaceAll(">", " ");
			
			XML += "<dados> \n";				
			XML += "<dt_log>"+GeneralParser.format_dateHSBR(new Date())+"</dt_log> \n";
			XML += "<tx_servico>"+tx_servico+"</tx_servico> \n";			
			XML += "<tx_mensagem>"+tx_mensagem+"</tx_mensagem> \n";
			XML += "<tx_erro> \n";			  
			XML += "<dados> \n";
			XML += "<tx_nivel>Nivel</tx_nivel> \n";
			XML += "<tx_classe>Classe:</tx_classe> \n";   // Mostra o nome da classe
			XML += "<tx_metodo>Metodo:</tx_metodo> \n";   // Nome do metodo gerador do erro
			XML += "<tx_arquivo>Arquivo:</tx_arquivo> \n";   // Nome do arquivo
			XML += "<tx_linha>Linha:</tx_linha> \n";   // Linha no arquivo
			XML += "</dados> \n";				 
			XML += "</tx_erro> \n";
			XML += "</dados> \n";				 

			saida.println(XML);
			saida.close();
			writer.close();  

			this.setCk_status(true);
			this.setTx_erro("");
			this.setCk_log_ok(true);
		}catch (Exception e){
			this.setCk_status(false);
			this.setTx_erro(e.getMessage());
			this.setCk_log_ok(false);
		}		
	}*/
	
	public void addLog(String tx_servico, String tx_mensagem, Exception ex) {
		try { 
			if (tx_mensagem==null)
				return;
			
			//FileWriter writer = new FileWriter(tx_caminho+tx_nomearquivo+".xml" ,true);
			//PrintWriter saida = new PrintWriter(writer,true);
			String XML = "";
			
			tx_mensagem = tx_mensagem.replaceAll("<", " ");
			tx_mensagem = tx_mensagem.replaceAll(">", " ");
			
			XML += "<dados> \n";				
			XML += "<dt_log>"+GeneralParser.format_dateHSBR(new Date())+"</dt_log> \n";
			XML += "<tx_servico>"+tx_servico+"</tx_servico> \n";			
			XML += "<tx_mensagem>"+tx_mensagem+"</tx_mensagem> \n";
			XML += ExceptionStackTrace(ex);		
			XML += "</dados> \n";				 
			
			//saida.println(XML);
			//saida.close();
			//writer.close();  

			TabLogErroObj Tab = new TabLogErroObj();
			Tab.setCdLogErro(null);
			Tab.setDtLog(new Date());
			Tab.setTxServico(tx_servico);
			Tab.setTxMensagem(tx_mensagem);
			Tab.setTxErro(ExceptionStackTrace(ex));
			tabLogErroRepository.save(Tab);
			
			if(Constants.LOG_SHOW)
			System.out.println(XML);
			this.setCk_status(true);
			this.setTx_erro("");
			this.setCk_log_ok(true);
		}catch (Exception e) {
			this.setCk_status(false);
			this.setTx_erro(e.getMessage());
			this.setCk_log_ok(false);
		}		
	}
	
	public static String ExceptionStackTrace(Exception e) {
		StackTraceElement Stl;   
		String StackTrace = "<tx_erro> \n";			  
		
		for(int i=0;i<e.getStackTrace().length;i++) {
			Stl=e.getStackTrace()[i];
			if (Stl.getClassName().indexOf(Constants.SCHEMA)>-1) {	 
				StackTrace += "<dados> \n";
				StackTrace += "<tx_nivel>Nivel "+i+ "</tx_nivel> \n";
				StackTrace += "<tx_classe>Classe: "+Stl.getClassName()+ "</tx_classe> \n";   // Mostra o nome da classe
				StackTrace += "<tx_metodo>Metodo: "+Stl.getMethodName().replaceAll("<init>", "init")+ "</tx_metodo> \n";   // Nome do metodo gerador do erro
				StackTrace += "<tx_arquivo>Arquivo: "+Stl.getFileName()+ "</tx_arquivo> \n";   // Nome do arquivo
				StackTrace += "<tx_linha>Linha: "+Stl.getLineNumber()+ "</tx_linha> \n";   // Linha no arquivo
				StackTrace += "</dados> \n";				 
			}
		}       
		
		StackTrace += "</tx_erro> \n";
		
		return StackTrace;
	}
	
	public String LeituraXMLLog(String tx_caminho,String tx_nomearquivo) { 
		String tx_linha_xml = "";
		FileReader reader;
		
		try {
			reader = new FileReader(tx_caminho+tx_nomearquivo+".xml");
			BufferedReader leitor = new BufferedReader(reader);
			
			String linha = null;
			tx_linha_xml += "<?xml version='1.0' encoding='ISO-8859-1' ?>";
			tx_linha_xml += "<list>";
			this.setCk_log_ok(true);
			
			try {
				while ((linha = leitor.readLine()) != null) {
					tx_linha_xml += linha.trim();
				}
			} catch (IOException e3) {
				this.setTx_erro(e3.getMessage());
				this.setCk_log_ok(false);				
			}
			
			tx_linha_xml += "</list>";
			
			try {
				leitor.close();
			} catch (IOException e2) {
				this.setTx_erro(e2.getMessage());
				this.setCk_log_ok(false);				
			}
			
			try {
				reader.close();
			} catch (IOException e1) {
				this.setTx_erro(e1.getMessage());
				this.setCk_log_ok(false);				
			}
		} catch (FileNotFoundException e) {
			this.setTx_erro(e.getMessage());
			this.setCk_log_ok(false);			
		}
		
		return tx_linha_xml;
	}

	
}
