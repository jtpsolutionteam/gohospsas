package br.com.gohosp.util.graficos;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

public class GraficosService {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<TabGraficoLinhasObj> lts = new ArrayList<TabGraficoLinhasObj>();
		TabGraficoLinhasObj Tab = new TabGraficoLinhasObj();
		Tab.setTxLabel("Sergio");
		Tab.setVlValorNumber(1);
		lts.add(Tab);
		Tab = new TabGraficoLinhasObj();
		Tab.setTxLabel("Marcio");
		Tab.setVlValorNumber(2);
		lts.add(Tab);
		
		
		System.out.println(new GraficosService().getJsonGrafico("Nome", "Qtde", "number", lts));
		
	}
	
	
	public String getJsonGrafico(String txNomaCampo, String txNomeValor, String txTipoValor, List<TabGraficoLinhasObj> lstLinhasValores) {
		
		String jsonStr = "";
		try {
			
			List<TabGraficoColunasObj> lstCols = new ArrayList<TabGraficoColunasObj>();		
			TabGraficoColunasObj TabCols = new TabGraficoColunasObj();
			//TabCols.setId("");
			TabCols.setLabel(txNomaCampo);
			//TabCols.setPattern("");
			TabCols.setType("string");
			lstCols.add(TabCols);
			
			TabCols = new TabGraficoColunasObj();
			//TabCols.setId("");
			TabCols.setLabel(txNomeValor);
			//TabCols.setPattern("");
			TabCols.setType(txTipoValor);
			lstCols.add(TabCols);
			
			TabCols = new TabGraficoColunasObj();
			//TabCols.setId("");
			//TabCols.setLabel(txNomeValor);
			//TabCols.setPattern("");
			TabCols.setRole("annotation");
			TabCols.setType("string");
			lstCols.add(TabCols);
			
			TabGraficoObj Tab = new TabGraficoObj();			
			Tab.setCols(lstCols);
			
			Gson gson = new Gson();
			jsonStr = gson.toJson(Tab);
			
			StringBuffer strRows = new StringBuffer();
			strRows.append("\"rows\": [\n");
			
			for (TabGraficoLinhasObj atual : lstLinhasValores) {
				
				strRows.append("{\"c\":[{\"v\":\""+atual.getTxLabel()+"\",\"f\":null},{\"v\":"+atual.getVlValorNumber()+",\"f\":null},{\"v\":"+atual.getTxValor()+",\"f\":null}]},\n");
				
				
			}
			
			jsonStr = jsonStr.substring(0,jsonStr.length()-1)+","+strRows.toString().substring(0, strRows.toString().length()-2)+"]}";
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		
		return jsonStr;
	}

}
