package br.com.gohosp.util.graficos;

import java.util.List;

public class TabGraficoObj {

	private List<TabGraficoColunasObj> cols;
	
	private List<TabGraficoLinhasObj> rows;

	public List<TabGraficoColunasObj> getCols() {
		return cols;
	}

	public void setCols(List<TabGraficoColunasObj> cols) {
		this.cols = cols;
	}

	public List<TabGraficoLinhasObj> getRows() {
		return rows;
	}

	public void setRows(List<TabGraficoLinhasObj> rows) {
		this.rows = rows;
	}
	
	
}
