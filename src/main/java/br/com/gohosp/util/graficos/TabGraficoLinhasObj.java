package br.com.gohosp.util.graficos;

import java.util.List;

public class TabGraficoLinhasObj {

	private String txLabel;
	
	private Integer vlValorNumber;
	
	private String txValor;

	public String getTxLabel() {
		return txLabel;
	}

	public void setTxLabel(String txLabel) {
		this.txLabel = txLabel;
	}

	public Integer getVlValorNumber() {
		return vlValorNumber;
	}

	public void setVlValorNumber(Integer vlValorNumber) {
		this.vlValorNumber = vlValorNumber;
	}

	public String getTxValor() {
		return txValor;
	}

	public void setTxValor(String txValor) {
		this.txValor = txValor;
	}

	
	
	
}
