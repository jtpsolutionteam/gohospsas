package br.com.gohosp.util.geradorcontroller;

import java.lang.reflect.Field;

import br.com.gohosp.Constants;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.geradorclasse.GeradorClasseBean;

public class GeradorJsonController {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		new GeradorJsonController().GeradorJSonTeste("TabOrigemDestinoObj");
	}
	
	
	private void GeradorJSonTeste(String obj) {
		
		try {
			
			String pathObj = new GeneralUtil().LocalizaArquivo(Constants.NOME_SISTEMA_LOCAL,"java",Constants.PATH_ARQS_JAVA_LOCAL,obj+".java");
			
			Object tabObj = Class.forName(pathObj).newInstance();
			
			GeradorClasseBean g = new GeradorClasseBean();  
			Field[] campos = tabObj.getClass().getDeclaredFields();
		
			String tx_obj = tabObj.getClass().getSimpleName();
			String json  = "{\n";
				   json += "\""+g.arrumaCampoMinusculaObj(tx_obj)+"\": {\n";
			for (int i = 0; i < campos.length; i++) {
			    // Tipo 
			    // Exemplo: class java.math.BigDecimal
			    //System.out.println(campos[i].getType());
				campos[i].setAccessible(true);
			    //System.out.println("Old");
			    //System.out.println(camposOld[i].getName());
			    //System.out.println(camposOld[i].get(objOld));
			    //System.out.println(camposOld[i].getModifiers());
				String tipo = campos[i].getType().toString();
				if (tipo.contains("String")) {
					json += " \""+campos[i].getName()+"\": \"teste\",\n";
				}else if (tipo.contains("Date")) {
					json += " \""+campos[i].getName()+"\": \"01/01/2018\",\n";
				}else if (tipo.contains("Float") || tipo.contains("Double")) {
					json += " \""+campos[i].getName()+"\": 10,00,\n";
				}else if (tipo.contains("Integer")) {
					json += " \""+campos[i].getName()+"\": 1,\n";
				}else  {
					json += " \""+campos[i].getName()+"\": \"outro\",\n";
				}
			}
			
			json = json.substring(0,json.length()-2);
			json += "\n },\n";
			
			json += " \"tabFolderUsuarioObj\":\n";
			json += " {\n";
			json += " \"txHashSeguranca\": \"5KKODRZ7H1QNVES8I2PG4BAJF3CXLT\",\n";
			json += " \"cdGrupoAcesso\": 1,\n";
			json += " \"cdGrupoVisao\": 1\n";
			json += " }\n";
			json += "}";

			System.out.println(json);
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		
		
		
		
	}
	
	

}
