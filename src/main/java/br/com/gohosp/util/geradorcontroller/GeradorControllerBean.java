package br.com.gohosp.util.geradorcontroller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.reflect.Field;

import br.com.gohosp.Constants;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.geradorclasse.GeradorClasseBean;

public class GeradorControllerBean {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		new GeneralUtil().ApagarArquivosDiretorioGeradores();
		
		// 1 - Normal; 2 - Filho; 3 - Pesquisa; 4 - Filho Modal; 5 - Pesquisa/Gravação; 6 - Pesquisa/Relatório
		new GeradorControllerBean().GeraListaController("RelFiltroPagarReceberObj", 6);
	}
	
	private void GerarControllerPackage(String txObj, Integer cdTipo) {
		try {
		    String pathObj = new GeneralUtil().LocalizaArquivo(Constants.NOME_SISTEMA_LOCAL,"java",Constants.PATH_ARQS_JAVA_LOCAL,txObj+".java").replaceAll("\\.", "/").replace(txObj, "");
    	
	        File[] files = new File(Constants.PATH_ARQS_JAVA_LOCAL+"src/main/java/"+pathObj).listFiles();  
	        for (int i = 0; i < files.length; ++i) {  
	            File pathname = files[i];  
	            String nm = pathname.getName();  
	            
	            if (nm.contains(".java")) {
	            	nm = pathname.getName().replace(".java", "");
	            	GeradorController(nm,cdTipo);
					
	            }
	            
	            Thread.sleep(1000);  
	        } 
	        
	        System.out.println("Geração do Lote de Controller terminado!");
		}catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	
	public void GeraListaController(String lista, Integer cdTipo) {
		
		  try {	
			String[] l = lista.split(";");
			
			for (int i=0; i<=l.length-1; i++) {
				
				GeradorController(l[i],cdTipo);
				Thread.sleep(1000);
			}
			
			System.out.println("Geração do Lote de Controllers terminado!");
		  }catch (Exception ex) {
			  ex.printStackTrace();
		  }
			
		}

	
	//private void GeradorService(String txObjTabela, String pkTabela, String txTipoField) {
	private void GeradorController(String txObj, Integer cdTipo) {	 
		try {
			
			String pathObj = new GeneralUtil().LocalizaArquivo(Constants.NOME_SISTEMA_LOCAL,"java",Constants.PATH_ARQS_JAVA_LOCAL,txObj+".java");
			
			Object tabObj = null;
			
			tabObj = Class.forName(pathObj).newInstance();
			
			Field[] campo = tabObj.getClass().getDeclaredFields();
			campo[0].setAccessible(true);
			
			GeradorClasseBean g = new GeradorClasseBean();			
			String field = g.arrumaCampoObj(campo[0].getName());
			String fieldType = campo[0].getType().toString();
			
			
			
			 if (fieldType.contains("Integer")) {
				 fieldType = "Integer";
			 }else if (fieldType.contains("Long")) {
				 fieldType = "Long";
			 }else {
				 fieldType = "String";
			 }
 
			
			 String texto;
			 String result = "";
			 
			 String txArquivo = "";
			 switch (cdTipo) {
			case 1:
				txArquivo = "ModeloControllerNormal.txt";
				break;
			case 2:
				txArquivo = "ModeloControllerFilho.txt";
				break;	
			case 3:
				txArquivo = "ModeloControllerPesquisa.txt";
				break;	
			case 4:
				txArquivo = "ModeloControllerFilhoModal.txt";
				break;
			case 5:
				txArquivo = "ModeloControllerPesquisaGravacao.txt";
				break;
			case 6:
				txArquivo = "ModeloControllerPesquisaRelatorio.txt";
				break;
			default:
				break;
			}
			 
			 FileReader fileR = new FileReader (Constants.PATH_ARQS_JAVA_LOCAL+"src/main/java/br/com/gohosp/util/geradorcontroller/"+txArquivo);
	         BufferedReader buffR = new BufferedReader (fileR);
	         String tx_classe = tabObj.getClass().getSimpleName().replace("Obj", "Controller");
	         String tx_service = tabObj.getClass().getSimpleName().replace("Obj", "");
	         String tabFiltroObj = tabObj.getClass().getSimpleName().replace("Tab", "TabFiltro");
	         
	         
	         while ((texto = buffR.readLine ()) != null) {
	        	 
	        	 String tabJoin = tx_classe.replace("Tab", "TabJoin").replace("Controller", "");
	        	 
	        	 result += texto.replaceAll("#TabObj", txObj).replaceAll("#tabObj", g.arrumaCampoMinusculaObj(txObj)).replaceAll("#Field", field).replaceAll("#TipoField", fieldType).replaceAll("#Service", tx_service).replaceAll("#Classe", tx_classe).replaceAll("#TabJoinObj", tabJoin).replaceAll("#TabFiltroObj", tabFiltroObj).replaceAll("#tabFiltroObj", g.arrumaCampoMinusculaObj(tabFiltroObj)).replaceAll("#Rel", txObj.toLowerCase().replace("obj", ""))+"\n";
	         }
	         
	         //System.out.println(result);
	        
	         File f = new File("/temp/obj/controller");
		        if (!f.exists()) {
		        	f.mkdirs();
		        }
	         
	          FileWriter arq = new FileWriter("/temp/obj/controller/"+tx_classe+".java");
		      PrintWriter gravarArq = new PrintWriter(arq);
		      gravarArq.printf(result);
		      gravarArq.close();
		      
		      System.out.println("Controller "+tx_classe+".java criado com sucesso!");
		      
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
	}

}
