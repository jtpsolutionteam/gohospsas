package br.com.gohosp.util.geradorcampos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import br.com.gohosp.dao.conexao.connect;

public class GeradorCamposBean {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		new GeradorCamposBean().GeradorCampos(41, "vw_tab_li_gestao");
		
		
	}
	
	private void GeradorCampos(Integer cdTela, String Tabela) {
		
		String tx_obj = "";
        Connection conn = connect.getConnectionSimpleMySql();
        connect connect = new connect();
        
	    try {

	      ResultSet result = connect.conexao_mysql_resultSet(conn,"select * from "+Tabela, "Select");  	    	  

	      ResultSetMetaData rs = result.getMetaData();	      
	      
	      int numColumns = rs.getColumnCount();

	      boolean ck_eof = false;

	      if (numColumns < 1) {
	        ck_eof = true;
	      }
	         
	      for (int i=0; i<numColumns; i++) {
	        String tx_nome = rs.getColumnName(i + 1).toLowerCase();
	        String tx_tamanho = String.valueOf(rs.getColumnDisplaySize(i + 1));
	        String tx_decimal = String.valueOf(rs.getScale(i + 1));
	        String tx_tipo_campo = rs.getColumnTypeName(i + 1).toUpperCase();
	        
	        if ((tx_tipo_campo.equals("NUMBER") || tx_tipo_campo.equals("INTEGER") || tx_tipo_campo.equals("INT")) && tx_decimal.equals("0")) {
	          System.out.println(arrumaCampo(tx_nome));
	          if (!verificaExisteCampo(cdTela, arrumaCampoMinusculaObj(tx_nome))) {
	        	  System.out.println(arrumaCampoMinusculaObj(arrumaCampoObj(tx_nome)));
	        	  IncluirCampo(cdTela, arrumaCampo(tx_nome), arrumaCampoMinusculaObj(arrumaCampoObj(tx_nome)), "integer");
	          }
	        }

	        if ((tx_tipo_campo.equals("NUMBER") || tx_tipo_campo.equals("FLOAT") || tx_tipo_campo.equals("DOUBLE")) && !tx_decimal.equals("0")) {
		          System.out.println(arrumaCampo(tx_nome));
		          if (!verificaExisteCampo(cdTela, arrumaCampoMinusculaObj(tx_nome))) {
		        	  System.out.println(arrumaCampoMinusculaObj(arrumaCampoObj(tx_nome)));  
		        	  IncluirCampo(cdTela, arrumaCampo(tx_nome), arrumaCampoMinusculaObj(arrumaCampoObj(tx_nome)), "float");
		          }
	        }

	        if (tx_tipo_campo.equals("DATE") || tx_tipo_campo.equals("DATETIME") || tx_tipo_campo.equals("TIMESTAMP")) {
		          System.out.println(arrumaCampo(tx_nome));
		          if (!verificaExisteCampo(cdTela, arrumaCampoMinusculaObj(tx_nome))) {
		        	  System.out.println(arrumaCampoMinusculaObj(arrumaCampoObj(tx_nome)));  
		        	  IncluirCampo(cdTela, arrumaCampo(tx_nome), arrumaCampoMinusculaObj(arrumaCampoObj(tx_nome)), "date");
		          }
	        }

	        if (tx_tipo_campo.equals("VARCHAR") || tx_tipo_campo.equals("CHAR") ||
	       		tx_tipo_campo.equals("VARCHAR2") || tx_tipo_campo.equals("NCHAR")
	            || tx_tipo_campo.equals("NVARCHAR") ) {
		          System.out.println(arrumaCampo(tx_nome));
		          if (!verificaExisteCampo(cdTela, arrumaCampoMinusculaObj(tx_nome))) {
		        	  System.out.println(arrumaCampoMinusculaObj(arrumaCampoObj(tx_nome)));  
		        	  IncluirCampo(cdTela, arrumaCampo(tx_nome).replace("Tx.", ""), arrumaCampoMinusculaObj(arrumaCampoObj(tx_nome)), "string");
		          }
	        }
	      }


	      conn.close();
	      if (ck_eof == false) {
	       //System.out.println(field);	  
	       System.out.println("Campos criados com sucesso!");	
	      }else {
		   System.out.println("Tabela não encontrada!");
	      }

	    }catch (Exception e) {
			  e.printStackTrace();
	    }
		
	}

	
	private void IncluirCampo(Integer cdTela, String txCampo, String txObjCampo, String txTipoCampo) {

		Connection conn = connect.getConnectionSimpleMySql();
		try {
			String StrSql = "insert into tab_campos (cd_tela, cd_grupo_visao, ck_alterar, tx_obj_campo, tx_tipo_campo, ck_visualizar, cd_ordem, tx_campo, vl_size)"
					      + " values (?,1,1,?,?,1,1,?,100) ";
			PreparedStatement prmt = conn.prepareStatement(StrSql);
			prmt.setInt(1, cdTela);
			prmt.setString(2, txObjCampo);
			prmt.setString(3, txTipoCampo);
			prmt.setString(4, txCampo);
			
			prmt.executeUpdate();
			
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private boolean verificaExisteCampo(Integer cdTela, String txObjCampo) {
		
		
		Connection conn = connect.getConnectionSimpleMySql(); 
		try {
			String StrSql = "select * from tab_campos where cd_tela = ? and tx_obj_campo = ?";
			PreparedStatement prmt = conn.prepareStatement(StrSql);
			prmt.setInt(1, cdTela);
			prmt.setString(2, txObjCampo);
			
			ResultSet rs = prmt.executeQuery();
			if (rs.isBeforeFirst()) {
				return true;
			}else {
				return false;
			}
			
			
		}catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	
	public String arrumaCampo(String campo) {
		
		String[] tx = campo.split("_");
		String tx1 = "";
		for (int i = 0; i<=tx.length-1; i++) {
			
			if (i == 0) {
				tx1 += tx[i].substring(0, 1).toUpperCase()+tx[i].substring(1, tx[i].length());
				if (tx1.equals("Cd")) {
					tx1 = "Cód.";
				}else {
					tx1 += ".";
				}
			}
			
			if (i > 0) {
				String tx2 = tx[i].substring(0, 1).toUpperCase();
				if (i>1) {
					tx2 = " "+tx[i].substring(0, 1).toUpperCase();
				}
				String tx3 = tx[i].substring(1, tx[i].length());
				
				tx1 += tx2+tx3;
			}
			
		}
		
		return tx1;
	}
	
	public String arrumaCampoObj(String campo) {
		
		String[] tx = campo.split("_");
		String tx1 = "";
		for (int i = 0; i<=tx.length-1; i++) {
						
			if (i >= 0) {
				String tx2 = tx[i].substring(0, 1).toUpperCase();
				String tx3 = tx[i].substring(1, tx[i].length());
				tx1 += tx2+tx3;
			}
			
		}
		
		return tx1;
	}
	
	public String arrumaCampoMinusculaObj(String campo) {
		
		String[] tx = campo.split("_");
		String tx1 = "";
		for (int i = 0; i<=tx.length-1; i++) {
						
			if (i >= 0) {
				String tx2 = tx[i].substring(0, 1).toLowerCase();  //Minusculo 1a. letra
				String tx3 = tx[i].substring(1, tx[i].length());
				tx1 += tx2+tx3;
			}
			
		}
		
		return tx1;
	}


}
