package br.com.gohosp.util.geradorclasse;

import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.geradorcontroller.GeradorControllerBean;
import br.com.gohosp.util.geradorhtml.GeradorHTMLBean;
import br.com.gohosp.util.geradorservice.GeradorServiceBean;

public class GeradorFullBean {

	public static void main(String[] args) {
		
		
		String listaOBJ = "TabRetornoConsultaTratativaObj";
		String nomeTelaHtml = "retornotratativa";		
		Integer cd_tipo_tela_html = 5; // 1 - Normal; 2 - Filho; 3 - Pesquisa; 4 - Filho Modal; 5 - Pesquisa/Gravação;  6 - Pesquisa/Relatório
		
		new GeneralUtil().ApagarArquivosDiretorioGeradores();		
		new GeradorServiceBean().GeraListaService(listaOBJ);
		new GeradorControllerBean().GeraListaController(listaOBJ, cd_tipo_tela_html);		
		new GeradorHTMLBean().GeraListaHTML(listaOBJ, nomeTelaHtml, cd_tipo_tela_html);
		System.out.println("GeradorFull terminado!");
		
		
	}
	
	
}
