package br.com.gohosp.util.geradorclasse;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import br.com.gohosp.dao.conexao.connect;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.Validator;

// TODO Auto-generated method stub

public class GeradorClasseBean {
	
	public static void main(String[] args) { 
		GeradorClasseBean g = new GeradorClasseBean();
		new GeneralUtil().ApagarArquivosDiretorioGeradores();
				
		g.GeraListaClasses("tab_retorno_consulta_tratativa;tab_tipo_katz");
				
	}

	public void GeraListaClasses(String lista) {
		
		
	  try {	
		String[] l = lista.split(";");
		
		for (int i=0; i<=l.length-1; i++) {
			
			GeradorClasseCompleta("","select * from "+l[i]);
			Thread.sleep(1000);
		}
		
		System.out.println("Geração do Lote de Classes terminado!");
	  }catch (Exception ex) {
		  ex.printStackTrace();
	  }
		
	}
	
	 
	
	private void GeradorClasseCompleta(String nomeArquivoNovo, String SQL) {
		
		String tx_obj = "";
        Connection conn = connect.getConnectionSimpleMySql();
        connect connect = new connect();
        
	    try {

	      ResultSet result = connect.conexao_mysql_resultSet(conn,SQL, "Select");  	    	  

	      ResultSetMetaData rs = result.getMetaData();	      
	      
	      int numColumns = rs.getColumnCount();

	      boolean ck_eof = false;

	      if (numColumns < 1) {
	        ck_eof = true;
	      }

	        String field = "";
	        
	      
	      //Inicio Classe	        
	        field += "import javax.persistence.Entity;\n";
	        field += "import javax.persistence.Id;\n";
	        field += "import javax.persistence.Table;\n";
	        field += "import javax.persistence.Column;\n";	        
			field += "import br.com.jtpsolution.Constants;\n\n\n";
	        
	        field += "@Entity\n";
	        field += "@Table(name=\""+rs.getTableName(1)+"\", schema=Constants.SCHEMA)\n";
	        if (Validator.isBlankOrNull(nomeArquivoNovo)) {
	           tx_obj =  arrumaCampoObj(rs.getTableName(1))+"Obj";   
	        }else {
	           tx_obj =  nomeArquivoNovo;	
	        }
	        
	         field += "public class "+tx_obj+" {\n\n\n";
	       
	         
	       String txToUpperCase = "";  
	      for (int i=0; i<numColumns; i++) {
	        String tx_nome = rs.getColumnName(i + 1).toLowerCase();
	        String tx_tamanho = String.valueOf(rs.getColumnDisplaySize(i + 1));
	        String tx_decimal = String.valueOf(rs.getScale(i + 1));
	        String tx_tipo_campo = rs.getColumnTypeName(i + 1).toUpperCase();
	        
	        
	        if (i == 0) {
	         field += "@Id @GeneratedValue \n";
	        }
	        field += "@Column(name = \""+tx_nome+"\") \n";

	        
	        if ((tx_tipo_campo.equals("NUMBER") || tx_tipo_campo.equals("INTEGER") || tx_tipo_campo.equals("INT")) && tx_decimal.equals("0")) {
	        	field += "//@NotNull(message = \""+arrumaCampo(tx_nome).replace("cd","").replace("tx", "").replace("vl", "").replace("dt", "")+" campo obrigatório!\")\n";
	        	field += "private Integer "+arrumaCampo(tx_nome)+"; \n";
	          
	        }

	        if ((tx_tipo_campo.equals("NUMBER") || tx_tipo_campo.equals("FLOAT") || tx_tipo_campo.equals("DOUBLE")) && !tx_decimal.equals("0")) {
	        	field += "//@NotEmpty(message = \""+arrumaCampo(tx_nome).replace("cd","").replace("tx", "").replace("vl", "").replace("dt", "")+" campo obrigatório!\")\n";
	        	field += "@NumberFormat(pattern = \"#,##0.00\") \n";
	        	field += "private BigDecimal "+arrumaCampo(tx_nome)+"; \n";

	        }

	        if (tx_tipo_campo.equals("DATE") || tx_tipo_campo.equals("DATETIME") || tx_tipo_campo.equals("TIMESTAMP")) {
	        	field += "@DateTimeFormat(pattern = \"dd/MM/yyyy\") \n";
	        	field += "@Temporal(TemporalType.DATE) \n";
	        	field += "private Date "+arrumaCampo(tx_nome)+"; \n";
	        }

	        if (tx_tipo_campo.equals("VARCHAR") || tx_tipo_campo.equals("CHAR") ||
	       		tx_tipo_campo.equals("VARCHAR2") || tx_tipo_campo.equals("NCHAR")
	            || tx_tipo_campo.equals("NVARCHAR") ) {
	        	field += "//@NotEmpty(message = \""+arrumaCampo(tx_nome).replace("cd","").replace("tx", "").replace("vl", "").replace("dt", "")+" campo obrigatório!\")\n";
	        	field += "@Size(max = "+tx_tamanho+", message = \""+arrumaCampo(tx_nome).replace("cd","").replace("tx", "").replace("vl", "").replace("dt", "")+" tamanho máximo de "+tx_tamanho+" caracteres\") \n";
	        	field += "private String "+arrumaCampo(tx_nome)+"; \n";
	        	txToUpperCase += "if (!Validator.isBlankOrNull("+arrumaCampo(tx_nome)+"))\n";
	        	txToUpperCase += arrumaCampo(tx_nome) +" = "+ arrumaCampo(tx_nome)+".toUpperCase();\n";
	        	
	        }
	        
	        field += " \n";
	      }
	      
	      field += "@PrePersist @PreUpdate \n";
	      field += "private void prePersistUpdate() { \n";	     
          field += txToUpperCase;  
          field += "}\n\n";
	      field += "}\n";
	        
	        File f = new File("/temp/obj/repository");
	        if (!f.exists()) {
	        	f.mkdirs();
	        }
	        f = new File("/temp/obj/join");
	        if (!f.exists()) {
	        	f.mkdirs();
	        }
	        FileWriter arq = new FileWriter("/temp/obj/"+tx_obj+".java");
	        PrintWriter gravarArq = new PrintWriter(arq);
	        gravarArq.printf(field);
	        gravarArq.close();
	        
	        //Criar Repository
	        field = "import org.springframework.data.jpa.repository.JpaRepository;\n";
	        field += "import br.com.jtpsolution.dao.siscomex."+tx_obj+";\n\n\n\n";	        
	        field += "public interface "+tx_obj.replace("Obj", "")+"Repository extends JpaRepository<"+tx_obj+", Integer> {" + 
	        		"\n\n\n\n}";
	        arq = new FileWriter("/temp/obj/repository/"+tx_obj.replace("Obj", "")+"Repository.java");
	        gravarArq = new PrintWriter(arq);
	        gravarArq.printf(field);
	        gravarArq.close();
	        
	        //Criar Join Dados Usuário
	        field = "import br.com.jtpsolution.dao.util.outros.TabFolderUsuarioObj;\n\n";	        	        
	        field += "public class "+tx_obj.replace("Tab", "TabJoin").replace("Obj","")+" {" + 
	        		"\n\n";
	        field += tx_obj+" "+arrumaCampoMinusculaObj(tx_obj)+";\n";
	        field += "TabFolderUsuarioObj tabFolderUsuarioObj;\n\n";
	        field += "}\n";
	        
	        arq = new FileWriter("/temp/obj/join/"+tx_obj.replace("Tab", "TabJoin").replace("Obj","")+".java");
	        gravarArq = new PrintWriter(arq);
	        gravarArq.printf(field);
	        gravarArq.close();
	        
	        //CheckList    
	      conn.close();
	      if (ck_eof == false) {
	       //System.out.println(field);	  
	       System.out.println("Classe "+tx_obj+" gerada com sucesso!");	
	      }else {
		   System.out.println("Tabela não encontrada!");
	      }

	    }catch (Exception e) {
			  e.printStackTrace();
	    }
		
	}
	
	public String arrumaCampo(String campo) {
		
		String[] tx = campo.split("_");
		String tx1 = "";
		for (int i = 0; i<=tx.length-1; i++) {
			
			if (i == 0) {
				tx1 += tx[i];
			}
			
			if (i > 0) {
				String tx2 = tx[i].substring(0, 1).toUpperCase();
				String tx3 = tx[i].substring(1, tx[i].length());
				tx1 += tx2+tx3;
			}
			
		}
		
		return tx1;
	}
	
	public String arrumaCampoObj(String campo) {
		
		String[] tx = campo.split("_");
		String tx1 = "";
		for (int i = 0; i<=tx.length-1; i++) {
						
			if (i >= 0) {
				String tx2 = tx[i].substring(0, 1).toUpperCase();
				String tx3 = tx[i].substring(1, tx[i].length());
				tx1 += tx2+tx3;
			}
			
		}
		
		return tx1;
	}
	
	public String arrumaCampoMinusculaObj(String campo) {
		
		String[] tx = campo.split("_");
		String tx1 = "";
		for (int i = 0; i<=tx.length-1; i++) {
						
			if (i >= 0) {
				String tx2 = tx[i].substring(0, 1).toLowerCase();  //Minusculo 1a. letra
				String tx3 = tx[i].substring(1, tx[i].length());
				tx1 += tx2+tx3;
			}
			
		}
		
		return tx1;
	}

	

	

}
