package br.com.gohosp.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.StringTokenizer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GeneralParser {

	public static void main(String[] args) {
		
	  
   	     //System.out.println(GeneralParser.retornaDataPosteriorDiasCorridos(new Date(), 1));
   	     //System.out.println(convertVlPagarME("11115"));
   	    // System.out.println(doubleToStringMoneyMask(15450.00,"######.00").replace(",", "."));
		//System.out.println(doubleToString(value));
		
		//System.out.println(new GeneralParser().format_dateUS("Sun Aug 12 10:00:00 BRT 2018", "yyyy-MM-dd"));
		
		
		System.out.println(new GeneralParser().DiferencaMinutosEntreData(GeneralParser.parseDateTime("14/11/2019 14:13:00"),new Date()));
	}
	
	
	public static String convartDataExtenso(Date data) {
		
		 // PEGO AQUI A DATA ATUAL
        Date dataAtual = data;

        // CRIO AQUI UM FORMATADOR
        // PASSANDO UM ESTILO DE FORMATAÇÃO : DateFormat.FULL
        // E PASSANDO UM LOCAL DA DATA : new Locale("pt", "BR")
        DateFormat formatador = DateFormat.getDateInstance(DateFormat.FULL, new Locale("pt", "BR"));

        // FORMATO A DATA, O FORMATADOR ME RETORNA 
        // A STRING DA DATA FORMATADA 
        String dataExtenso = formatador.format(dataAtual);


        // MOSTRA A DATA
        //System.out.println("Data com o dia da semana : " + dataExtenso);


        // AQUI É CASO VOCÊ QUEIRA TIRAR
        // O DIA DA SEMANA QUE APARECE NO 
        // PRIMEIRO EXEMPLO
        int index  = dataExtenso.indexOf(",");
        int lenght = dataExtenso.length();


        // MOSTRA A DATA
        //System.out.println("Data sem o dia da semana :  " + dataExtenso.substring(++index, lenght));

        return dataExtenso.substring(++index, lenght);
		
		
	}
	
	public static String convertObjJson(Object object) {
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			String simpleJSON = mapper.writeValueAsString(object);
			//System.out.println(simpleJSON);
			return simpleJSON; 
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "erro";
		}
		
	}
	
	public static String format_dateUS(String dataString, String txFormat) {
		try {
			dataString = new SimpleDateFormat(txFormat)
					.format(new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy", Locale.US)
							.parse(dataString));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataString;
	}

	public static BigDecimal parserBigDecimal(double valor) {
		
		return new BigDecimal(valor);
	}
	
	public static BigDecimal parserBigDecimal(String valor) {
		
		return new BigDecimal(valor);
	}
	
	public static double arredondaValor(double valor, Integer casasdecimais) {
		
		BigDecimal bd = null;
		try {
			
			bd = new BigDecimal(valor).setScale(casasdecimais, RoundingMode.HALF_EVEN);
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return bd.doubleValue();
	}
	
	public static String convertISO8859ToUTF8(String s) {
		String out = s;
        //String out = null;
        
        try {
            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }
	
	public static String convertUTF8TOIso8859(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("ISO-8859-1"), "UTF-8");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }
	
	public static double convertVlPagarME(String valor) {
		double vl = 0;
		if (valor.length() == 1) {
			vl = GeneralParser.parseDouble("0,0"+valor);
		}else if (valor.length() == 2){
			vl = GeneralParser.parseDouble("0,"+valor);
		}else if (valor.length() > 2){			
			vl = GeneralParser.parseDouble(valor.substring(0,valor.length()-2)+","+valor.substring(valor.length()-2,valor.length()));
		}
		
		return vl;
	}
		
	public static Calendar stringToCalendar(String date, String tx_formato) throws ParseException {
	    SimpleDateFormat format = new SimpleDateFormat(tx_formato);
	    Date d = GeneralParser.parseDate(tx_formato, date);
	    Calendar c = format.getCalendar();
	    c.setTime(d);
	    //System.out.println(c);
	    return c;
	}
	
	public static Date retornaDataPosteriorHorasCorridos(Date date, Integer qtde_horas) {
		
		Date dt_retorno = null;
		try {
			Calendar cal = Calendar.getInstance();
			Date data = new Date(date.getTime());
			cal.setTime(data);
			cal.add(Calendar.HOUR, + qtde_horas);
			
			dt_retorno = new Date(cal.getTime().getTime());
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return dt_retorno;
	}


	
	public static Date retornaDataPosteriorDiasCorridosUteis(Date date, Integer qtde_dias) {
		
		Date dt_retorno = null;
		try {
			Calendar cal = Calendar.getInstance();
			Date data = new Date(date.getTime());
			cal.setTime(data);
			cal.add(Calendar.DATE, + qtde_dias);
			
			if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
		    {
				cal.add(Calendar.DATE, 1);
		        System.out.println("Eh domingo, mudando data para +1 dias");
		    }
		    // se for s�bado
		    else if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
		    {
		    	cal.add(Calendar.DATE, 2);
		        System.out.println("Eh sabado, mudando data para +2 dias");
		    }
			
			dt_retorno = new Date(cal.getTime().getTime());
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return dt_retorno;
	}
	
	public static Date retornaDataPosteriorDiasCorridos(Date date, Integer qtde_dias) {
		
		Date dt_retorno = null;
		try {
			Calendar cal = Calendar.getInstance();
			Date data = new Date(date.getTime());
			cal.setTime(data);
			cal.add(Calendar.DATE, + qtde_dias);
			
			dt_retorno = new Date(cal.getTime().getTime());
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return dt_retorno;
	}
	
	public static Date retornaDataAnteriorDiasCorridos(Date date, Integer qtde_dias) {
		
		Date dt_retorno = null;
		try {
			Calendar cal = Calendar.getInstance();
			Date data = new Date(date.getTime());
			cal.setTime(data);
			cal.add(Calendar.DATE, - qtde_dias);
			
			dt_retorno = new Date(cal.getTime().getTime());
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return dt_retorno;
	}
	
	public static String HTMLToISO(String tx_string) {
		
		if (!Validator.isBlankOrNull(tx_string)) {
		 tx_string = tx_string.replaceAll("%2d", "-");
		 tx_string = tx_string.replaceAll("%2e", ".");
		 tx_string = tx_string.replaceAll("%2f", "/");
		 tx_string = tx_string.replaceAll("%3a", ":");
		}
		
		return tx_string;
	}
	
	
	public static String doubleToString(double value, String tx_mascara) {
		String result = "";

		try {
			DecimalFormat decimalFormatter = new DecimalFormat("#,###,###,##"+tx_mascara); 
			result = decimalFormatter.format(value);
		}catch (Exception ex) {
			result = "";	
		}

		return result;
	}	

	
	public static String doubleToString(double value) {
		String result = "";

		try {
			DecimalFormat decimalFormatter = new DecimalFormat("#,###,###,##0.00"); 
			result = decimalFormatter.format(value);
		}catch (Exception ex) {
			result = "";	
		}

		return result;
	}	

	
	/*
	public static String formatUltimoDiaMes(Date data){
		
		Integer Mes = parseInt(format_date("MM", data));
		Integer Ano = parseInt(format_date("yyyy", data));
		
		BeanLibGlobal lb = new BeanLibGlobal();
		
		Calendar cal = new GregorianCalendar(Ano, Mes - 1, 1);		
		return    cal.getActualMaximum(Calendar.DAY_OF_MONTH)+"/"+ BeanLibGlobal.setFormatZeroEsq(Mes.toString(),2)+"/"+Ano.toString();
	}
	*/	

	
	
	 public static String format_dateBRMySql(java.util.Date data) {
		 try {
			 SimpleDateFormat format_date = new SimpleDateFormat("yyyy-MM-dd");
			 return format_date.format(data);
		 } catch (RuntimeException e) {
			 return null;
		 }
	}     	
	
	public static final String STANDARD_ID_SEPARATOR = ":";
	
	public static int parseInt(String value){
	  int result = 0;
	  
	  if (value != null) {
	    try{
	      result = Integer.parseInt(value);
	    }catch(Exception e){
	      	
	    }
	  }
	  
	  return result;
	}
	
	/*
	public static String RetornaDt2_Dt1(Date dt1, Date dt2){
		String valor = "";
		BeanLibGlobal lb = new BeanLibGlobal();
		
  	    int delta = (int)((dt2.getTime() - dt1.getTime()) / 1000L); // seconds   
	    int h = delta / 3600;   
	    int m = delta / 60 % 60;   
	    int s = delta % 60;   
	    //System.out.printf("%02d:%02d:%02d\n", h, m, s);  
		
		valor = BeanLibGlobal.setFormatZeroEsq(String.valueOf(h), 2)+":"+BeanLibGlobal.setFormatZeroEsq(String.valueOf(m), 2)+":"+BeanLibGlobal.setFormatZeroEsq(String.valueOf(s), 2);
		
		//System.out.println(valor);
		
		return valor;
	}*/
	
	public static double parseDouble2(String value) throws NumberFormatException {
		double result;

		/** Caso necess�rio, troca v�rgula por ponto */
		int index = value.lastIndexOf(",");
		if (index != -1) {
			String tmp = value.substring(0, index);
			tmp += ".";
			tmp += value.substring(index + 1);
			value = tmp;
		}

		result = Double.parseDouble(value);

		return result;
	}

	public static double parseDouble(String value) throws NumberFormatException {
		double result=0;

        try {
		/** Caso necess�rio, troca v�rgula por ponto */
/*
        int index = value.lastIndexOf(",");
		if (index != -1) {
			String tmp = value.substring(0, index);
			tmp += ".";
			tmp += value.substring(index + 1);
			value = tmp;
			}
*/			
        	
          String valor = value;
          if (value.indexOf(",") > -1) {          
                 valor = value.replace('.', ' ');
                 valor = valor.replaceAll(" ", "");                 
        	     valor = valor.replace(',', '.');
          }	     
 
		result = Double.parseDouble(valor);
		}catch (Exception ex) {
			
		}
		return result;
	}
	
	public static Integer DiferencaEntreDatas(Date dt1, Date dt2) {
		
         long dt = (dt2.getTime() - dt1.getTime()) + 3600000;      
         long dias = (dt / 86400000L);  
         return (int)dias; 
		
		
	}
	
	public static Integer DiferencaMinutosEntreData(Date dtInicial, Date dtFinal) {
	
		long horas = (dtFinal.getTime() - dtInicial.getTime()) / 3600000;
		long minutos = (dtFinal.getTime() - dtInicial.getTime() - horas*3600000) / 60000;
		
		//long difference = Math.abs(dt1.getTime() - dt2.getTime());

		// faça isso:
		//long minutes = difference / (1000 * 60); // isso trunca

		// ou isso:
		//int minutes = difference / (1000 * 60); // pega os minutos
		//difference -= minutes * 60000; // desconta o valor já retirado
		//int seconds = difference / 1000; // pega os segundos
		
		
		return (int)minutos;
	}

	public static float parseFloat(String value) throws NumberFormatException {
		float result=0;

        try {
		/** Caso necess�rio, troca v�rgula por ponto */
/*
        int index = value.lastIndexOf(",");
		if (index != -1) {
			String tmp = value.substring(0, index);
			tmp += ".";
			tmp += value.substring(index + 1);
			value = tmp;
			}
*/			
        	
          String valor = value;
          if (value.indexOf(",") > -1) {          
                 valor = value.replace('.', ' ');
                 valor = valor.replaceAll(" ", "");                 
        	     valor = valor.replace(',', '.');
          }	     
 
		result = Float.parseFloat(valor);
		}catch (Exception ex) {
          
		}
		return result;
	}
		
	public static Date parseDate(String value) {
	  Date result = null;
      
	  if (value != null) {
		try {
			SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
			formater.setLenient(false);
			result = formater.parse(value);
		} catch (ParseException e) {
			
		}
      }	
	  
	  return result;
	}


	
	public static java.sql.Date parseDateSQL(String inData) {
		java.util.Date auxData;
		java.sql.Date outData;
		int dia = 0, mes = 0, ano = 0;
		Calendar calendario;
		calendario = Calendar.getInstance();
		
		try{
			StringTokenizer tokens = new StringTokenizer(inData, "/");
			dia = Integer.parseInt(tokens.nextToken());
			mes = Integer.parseInt(tokens.nextToken());
			ano = Integer.parseInt(tokens.nextToken());
		
			calendario.set(ano, (mes - 1), dia, 0, 0, 0); // os meses comeam por 0
			calendario.set(Calendar.MILLISECOND, 0);
			auxData = calendario.getTime();
		
			outData = new java.sql.Date(auxData.getTime());
			return outData;
		}catch (Exception e) {
		  return null;	
		}	
	}
	
	public static Date parseDateMySql(String value) {
		Date result = null;
      if (value != null) {
		try {
			SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
			formater.setLenient(false);
			result = formater.parse(value);
		} catch (ParseException e) {
			
		}
      }	
		return result;
	}
	
	public static Date parseDateUSA(String value) {
		Date result = null;
      if (value != null) {
		try {
			SimpleDateFormat formater = new SimpleDateFormat("MM/dd/yyyy");
			formater.setLenient(false);
			result = formater.parse(value);
		} catch (ParseException e) {
			
		}
      }	
		return result;
	}
	
	public static Date parseDateTime(String value) {
		Date result = null;
      if (value != null) {
		try {
			SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			formater.setLenient(false);
			result = formater.parse(value);
		} catch (ParseException e) {
			
		}
      }	
		return result;
	}

	public static Date parseDateTime(String tx_formato, String value) {
		Date result = null;
      if (value != null) {
		try {
			SimpleDateFormat formater = new SimpleDateFormat(tx_formato);
			formater.setLenient(false);
			result = formater.parse(value);
		} catch (ParseException e) {
			
		}
      }	
		return result;
	}
	
	
	public static Date parseDateTimeUSA(String value) {
	  Date result = null;
      
	  if (value != null) {
		try {
			SimpleDateFormat formater = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			formater.setLenient(false);
			result = formater.parse(value);
		} catch (ParseException e) {
			
		}
      }	
	  
	  return result;
	}

	public static java.sql.Timestamp parseTimestamp(java.util.Date inData) {
		return new java.sql.Timestamp(inData.getTime());
	}
	
	public static String TimestampToString(java.sql.Timestamp inData) {
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		formatador.setLenient(false);
		return formatador.format(inData);
	}

	public static Date parseDate(String tx_formato, String value) {
		Date result = null;
      if (value != null) {
		try {
			SimpleDateFormat formater = new SimpleDateFormat(tx_formato);
			formater.setLenient(false);
			result = formater.parse(value);
		} catch (ParseException e) {

			System.out.println(e.getMessage());
		}
      }	
		return result;
	}
	
	
	public static Set parseIds(String ids) {
		return GeneralParser.parseIds(ids, GeneralParser.STANDARD_ID_SEPARATOR);
	}

	public static Set parseIds(String ids, String separator) {
		Set set = new HashSet();
		StringTokenizer st = new StringTokenizer(ids, ":");

		// Cria uma collection com ids
		while (st.hasMoreTokens()) {
			String nt = st.nextToken();
			if (!Validator.isBlankOrNull(nt))
				set.add(nt);
		}

		return set;
	}

	public static String doubleToStringMoney(double value) {
			
		DecimalFormat decimalFormatter = new DecimalFormat("#,###,###,##0.00"); 
		String result = decimalFormatter.format(value);
		
		return result;
	}

	public static String doubleToStringSemFormatacao(double value) {
		
		DecimalFormat decimalFormatter = new DecimalFormat("#########0.00"); 
		String result = decimalFormatter.format(value);
		
		return result;
	}
	
	
	public static String floatToStringMoney(float value) {
		String result = "";
		
        try {
		DecimalFormat decimalFormatter = new DecimalFormat("#,###,###,##0.00"); 
		result = decimalFormatter.format(value);
        }catch (Exception ex) {
        	
         result = "";	
        	
		}
		
		return result;
	}
	
	public static String doubleToStringMoneyMask(double value, String mask) {
		String result = "";
        try {
		DecimalFormat decimalFormatter = new DecimalFormat(mask); 
		result = decimalFormatter.format(value);
        }catch (Exception ex) {
        	
         result = "";	
        	
		}
		
		return result;
	}	


	public static String floatToString(float value, String tx_mascara) {
		String result = "";
		
        try {
		DecimalFormat decimalFormatter = new DecimalFormat("#,###,###,##"+tx_mascara); 
		result = decimalFormatter.format(value);
        }catch (Exception ex) {
        	
         result = "";	
        	
		}
		
		return result;
	}

	
	public static String format_date(String format, java.util.Date data) {
	    SimpleDateFormat format_date = new SimpleDateFormat(format);
	    return format_date.format(data);
 }

 public static String format_dateBR(java.util.Date data) {
	    SimpleDateFormat format_date = new SimpleDateFormat("dd-MM-yyyy");
	    return format_date.format(data);
 }     

 public static String format_dateBRSBarras(java.util.Date data) {
	    SimpleDateFormat format_date = new SimpleDateFormat("ddMMyyyy");
	    return format_date.format(data);
 }      
 
 public static String format_dateBR2(java.util.Date data) {
	    SimpleDateFormat format_date = new SimpleDateFormat("dd/MM/yyyy");
	    return format_date.format(data);
}     
 
 public static String format_dateHSBR(java.util.Date data) {
	    SimpleDateFormat format_date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	    return format_date.format(data);
 }     

 public static String format_dateHSBR(String tx_formato, Timestamp data) {
	    SimpleDateFormat format_date = new SimpleDateFormat(tx_formato);
	    return format_date.format(data);
}     

 public static String format_dateHSBR(Timestamp data) {
	    SimpleDateFormat format_date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	    return format_date.format(data);
}
 
 public static String format_HSBR(java.util.Date data) {
	    SimpleDateFormat format_date = new SimpleDateFormat("HH:mm:ss");
	    return format_date.format(data);
}     

 public static String format_HSBR(Timestamp data) {
	    SimpleDateFormat format_date = new SimpleDateFormat("HH:mm:ss");
	    return format_date.format(data);
}     

 
 public static String format_dateHSSQL(java.util.Date data) {
	    SimpleDateFormat format_date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    return format_date.format(data);
}     

 public static String format_dateHSStr(java.util.Date data) {
	    SimpleDateFormat format_date = new SimpleDateFormat("yyyyMMddHHmmss");
	    return format_date.format(data);
}     
 
 
public static String format_dateSQL(java.util.Date data) {
  SimpleDateFormat format_date = new SimpleDateFormat("yyyy/MM/dd");
  return format_date.format(data);
}      
 
	public static String isnull(Object valor, String nulo) {
		// M�todo criado para retornar o par�metro nulo, caso valor seja null
		String tx_result = "";
		if (valor != null) {
			tx_result = valor.toString();
		} else
			tx_result = nulo;
		return tx_result;
	}
 
	
	public  static String isnull(String valor, String nulo) {
		// M�todo criado para retornar o par�metro nulo, caso valor seja null
		String tx_result = "";
		if (valor != null && !valor.equals("null")) 
			tx_result = valor;
        else
			tx_result = nulo;			
		
		return tx_result;
	}
 

	public static String setFormatZeroEsq(String valor, int ntotal){

	     int valstr = valor.length();
	     valstr = ntotal-valstr;
	     String val = "";

	     for (int i=1;i <= valstr; i++){
	      val += "0";
	     }

	     return val+valor;

	   }

	public static String setFormatEspacoDireita(String valor, int ntotal){

		 int valstr = valor.length();
	     valstr = ntotal-valstr;
	     String val = "";

	     for (int i=1;i <= valstr; i++){
	      val += " ";
	     }

	     return valor+val;

	   }

	public static String setFormatCaracterDireita(String valor, int ntotal, String caracter){

	     int valstr = valor.length();
	     valstr = ntotal-valstr;
	     String val = "";

	     for (int i=1;i <= valstr; i++){
	      val += caracter;
	     }

	     return valor+val;

	   }


	public static String setFormatEspacoEsq(String valor, int ntotal){

	     int valstr = valor.length();
	     valstr = ntotal-valstr;
	     String val = "";

	     for (int i=1;i <= valstr; i++){
	      val += " ";
	     }

	     return val+valor;

	   }

	public static String floatToStringMoney(String tx_formato, float value) {
		String result = "";
		
        try {
		DecimalFormat decimalFormatter = new DecimalFormat(tx_formato); 
		result = decimalFormatter.format(value);
        }catch (Exception ex) {
        	
         result = "";	
        	
		}
		
		return result;
	}
	
	
	public static long dateToLong(String date) {
		long dateInLong = 0;
		try {
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");	    
		Date data = formatter.parse(date);
		dateInLong = data.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		return dateInLong;
		
	}
	
	
	public static String getHoje(){   
	   	  DateFormat dfmt = new SimpleDateFormat("EEEE, d MMMM yyyy");   
	      Date hoje = Calendar.getInstance(Locale.getDefault()).getTime();   
	      //System.out.println(dfmt.format(hoje)); 
   
	    return dfmt.format(hoje);   
	  }   
	
	
	/**Character   Description *  
	 * #           Qualquer numero valido. *
	 * 
	 *  '           Usado para n�o usar nenhum caracter especial na formatacao ("\n", "\t"....) 
	 *  U           Qualquer caracter *  Todas as letras minusculas sao passadas para maiuscula. 
	 *  * *  L           Qualquer caracter * Todas as letras maiusculas sao passadas para minusculas 
	 *  * *  A          Qualquer caracter ou numero *  ( Character.isLetter or Character.isDigit ) 
	 *  * *  ?           Qualquer caracter ( Character.isLetter ). 
	 *  * *  *           Qualquer Coisa. 
	 *  * *  H           Qualquer caracter hexa (0-9, a-f ou A-F). 
	 *  * * ==================================== 
	 *  * ex: 
	 *  * value = "A1234B567Z" 
	 *  * mask = "A-AAAA-AAAA-A" 
	 *  * output : A-1234-B567-Z * * =================================== 
	 *  * @param string 
	 *  * @param mask 
	 *  * @return 
	 *  * @throws java.text.ParseException 
	 *  */
	public static String formatString(String string, String mask) { 
		javax.swing.text.MaskFormatter mf = null;   
		String retorno = "";
		try {
		 mf = new javax.swing.text.MaskFormatter(mask);	
		 mf.setValueContainsLiteralCharacters(false);
		 retorno = mf.valueToString(string);
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return retorno;
	}	
	
	
}
