package br.com.gohosp.util.log.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.gohosp.dao.administracao.TabTelasObj;
import br.com.gohosp.dao.util.log.VwTabLogObj;
import br.com.gohosp.dao.util.log.repository.VwTabLogRepository;
import br.com.gohosp.modulos.administrador.service.TabTelasService;

@Controller
@RequestMapping("/logfield")
public class LogController {

	@Autowired 
	private VwTabLogRepository tabLogRepository;
	
	@Autowired 
	private TabTelasService tabTelasService;
	
	
	
	@RequestMapping(value = "/{txServiceClass}/{txField}/{txRef}")
	public @ResponseBody List<VwTabLogObj> logfield(@PathVariable String txServiceClass, @PathVariable String txField, @PathVariable String txRef) {
		
		List<VwTabLogObj> listLog = tabLogRepository.findByTxServiceAndTxCampoAndTxRefOrderByDtDataDesc(txServiceClass, txField, txRef);
		
		return listLog;

	}

	
	@RequestMapping(value = "/verificakeyfield/{txServiceClass}/{txField}")
	public @ResponseBody String verificakeyfield(@PathVariable String txServiceClass, @PathVariable String txField) {
	
		TabTelasObj tabTela = tabTelasService.consultarService(txServiceClass);
		
		return tabTela.getTxKeyfield();

	}

	
	
}
