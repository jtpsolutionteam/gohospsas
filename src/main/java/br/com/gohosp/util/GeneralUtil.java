package br.com.gohosp.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.json.JSONObject;

import br.com.gohosp.Constants;

public class GeneralUtil {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// new GeneralUtil().ApagarArquivosDiretorioGeradores();

		// new GeneralUtil().LerIdHTML("/temp/outros.html", "select");

		//System.out.println(new GeneralUtil().LerArquivo("/temp/di.txt"));
		
		System.out.println(new GeneralUtil().TiraNaonumero("24 dias"));
	}
	
	
	public static boolean getProducao() {
		try {			
			//System.out.println(InetAddress.getLocalHost().getHostAddress());
			if (InetAddress.getLocalHost().getHostAddress().equals("172.31.61.235")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
		}
		return false;
	}
	
	public static boolean isInteger(String strVl) {
		
		try {
			
		  Integer vl =	Integer.parseInt(strVl);
		  return true;
			
		}catch (Exception ex) {
			return false;
		}
		
	}
	
	public static String TiraNaonumero(String str) {
		
		String arr_str[] = Pattern.compile("").split(str);
		String strValor = "";
		for (int i=0; i<=arr_str.length-1; i++) {
			//System.out.println(arr_str[i]);
			if (isInteger(arr_str[i]))
			strValor += arr_str[i];
		}
		
		return strValor;
	}
	
	
	public void DeletarArquivo(String tx_arquivo) {
	    
		 File f = new File(tx_arquivo);
		 try {
		 f.delete();
		 }catch (Exception ex) {
		     
		 }
		    
		}
	
	
	public static String returnWhereQueryRelatorio(Object object, String query) {
		
		String StrSql = query;
		try {
		
		Field[] campos = object.getClass().getDeclaredFields();
		
		for (int i = 0; i < campos.length; i++) {
			campos[i].setAccessible(true);
			//System.out.println(campos[i].getName());
			String txType = campos[i].getType().toString().toLowerCase();

			if (campos[i].get(object) != null && !campos[i].get(object).equals("") && !campos[i].getName().equals("cdRelatorio")) {
		    		 //System.out.println(campos[i].getName());
				     //System.out.println(campos[i].get(object));
				     if (txType.contains("string") && !campos[i].getName().equals("dtDataFiltro") && !campos[i].getName().equals("txCampoFiltro") && !campos[i].getName().equals("txCampoValueFiltro")) {
				       StrSql += " and "+campos[i].getName()+"='"+campos[i].get(object)+"'";
				     }else if (txType.contains("integer")) {
					       StrSql += " and "+campos[i].getName()+"="+campos[i].get(object)+"";
				     }else if (campos[i].getName().equals("dtDataFiltro")) {
				    	StrSql += " and date("+campos[i].get(object)+") ";
				     }else if (campos[i].getName().equals("dtDataFiltroInicial")) {
				    	 StrSql += " between '"+GeneralParser.format_dateUS(campos[i].get(object).toString(),"yyyy-MM-dd")+"' ";
					 }else if (campos[i].getName().equals("dtDataFiltroFinal")) {
						 StrSql += " and '"+GeneralParser.format_dateUS(campos[i].get(object).toString(),"yyyy-MM-dd")+"' ";
					 }else if (campos[i].getName().equals("txCampoFiltro")) {
						 StrSql += " and "+campos[i].get(object);
					 }else if (campos[i].getName().equals("txCampoValueFiltro")) {
						 StrSql += " = '"+campos[i].get(object)+"'";
					 }

		    	}
			}

		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return StrSql;
	}
	

	private StringBuilder tx_where = new StringBuilder();

	public void set_filter(String filtro) {
		if (filtro != null)
			if (!filtro.equals("")) {
				String aux[] = filtro.split(";"); // Separa os elementos para cada campos do filtro

				for (int x = 0; x < aux.length; x++) {
					String valor = "";
					String[] aux2 = null; // Separa os campos dos valores de cada elemento

					if (aux[x].indexOf("!") > -1) {
						setFilterLIKE(aux[x].replace("!", "="));
					} else {
						aux2 = aux[x].split("=");

						if (aux2.length < 2)
							valor = "";
						else
							valor = aux2[1];

						// localiza_registro_unico(aux2[0], valor); Incorpora o filtro no SQL do grid
						// corrente
						if (!(tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
								&& !(aux2[0].toUpperCase().indexOf("BETWEEN") == -1)) {
							tx_where.append(" and " + aux2[0]);
						} else if ((tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
								&& !(aux2[0].toUpperCase().indexOf("BETWEEN") == -1)) {
							tx_where.append(" where " + aux2[0]);
						} else if (aux2[0].toLowerCase().indexOf("null") != -1) {
							if ((tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
									&& (aux2[0].toUpperCase().indexOf("BETWEEN") == -1)) {
								tx_where.append(" where " + aux2[0]);
							} else if (!(tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
									&& (aux2[0].toUpperCase().indexOf("BETWEEN") == -1)) {
								tx_where.append(" and " + aux2[0]);
							}
						} else if (aux2[0].toLowerCase().indexOf(">") != -1) {
							if ((tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
									&& (aux2[0].toUpperCase().indexOf("BETWEEN") == -1)) {
								tx_where.append(" where " + aux2[0]);
							} else if (!(tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
									&& (aux2[0].toUpperCase().indexOf("BETWEEN") == -1)) {
								tx_where.append(" and " + aux2[0]);
							}
						} else if (aux2.length > 1 && aux2[1].toLowerCase().indexOf("<") != -1) {
							if ((tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
									&& (aux2[0].toUpperCase().indexOf("BETWEEN") == -1)) {
								tx_where.append(" where " + aux2[0] + " < '" + valor.substring(1) + "'");
							} else if (!(tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
									&& (aux2[0].toUpperCase().indexOf("BETWEEN") == -1)) {
								tx_where.append(" and " + aux2[0] + " < '" + valor.substring(1) + "'");
							}
						} else if (aux2[0].toLowerCase().indexOf("[in]") != -1) {
							String var_aux = aux2[0].toLowerCase().replace("[in]", "in");
							String aux3[] = var_aux.split("in");
							valor = aux3[1];

							if ((tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
									&& (aux2[0].toUpperCase().indexOf("BETWEEN") == -1)) {
								tx_where.append(" where " + aux3[0] + " IN (" + valor + ")");
							} else if (!(tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
									&& (aux2[0].toUpperCase().indexOf("BETWEEN") == -1)) {
								tx_where.append(" and " + aux3[0] + " IN (" + valor + ")");
							}
						} else if (aux2[0].toLowerCase().indexOf("[not in]") != -1) {
							String var_aux = aux2[0].toLowerCase().replace("[not in]", " not in ");
							String aux3[] = var_aux.split("not in");
							String aux4[] = aux3[1].split("\\|");
							valor = aux4[0];

							if ((tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
									&& (aux2[0].toUpperCase().indexOf("BETWEEN") == -1)) {
								tx_where.append(" where " + aux3[0] + " NOT IN (" + valor + ")");
							} else if (!(tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
									&& (aux2[0].toUpperCase().indexOf("BETWEEN") == -1)) {
								tx_where.append(" and " + aux3[0] + " NOT IN (" + valor + ")");
							}
						} else if ((tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
								&& (aux2[0].toUpperCase().indexOf("BETWEEN") == -1)) {
							tx_where.append(" where " + aux2[0] + " = '" + valor + "'");
						} else if (!(tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
								&& (aux2[0].toUpperCase().indexOf("BETWEEN") == -1)) {
							tx_where.append(" and " + aux2[0] + " = '" + valor + "'");
						}
					}
				}
			}
	}

	public void setFilterLIKE(String filtro) {
		if (!Validator.isBlankOrNull(filtro)) {
			String aux[] = filtro.split(";"); // -- Separa os elementos para
												// cada campos do filtro

			for (int x = 0; x < aux.length; x++) {
				String valor = "";
				String aux2[] = aux[x].split("="); // -- Separa os campos dos
													// valores de cada elemento

				if (aux2.length < 2)
					valor = "";
				else
					valor = aux2[1];

				// localiza_registro_unico(aux2[0], valor); //-- Incorpora o
				// filtro no SQL do grid corrente
				if (!Validator.isBlankOrNull(valor)) {
					if (tx_where.toString().toUpperCase().indexOf("WHERE") == -1) {
						tx_where.append(" where " + aux2[0] + " like '%" + valor + "%'");
					} else {
						tx_where.append(" and " + aux2[0] + " like '%" + valor + "%'");
					}
				} else {
					if (tx_where.toString().toUpperCase().indexOf("WHERE") == -1) {
						tx_where.append(" where " + aux2[0] + " is null");
					} else {
						tx_where.append(" and " + aux2[0] + " is null");
					}
				}
			}
		}
	}

	public void setFilterLikeDireita(String filtro) {
		if (!Validator.isBlankOrNull(filtro)) {
			String aux[] = filtro.split(";"); // -- Separa os elementos para
												// cada campos do filtro

			for (int x = 0; x < aux.length; x++) {
				String valor = "";
				String aux2[] = aux[x].split("="); // -- Separa os campos dos
													// valores de cada elemento

				if (aux2.length < 2)
					valor = "";
				else
					valor = aux2[1];

				// localiza_registro_unico(aux2[0], valor); //-- Incorpora o
				// filtro no SQL do grid corrente
				if (!Validator.isBlankOrNull(valor)) {
					if (tx_where.toString().toUpperCase().indexOf("WHERE") == -1) {
						tx_where.append(" where " + aux2[0] + " like '" + valor + "%'");
					} else {
						tx_where.append(" and " + aux2[0] + " like '" + valor + "%'");
					}
				} else {
					if (tx_where.toString().toUpperCase().indexOf("WHERE") == -1) {
						tx_where.append(" where " + aux2[0] + " is null");
					} else {
						tx_where.append(" and " + aux2[0] + " is null");
					}
				}
			}
		}
	}

	public void setFilterIN(String filtro) {
		String tx_aux = "";

		if (!Validator.isBlankOrNull(filtro)) {
			String aux[] = filtro.split(";"); // -- Separa os elementos para
												// cada campos do filtro

			for (int x = 0; x < aux.length; x++) {
				String aux2[] = aux[x].split("="); // -- Separa os campos dos
													// valores de cada elemento

				if (tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
					tx_aux = " where ";
				else
					tx_aux = " and ";

				tx_where.append(tx_aux + aux2[0] + " in (" + aux2[1] + ") ");
			}
		}
	}

	public void setFilterNotIN(String filtro) {
		String tx_aux = "";

		if (!Validator.isBlankOrNull(filtro)) {
			String aux[] = filtro.split(";"); // -- Separa os elementos para
												// cada campos do filtro

			for (int x = 0; x < aux.length; x++) {
				String aux2[] = aux[x].split("="); // -- Separa os campos dos
													// valores de cada elemento

				if (tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
					tx_aux = " where ";
				else
					tx_aux = " and ";

				tx_where.append(tx_aux + aux2[0] + " not in (" + aux2[1] + ") ");
			}
		}
	}

	public void setFilterOR(String filtro) {
		String tx_aux = "";

		if (!Validator.isBlankOrNull(filtro)) {
			String aux[] = filtro.split(",");
			String tx_filtro = " (";

			for (int x = 0; x < aux.length; x++) {
				tx_filtro += aux[x] + " or ";
			}

			tx_filtro = tx_filtro.substring(0, tx_filtro.length() - 4) + ") ";

			if (tx_where.toString().toUpperCase().indexOf("WHERE") == -1)
				tx_aux = " where ";
			else
				tx_aux = " and ";

			tx_where.append(tx_aux + tx_filtro);
		}
	}

	public static String LimpaMensagemException(String mensagem) {

		String[] tx = mensagem.split("\\'");

		return tx[1];

	}

	public static String MessageExceptionConstraint(ConstraintViolationException ex) {

		Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
		ConstraintViolationImpl violation = (ConstraintViolationImpl) violations.iterator().next();

		return violation.getMessage();
	}

	public static String ExceptionStackTrace(Exception e) {
		StackTraceElement Stl;
		String StackTrace = "<tx_erro> \n";

		for (int i = 0; i < e.getStackTrace().length; i++) {
			Stl = e.getStackTrace()[i];
			if (Stl.getClassName().indexOf(Constants.SCHEMA) > -1) {
				StackTrace += "<dados> \n";
				StackTrace += "<tx_nivel>Nivel " + i + "</tx_nivel> \n";
				StackTrace += "<tx_classe>Classe: " + Stl.getClassName() + "</tx_classe> \n"; // Mostra o nome da classe
				StackTrace += "<tx_metodo>Metodo: " + Stl.getMethodName().replaceAll("<init>", "init")
						+ "</tx_metodo> \n"; // Nome do metodo gerador do erro
				StackTrace += "<tx_arquivo>Arquivo: " + Stl.getFileName() + "</tx_arquivo> \n"; // Nome do arquivo
				StackTrace += "<tx_linha>Linha: " + Stl.getLineNumber() + "</tx_linha> \n"; // Linha no arquivo
				StackTrace += "</dados> \n";
			}
		}

		StackTrace += "</tx_erro> \n";

		return StackTrace;
	}

	public void ApagarArquivosDiretorioGeradores() {

		ApagarArquivosJavaDiretorio("/temp/obj");
		ApagarArquivosJavaDiretorio("/temp/obj/service");
		ApagarArquivosJavaDiretorio("/temp/obj/controller");
		ApagarArquivosJavaDiretorio("/temp/obj/join");
		ApagarArquivosJavaDiretorio("/temp/obj/repository");
		ApagarArquivosJavaDiretorio("/temp/obj/html");
	}

	public void ApagarArquivosDiretorio(String tx_path) {

		File file = new File(tx_path);
		File afile[] = file.listFiles();
		int i = 0;
		for (int j = afile.length; i < j; i++) {
			File arquivos = afile[i];
			System.out.println(arquivos.getName());
			arquivos.delete();
		}

	}

	public void ApagarArquivosJavaDiretorio(String tx_path) {

		try {

			File file = new File(tx_path);
			File afile[] = file.listFiles();
			int i = 0;
			for (int j = afile.length; i < j; i++) {
				File arquivos = afile[i];
				System.out.println(arquivos.getName());
				if (arquivos.getName().contains(".java") || arquivos.getName().contains(".html")) {
					arquivos.delete();
				}
			}
		} catch (Exception ex) {

		}

	}

	public static String setTiranaoNumero(String valor) {

		valor = valor.replace('.', ' ');
		valor = valor.replaceAll(" ", "");
		valor = valor.replaceAll(",", "");
		valor = valor.replaceAll("-", "");
		valor = valor.replaceAll("/", "");
		return valor;
	}

	public static boolean VerificaNumeroDI(String numero) {

		boolean ck_retorno = false;

		String tx_numero = setTiranaoNumero(numero);

		Integer DigitoDi = modulo11("2" + tx_numero.substring(0, 9));

		String tx_numeroVerificado = tx_numero.substring(0, 9) + DigitoDi;

		if (tx_numeroVerificado.equals(tx_numero))
			ck_retorno = true;

		return ck_retorno;
	}

	public static int modulo11(String numero) {
		int peso, total, contador, modulo, digito;
		digito = 0;
		peso = 2;
		total = 0;
		contador = numero.length() - 1;

		while (contador >= 0) {
			// System.out.println(numero.substring(contador,contador+1)+"*"+peso+"="+Integer.parseInt(numero.substring(contador,contador+1))
			// * peso);
			total += Integer.parseInt(numero.substring(contador, contador + 1)) * peso;
			contador--;
			peso++;
			if (peso > 9)
				peso = 2;
		}
		if (total >= 11) {
			modulo = total % 11;
			if (modulo > 1) {
				digito = 11 - modulo;
			} else {
				digito = 0;
			}
		} else {
			digito = 11 - total;
		}
		return digito;
	}

	public int modulo10(String numero) {

		int posicao, multi, posicao1, posicao2, acumula, resultado, dac;
		dac = 0;
		posicao1 = numero.length() - 1;
		multi = 2;
		acumula = 0;

		while (posicao1 >= 0) {
			resultado = Integer.parseInt(numero.substring(posicao1, 1)) * multi;
			posicao2 = Integer.toString(resultado).length() - 1;
			while (posicao2 >= 0) {
				acumula += Integer.parseInt(Integer.toString(resultado).substring(posicao2, 1));
				posicao2--;
			}

			if (multi == 2)
				multi = 1;
			else
				multi = 2;

			posicao1--;
		}

		dac = acumula % 10;
		dac = 10 - dac;

		if (dac == 10)
			dac = 0;

		return dac;
	}

	public static String jsonPreparaValorString(JSONObject jsonObj, String field) {

		try {
			if (jsonObj.isNull(field)) {
				return "";
			}
			return jsonObj.getString(field);
		} catch (Exception ex) {
			
			return "";
		}

	}
	
	public static double jsonPreparaValorDouble(JSONObject jsonObj, String field) {

		try {
			if (jsonObj.isNull(field)) {
				return 0;
			}
			return jsonObj.getDouble(field);
		} catch (Exception ex) {
			
			return 0;
		}

	}
	
	public static double jsonPreparaValorInteger(JSONObject jsonObj, String field) {

		try {
			if (jsonObj.isNull(field)) {
				return 0;
			}
			return jsonObj.getInt(field);
		} catch (Exception ex) {
			
			return 0;
		}

	}


	public String LerArquivo(String arquivo) {

		String linha = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(arquivo));
			while (br.ready()) {
				linha += br.readLine();
			}
			br.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		return linha;

	}

	public String LocalizaArquivo(String tx_sistema, String esquema, String tx_path, String tx_arquivo) {

		List files = new ArrayList();
		findFile(files, new File(tx_path), tx_arquivo, tx_sistema);
		String[] t = files.toString().split(esquema);
		String tx = t[1].substring(1, t[1].toString().length() - 1).replace('\\', '.');
		return tx;
	}

	/**
	 * Primeiro listamos TODOS os arquivos, então pegamos os diretórios e listamos
	 * recursivamente
	 */
	public static void findFile(List l, File dir, String name, String sistema) {
		String tx = "";
		File[] files = dir.listFiles();
		for (int i = 0; i < files.length; ++i) {
			File pathname = files[i];
			String nm = pathname.getName();
			if (nm.equalsIgnoreCase(name)) {
				if (pathname.toString().contains(sistema))
					l.add(pathname);
			}
			if (pathname.isDirectory() && !nm.equals(".") && !nm.equals("..")) {
				findFile(l, pathname, name, sistema);
			}
		}

	}

	public static String setFormatZeroEsq(String valor, int ntotal) {
		int valstr = valor.length();
		valstr = ntotal - valstr;
		String val = "";

		for (int i = 1; i <= valstr; i++) {
			val += "0";
		}

		return val + valor;

	}

	public static String setTiraZeroEsq(String valor) {
		String temp = "";

		for (int i = 0; i < valor.length(); i++) {
			if (!valor.substring(i, i + 1).equals("0")) {
				temp = valor.substring(i);
				break;
			}
		}

		return temp;
	}

	public String isnull(Object valor, String nulo) {
		// Método criado para retornar o parâmetro nulo, caso valor seja null
		String tx_result = "";

		try {
			if (!valor.toString().toUpperCase().equals("NULL"))
				tx_result = valor.toString();
			else
				tx_result = nulo;
		} catch (Exception e) {
			tx_result = nulo;
		}

		return tx_result;
	}

	public Integer isnull(int valor, int nulo) {
		// Método criado para retornar o parâmetro nulo, caso valor seja null
		int tx_result = 0;
		if (valor != 0) {
			tx_result = valor;
		} else
			tx_result = nulo;
		return tx_result;
	}

	public int isnull(Object valor, int nulo) {
		// Método criado para retornar o parâmetro nulo, caso valor seja null
		int tx_result = 0;

		if (valor != null) {
			tx_result = GeneralParser.parseInt(valor.toString());
		} else
			tx_result = nulo;

		return tx_result;
	}

	public double isnull(Double valor, double nulo) {
		// Método criado para retornar o parâmetro nulo, caso valor seja null
		double tx_result = 0;

		if (valor != null) {
			tx_result = GeneralParser.parseDouble(valor.toString());
		} else
			tx_result = nulo;

		return tx_result;
	}

	public int isnull(Integer valor, int nulo) {
		// Método criado para retornar o parâmetro nulo, caso valor seja null
		Integer tx_result = 0;

		try {
			if (valor > 0) {
				tx_result = valor;
			} else
				tx_result = nulo;
		} catch (Exception e) {

		}

		return tx_result;
	}

	public String isnull(Object valor, Object nulo) {
		// Método criado para retornar o parâmetro nulo, caso valor seja null
		String tx_result = "";

		if (valor != null) {
			tx_result = (valor.toString());
		} else if (nulo != null)
			tx_result = nulo.toString();

		return tx_result;
	}

	public String isnull(String valor, String nulo) {
		// Método criado para retornar o parâmetro nulo, caso valor seja null
		String tx_result = "";
		if (valor != null && !valor.equals("null"))
			tx_result = valor;
		else
			tx_result = nulo;

		return tx_result;
	}

	public Date isnull(Date valor, Date nulo) {
		// Método criado para retornar o parâmetro nulo, caso valor seja null
		Date tx_result = null;
		if (valor != null)
			tx_result = valor;
		else
			tx_result = nulo;

		return tx_result;
	}

	public double isnull(double valor, double nulo) {
		// Método criado para retornar o parâmetro nulo, caso valor seja null
		double tx_result;

		if (valor != 0) {
			tx_result = valor;
		} else
			tx_result = nulo;

		return tx_result;
	}

	public static String RequestHashSeguranca(HttpServletRequest request) {

		String hash = "nulo";
		try {
			if (request.getParameter("tx_hash_seguranca") != null) {
				hash = request.getParameter("tx_hash_seguranca");
			}
		} catch (Exception ex) {

		}

		return hash;

	}

	public static boolean getAmbiente() {
		try {
			if (InetAddress.getLocalHost().getHostAddress().indexOf("172.") == -1
					&& InetAddress.getLocalHost().getHostAddress().indexOf("192.168.") == -1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
		}
		return true;
	}
	
	
	public static String getUrlPaginacao() {
		try {
			if (InetAddress.getLocalHost().getHostAddress().indexOf("192.168.") == -1) {
				return Constants.URL_PAGINACAO;
			} else {
				return "localhost:8080";				
			}
		} catch (Exception e) {
		}
		return Constants.URL_PAGINACAO;
	}

	public static String getPropUrl() throws IOException {
		Properties props = new Properties();

		props.load(GeneralUtil.class.getClassLoader().getResourceAsStream("proxool.properties"));

		String tx_url_banco = "";

		for (int i = 0; i < 12; i++) {
			tx_url_banco = props.getProperty("jdbc-" + i + ".proxool.driver-url");
			if (tx_url_banco != null) {
				break;
			}
		}

		return tx_url_banco;
	}

	public static String doRemoveNrLinhaGrid(String tx_campo) {
		String tx_retorno = tx_campo;

		try {
			while (Integer.parseInt(tx_retorno.substring(tx_retorno.length() - 1)) > -1) {
				tx_retorno = tx_retorno.substring(0, tx_retorno.length() - 1);
			}
		} catch (Exception e) {

		}

		return tx_retorno;
	}


	public StringBuilder getTx_where() {
		return tx_where;
	}

	public void setTx_where(StringBuilder tx_where) {
		this.tx_where = tx_where;
	}

}
