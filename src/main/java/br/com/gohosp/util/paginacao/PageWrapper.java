package br.com.gohosp.util.paginacao;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.gohosp.util.GeneralUtil;

public class PageWrapper<T> {

	private Page<T> page;
	private UriComponentsBuilder uriBuilder;
	private String serverName;
	private String contextPath;
	
	public PageWrapper(Page<T> page, HttpServletRequest httpServletRequest) {
		this.page = page;
		this.uriBuilder = ServletUriComponentsBuilder.fromRequest(httpServletRequest);
		this.serverName = httpServletRequest.getServerName();
		this.contextPath = httpServletRequest.getContextPath();
	}
	
	public List<T> getConteudo() {
		return page.getContent();
	}
	
	public boolean isVazia() {
		return page.getContent().isEmpty();
	}
	
	public int getAtual() {
		return page.getNumber();
	}
	
	public boolean isPrimeira() {
		return page.isFirst();
	}
	
	public boolean isUltima() {
		return page.isLast();
	}
	
	public int getTotal() {
		return page.getTotalPages();
	}
	
	public String urlParaPagina(int pagina) {
		
		//System.out.println(uriBuilder.build(false).encode().toUriString());
		//System.out.println(serverName);
		//System.out.println(contextPath);
		//String urlPage = uriBuilder.replaceQueryParam("page", pagina).build(false).encode().toUriString().replace("/consultar", "").replace("/novo", "").replace("localhost:8080", GeneralUtil.getUrlPaginacao());
		//System.out.println(urlPage);
		return uriBuilder.replaceQueryParam("page", pagina).build(false).encode().toUriString().replace("/consultar", "").replace("/novo", "");
		//return urlPage;
	}

	public Page<T> getPage() {
		return page;
	}

	
}