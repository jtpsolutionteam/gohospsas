package br.com.gohosp.util.geradorservice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.reflect.Field;

import br.com.gohosp.Constants;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.geradorclasse.GeradorClasseBean;

public class GeradorServiceBean {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new GeneralUtil().ApagarArquivosDiretorioGeradores();
		new GeradorServiceBean().GeraListaService("TabDepoimentosObj");
		
	}
	
	private void GerarServicePackage(String txObj) {
		try {
		    String pathObj = new GeneralUtil().LocalizaArquivo(Constants.NOME_SISTEMA_LOCAL,"java",Constants.PATH_ARQS_JAVA_LOCAL,txObj+".java").replaceAll("\\.", "/").replace(txObj, "");
    	
	        File[] files = new File(Constants.PATH_ARQS_JAVA_LOCAL+"src/main/java/"+pathObj).listFiles();  
	        for (int i = 0; i < files.length; ++i) {  
	            File pathname = files[i];  
	            String nm = pathname.getName();  
	            
	            if (nm.contains(".java")) {
	            	nm = pathname.getName().replace(".java", "");
	            	GeradorService(nm);
					
	            }
	            
	            Thread.sleep(1000);  
	        } 
	        
	        System.out.println("Geração do Lote de Services terminado!");
		}catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void GeraListaService(String lista) {
		
		  try {	
			String[] l = lista.split(";");
			
			for (int i=0; i<=l.length-1; i++) {
				
				GeradorService(l[i]);
				Thread.sleep(1000);
			}
			
			System.out.println("Geração do Lote de Services terminado!");
		  }catch (Exception ex) {
			  ex.printStackTrace();
		  }
			
		}

	
	//private void GeradorService(String txObjTabela, String pkTabela, String txTipoField) {
	private void GeradorService(String txObj) {	 
		try {
			
			String pathObj = new GeneralUtil().LocalizaArquivo(Constants.NOME_SISTEMA_LOCAL,"java",Constants.PATH_ARQS_JAVA_LOCAL,txObj+".java");
			
			Object tabObj = null;
			
			tabObj = Class.forName(pathObj).newInstance();
			
			Field[] campo = tabObj.getClass().getDeclaredFields();
			campo[0].setAccessible(true);
			
			GeradorClasseBean g = new GeradorClasseBean();			
			String field = g.arrumaCampoObj(campo[0].getName());
			String fieldType = campo[0].getType().toString();
			
			 if (fieldType.contains("Integer")) {
				 fieldType = "Integer";
			 }else if (fieldType.contains("Long")) {
				 fieldType = "Long";
			 }else {
				 fieldType = "String";
			 }
			
			 String texto;
			 String result = "";
			 FileReader fileR = new FileReader (Constants.PATH_ARQS_JAVA_LOCAL+"src/main/java/br/com/gohosp/util/geradorservice/ModeloService.txt");
	         BufferedReader buffR = new BufferedReader (fileR);
	         String tx_classe = tabObj.getClass().getSimpleName().replace("Obj", "Service");
	         
	         while ((texto = buffR.readLine ()) != null) {
	        	 
	        	 String repository = tx_classe.replace("Service", "Repository");
	        	 String tabJoin = tx_classe.replace("Tab", "TabJoin").replace("Service", "");
	        	 
	        	 result += texto.replaceAll("#TabObj", txObj).replaceAll("#Field", field).replaceAll("#TipoField", fieldType).replaceAll("#Repository", repository).replaceAll("#Classe", tx_classe).replaceAll("#TabJoinObj", tabJoin)+"\n";
	         }
	         
	         //System.out.println(result);
	        
	         File f = new File("/temp/obj/service");
		        if (!f.exists()) {
		        	f.mkdirs();
		        }
	         
	          FileWriter arq = new FileWriter("/temp/obj/service/"+tx_classe+".java");
		      PrintWriter gravarArq = new PrintWriter(arq);
		      gravarArq.printf(result);
		      gravarArq.close();
		      
		      System.out.println("Service "+tx_classe+".java criado com sucesso!");
		      
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
	}

}
