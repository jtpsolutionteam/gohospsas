package br.com.gohosp.util.regras;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.ObjectError;

import br.com.gohosp.dao.administracao.repository.VwTabRegrasRepository;
import br.com.gohosp.dao.administracao.vw.VwTabRegrasObj;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralParser;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.logErro.JTPLogErroBean;

@Service
public class RegrasBean {

	@Autowired
	private JTPLogErroBean logErroBean;

	@Autowired
	private UsuarioBean tabUsuarioService;

	@Autowired 
	private VwTabRegrasRepository vwTabRegrasRepository;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	
	
	public List<ObjectError> verificaregras(String txController, HttpServletRequest request) {
		
		List<ObjectError> listError = new ArrayList<ObjectError>();		
		try {
			
			// 1 = NotNull
			// 2 = Condição 
			// 3 = Condição Data menor
			
			
			List<VwTabRegrasObj> listRegras = vwTabRegrasRepository.findByTxControllerOrderByCdOrdemAsc(txController);
			ObjectError objError = null;
			
			if (!listRegras.isEmpty()) {				
				for (VwTabRegrasObj atual : listRegras) {
				
				  String txObjCampo = atual.getTxObjcampo();
				  String txObjCampoCond = atual.getTxObjcampoCond();
				
				  switch (atual.getCdTipo()) {
					case 1: { 
						String txFieldValue = request.getParameter(txObjCampo);
						if (Validator.isBlankOrNull(txFieldValue)) {
							objError = new ObjectError("NotNull",atual.getTxCampo()+" campo obrigatório!");						
							listError.add(objError);							
						}
						break;
					}	
					case 2: { 
						String txFieldValue = request.getParameter(txObjCampo);
						String txFieldCondValue = request.getParameter(txObjCampoCond);
						if (!Validator.isBlankOrNull(txFieldValue) && Validator.isBlankOrNull(txFieldCondValue)) {
							objError = new ObjectError("Condicao",atual.getTxCampo()+" não pode ser preenchido antes do "+atual.getTxCampoCond());						
							listError.add(objError);							
						}
						break;
					}
					case 3: { 
						String txFieldValue = request.getParameter(txObjCampo);
						String txFieldCondValue = request.getParameter(txObjCampoCond);
						if (!Validator.isBlankOrNull(txFieldValue) && !Validator.isBlankOrNull(txFieldCondValue)) {
							if (Validator.isValidDate(txFieldValue) && Validator.isValidDate(txFieldCondValue)) {
								long dtObjCampo = GeneralParser.dateToLong(txFieldValue);
								long dtObjCampocond = GeneralParser.dateToLong(txFieldCondValue);							
								if (dtObjCampo < dtObjCampocond) {
									objError = new ObjectError("CondicaoNotDataMenor",atual.getTxCampo()+" não pode ser menor que o "+atual.getTxCampoCond());						
									listError.add(objError);			
								}
							}else {
								objError = new ObjectError("CondicaoNotDataMenor",atual.getTxCampo()+" ou "+atual.getTxCampoCond()+" - Data Inválida!");						
								listError.add(objError);											
							}
						}
						break;
					}						
				  }
				}			
			}
			
		}catch (Exception ex) {
			logErroBean.addLog(getClass().getSimpleName(), ex.getMessage(), ex);
		}
		
		return listError;
		
	}
	

	//public String 
	
	
	
}
