package br.com.gohosp.util.txt;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.StringTokenizer;

import br.com.gohosp.dao.conexao.connect;
import br.com.gohosp.util.Validator;

public class LerTxtBean {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//new LerTxtBean().LerCVS();
	}
	
	
	
	private void LerTxt() {
		
		connect c = new connect();
		
		Connection conn = c.getConnectionSimpleMySql();
		
	    try {
	      FileReader arq = new FileReader("/temp/tratativas.txt");
	      BufferedReader lerArq = new BufferedReader(arq);
	 
	      String linha = lerArq.readLine(); // lê a primeira linha
	      StringBuilder str = new StringBuilder();
	      try {
		     if (!Validator.isBlankOrNull(linha)) {
		           PreparedStatement prmt = conn.prepareStatement("select * from tab_retorno_consulta_tratativa where tx_cod_segurado like '%"+linha+"%'");
		           
		           ResultSet rs = prmt.executeQuery();
		           if (rs.isBeforeFirst()) {
		        	   rs.next();
		        	   str.append(rs.getString("tx_tratativa_id")+";"+rs.getString("tx_nome_segurado")+";"+rs.getDate("dt_data_solicitacao")+";"+rs.getDate("dt_novo_contato")+";"+rs.getDate("dt_ultimo_envio")+"\n");
		           
		           }else {
		        	   System.out.println(linha);   
		           }
		           
		     }  
		   }catch (Exception ex) {
		        	ex.printStackTrace();
		   }
	      
	      
	// a variável "linha" recebe o valor "null" quando o processo
	// de repetição atingir o final do arquivo texto
	      while (linha != null) {
	        //System.out.printf("%s\n", linha);
	 
	        linha = lerArq.readLine(); // lê da segunda até a última linha
	        //System.out.println(linha);
	        
	        try {
	         if (!Validator.isBlankOrNull(linha)) {
		           PreparedStatement prmt = conn.prepareStatement("select * from tab_retorno_consulta_tratativa where tx_cod_segurado like '%"+linha+"%'");
		           
		           ResultSet rs = prmt.executeQuery();
		           if (rs.isBeforeFirst()) {
		        	   rs.next();
		        	   str.append(rs.getString("tx_tratativa_id")+";"+rs.getString("tx_nome_segurado")+";"+rs.getDate("dt_data_solicitacao")+";"+rs.getDate("dt_novo_contato")+";"+rs.getDate("dt_ultimo_envio")+"\n");		           }else {
		        	   System.out.println(linha);   
		           }
	         }  
	        }catch (Exception ex) {
	        	ex.printStackTrace();
	        }
	        
	      }
	 
	      arq.close();
	      
	      System.out.println(str);
	    } catch (IOException e) {
	        System.err.printf("Erro na abertura do arquivo: %s.\n",
	          e.getMessage());
	    }
		
	}
	

	private void VerificaTxtTratativas() {
		
		connect c = new connect();
		
		Connection conn = c.getConnectionSimpleMySql();
		
	    try {
	      FileReader arq = new FileReader("/temp/tratativas.txt");
	      BufferedReader lerArq = new BufferedReader(arq);
	 
	      String linha = lerArq.readLine(); // lê a primeira linha
	      StringBuilder strOk = new StringBuilder();
	      StringBuilder strNOk = new StringBuilder();
	      try {
		     if (!Validator.isBlankOrNull(linha)) {
		           PreparedStatement prmt = conn.prepareStatement("select * from vw_tab_retorno_consulta_tratativa where tx_tratativa_id like '%"+linha+"%'");
		           
		           ResultSet rs = prmt.executeQuery();
		           if (!rs.isBeforeFirst()) {
		        	   strNOk.append(linha+"\n");		           		        	   
		           }else {
		        	   rs.next();
		        	   strOk.append(rs.getString("tx_tratativa_id")+";"+rs.getString("tx_nome_segurado")+";"+rs.getDate("dt_data_solicitacao")+";"+rs.getDate("dt_novo_contato")+";"+rs.getDate("dt_ultimo_envio")+";"+rs.getString("tx_status")+"\n");		           		        	   
		           }
		           
		     }  
		   }catch (Exception ex) {
		        	ex.printStackTrace();
		   }
	      
	      
	// a variável "linha" recebe o valor "null" quando o processo
	// de repetição atingir o final do arquivo texto
	      while (linha != null) {
	        //System.out.printf("%s\n", linha);
	 
	        linha = lerArq.readLine(); // lê da segunda até a última linha
	        //System.out.println(linha);
	        
	        try {
	         if (!Validator.isBlankOrNull(linha)) {
		           PreparedStatement prmt = conn.prepareStatement("select * from vw_tab_retorno_consulta_tratativa where tx_tratativa_id like '%"+linha+"%'");
		           
		           ResultSet rs = prmt.executeQuery();
		           if (!rs.isBeforeFirst()) {
		        	   strNOk.append(linha+"\n");		           		        	   
		           }else {
		        	   rs.next();
		        	   strOk.append(rs.getString("tx_tratativa_id")+";"+rs.getString("tx_nome_segurado")+";"+rs.getDate("dt_data_solicitacao")+";"+rs.getDate("dt_novo_contato")+";"+rs.getDate("dt_ultimo_envio")+";"+rs.getString("tx_status")+"\n");		           		        	   
		           }
	         }  
	        }catch (Exception ex) {
	        	ex.printStackTrace();
	        }
	        
	      }
	 
	      arq.close();
	      
	      System.out.println(strOk);
	      
	      System.out.println(strNOk);
	    } catch (IOException e) {
	        System.err.printf("Erro na abertura do arquivo: %s.\n",
	          e.getMessage());
	    }
		
	}

	

}
