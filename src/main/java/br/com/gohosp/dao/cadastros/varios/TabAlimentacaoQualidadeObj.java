package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_alimentacao_qualidade", schema = Constants.SCHEMA)
public class TabAlimentacaoQualidadeObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_alimentacao_qualidade")
	// @NotNull(message = "AlimentacaoQualidade campo obrigatório!")
	private Integer cdAlimentacaoQualidade;

	@Column(name = "tx_alimentacao_qualidade")
	// @NotEmpty(message = "AlimentacaoQualidade campo obrigatório!")
	@Size(max = 45, message = "AlimentacaoQualidade tamanho máximo de 45 caracteres")
	private String txAlimentacaoQualidade;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txAlimentacaoQualidade))
			txAlimentacaoQualidade = txAlimentacaoQualidade.toUpperCase();
	}

}
