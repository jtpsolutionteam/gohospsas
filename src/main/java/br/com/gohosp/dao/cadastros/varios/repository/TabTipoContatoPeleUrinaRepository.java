package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabTipoContatoPeleUrinaObj;



public interface TabTipoContatoPeleUrinaRepository extends JpaRepository<TabTipoContatoPeleUrinaObj, Integer> {



}