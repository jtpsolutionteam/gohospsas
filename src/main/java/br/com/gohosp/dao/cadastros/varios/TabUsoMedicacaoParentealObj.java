package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_uso_medicacao_parenteal", schema = Constants.SCHEMA)
public class TabUsoMedicacaoParentealObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_uso_medicacao_parenteal")
	// @NotNull(message = "UsoMedicacaoParenteal campo obrigatório!")
	private Integer cdUsoMedicacaoParenteal;

	@Column(name = "tx_uso_medicacao_parenteal")
	// @NotEmpty(message = "UsoMedicacaoParenteal campo obrigatório!")
	@Size(max = 45, message = "UsoMedicacaoParenteal tamanho máximo de 45 caracteres")
	private String txUsoMedicacaoParenteal;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txUsoMedicacaoParenteal))
			txUsoMedicacaoParenteal = txUsoMedicacaoParenteal.toUpperCase();
	}

}
