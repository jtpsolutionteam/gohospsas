package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_vestir", schema = Constants.SCHEMA)
public class TabTipoVestirObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_vestir")
	// @NotNull(message = "TipoVestir campo obrigatório!")
	private Integer cdTipoVestir;

	@Column(name = "tx_tipo_vestir")
	// @NotEmpty(message = "TipoVestir campo obrigatório!")
	@Size(max = 200, message = "TipoVestir tamanho máximo de 200 caracteres")
	private String txTipoVestir;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoVestir))
			txTipoVestir = txTipoVestir.toUpperCase();
	}

}
