package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabTipoAndarCamaObj;



public interface TabTipoAndarCamaRepository extends JpaRepository<TabTipoAndarCamaObj, Integer> {



}