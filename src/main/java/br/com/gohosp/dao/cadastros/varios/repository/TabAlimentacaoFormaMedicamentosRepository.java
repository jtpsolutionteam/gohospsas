package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabAlimentacaoFormaMedicamentosObj;



public interface TabAlimentacaoFormaMedicamentosRepository extends JpaRepository<TabAlimentacaoFormaMedicamentosObj, Integer> {



}