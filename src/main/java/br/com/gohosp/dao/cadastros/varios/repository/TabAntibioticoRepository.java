package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabAntibioticoObj;



public interface TabAntibioticoRepository extends JpaRepository<TabAntibioticoObj, Integer> {



}