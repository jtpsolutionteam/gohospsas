package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabTipoControleBanheiroObj;



public interface TabTipoControleBanheiroRepository extends JpaRepository<TabTipoControleBanheiroObj, Integer> {



}