package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabImunoglobulinasObj;



public interface TabImunoglobulinasRepository extends JpaRepository<TabImunoglobulinasObj, Integer> {



}