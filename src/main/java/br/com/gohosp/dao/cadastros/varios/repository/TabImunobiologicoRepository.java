package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabImunobiologicoObj;



public interface TabImunobiologicoRepository extends JpaRepository<TabImunobiologicoObj, Integer> {



}