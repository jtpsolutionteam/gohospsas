package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabRadioterapiaObj;


public interface TabRadioterapiaRepository extends JpaRepository<TabRadioterapiaObj, Integer> {



}