package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name="tab_alimentacao_estado_nutricional", schema=Constants.SCHEMA)
public class TabAlimentacaoEstadoNutricionalObj {


@Id @GeneratedValue 
@Column(name = "cd_alimentacao_estado_nutricional") 
//@NotNull(message = "AlimentacaoEstadoNutricional campo obrigatório!")
private Integer cdAlimentacaoEstadoNutricional; 
 
@Column(name = "tx_alimentacao_estado_nutricional") 
//@NotEmpty(message = "AlimentacaoEstadoNutricional campo obrigatório!")
@Size(max = 45, message = "AlimentacaoEstadoNutricional tamanho máximo de 45 caracteres") 
private String txAlimentacaoEstadoNutricional; 
 
@PrePersist @PreUpdate 
private void prePersistUpdate() { 
if (!Validator.isBlankOrNull(txAlimentacaoEstadoNutricional))
txAlimentacaoEstadoNutricional = txAlimentacaoEstadoNutricional.toUpperCase();
}

}
