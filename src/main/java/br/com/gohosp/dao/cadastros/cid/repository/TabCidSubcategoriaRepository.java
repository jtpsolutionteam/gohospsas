package br.com.gohosp.dao.cadastros.cid.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.cid.TabCidSubcategoriaObj;



public interface TabCidSubcategoriaRepository extends JpaRepository<TabCidSubcategoriaObj, Integer> {

   List<TabCidSubcategoriaObj> findByTxDescricaoAbreviadaContaining(String txDescricaoAbreviada);

}