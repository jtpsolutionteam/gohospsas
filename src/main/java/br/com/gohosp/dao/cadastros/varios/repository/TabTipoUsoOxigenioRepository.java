package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabTipoUsoOxigenioObj;



public interface TabTipoUsoOxigenioRepository extends JpaRepository<TabTipoUsoOxigenioObj, Integer> {



}