package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabAspiracoesViasAereasObj;



public interface TabAspiracoesViasAereasRepository extends JpaRepository<TabAspiracoesViasAereasObj, Integer> {



}