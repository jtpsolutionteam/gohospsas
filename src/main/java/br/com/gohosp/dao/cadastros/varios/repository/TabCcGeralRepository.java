package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabCcGeralObj;



public interface TabCcGeralRepository extends JpaRepository<TabCcGeralObj, Integer> {



}