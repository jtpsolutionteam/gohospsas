package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "tab_precos_tipo", schema = Constants.SCHEMA)
public class TabPrecosTipoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_preco_tipo")
	// @NotNull(message = "PrecoTipo campo obrigatório!")
	private Integer cdPrecoTipo;

	@Column(name = "tx_preco_tipo")
	// @NotEmpty(message = "PrecoTipo campo obrigatório!")
	@Size(max = 10, message = "PrecoTipo tamanho máximo de 10 caracteres")
	private String txPrecoTipo;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txPrecoTipo))
			txPrecoTipo = txPrecoTipo.toUpperCase();
	}

	public Integer getCdPrecoTipo() {
		return cdPrecoTipo;
	}

	public void setCdPrecoTipo(Integer cdPrecoTipo) {
		this.cdPrecoTipo = cdPrecoTipo;
	}

	public String getTxPrecoTipo() {
		return txPrecoTipo;
	}

	public void setTxPrecoTipo(String txPrecoTipo) {
		this.txPrecoTipo = txPrecoTipo;
	}

}
