package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_pendencia_tratamento", schema = Constants.SCHEMA)
public class TabPendenciaTratamentoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_pendencia_tratamento")
	// @NotNull(message = "PendenciaTratamento campo obrigatório!")
	private Integer cdPendenciaTratamento;

	@Column(name = "tx_pendencia_tratamento")
	// @NotEmpty(message = "PendenciaTratamento campo obrigatório!")
	@Size(max = 45, message = "PendenciaTratamento tamanho máximo de 45 caracteres")
	private String txPendenciaTratamento;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txPendenciaTratamento))
			txPendenciaTratamento = txPendenciaTratamento.toUpperCase();
	}

}
