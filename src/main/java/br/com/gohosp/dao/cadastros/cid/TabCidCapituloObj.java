package br.com.gohosp.dao.cadastros.cid;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.gohosp.Constants;

@Entity
@Table(name = "tab_cid_capitulo", schema = Constants.SCHEMA)
public class TabCidCapituloObj {

	@Id	
	@Column(name = "cd_capitulo")
	// @NotNull(message = "Capitulo campo obrigatório!")
	private Integer cdCapitulo;

	@Column(name = "tx_descricao")
	// @NotEmpty(message = "Descricao campo obrigatório!")
	@Size(max = 1000, message = "Descricao tamanho máximo de 1000 caracteres")
	private String txDescricao;

	@Column(name = "tx_descricao_abreviada")
	// @NotEmpty(message = "DescricaoAbreviada campo obrigatório!")
	@Size(max = 1000, message = "DescricaoAbreviada tamanho máximo de 1000 caracteres")
	private String txDescricaoAbreviada;

	@Column(name = "tx_categoria_inicial")
	// @NotEmpty(message = "CategoriaInicial campo obrigatório!")
	@Size(max = 10, message = "CategoriaInicial tamanho máximo de 10 caracteres")
	private String txCategoriaInicial;

	@Column(name = "tx_categoria_final")
	// @NotEmpty(message = "CategoriaFinal campo obrigatório!")
	@Size(max = 10, message = "CategoriaFinal tamanho máximo de 10 caracteres")
	private String txCategoriaFinal;

	@Column(name = "dt_criacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtCriacao;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}

	public Integer getCdCapitulo() {
		return cdCapitulo;
	}

	public void setCdCapitulo(Integer cdCapitulo) {
		this.cdCapitulo = cdCapitulo;
	}

	public String getTxDescricao() {
		return txDescricao;
	}

	public void setTxDescricao(String txDescricao) {
		this.txDescricao = txDescricao;
	}

	public String getTxDescricaoAbreviada() {
		return txDescricaoAbreviada;
	}

	public void setTxDescricaoAbreviada(String txDescricaoAbreviada) {
		this.txDescricaoAbreviada = txDescricaoAbreviada;
	}

	public String getTxCategoriaInicial() {
		return txCategoriaInicial;
	}

	public void setTxCategoriaInicial(String txCategoriaInicial) {
		this.txCategoriaInicial = txCategoriaInicial;
	}

	public String getTxCategoriaFinal() {
		return txCategoriaFinal;
	}

	public void setTxCategoriaFinal(String txCategoriaFinal) {
		this.txCategoriaFinal = txCategoriaFinal;
	}

	public Date getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	



	
	

}
