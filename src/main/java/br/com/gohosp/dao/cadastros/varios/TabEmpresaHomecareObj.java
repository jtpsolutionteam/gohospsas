package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_empresa_homecare", schema = Constants.SCHEMA)
public class TabEmpresaHomecareObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_empresa_homecare")
	// @NotNull(message = "EmpresaHomecare campo obrigatório!")
	private Integer cdEmpresaHomecare;

	@Column(name = "tx_empresa_homecare")
	// @NotEmpty(message = "EmpresaHomecare campo obrigatório!")
	@Size(max = 45, message = "EmpresaHomecare tamanho máximo de 45 caracteres")
	private String txEmpresaHomecare;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txEmpresaHomecare))
			txEmpresaHomecare = txEmpresaHomecare.toUpperCase();
	}

}
