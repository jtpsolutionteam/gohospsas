package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_acamado_cadeira_rodas", schema = Constants.SCHEMA)
public class TabTipoAcamadoCadeiraRodasObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_acamado_cadeira_rodas")
	// @NotNull(message = "TipoAcamadoCadeiraRodas campo obrigatório!")
	private Integer cdTipoAcamadoCadeiraRodas;

	@Column(name = "tx_tipo_acamado_cadeira_rodas")
	// @NotEmpty(message = "TipoAcamadoCadeiraRodas campo obrigatório!")
	@Size(max = 45, message = "TipoAcamadoCadeiraRodas tamanho máximo de 45 caracteres")
	private String txTipoAcamadoCadeiraRodas;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoAcamadoCadeiraRodas))
			txTipoAcamadoCadeiraRodas = txTipoAcamadoCadeiraRodas.toUpperCase();
	}

}
