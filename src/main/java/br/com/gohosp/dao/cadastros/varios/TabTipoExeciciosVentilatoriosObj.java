package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_execicios_ventilatorios", schema = Constants.SCHEMA)
public class TabTipoExeciciosVentilatoriosObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_execicios_ventilatorios")
	// @NotNull(message = "TipoExeciciosVentilatorios campo obrigatório!")
	private Integer cdTipoExeciciosVentilatorios;

	@Column(name = "tx_tipo_execicios_ventilatorios")
	// @NotEmpty(message = "TipoExeciciosVentilatorios campo obrigatório!")
	@Size(max = 45, message = "TipoExeciciosVentilatorios tamanho máximo de 45 caracteres")
	private String txTipoExeciciosVentilatorios;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoExeciciosVentilatorios))
			txTipoExeciciosVentilatorios = txTipoExeciciosVentilatorios.toUpperCase();
	}

}
