package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "tab_prob_encontrados", schema = Constants.SCHEMA)
public class TabProbEncontradosObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_prob")
	// @NotNull(message = "Prob campo obrigatório!")
	private Integer cdProb;

	@Column(name = "tx_prob")
	// @NotEmpty(message = "Prob campo obrigatório!")
	@Size(max = 50, message = "Prob tamanho máximo de 50 caracteres")
	private String txProb;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}

	public Integer getCdProb() {
		return cdProb;
	}

	public void setCdProb(Integer cdProb) {
		this.cdProb = cdProb;
	}

	public String getTxProb() {
		return txProb;
	}

	public void setTxProb(String txProb) {
		this.txProb = txProb;
	}

}
