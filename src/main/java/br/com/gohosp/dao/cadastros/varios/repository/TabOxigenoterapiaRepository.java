package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabOxigenoterapiaObj;



public interface TabOxigenoterapiaRepository extends JpaRepository<TabOxigenoterapiaObj, Integer> {



}