package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabTipoVestirObj;



public interface TabTipoVestirRepository extends JpaRepository<TabTipoVestirObj, Integer> {



}