package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_internacoes_ano", schema = Constants.SCHEMA)
public class TabInternacoesAnoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_paciente_internacoes_ultimo_ano")
	// @NotNull(message = "Codigo campo obrigatório!")
	private Integer cdPacienteInternacoesUltimoAno;

	@Column(name = "tx_paciente_internacoes_ultimo_ano")
	// @NotEmpty(message = "Descricao campo obrigatório!")
	@Size(max = 45, message = "Descricao tamanho máximo de 45 caracteres")
	private String txPacienteInternacoesUltimoAno;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txPacienteInternacoesUltimoAno))
			txPacienteInternacoesUltimoAno = txPacienteInternacoesUltimoAno.toUpperCase();
	}

}
