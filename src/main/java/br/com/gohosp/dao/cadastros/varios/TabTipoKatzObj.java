package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_katz", schema = Constants.SCHEMA)
public class TabTipoKatzObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_katz")
	// @NotNull(message = "TipoKatz campo obrigatório!")
	private Integer cdTipoKatz;

	@Column(name = "tx_tipo_katz")
	// @NotEmpty(message = "TipoKatz campo obrigatório!")
	@Size(max = 45, message = "TipoKatz tamanho máximo de 45 caracteres")
	private String txTipoKatz;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoKatz))
			txTipoKatz = txTipoKatz.toUpperCase();
	}

}
