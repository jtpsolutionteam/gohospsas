package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabImpressaoGeralObj;



public interface TabImpressaoGeralRepository extends JpaRepository<TabImpressaoGeralObj, Integer> {



}