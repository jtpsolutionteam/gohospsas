package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "tab_status", schema = Constants.SCHEMA)
public class TabStatusObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_status")
	private Integer cdStatus;

	@Column(name = "tx_status")
	@Size(max = 45, message = "Descricao tamanho máximo de 45 caracteres")
	private String txStatus;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}

	public Integer getCdStatus() {
		return cdStatus;
	}

	public void setCdStatus(Integer cdStatus) {
		this.cdStatus = cdStatus;
	}

	public String getTxStatus() {
		return txStatus;
	}

	public void setTxStatus(String txStatus) {
		this.txStatus = txStatus;
	}

	

}
