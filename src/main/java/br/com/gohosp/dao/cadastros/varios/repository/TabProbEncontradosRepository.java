package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabProbEncontradosObj;



public interface TabProbEncontradosRepository extends JpaRepository<TabProbEncontradosObj, Integer> {



}