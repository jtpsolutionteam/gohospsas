package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name="tab_alimentacao_parenteral", schema=Constants.SCHEMA)
public class TabAlimentacaoParenteralObj {


@Id @GeneratedValue 
@Column(name = "cd_alimentacao_parenteral") 
//@NotNull(message = "AlimentacaoParenteral campo obrigatório!")
private Integer cdAlimentacaoParenteral; 
 
@Column(name = "tx_alimentacao_parenteral") 
//@NotEmpty(message = "AlimentacaoParenteral campo obrigatório!")
@Size(max = 45, message = "AlimentacaoParenteral tamanho máximo de 45 caracteres") 
private String txAlimentacaoParenteral; 
 
@PrePersist @PreUpdate 
private void prePersistUpdate() { 
if (!Validator.isBlankOrNull(txAlimentacaoParenteral))
txAlimentacaoParenteral = txAlimentacaoParenteral.toUpperCase();
}

}
