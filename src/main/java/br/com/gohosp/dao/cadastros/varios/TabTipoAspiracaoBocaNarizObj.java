package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_aspiracao_boca_nariz", schema = Constants.SCHEMA)
public class TabTipoAspiracaoBocaNarizObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_aspiracao_boca_nariz")
	// @NotNull(message = "TipoAspiracaoBocaNariz campo obrigatório!")
	private Integer cdTipoAspiracaoBocaNariz;

	@Column(name = "tx_tipo_aspiracao_boca_nariz")
	// @NotEmpty(message = "TipoAspiracaoBocaNariz campo obrigatório!")
	@Size(max = 45, message = "TipoAspiracaoBocaNariz tamanho máximo de 45 caracteres")
	private String txTipoAspiracaoBocaNariz;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoAspiracaoBocaNariz))
			txTipoAspiracaoBocaNariz = txTipoAspiracaoBocaNariz.toUpperCase();
	}

}
