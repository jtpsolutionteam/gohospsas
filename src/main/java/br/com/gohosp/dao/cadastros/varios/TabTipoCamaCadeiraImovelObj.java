package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_cama_cadeira_imovel", schema = Constants.SCHEMA)
public class TabTipoCamaCadeiraImovelObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_cama_cadeira_imovel")
	// @NotNull(message = "TipoCamaCadeiraImovel campo obrigatório!")
	private Integer cdTipoCamaCadeiraImovel;

	@Column(name = "tx_tipo_cama_cadeira_imovel")
	// @NotEmpty(message = "TipoCamaCadeiraImovel campo obrigatório!")
	@Size(max = 45, message = "TipoCamaCadeiraImovel tamanho máximo de 45 caracteres")
	private String txTipoCamaCadeiraImovel;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoCamaCadeiraImovel))
			txTipoCamaCadeiraImovel = txTipoCamaCadeiraImovel.toUpperCase();
	}

}
