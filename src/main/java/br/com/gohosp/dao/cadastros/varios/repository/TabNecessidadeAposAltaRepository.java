package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabNecessidadeAposAltaObj;



public interface TabNecessidadeAposAltaRepository extends JpaRepository<TabNecessidadeAposAltaObj, Integer> {



}