package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabMedicacoesAdministradasObj;



public interface TabMedicacoesAdministradasRepository extends JpaRepository<TabMedicacoesAdministradasObj, Integer> {



}