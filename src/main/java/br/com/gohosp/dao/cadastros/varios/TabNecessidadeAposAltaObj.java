package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_necessidade_apos_alta", schema = Constants.SCHEMA)
public class TabNecessidadeAposAltaObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_necessidade_apos_alta")
	// @NotNull(message = "NecessidadeAposAlta campo obrigatório!")
	private Integer cdNecessidadeAposAlta;

	@Column(name = "tx_necessidade_apos_alta")
	// @NotEmpty(message = "NecessidadeAposAlta campo obrigatório!")
	@Size(max = 100, message = "NecessidadeAposAlta tamanho máximo de 100 caracteres")
	private String txNecessidadeAposAlta;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txNecessidadeAposAlta))
			txNecessidadeAposAlta = txNecessidadeAposAlta.toUpperCase();
	}

}
