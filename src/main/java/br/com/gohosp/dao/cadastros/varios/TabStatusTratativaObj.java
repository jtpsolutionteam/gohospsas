package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "tab_status_tratativa", schema = Constants.SCHEMA)
public class TabStatusTratativaObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_status_tratativa")
	// @NotNull(message = "StatusTratativa campo obrigatório!")
	private Integer cdStatusTratativa;

	@Column(name = "tx_status_tratativa")
	// @NotEmpty(message = "StatusTratativa campo obrigatório!")
	@Size(max = 45, message = "StatusTratativa tamanho máximo de 45 caracteres")
	private String txStatusTratativa;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txStatusTratativa))
			txStatusTratativa = txStatusTratativa.toUpperCase();
	}

	public Integer getCdStatusTratativa() {
		return cdStatusTratativa;
	}

	public void setCdStatusTratativa(Integer cdStatusTratativa) {
		this.cdStatusTratativa = cdStatusTratativa;
	}

	public String getTxStatusTratativa() {
		return txStatusTratativa;
	}

	public void setTxStatusTratativa(String txStatusTratativa) {
		this.txStatusTratativa = txStatusTratativa;
	}

}
