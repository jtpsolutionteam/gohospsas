package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_alimentacao_forma_medicamentos", schema = Constants.SCHEMA)
public class TabAlimentacaoFormaMedicamentosObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_alimentacao_forma_medicamentos")
	// @NotNull(message = "AlimentacaoFormaMedicamentos campo obrigatório!")
	private Integer cdAlimentacaoFormaMedicamentos;

	@Column(name = "tx_alimentacao_forma_medicamentos")
	// @NotEmpty(message = "AlimentacaoFormaMedicamentos campo obrigatório!")
	@Size(max = 45, message = "AlimentacaoFormaMedicamentos tamanho máximo de 45 caracteres")
	private String txAlimentacaoFormaMedicamentos;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txAlimentacaoFormaMedicamentos))
			txAlimentacaoFormaMedicamentos = txAlimentacaoFormaMedicamentos.toUpperCase();
	}

}
