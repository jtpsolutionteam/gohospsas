package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabTipoCamaCadeiraImovelObj;



public interface TabTipoCamaCadeiraImovelRepository extends JpaRepository<TabTipoCamaCadeiraImovelObj, Integer> {



}