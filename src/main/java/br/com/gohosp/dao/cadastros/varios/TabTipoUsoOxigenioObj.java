package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_uso_oxigenio", schema = Constants.SCHEMA)
public class TabTipoUsoOxigenioObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_uso_oxigenio")
	// @NotNull(message = "TipoUsoOxigenio campo obrigatório!")
	private Integer cdTipoUsoOxigenio;

	@Column(name = "tx_tipo_uso_oxigenio")
	// @NotEmpty(message = "TipoUsoOxigenio campo obrigatório!")
	@Size(max = 45, message = "TipoUsoOxigenio tamanho máximo de 45 caracteres")
	private String txTipoUsoOxigenio;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoUsoOxigenio))
			txTipoUsoOxigenio = txTipoUsoOxigenio.toUpperCase();
	}

}
