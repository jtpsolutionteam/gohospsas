package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabPrecosTipoObj;



public interface TabPrecosTipoRepository extends JpaRepository<TabPrecosTipoObj, Integer> {



}