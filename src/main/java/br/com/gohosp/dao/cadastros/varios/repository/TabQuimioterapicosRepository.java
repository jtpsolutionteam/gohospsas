package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabQuimioterapicosObj;



public interface TabQuimioterapicosRepository extends JpaRepository<TabQuimioterapicosObj, Integer> {



}