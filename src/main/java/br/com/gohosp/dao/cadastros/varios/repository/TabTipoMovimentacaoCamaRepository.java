package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabTipoMovimentacaoCamaObj;



public interface TabTipoMovimentacaoCamaRepository extends JpaRepository<TabTipoMovimentacaoCamaObj, Integer> {



}