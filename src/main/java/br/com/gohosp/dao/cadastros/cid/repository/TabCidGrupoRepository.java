package br.com.gohosp.dao.cadastros.cid.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.cid.TabCidGrupoObj;



public interface TabCidGrupoRepository extends JpaRepository<TabCidGrupoObj, Integer> {



}