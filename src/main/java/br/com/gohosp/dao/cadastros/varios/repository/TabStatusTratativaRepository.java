package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabStatusTratativaObj;



public interface TabStatusTratativaRepository extends JpaRepository<TabStatusTratativaObj, Integer> {



}