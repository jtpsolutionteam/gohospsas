package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_ida_banheiro", schema = Constants.SCHEMA)
public class TabTipoIdaBanheiroObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_ida_banheiro")
	// @NotNull(message = "TipoIdaBanheiro campo obrigatório!")
	private Integer cdTipoIdaBanheiro;

	@Column(name = "tx_tipo_ida_banheiro")
	// @NotEmpty(message = "TipoIdaBanheiro campo obrigatório!")
	@Size(max = 200, message = "TipoIdaBanheiro tamanho máximo de 200 caracteres")
	private String txTipoIdaBanheiro;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoIdaBanheiro))
			txTipoIdaBanheiro = txTipoIdaBanheiro.toUpperCase();
	}

}
