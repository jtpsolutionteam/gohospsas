package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabAlimentacaoEstadoNutricionalObj;



public interface TabAlimentacaoEstadoNutricionalRepository extends JpaRepository<TabAlimentacaoEstadoNutricionalObj, Integer> {



}