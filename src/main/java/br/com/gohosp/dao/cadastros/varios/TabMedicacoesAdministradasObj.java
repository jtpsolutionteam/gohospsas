package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_medicacoes_administradas", schema = Constants.SCHEMA)
public class TabMedicacoesAdministradasObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_medicacoes_administradas")
	// @NotNull(message = "MedicacoesAdministradas campo obrigatório!")
	private Integer cdMedicacoesAdministradas;

	@Column(name = "tx_medicacoes_administradas")
	// @NotEmpty(message = "MedicacoesAdministradas campo obrigatório!")
	@Size(max = 45, message = "MedicacoesAdministradas tamanho máximo de 45 caracteres")
	private String txMedicacoesAdministradas;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txMedicacoesAdministradas))
			txMedicacoesAdministradas = txMedicacoesAdministradas.toUpperCase();
	}

}
