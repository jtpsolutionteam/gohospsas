package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabTipoAcamadoCadeiraRodasObj;



public interface TabTipoAcamadoCadeiraRodasRepository extends JpaRepository<TabTipoAcamadoCadeiraRodasObj, Integer> {



}