package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabAlimentacaoQualidadeObj;



public interface TabAlimentacaoQualidadeRepository extends JpaRepository<TabAlimentacaoQualidadeObj, Integer> {



}