package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabEmpresaHomecareObj;



public interface TabEmpresaHomecareRepository extends JpaRepository<TabEmpresaHomecareObj, Integer> {



}