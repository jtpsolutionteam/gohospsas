package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabAlimentacaoTiposObj;



public interface TabAlimentacaoTiposRepository extends JpaRepository<TabAlimentacaoTiposObj, Integer> {



}