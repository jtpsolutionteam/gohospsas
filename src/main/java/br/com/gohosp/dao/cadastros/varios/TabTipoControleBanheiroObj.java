package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_controle_banheiro", schema = Constants.SCHEMA)
public class TabTipoControleBanheiroObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_controle_banheiro")
	// @NotNull(message = "TipoControleBanheiro campo obrigatório!")
	private Integer cdTipoControleBanheiro;

	@Column(name = "tx_tipo_controle_banheiro")
	// @NotEmpty(message = "TipoControleBanheiro campo obrigatório!")
	@Size(max = 200, message = "TipoControleBanheiro tamanho máximo de 200 caracteres")
	private String txTipoControleBanheiro;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoControleBanheiro))
			txTipoControleBanheiro = txTipoControleBanheiro.toUpperCase();
	}

}
