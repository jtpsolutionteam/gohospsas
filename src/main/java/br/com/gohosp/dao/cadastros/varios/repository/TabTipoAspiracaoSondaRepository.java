package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabTipoAspiracaoSondaObj;



public interface TabTipoAspiracaoSondaRepository extends JpaRepository<TabTipoAspiracaoSondaObj, Integer> {



}