package br.com.gohosp.dao.cadastros.cid.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.cid.TabCidCategoriaObj;



public interface TabCidCategoriaRepository extends JpaRepository<TabCidCategoriaObj, Integer> {



}