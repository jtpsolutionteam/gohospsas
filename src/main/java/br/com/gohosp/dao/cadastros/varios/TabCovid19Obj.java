package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "tab_covid_19", schema = Constants.SCHEMA)
public class TabCovid19Obj {

	@Id
	@GeneratedValue
	@Column(name = "cd_covid")
	// @NotNull(message = "Convid campo obrigatório!")
	private Integer cdCovid;

	@Column(name = "tx_covid")
	// @NotEmpty(message = "Convid campo obrigatório!")
	@Size(max = 50, message = "Covid tamanho máximo de 50 caracteres")
	private String txCovid;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}

	public Integer getCdCovid() {
		return cdCovid;
	}

	public void setCdCovid(Integer cdCovid) {
		this.cdCovid = cdCovid;
	}

	public String getTxCovid() {
		return txCovid;
	}

	public void setTxCovid(String txCovid) {
		this.txCovid = txCovid;
	}

	

}
