package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabCovid19Obj;



public interface TabCovid19Repository extends JpaRepository<TabCovid19Obj, Integer> {



}