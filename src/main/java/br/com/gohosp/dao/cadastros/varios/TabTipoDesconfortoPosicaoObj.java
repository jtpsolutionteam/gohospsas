package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_desconforto_posicao", schema = Constants.SCHEMA)
public class TabTipoDesconfortoPosicaoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_desconforto")
	// @NotNull(message = "TipoDesconforto campo obrigatório!")
	private Integer cdTipoDesconforto;

	@Column(name = "tx_tipo_desconforto")
	// @NotEmpty(message = "TipoDesconforto campo obrigatório!")
	@Size(max = 45, message = "TipoDesconforto tamanho máximo de 45 caracteres")
	private String txTipoDesconforto;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoDesconforto))
			txTipoDesconforto = txTipoDesconforto.toUpperCase();
	}

}
