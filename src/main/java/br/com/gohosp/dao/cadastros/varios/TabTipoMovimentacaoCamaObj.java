package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_movimentacao_cama", schema = Constants.SCHEMA)
public class TabTipoMovimentacaoCamaObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_movimentacao_cama")
	// @NotNull(message = "TipoMovimentacaoCama campo obrigatório!")
	private Integer cdTipoMovimentacaoCama;

	@Column(name = "tx_tipo_movimentacao_cama")
	// @NotEmpty(message = "TipoMovimentacaoCama campo obrigatório!")
	@Size(max = 200, message = "TipoMovimentacaoCama tamanho máximo de 200 caracteres")
	private String txTipoMovimentacaoCama;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoMovimentacaoCama))
			txTipoMovimentacaoCama = txTipoMovimentacaoCama.toUpperCase();
	}

}
