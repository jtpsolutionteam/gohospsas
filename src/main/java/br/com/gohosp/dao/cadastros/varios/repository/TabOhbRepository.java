package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabOhbObj;



public interface TabOhbRepository extends JpaRepository<TabOhbObj, Integer> {



}