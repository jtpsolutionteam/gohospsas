package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabAnticoagulantesObj;



public interface TabAnticoagulantesRepository extends JpaRepository<TabAnticoagulantesObj, Integer> {



}