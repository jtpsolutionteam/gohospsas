package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabTipoLesoesPeleObj;



public interface TabTipoLesoesPeleRepository extends JpaRepository<TabTipoLesoesPeleObj, Integer> {



}