package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabAcessoVenosoObj;



public interface TabAcessoVenosoRepository extends JpaRepository<TabAcessoVenosoObj, Integer> {



}