package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabInternacoesAnoObj;



public interface TabInternacoesAnoRepository extends JpaRepository<TabInternacoesAnoObj, Integer> {



}