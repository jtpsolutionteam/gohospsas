package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_alimentacao_tipos", schema = Constants.SCHEMA)
public class TabAlimentacaoTiposObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_alimentacao_tipos")
	// @NotNull(message = "AlimantacaoTipos campo obrigatório!")
	private Integer cdAlimentacaoTipos;

	@Column(name = "tx_alimentacao_tipos")
	// @NotEmpty(message = "AlimantacaoTipos campo obrigatório!")
	@Size(max = 200, message = "AlimentacaoTipos tamanho máximo de 200 caracteres")
	private String txAlimentacaoTipos;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txAlimentacaoTipos))
			txAlimentacaoTipos = txAlimentacaoTipos.toUpperCase();
	}

}
