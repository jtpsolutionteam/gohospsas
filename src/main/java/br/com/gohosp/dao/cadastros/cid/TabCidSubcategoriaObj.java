package br.com.gohosp.dao.cadastros.cid;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.gohosp.Constants;

@Entity
@Table(name = "tab_cid_subcategoria", schema = Constants.SCHEMA)
public class TabCidSubcategoriaObj {

	@Id
	@Column(name = "cd_subcategoria")
	// @NotNull(message = "Subcategoria campo obrigatório!")
	private Integer cdSubcategoria;

	@Column(name = "cd_categoria")
	// @NotNull(message = "Categoria campo obrigatório!")
	private Integer cdCategoria;

	@Column(name = "tx_subcategoria")
	// @NotEmpty(message = "Subcategoria campo obrigatório!")
	@Size(max = 10, message = "Subcategoria tamanho máximo de 10 caracteres")
	private String txSubcategoria;

	@Column(name = "tx_classificacao")
	// @NotEmpty(message = "Classificacao campo obrigatório!")
	@Size(max = 10, message = "Classificacao tamanho máximo de 10 caracteres")
	private String txClassificacao;

	@Column(name = "tx_sexo_restrit")
	// @NotEmpty(message = "SexoRestrit campo obrigatório!")
	@Size(max = 10, message = "SexoRestrit tamanho máximo de 10 caracteres")
	private String txSexoRestrit;

	@Column(name = "tx_causa_obito")
	// @NotEmpty(message = "CausaObito campo obrigatório!")
	@Size(max = 10, message = "CausaObito tamanho máximo de 10 caracteres")
	private String txCausaObito;

	@Column(name = "tx_referencia")
	// @NotEmpty(message = "Referencia campo obrigatório!")
	@Size(max = 100, message = "Referencia tamanho máximo de 20 caracteres")
	private String txReferencia;

	@Column(name = "tx_descricao")
	// @NotEmpty(message = "Descricao campo obrigatório!")
	@Size(max = 1000, message = "Descricao tamanho máximo de 1000 caracteres")
	private String txDescricao;

	@Column(name = "tx_descricao_abreviada")
	// @NotEmpty(message = "DescricaoAbreviada campo obrigatório!")
	@Size(max = 1000, message = "DescricaoAbreviada tamanho máximo de 1000 caracteres")
	private String txDescricaoAbreviada;

	@Column(name = "tx_excluido")
	// @NotEmpty(message = "Excluido campo obrigatório!")
	@Size(max = 100, message = "Excluido tamanho máximo de 10 caracteres")
	private String txExcluido;

	@Column(name = "dt_criacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCriacao;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}

	public Integer getCdSubcategoria() {
		return cdSubcategoria;
	}

	public void setCdSubcategoria(Integer cdSubcategoria) {
		this.cdSubcategoria = cdSubcategoria;
	}

	public Integer getCdCategoria() {
		return cdCategoria;
	}

	public void setCdCategoria(Integer cdCategoria) {
		this.cdCategoria = cdCategoria;
	}

	public String getTxSubcategoria() {
		return txSubcategoria;
	}

	public void setTxSubcategoria(String txSubcategoria) {
		this.txSubcategoria = txSubcategoria;
	}

	public String getTxClassificacao() {
		return txClassificacao;
	}

	public void setTxClassificacao(String txClassificacao) {
		this.txClassificacao = txClassificacao;
	}

	public String getTxSexoRestrit() {
		return txSexoRestrit;
	}

	public void setTxSexoRestrit(String txSexoRestrit) {
		this.txSexoRestrit = txSexoRestrit;
	}

	public String getTxCausaObito() {
		return txCausaObito;
	}

	public void setTxCausaObito(String txCausaObito) {
		this.txCausaObito = txCausaObito;
	}

	public String getTxReferencia() {
		return txReferencia;
	}

	public void setTxReferencia(String txReferencia) {
		this.txReferencia = txReferencia;
	}

	public String getTxDescricao() {
		return txDescricao;
	}

	public void setTxDescricao(String txDescricao) {
		this.txDescricao = txDescricao;
	}

	public String getTxDescricaoAbreviada() {
		return txDescricaoAbreviada;
	}

	public void setTxDescricaoAbreviada(String txDescricaoAbreviada) {
		this.txDescricaoAbreviada = txDescricaoAbreviada;
	}

	public String getTxExcluido() {
		return txExcluido;
	}

	public void setTxExcluido(String txExcluido) {
		this.txExcluido = txExcluido;
	}

	public Date getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}
	

}
