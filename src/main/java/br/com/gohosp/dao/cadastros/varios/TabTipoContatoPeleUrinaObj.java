package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_contato_pele_urina", schema = Constants.SCHEMA)
public class TabTipoContatoPeleUrinaObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_paciente_pele_contato_urina")
	// @NotNull(message = "PacientePeleContatoUrina campo obrigatório!")
	private Integer cdPacientePeleContatoUrina;

	@Column(name = "tx_paciente_pele_contato_urina")
	// @NotEmpty(message = "PacientePeleContatoUrina campo obrigatório!")
	@Size(max = 45, message = "PacientePeleContatoUrina tamanho máximo de 45 caracteres")
	private String txPacientePeleContatoUrina;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txPacientePeleContatoUrina))
			txPacientePeleContatoUrina = txPacientePeleContatoUrina.toUpperCase();
	}

}
