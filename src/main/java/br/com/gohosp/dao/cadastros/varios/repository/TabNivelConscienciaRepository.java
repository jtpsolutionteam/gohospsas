package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabNivelConscienciaObj;



public interface TabNivelConscienciaRepository extends JpaRepository<TabNivelConscienciaObj, Integer> {



}