package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_andar_cama", schema = Constants.SCHEMA)
public class TabTipoAndarCamaObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_andar_cama")
	// @NotNull(message = "TipoAndarCama campo obrigatório!")
	private Integer cdTipoAndarCama;

	@Column(name = "tx_tipo_andar_cama")
	// @NotEmpty(message = "TipoAndarCama campo obrigatório!")
	@Size(max = 45, message = "TipoAndarCama tamanho máximo de 45 caracteres")
	private String txTipoAndarCama;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoAndarCama))
			txTipoAndarCama = txTipoAndarCama.toUpperCase();
	}

}
