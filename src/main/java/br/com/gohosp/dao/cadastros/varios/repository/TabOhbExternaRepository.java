package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabOhbExternaObj;



public interface TabOhbExternaRepository extends JpaRepository<TabOhbExternaObj, Integer> {



}