package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_banho", schema = Constants.SCHEMA)
public class TabTipoBanhoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_banho")
	// @NotNull(message = "TipoBanho campo obrigatório!")
	private Integer cdTipoBanho;

	@Column(name = "tx_tipo_banho")
	// @NotEmpty(message = "TipoBanho campo obrigatório!")
	@Size(max = 300, message = "TipoBanho tamanho máximo de 300 caracteres")
	private String txTipoBanho;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoBanho))
			txTipoBanho = txTipoBanho.toUpperCase();
	}

}
