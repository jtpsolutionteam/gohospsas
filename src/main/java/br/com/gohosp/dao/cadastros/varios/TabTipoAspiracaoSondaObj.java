package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_aspiracao_sonda", schema = Constants.SCHEMA)
public class TabTipoAspiracaoSondaObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_aspiracao_sonda")
	// @NotNull(message = "TipoAspiracaoSonda campo obrigatório!")
	private Integer cdTipoAspiracaoSonda;

	@Column(name = "tx_tipo_aspiracao_sonda")
	// @NotEmpty(message = "TipoAspiracaoSonda campo obrigatório!")
	@Size(max = 45, message = "TipoAspiracaoSonda tamanho máximo de 45 caracteres")
	private String txTipoAspiracaoSonda;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoAspiracaoSonda))
			txTipoAspiracaoSonda = txTipoAspiracaoSonda.toUpperCase();
	}

}
