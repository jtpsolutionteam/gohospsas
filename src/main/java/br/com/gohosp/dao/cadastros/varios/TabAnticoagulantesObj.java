package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "tab_anticoagulantes", schema = Constants.SCHEMA)
public class TabAnticoagulantesObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_codigo")
	// @NotNull(message = "Codigo campo obrigatório!")
	private Integer cdCodigo;

	@Column(name = "tx_descricao")
	// @NotEmpty(message = "Descricao campo obrigatório!")
	@Size(max = 200, message = "Descricao tamanho máximo de 200 caracteres")
	private String txDescricao;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txDescricao))
			txDescricao = txDescricao.toUpperCase();
	}

	public Integer getCdCodigo() {
		return cdCodigo;
	}

	public void setCdCodigo(Integer cdCodigo) {
		this.cdCodigo = cdCodigo;
	}

	public String getTxDescricao() {
		return txDescricao;
	}

	public void setTxDescricao(String txDescricao) {
		this.txDescricao = txDescricao;
	}
	
	

}
