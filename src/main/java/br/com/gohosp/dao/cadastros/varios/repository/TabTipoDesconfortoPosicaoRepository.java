package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabTipoDesconfortoPosicaoObj;



public interface TabTipoDesconfortoPosicaoRepository extends JpaRepository<TabTipoDesconfortoPosicaoObj, Integer> {



}