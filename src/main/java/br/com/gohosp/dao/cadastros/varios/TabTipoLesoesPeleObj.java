package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_lesoes_pele", schema = Constants.SCHEMA)
public class TabTipoLesoesPeleObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_lesao_pele")
	// @NotNull(message = "TipoLesaoPele campo obrigatório!")
	private Integer cdTipoLesaoPele;

	@Column(name = "tx_tipo_lesao_pele")
	// @NotEmpty(message = "TipoLesaoPele campo obrigatório!")
	@Size(max = 200, message = "TipoLesaoPele tamanho máximo de 200 caracteres")
	private String txTipoLesaoPele;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoLesaoPele))
			txTipoLesaoPele = txTipoLesaoPele.toUpperCase();
	}

}
