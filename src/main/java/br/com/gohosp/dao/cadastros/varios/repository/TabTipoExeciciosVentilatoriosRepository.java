package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabTipoExeciciosVentilatoriosObj;



public interface TabTipoExeciciosVentilatoriosRepository extends JpaRepository<TabTipoExeciciosVentilatoriosObj, Integer> {



}