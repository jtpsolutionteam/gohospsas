package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabCuidadorTipoObj;



public interface TabCuidadorTipoRepository extends JpaRepository<TabCuidadorTipoObj, Integer> {



}