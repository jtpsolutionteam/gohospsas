package br.com.gohosp.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_local_paciente", schema = Constants.SCHEMA)
public class TabLocalPacienteObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_local_paciente")
	// @NotNull(message = "LocalPaciente campo obrigatório!")
	private Integer cdLocalPaciente;

	@Column(name = "tx_local_paciente")
	// @NotEmpty(message = "LocalPaciente campo obrigatório!")
	@Size(max = 45, message = "LocalPaciente tamanho máximo de 45 caracteres")
	private String txLocalPaciente;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txLocalPaciente))
			txLocalPaciente = txLocalPaciente.toUpperCase();
	}

}
