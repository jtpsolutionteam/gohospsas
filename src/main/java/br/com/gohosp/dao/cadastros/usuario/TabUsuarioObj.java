package br.com.gohosp.dao.cadastros.usuario;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import br.com.gohosp.Constants;

@Entity
@Table(name = "tab_usuario", schema = Constants.SCHEMA)
public class TabUsuarioObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_usuario")
	// @NotNull(message = "Usuario campo obrigatório!")
	private Integer cdUsuario;

	@Column(name = "ck_ativo")
	@NotNull(message = "Ativo? campo obrigatório!")
	private Integer ckAtivo;

	@Column(name = "tx_nome")
	@NotEmpty(message = "Nome campo obrigatório!")
	@Size(max = 200, message = "Nome tamanho máximo de 200 caracteres")
	private String txNome;

	@Column(name = "tx_apelido")
	@NotEmpty(message = "Apelido campo obrigatório!")
	@Size(max = 200, message = "Apelido tamanho máximo de 200 caracteres")
	private String txApelido;

	@Column(name = "tx_senha")
	// @NotEmpty(message = "Senha campo obrigatório!")
	@Size(max = 200, message = "Senha tamanho máximo de 200 caracteres")
	private String txSenha;

	@Column(name = "dt_validade_senha")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	@NotNull(message = "Validade Senha campo obrigatório!")
	private Date dtValidadeSenha;

	@Column(name = "tx_email")
	@NotEmpty(message = "Email campo obrigatório!")
	@Size(max = 100, message = "Email tamanho máximo de 100 caracteres")
	private String txEmail;

	@Column(name = "tx_tel")
	// @NotEmpty(message = "Tel campo obrigatório!")
	@Size(max = 20, message = "Tel tamanho máximo de 20 caracteres")
	private String txTel;

	@Column(name = "cd_grupo_visao")
	@NotNull(message = "Grupo de Visão campo obrigatório!")
	private Integer cdGrupoVisao;

	@Column(name = "tx_sexo")
	@NotEmpty(message = "Sexo campo obrigatório!")
	@Size(max = 20, message = "Sexo tamanho máximo de 20 caracteres")
	private String txSexo;

	
	public Integer getCdUsuario() {
		return cdUsuario;
	}

	public void setCdUsuario(Integer cdUsuario) {
		this.cdUsuario = cdUsuario;
	}

	public Integer getCkAtivo() {
		return ckAtivo;
	}

	public void setCkAtivo(Integer ckAtivo) {
		this.ckAtivo = ckAtivo;
	}

	public String getTxNome() {
		return txNome;
	}

	public void setTxNome(String txNome) {
		this.txNome = txNome;
	}

	public String getTxApelido() {
		return txApelido;
	}

	public void setTxApelido(String txApelido) {
		this.txApelido = txApelido;
	}

	public String getTxSenha() {
		return txSenha;
	}

	public void setTxSenha(String txSenha) {
		this.txSenha = txSenha;
	}

	public Date getDtValidadeSenha() {
		return dtValidadeSenha;
	}

	public void setDtValidadeSenha(Date dtValidadeSenha) {
		this.dtValidadeSenha = dtValidadeSenha;
	}

	public String getTxEmail() {
		return txEmail;
	}

	public void setTxEmail(String txEmail) {
		this.txEmail = txEmail;
	}

	public String getTxTel() {
		return txTel;
	}

	public void setTxTel(String txTel) {
		this.txTel = txTel;
	}

	public Integer getCdGrupoVisao() {
		return cdGrupoVisao;
	}

	public void setCdGrupoVisao(Integer cdGrupoVisao) {
		this.cdGrupoVisao = cdGrupoVisao;
	}

	public String getTxSexo() {
		return txSexo;
	}

	public void setTxSexo(String txSexo) {
		this.txSexo = txSexo;
	}

	
	
}
