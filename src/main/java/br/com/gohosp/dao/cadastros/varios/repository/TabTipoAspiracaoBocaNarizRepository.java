package br.com.gohosp.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.varios.TabTipoAspiracaoBocaNarizObj;



public interface TabTipoAspiracaoBocaNarizRepository extends JpaRepository<TabTipoAspiracaoBocaNarizObj, Integer> {



}