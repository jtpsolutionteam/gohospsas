package br.com.gohosp.dao.cadastros.usuario.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.usuario.TabUsuarioObj;


public interface TabUsuarioRepository extends JpaRepository<TabUsuarioObj, Integer> {
	
	TabUsuarioObj findByTxEmail(String email);
	
	TabUsuarioObj findByTxEmailAndTxSenha(String email, String senha);
	
	TabUsuarioObj findByTxEmailAndTxSenhaAndCkAtivo(String email, String senha, Integer ativo);
		
	TabUsuarioObj findByTxEmailAndCkAtivo(String txEmail, Integer ckAtivo);
	
	
	
}
