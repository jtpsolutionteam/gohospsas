package br.com.gohosp.dao.cadastros.cid.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.cadastros.cid.TabCidCapituloObj;



public interface TabCidCapituloRepository extends JpaRepository<TabCidCapituloObj, Integer> {



}