package br.com.gohosp.dao.administracao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.gohosp.Constants;

@Entity
@Table(name = "tab_telas", schema = Constants.SCHEMA)
public class TabTelasObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tela")
	// @NotNull(message = "Tela campo obrigatório!")
	private Integer cdTela;

	@Column(name = "tx_tela")
	@NotEmpty(message = "Tela campo obrigatório!")
	@Size(max = 100, message = "Tela tamanho máximo de 100 caracteres")
	private String txTela;

	@Column(name = "tx_telaingles")
	// @NotEmpty(message = "Tela Inglês campo obrigatório!")
	@Size(max = 100, message = "Telaingles tamanho máximo de 100 caracteres")
	private String txTelaingles;

	@Column(name = "tx_service")
	@NotEmpty(message = "Service campo obrigatório!")
	@Size(max = 100, message = "Service tamanho máximo de 100 caracteres")
	private String txService;

	@Column(name = "tx_alias_tela")
	@NotEmpty(message = "Alias Tela campo obrigatório!")
	@Size(max = 100, message = "AliasTela tamanho máximo de 100 caracteres")
	private String txAliasTela;

	@Column(name = "tx_controller")
	@NotEmpty(message = "Controller campo obrigatório!")
	@Size(max = 100, message = "Controller tamanho máximo de 100 caracteres")
	private String txController;
	
	@Column(name = "tx_keyfield")
	@NotEmpty(message = "KeyField campo obrigatório!")
	@Size(max = 45, message = "KeyField tamanho máximo de 45 caracteres")
	private String txKeyfield;

	
	
	public Integer getCdTela() {
		return cdTela;
	}

	public void setCdTela(Integer cdTela) {
		this.cdTela = cdTela;
	}

	public String getTxTela() {
		return txTela;
	}

	public void setTxTela(String txTela) {
		this.txTela = txTela;
	}

	public String getTxTelaingles() {
		return txTelaingles;
	}

	public void setTxTelaingles(String txTelaingles) {
		this.txTelaingles = txTelaingles;
	}

	public String getTxService() {
		return txService;
	}

	public void setTxService(String txService) {
		this.txService = txService;
	}

	public String getTxAliasTela() {
		return txAliasTela;
	}

	public void setTxAliasTela(String txAliasTela) {
		this.txAliasTela = txAliasTela;
	}

	public String getTxController() {
		return txController;
	}

	public void setTxController(String txController) {
		this.txController = txController;
	}

	public String getTxKeyfield() {
		return txKeyfield;
	}

	public void setTxKeyfield(String txKeyfield) {
		this.txKeyfield = txKeyfield;
	}

	

}
