package br.com.gohosp.dao.administracao.vw;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "vw_tab_regras", schema = Constants.SCHEMA)
public class VwTabRegrasObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_regra")
//@NotNull(message = "Regra campo obrigatório!")
	private Integer cdRegra;

	@Column(name = "cd_tipo")
//@NotNull(message = "Tipo campo obrigatório!")
	private Integer cdTipo;

	@Column(name = "tx_tipo")
	private String txTipo;

	
	@Column(name = "tx_campo")
//@NotEmpty(message = "Campo campo obrigatório!")
	@Size(max = 50, message = "Campo tamanho máximo de 50 caracteres")
	private String txCampo;

	@Column(name = "tx_objcampo")
//@NotEmpty(message = "Objcampo campo obrigatório!")
	@Size(max = 50, message = "Objcampo tamanho máximo de 50 caracteres")
	private String txObjcampo;

	@Column(name = "cd_tela")
//@NotNull(message = "Tela campo obrigatório!")
	private Integer cdTela;

	@Column(name = "cd_grupo_visao")
//@NotNull(message = "GrupoVisao campo obrigatório!")
	private Integer cdGrupoVisao;

	@Column(name = "tx_campo_cond")
//@NotEmpty(message = "CampoCond campo obrigatório!")
	@Size(max = 80, message = "CampoCond tamanho máximo de 80 caracteres")
	private String txCampoCond;

	@Column(name = "tx_objcampo_cond")
//@NotEmpty(message = "ObjcampoCond campo obrigatório!")
	@Size(max = 30, message = "ObjcampoCond tamanho máximo de 30 caracteres")
	private String txObjcampoCond;

	@Column(name = "tx_mensagem")
//@NotEmpty(message = "Mensagem campo obrigatório!")
	@Size(max = 1000, message = "Mensagem tamanho máximo de 1000 caracteres")
	private String txMensagem;

	@Column(name = "tx_aux")
//@NotEmpty(message = "Aux campo obrigatório!")
	@Size(max = 200, message = "Aux tamanho máximo de 200 caracteres")
	private String txAux;

	@Column(name = "tx_email")
//@NotEmpty(message = "Email campo obrigatório!")
	@Size(max = 2000, message = "Email tamanho máximo de 2000 caracteres")
	private String txEmail;

	@Column(name = "cd_ordem")
//@NotNull(message = "Ordem campo obrigatório!")
	private Integer cdOrdem;

	@Column(name = "ck_ignora")
//@NotNull(message = "ckIgnora campo obrigatório!")
	private Integer ckIgnora;

	@Column(name = "tx_ignora")
//@NotEmpty(message = "Ignora campo obrigatório!")
	@Size(max = 50, message = "Ignora tamanho máximo de 50 caracteres")
	private String txIgnora;

	@Column(name = "tx_controller")
//@NotEmpty(message = "Controller campo obrigatório!")
	@Size(max = 100, message = "Controller tamanho máximo de 100 caracteres")
	private String txController;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txCampo))
			txCampo = txCampo.toUpperCase();
		if (!Validator.isBlankOrNull(txObjcampo))
			txObjcampo = txObjcampo.toUpperCase();
		if (!Validator.isBlankOrNull(txCampoCond))
			txCampoCond = txCampoCond.toUpperCase();
		if (!Validator.isBlankOrNull(txObjcampoCond))
			txObjcampoCond = txObjcampoCond.toUpperCase();
		if (!Validator.isBlankOrNull(txMensagem))
			txMensagem = txMensagem.toUpperCase();
		if (!Validator.isBlankOrNull(txAux))
			txAux = txAux.toUpperCase();
		if (!Validator.isBlankOrNull(txEmail))
			txEmail = txEmail.toUpperCase();
		if (!Validator.isBlankOrNull(txIgnora))
			txIgnora = txIgnora.toUpperCase();
		if (!Validator.isBlankOrNull(txController))
			txController = txController.toUpperCase();
	}

	public Integer getCdRegra() {
		return cdRegra;
	}

	public void setCdRegra(Integer cdRegra) {
		this.cdRegra = cdRegra;
	}

	public Integer getCdTipo() {
		return cdTipo;
	}

	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	public String getTxCampo() {
		return txCampo;
	}

	public void setTxCampo(String txCampo) {
		this.txCampo = txCampo;
	}

	public String getTxObjcampo() {
		return txObjcampo;
	}

	public void setTxObjcampo(String txObjcampo) {
		this.txObjcampo = txObjcampo;
	}

	public Integer getCdTela() {
		return cdTela;
	}

	public void setCdTela(Integer cdTela) {
		this.cdTela = cdTela;
	}

	public Integer getCdGrupoVisao() {
		return cdGrupoVisao;
	}

	public void setCdGrupoVisao(Integer cdGrupoVisao) {
		this.cdGrupoVisao = cdGrupoVisao;
	}

	public String getTxCampoCond() {
		return txCampoCond;
	}

	public void setTxCampoCond(String txCampoCond) {
		this.txCampoCond = txCampoCond;
	}

	public String getTxObjcampoCond() {
		return txObjcampoCond;
	}

	public void setTxObjcampoCond(String txObjcampoCond) {
		this.txObjcampoCond = txObjcampoCond;
	}

	public String getTxMensagem() {
		return txMensagem;
	}

	public void setTxMensagem(String txMensagem) {
		this.txMensagem = txMensagem;
	}

	public String getTxAux() {
		return txAux;
	}

	public void setTxAux(String txAux) {
		this.txAux = txAux;
	}

	public String getTxEmail() {
		return txEmail;
	}

	public void setTxEmail(String txEmail) {
		this.txEmail = txEmail;
	}

	public Integer getCdOrdem() {
		return cdOrdem;
	}

	public void setCdOrdem(Integer cdOrdem) {
		this.cdOrdem = cdOrdem;
	}

	public Integer getCkIgnora() {
		return ckIgnora;
	}

	public void setCkIgnora(Integer ckIgnora) {
		this.ckIgnora = ckIgnora;
	}

	public String getTxIgnora() {
		return txIgnora;
	}

	public void setTxIgnora(String txIgnora) {
		this.txIgnora = txIgnora;
	}

	public String getTxController() {
		return txController;
	}

	public void setTxController(String txController) {
		this.txController = txController;
	}

	public String getTxTipo() {
		return txTipo;
	}

	public void setTxTipo(String txTipo) {
		this.txTipo = txTipo;
	}

}
