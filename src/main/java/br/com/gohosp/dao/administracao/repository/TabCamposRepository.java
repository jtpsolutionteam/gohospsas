package br.com.gohosp.dao.administracao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.administracao.TabCamposObj;

public interface TabCamposRepository extends JpaRepository<TabCamposObj, Integer> {

	@Query("select t from TabCamposObj t where t.cdGrupoVisao = ?1 and t.cdTela = ?2 order by t.txObjCampo")	
	List<TabCamposObj> findByCdGrupoCdTelaQuery(Integer cdGrupoVisao, Integer cdTela);
	
	
	
}
