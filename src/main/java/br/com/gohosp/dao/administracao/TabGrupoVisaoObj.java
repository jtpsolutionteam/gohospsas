package br.com.gohosp.dao.administracao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.gohosp.Constants;

@Entity
@Table(name="tab_grupo_visao", schema=Constants.SCHEMA)
public class TabGrupoVisaoObj {

	@Id @GeneratedValue
	@Column(name = "cd_grupo_visao")
	private Integer cdGrupoVisao;

	@Column(name = "tx_grupo_visao")
	private String txGrupoVisao;

	public Integer getCdGrupoVisao() {
		return cdGrupoVisao;
	}

	public void setCdGrupoVisao(Integer cdGrupoVisao) {
		this.cdGrupoVisao = cdGrupoVisao;
	}

	public String getTxGrupoVisao() {
		return txGrupoVisao;
	}

	public void setTxGrupoVisao(String txGrupoVisao) {
		this.txGrupoVisao = txGrupoVisao;
	}

	
}
