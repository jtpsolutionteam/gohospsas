package br.com.gohosp.dao.administracao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.administracao.TabRegrasTipoObj;




public interface TabRegrasTipoRepository extends JpaRepository<TabRegrasTipoObj, Integer> {



}