package br.com.gohosp.dao.administracao.vw;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.administracao.TabMenuSubObj;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "vw_tab_menu", schema = Constants.SCHEMA)
public class VwTabMenuObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_menu")
	// @NotNull(message = "Menu campo obrigatório!")
	private Integer cdMenu;

	@Column(name = "tx_menu")
	// @NotEmpty(message = "Menu campo obrigatório!")
	@Size(max = 45, message = "Menu tamanho máximo de 45 caracteres")
	private String txMenu;

	@Column(name = "cd_ordem")
	// @NotNull(message = "Ordem campo obrigatório!")
	private Integer cdOrdem;

	@Column(name = "tx_class")
	// @NotEmpty(message = "Class campo obrigatório!")
	@Size(max = 100, message = "Class tamanho máximo de 100 caracteres")
	private String txClass;

	@Column(name = "tx_url_page")
	// @NotEmpty(message = "UrlPage campo obrigatório!")
	@Size(max = 100, message = "UrlPage tamanho máximo de 100 caracteres")
	private String txUrlPage;

	@Column(name = "tx_alias")
	// @NotEmpty(message = "Alias campo obrigatório!")
	@Size(max = 20, message = "Alias tamanho máximo de 20 caracteres")
	private String txAlias;

	@Column(name = "cd_grupo_visao")
	// @NotNull(message = "GrupoVisao campo obrigatório!")
	private Integer cdGrupoVisao;
	
	@OneToMany(mappedBy = "cdMenu", targetEntity = TabMenuSubObj.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<TabMenuSubObj> listaSubMenus;


	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txMenu))
			txMenu = txMenu.toUpperCase();
		if (!Validator.isBlankOrNull(txClass))
			txClass = txClass.toUpperCase();
		if (!Validator.isBlankOrNull(txUrlPage))
			txUrlPage = txUrlPage.toUpperCase();
		if (!Validator.isBlankOrNull(txAlias))
			txAlias = txAlias.toUpperCase();
	}

	

}
