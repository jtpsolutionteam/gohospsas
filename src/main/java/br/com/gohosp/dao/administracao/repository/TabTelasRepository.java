package br.com.gohosp.dao.administracao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.administracao.TabTelasObj;

public interface TabTelasRepository extends JpaRepository<TabTelasObj, Integer> {

	List<TabTelasObj> findByOrderByTxTelaAsc();
	
	//@Query("select t from TabTelasObj t where t.txService = ?1 limit 1")
	TabTelasObj findTop1ByTxService(String txService);
	
	TabTelasObj findByTxController(String txController);
}
