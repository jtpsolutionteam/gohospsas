package br.com.gohosp.dao.administracao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.gohosp.Constants;

@Entity
@Table(name = "tab_campos", schema = Constants.SCHEMA)
public class TabCamposObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_campo")
	// @NotNull(message = "Campo campo obrigatório!")
	private Integer cdCampo;

	@Column(name = "cd_tela")
	@NotNull(message = "Tela campo obrigatório!")
	private Integer cdTela;

	@Column(name = "cd_grupo_visao")
	@NotNull(message = "GrupoVisao campo obrigatório!")
	private Integer cdGrupoVisao;

	@Column(name = "ck_alterar")
	@NotNull(message = "Alterar campo obrigatório!")
	private Integer ckAlterar;

	@Column(name = "tx_aux")
	// @NotEmpty(message = "Aux campo obrigatório!")
	@Size(max = 1000, message = "Aux tamanho máximo de 1000 caracteres")
	private String txAux;

	@Column(name = "tx_obj_campo")
	@NotEmpty(message = "Objcampo campo obrigatório!")
	@Size(max = 100, message = "Objcampo tamanho máximo de 100 caracteres")
	private String txObjCampo;

	@Column(name = "tx_tipo_campo")
	//@NotEmpty(message = "TipoCampo campo obrigatório!")
	@Size(max = 100, message = "TipoCampo tamanho máximo de 100 caracteres")
	private String txTipoCampo;

	@Column(name = "ck_visualizar")
	@NotNull(message = "Visualizar campo obrigatório!")
	private Integer ckVisualizar;
	
	@Column(name = "cd_ordem")
	private Integer cdOrdem;
	
	@Column(name = "tx_campo")
	private String txCampo;
	
	@Column(name = "vl_size")
	private Integer vlSize;
	

	public Integer getCdCampo() {
		return cdCampo;
	}

	public void setCdCampo(Integer cdCampo) {
		this.cdCampo = cdCampo;
	}

	public Integer getCdTela() {
		return cdTela;
	}

	public void setCdTela(Integer cdTela) {
		this.cdTela = cdTela;
	}

	public Integer getCdGrupoVisao() {
		return cdGrupoVisao;
	}

	public void setCdGrupoVisao(Integer cdGrupoVisao) {
		this.cdGrupoVisao = cdGrupoVisao;
	}

	public Integer getCkAlterar() {
		return ckAlterar;
	}

	public void setCkAlterar(Integer ckAlterar) {
		this.ckAlterar = ckAlterar;
	}

	public String getTxAux() {
		return txAux;
	}

	public void setTxAux(String txAux) {
		this.txAux = txAux;
	}

	public String getTxObjCampo() {
		return txObjCampo;
	}

	public void setTxObjCampo(String txObjCampo) {
		this.txObjCampo = txObjCampo;
	}

	public String getTxTipoCampo() {
		return txTipoCampo;
	}

	public void setTxTipoCampo(String txTipoCampo) {
		this.txTipoCampo = txTipoCampo;
	}

	public Integer getCkVisualizar() {
		return ckVisualizar;
	}

	public void setCkVisualizar(Integer ckVisualizar) {
		this.ckVisualizar = ckVisualizar;
	}

	public Integer getCdOrdem() {
		return cdOrdem;
	}

	public void setCdOrdem(Integer cdOrdem) {
		this.cdOrdem = cdOrdem;
	}

	public String getTxCampo() {
		return txCampo;
	}

	public void setTxCampo(String txCampo) {
		this.txCampo = txCampo;
	}

	public Integer getVlSize() {
		return vlSize;
	}

	public void setVlSize(Integer vlSize) {
		this.vlSize = vlSize;
	}

	
	

}
