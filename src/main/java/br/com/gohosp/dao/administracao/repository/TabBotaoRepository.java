package br.com.gohosp.dao.administracao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.administracao.TabBotaoObj;


public interface TabBotaoRepository extends JpaRepository<TabBotaoObj, Integer> {

    @Query("select t from TabBotaoObj t where t.cdGrupoVisao = ?1 and t.cdTela = ?2 order by t.txObjBotao")	
	List<TabBotaoObj> findByCdGrupoVisaoCdTelaQuery(Integer cdGrupoVisao, Integer cdTela);
	
	
	
}
