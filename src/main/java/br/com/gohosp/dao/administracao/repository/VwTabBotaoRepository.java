package br.com.gohosp.dao.administracao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.administracao.vw.VwTabBotaoObj;



public interface VwTabBotaoRepository extends JpaRepository<VwTabBotaoObj, Integer> {

  @Query("select t from VwTabBotaoObj t where t.cdGrupoVisao = ?1 and t.txController = ?2")
  List<VwTabBotaoObj> findBycdGrupoVisaotxControllerQuery(Integer cdGrupoVisao, String txController);
}