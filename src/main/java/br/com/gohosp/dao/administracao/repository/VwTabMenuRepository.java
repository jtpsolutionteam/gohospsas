package br.com.gohosp.dao.administracao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.administracao.vw.VwTabMenuObj;

public interface VwTabMenuRepository extends JpaRepository<VwTabMenuObj, Integer> {

	@Query("select t from VwTabMenuObj t where t.cdGrupoVisao = ?1 order by t.cdOrdem ")
	List<VwTabMenuObj> findOrderByCdOrdemQuery(Integer cdGrupoVisao);
	


}
