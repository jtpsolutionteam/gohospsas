package br.com.gohosp.dao.administracao.vw;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "vw_tab_campos", schema = Constants.SCHEMA)
public class VwTabCamposObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_campo")
	// @NotNull(message = "Campo campo obrigatório!")
	private Integer cdCampo;

	@Column(name = "cd_tela")
	// @NotNull(message = "Tela campo obrigatório!")
	private Integer cdTela;

	@Column(name = "cd_grupo_visao")
	// @NotNull(message = "GrupoVisao campo obrigatório!")
	private Integer cdGrupoVisao;

	@Column(name = "ck_alterar")
	// @NotNull(message = "ckAlterar campo obrigatório!")
	private Integer ckAlterar;

	@Column(name = "tx_aux")
	// @NotEmpty(message = "Aux campo obrigatório!")
	@Size(max = 1000, message = "Aux tamanho máximo de 1000 caracteres")
	private String txAux;

	@Column(name = "tx_obj_campo")
	// @NotEmpty(message = "ObjCampo campo obrigatório!")
	@Size(max = 100, message = "ObjCampo tamanho máximo de 100 caracteres")
	private String txObjCampo;

	@Column(name = "tx_tipo_campo")
	// @NotEmpty(message = "TipoCampo campo obrigatório!")
	@Size(max = 100, message = "TipoCampo tamanho máximo de 100 caracteres")
	private String txTipoCampo;

	@Column(name = "ck_visualizar")
	// @NotNull(message = "ckVisualizar campo obrigatório!")
	private Integer ckVisualizar;

	@Column(name = "tx_alias_tela")
	// @NotEmpty(message = "AliasTela campo obrigatório!")
	@Size(max = 100, message = "AliasTela tamanho máximo de 100 caracteres")
	private String txAliasTela;

	@Column(name = "tx_controller")
	// @NotEmpty(message = "Controller campo obrigatório!")
	@Size(max = 100, message = "Controller tamanho máximo de 100 caracteres")
	private String txController;

	@Column(name = "tx_service")
	// @NotEmpty(message = "Service campo obrigatório!")
	@Size(max = 100, message = "Service tamanho máximo de 100 caracteres")
	private String txService;

	@Column(name = "tx_tela")
	// @NotEmpty(message = "Tela campo obrigatório!")
	@Size(max = 100, message = "Tela tamanho máximo de 100 caracteres")
	private String txTela;
	
	@Column(name = "cd_ordem")
	private Integer cdOrdem;
	
	@Column(name = "tx_campo")
	private String txCampo;
	
	@Column(name = "vl_size")
	private Integer vlSize;
	

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txAux))
			txAux = txAux.toUpperCase();
		if (!Validator.isBlankOrNull(txObjCampo))
			txObjCampo = txObjCampo.toUpperCase();
		if (!Validator.isBlankOrNull(txTipoCampo))
			txTipoCampo = txTipoCampo.toUpperCase();
		if (!Validator.isBlankOrNull(txAliasTela))
			txAliasTela = txAliasTela.toUpperCase();
		if (!Validator.isBlankOrNull(txController))
			txController = txController.toUpperCase();
		if (!Validator.isBlankOrNull(txService))
			txService = txService.toUpperCase();
		if (!Validator.isBlankOrNull(txTela))
			txTela = txTela.toUpperCase();
	}

	public Integer getCdCampo() {
		return cdCampo;
	}

	public void setCdCampo(Integer cdCampo) {
		this.cdCampo = cdCampo;
	}

	public Integer getCdTela() {
		return cdTela;
	}

	public void setCdTela(Integer cdTela) {
		this.cdTela = cdTela;
	}

	public Integer getCdGrupoVisao() {
		return cdGrupoVisao;
	}

	public void setCdGrupoVisao(Integer cdGrupoVisao) {
		this.cdGrupoVisao = cdGrupoVisao;
	}

	public Integer getCkAlterar() {
		return ckAlterar;
	}

	public void setCkAlterar(Integer ckAlterar) {
		this.ckAlterar = ckAlterar;
	}

	public String getTxAux() {
		return txAux;
	}

	public void setTxAux(String txAux) {
		this.txAux = txAux;
	}

	public String getTxObjCampo() {
		return txObjCampo;
	}

	public void setTxObjCampo(String txObjCampo) {
		this.txObjCampo = txObjCampo;
	}

	public String getTxTipoCampo() {
		return txTipoCampo;
	}

	public void setTxTipoCampo(String txTipoCampo) {
		this.txTipoCampo = txTipoCampo;
	}

	public Integer getCkVisualizar() {
		return ckVisualizar;
	}

	public void setCkVisualizar(Integer ckVisualizar) {
		this.ckVisualizar = ckVisualizar;
	}

	public String getTxAliasTela() {
		return txAliasTela;
	}

	public void setTxAliasTela(String txAliasTela) {
		this.txAliasTela = txAliasTela;
	}

	public String getTxController() {
		return txController;
	}

	public void setTxController(String txController) {
		this.txController = txController;
	}

	public String getTxService() {
		return txService;
	}

	public void setTxService(String txService) {
		this.txService = txService;
	}

	public String getTxTela() {
		return txTela;
	}

	public void setTxTela(String txTela) {
		this.txTela = txTela;
	}

	public Integer getCdOrdem() {
		return cdOrdem;
	}

	public void setCdOrdem(Integer cdOrdem) {
		this.cdOrdem = cdOrdem;
	}

	public String getTxCampo() {
		return txCampo;
	}

	public void setTxCampo(String txCampo) {
		this.txCampo = txCampo;
	}

	public Integer getVlSize() {
		return vlSize;
	}

	public void setVlSize(Integer vlSize) {
		this.vlSize = vlSize;
	}

}
