package br.com.gohosp.dao.administracao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.administracao.vw.VwTabRegrasObj;



public interface VwTabRegrasRepository extends JpaRepository<VwTabRegrasObj, Integer> {

	List<VwTabRegrasObj> findByCdTelaOrderByCdOrdemAsc(Integer cdTela);

	List<VwTabRegrasObj> findByTxControllerOrderByCdOrdemAsc(String txController);

	@Query("select t from VwTabRegrasObj t where t.cdGrupoVisao = ?1 and t.txController = ?2")
	  List<VwTabRegrasObj> findBycdGrupoVisaotxControllerQuery(Integer cdGrupoVisao, String txController);
	
	@Query("select t from VwTabRegrasObj t where t.cdGrupoVisao = ?1 and t.cdTela = ?2 order by t.cdOrdem")	
	List<VwTabRegrasObj> findByCdGrupoVisaoCdTelaQuery(Integer cdGrupoVisao, Integer cdTela);


	
}