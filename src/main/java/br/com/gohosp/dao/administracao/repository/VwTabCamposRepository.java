package br.com.gohosp.dao.administracao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.administracao.vw.VwTabCamposObj;



public interface VwTabCamposRepository extends JpaRepository<VwTabCamposObj, Integer> {

	 @Query("select t from VwTabCamposObj t where t.cdGrupoVisao = ?1 and t.txController = ?2")
	  List<VwTabCamposObj> findBycdGrupoVisaotxControllerQuery(Integer cdGrupoVisao, String txController);

	 @Query("select t from VwTabCamposObj t where t.txService = ?1 and t.txObjCampo = ?2")
	 VwTabCamposObj findByTxObjCampoQuery(String txService, String txObjCampo);
	 
	 List<VwTabCamposObj> findByTxServiceAndCkVisualizarOrderByTxCampoAsc(String txService, Integer ckVisualizar); 

	 
}