package br.com.gohosp.dao.administracao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.administracao.TabMenuSubObj;



public interface TabMenuSubRepository extends JpaRepository<TabMenuSubObj, Integer> {



}