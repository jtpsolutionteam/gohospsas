package br.com.gohosp.dao.administracao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.administracao.TabRegrasObj;



public interface TabRegrasRepository extends JpaRepository<TabRegrasObj, Integer> {

	
    @Query("select t from TabRegrasObj t where t.cdGrupoVisao = ?1 and t.cdTela = ?2 order by t.txObjcampo")	
	List<TabRegrasObj> findByCdGrupoVisaoCdTelaQuery(Integer cdGrupoVisao, Integer cdTela);

	
}