package br.com.gohosp.dao.administracao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.gohosp.Constants;

@Entity
@Table(name = "tab_botao", schema = Constants.SCHEMA)
public class TabBotaoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_botao")
	// @NotNull(message = "Botao campo obrigatório!")
	private Integer cdBotao;

	@Column(name = "cd_tela")
	@NotNull(message = "Tela campo obrigatório!")
	private Integer cdTela;

	@Column(name = "cd_grupo_visao")
	@NotNull(message = "Grupo Visão campo obrigatório!")
	private Integer cdGrupoVisao;

	@Column(name = "tx_acao")
	// @NotEmpty(message = "Acao campo obrigatório!")
	@Size(max = 100, message = "Acao tamanho máximo de 100 caracteres")
	private String txAcao;

	@Column(name = "tx_obj_botao")
	@NotEmpty(message = "Botao campo obrigatório!")
	@Size(max = 100, message = "Botao tamanho máximo de 100 caracteres")
	private String txObjBotao;

	@Column(name = "cd_tipo_acao")
	@NotNull(message = "Tipo da Ação campo obrigatório!")
	private Integer cdTipoAcao;
	
	@Column(name = "ck_visualiza")
	@NotNull(message = "Visualiza ? campo obrigatório!")
	private Integer ckVisualiza;

	public Integer getCdBotao() {
		return cdBotao;
	}

	public void setCdBotao(Integer cdBotao) {
		this.cdBotao = cdBotao;
	}

	public Integer getCdTela() {
		return cdTela;
	}

	public void setCdTela(Integer cdTela) {
		this.cdTela = cdTela;
	}

	public Integer getCdGrupoVisao() {
		return cdGrupoVisao;
	}

	public void setCdGrupoVisao(Integer cdGrupoVisao) {
		this.cdGrupoVisao = cdGrupoVisao;
	}

	public String getTxAcao() {
		return txAcao;
	}

	public void setTxAcao(String txAcao) {
		this.txAcao = txAcao;
	}

	public String getTxObjBotao() {
		return txObjBotao;
	}

	public void setTxObjBotao(String txObjBotao) {
		this.txObjBotao = txObjBotao;
	}

	public Integer getCdTipoAcao() {
		return cdTipoAcao;
	}

	public void setCdTipoAcao(Integer cdTipoAcao) {
		this.cdTipoAcao = cdTipoAcao;
	}

	public Integer getCkVisualiza() {
		return ckVisualiza;
	}

	public void setCkVisualiza(Integer ckVisualiza) {
		this.ckVisualiza = ckVisualiza;
	}

	
	

}
