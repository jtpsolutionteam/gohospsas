package br.com.gohosp.dao.administracao.vw;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;

@Entity
@Table(name = "vw_tab_botao", schema = Constants.SCHEMA)
public class VwTabBotaoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_botao")
	// @NotNull(message = "Botao campo obrigatório!")
	private Integer cdBotao;

	@Column(name = "cd_tela")
	// @NotNull(message = "Tela campo obrigatório!")
	private Integer cdTela;

	@Column(name = "cd_grupo_visao")
	// @NotNull(message = "GrupoVisao campo obrigatório!")
	private Integer cdGrupoVisao;

	@Column(name = "tx_acao")
	// @NotEmpty(message = "Acao campo obrigatório!")
	@Size(max = 100, message = "Acao tamanho máximo de 100 caracteres")
	private String txAcao;

	@Column(name = "tx_obj_botao")
	// @NotEmpty(message = "ObjBotao campo obrigatório!")
	@Size(max = 100, message = "ObjBotao tamanho máximo de 100 caracteres")
	private String txObjBotao;

	@Column(name = "ck_visualiza")
	// @NotNull(message = "ckVisualiza campo obrigatório!")
	private Integer ckVisualiza;

	@Column(name = "cd_tipo_acao")
	// @NotNull(message = "TipoAcao campo obrigatório!")
	private Integer cdTipoAcao;

	@Column(name = "tx_alias_tela")
	// @NotEmpty(message = "AliasTela campo obrigatório!")
	@Size(max = 100, message = "AliasTela tamanho máximo de 100 caracteres")
	private String txAliasTela;

	@Column(name = "tx_controller")
	// @NotEmpty(message = "Controller campo obrigatório!")
	@Size(max = 100, message = "Controller tamanho máximo de 100 caracteres")
	private String txController;

	@Column(name = "tx_service")
	// @NotEmpty(message = "Service campo obrigatório!")
	@Size(max = 100, message = "Service tamanho máximo de 100 caracteres")
	private String txService;

	@Column(name = "tx_tela")
	// @NotEmpty(message = "Tela campo obrigatório!")
	@Size(max = 100, message = "Tela tamanho máximo de 100 caracteres")
	private String txTela;

	public Integer getCdBotao() {
		return cdBotao;
	}

	public void setCdBotao(Integer cdBotao) {
		this.cdBotao = cdBotao;
	}

	public Integer getCdTela() {
		return cdTela;
	}

	public void setCdTela(Integer cdTela) {
		this.cdTela = cdTela;
	}

	public Integer getCdGrupoVisao() {
		return cdGrupoVisao;
	}

	public void setCdGrupoVisao(Integer cdGrupoVisao) {
		this.cdGrupoVisao = cdGrupoVisao;
	}

	public String getTxAcao() {
		return txAcao;
	}

	public void setTxAcao(String txAcao) {
		this.txAcao = txAcao;
	}

	public String getTxObjBotao() {
		return txObjBotao;
	}

	public void setTxObjBotao(String txObjBotao) {
		this.txObjBotao = txObjBotao;
	}

	public Integer getCkVisualiza() {
		return ckVisualiza;
	}

	public void setCkVisualiza(Integer ckVisualiza) {
		this.ckVisualiza = ckVisualiza;
	}

	public Integer getCdTipoAcao() {
		return cdTipoAcao;
	}

	public void setCdTipoAcao(Integer cdTipoAcao) {
		this.cdTipoAcao = cdTipoAcao;
	}

	public String getTxAliasTela() {
		return txAliasTela;
	}

	public void setTxAliasTela(String txAliasTela) {
		this.txAliasTela = txAliasTela;
	}

	public String getTxController() {
		return txController;
	}

	public void setTxController(String txController) {
		this.txController = txController;
	}

	public String getTxService() {
		return txService;
	}

	public void setTxService(String txService) {
		this.txService = txService;
	}

	public String getTxTela() {
		return txTela;
	}

	public void setTxTela(String txTela) {
		this.txTela = txTela;
	}

	

}
