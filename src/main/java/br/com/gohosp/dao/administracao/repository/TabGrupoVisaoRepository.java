package br.com.gohosp.dao.administracao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.administracao.TabGrupoVisaoObj;

public interface TabGrupoVisaoRepository extends JpaRepository<TabGrupoVisaoObj, Integer> {

}
