package br.com.gohosp.dao.administracao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.administracao.TabMenuObj;

public interface TabMenuRepository extends JpaRepository<TabMenuObj, Integer> {

	@Query("select t from TabMenuObj t order by t.cdOrdem ")
	List<TabMenuObj> findOrderByCdOrdemQuery();
	
	
}
