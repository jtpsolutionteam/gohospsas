package br.com.gohosp.dao.administracao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.gohosp.Constants;

@Entity
@Table(name = "tab_menu", schema = Constants.SCHEMA)
public class TabMenuObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_menu")
	// @NotNull(message = "Menu campo obrigatório!")
	private Integer cdMenu;

	@Column(name = "tx_menu")
	@NotEmpty(message = "Menu campo obrigatório!")
	@Size(max = 45, message = "Menu tamanho máximo de 45 caracteres")
	private String txMenu;

	@Column(name = "cd_ordem")
	@NotNull(message = "Ordem campo obrigatório!")
	private Integer cdOrdem;

	@Column(name = "tx_class")
	@NotEmpty(message = "Class campo obrigatório!")
	@Size(max = 100, message = "Class tamanho máximo de 100 caracteres")
	private String txClass;

	@Column(name = "tx_url_page")
	// @NotEmpty(message = "UrlPage campo obrigatório!")
	@Size(max = 100, message = "UrlPage tamanho máximo de 100 caracteres")
	private String txUrlPage;

	@Column(name = "tx_alias")
	// @NotEmpty(message = "Alias campo obrigatório!")
	@Size(max = 20, message = "Alias tamanho máximo de 20 caracteres")
	private String txAlias;

	@OneToMany(mappedBy = "cdMenu", targetEntity = TabMenuSubObj.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<TabMenuSubObj> listaSubMenus;

	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}

	public Integer getCdMenu() {
		return cdMenu;
	}

	public void setCdMenu(Integer cdMenu) {
		this.cdMenu = cdMenu;
	}

	public String getTxMenu() {
		return txMenu;
	}

	public void setTxMenu(String txMenu) {
		this.txMenu = txMenu;
	}

	public Integer getCdOrdem() {
		return cdOrdem;
	}

	public void setCdOrdem(Integer cdOrdem) {
		this.cdOrdem = cdOrdem;
	}

	public String getTxClass() {
		return txClass;
	}

	public void setTxClass(String txClass) {
		this.txClass = txClass;
	}

	public String getTxUrlPage() {
		return txUrlPage;
	}

	public void setTxUrlPage(String txUrlPage) {
		this.txUrlPage = txUrlPage;
	}

	public String getTxAlias() {
		return txAlias;
	}

	public void setTxAlias(String txAlias) {
		this.txAlias = txAlias;
	}

	public List<TabMenuSubObj> getListaSubMenus() {
		return listaSubMenus;
	}

	public void setListaSubMenus(List<TabMenuSubObj> listaSubMenus) {
		this.listaSubMenus = listaSubMenus;
	}


}
