package br.com.gohosp.dao.administracao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "tab_menu_sub", schema = Constants.SCHEMA)
public class TabMenuSubObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_submenu")
	// @NotNull(message = "Submenu campo obrigatório!")
	private Integer cdSubmenu;

	@Column(name = "cd_menu")
	@NotNull(message = "Menu campo obrigatório!")
	private Integer cdMenu;

	@Column(name = "tx_submenu")
	@NotEmpty(message = "SubMenu campo obrigatório!")
	@Size(max = 45, message = "Menu tamanho máximo de 45 caracteres")
	private String txSubMenu;

	@Column(name = "tx_menu_letra")
	@NotEmpty(message = "Menu Letra campo obrigatório!")
	@Size(max = 2, message = "Menu Letra tamanho máximo de 2 caracteres")
	private String txMenuLetra;

	@Column(name = "cd_ordem")
	@NotNull(message = "Ordem campo obrigatório!")
	private Integer cdOrdem;

	@Column(name = "tx_url_page")
	@NotEmpty(message = "UrlPage campo obrigatório!")
	@Size(max = 100, message = "UrlPage tamanho máximo de 100 caracteres")
	private String txUrlPage;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txSubMenu))
			txSubMenu = txSubMenu.toUpperCase();
		if (!Validator.isBlankOrNull(txMenuLetra))
			txMenuLetra = txMenuLetra.toUpperCase();
		
	}

	public Integer getCdSubmenu() {
		return cdSubmenu;
	}

	public void setCdSubmenu(Integer cdSubmenu) {
		this.cdSubmenu = cdSubmenu;
	}

	public Integer getCdMenu() {
		return cdMenu;
	}

	public void setCdMenu(Integer cdMenu) {
		this.cdMenu = cdMenu;
	}



	public String getTxMenuLetra() {
		return txMenuLetra;
	}

	public void setTxMenuLetra(String txMenuLetra) {
		this.txMenuLetra = txMenuLetra;
	}

	public Integer getCdOrdem() {
		return cdOrdem;
	}

	public void setCdOrdem(Integer cdOrdem) {
		this.cdOrdem = cdOrdem;
	}

	public String getTxUrlPage() {
		return txUrlPage;
	}

	public void setTxUrlPage(String txUrlPage) {
		this.txUrlPage = txUrlPage;
	}

	public String getTxSubMenu() {
		return txSubMenu;
	}

	public void setTxSubMenu(String txSubMenu) {
		this.txSubMenu = txSubMenu;
	}

}
