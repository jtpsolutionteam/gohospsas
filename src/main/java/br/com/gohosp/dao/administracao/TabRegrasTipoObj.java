package br.com.gohosp.dao.administracao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "tab_regras_tipo", schema = Constants.SCHEMA)
public class TabRegrasTipoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_regra")
//@NotNull(message = "TipoRegra campo obrigatório!")
	private Integer cdTipoRegra;

	@Column(name = "tx_tipo_regra")
//@NotEmpty(message = "TipoRegra campo obrigatório!")
	@Size(max = 20, message = "TipoRegra tamanho máximo de 20 caracteres")
	private String txTipoRegra;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoRegra))
			txTipoRegra = txTipoRegra.toUpperCase();
	}

	public Integer getCdTipoRegra() {
		return cdTipoRegra;
	}

	public void setCdTipoRegra(Integer cdTipoRegra) {
		this.cdTipoRegra = cdTipoRegra;
	}

	public String getTxTipoRegra() {
		return txTipoRegra;
	}

	public void setTxTipoRegra(String txTipoRegra) {
		this.txTipoRegra = txTipoRegra;
	}

}
