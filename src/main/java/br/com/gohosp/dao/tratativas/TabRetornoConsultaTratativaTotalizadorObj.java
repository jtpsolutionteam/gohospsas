package br.com.gohosp.dao.tratativas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.gohosp.Constants;

@Entity
@Table(name = "tab_retorno_consulta_tratativa_totalizador", schema = Constants.SCHEMA)
public class TabRetornoConsultaTratativaTotalizadorObj {

	@Id	
	@Column(name = "tx_total")
	private String txTotal;

	@Column(name = "vl_total")
	private Integer vlTotal;


	public String getTxTotal() {
		return txTotal;
	}

	public void setTxTotal(String txTotal) {
		this.txTotal = txTotal;
	}

	public Integer getVlTotal() {
		return vlTotal;
	}

	public void setVlTotal(Integer vlTotal) {
		this.vlTotal = vlTotal;
	}
	

	

}
