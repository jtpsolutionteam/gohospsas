package br.com.gohosp.dao.tratativas;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_retorno_consulta_tratativa_capa", schema = Constants.SCHEMA)
public class TabRetornoConsultaTratativaCapaObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tratativa")
	// @NotNull(message = "Tratativa campo obrigatório!")
	private Integer cdTratativa;

	@Column(name = "tx_cpf_segurado")
	// @NotEmpty(message = "CpfSegurado campo obrigatório!")
	@Size(max = 20, message = "CpfSegurado tamanho máximo de 20 caracteres")
	private String txCpfSegurado;

	@Column(name = "tx_tratativa_id")
	// @NotEmpty(message = "TratativaId campo obrigatório!")
	@Size(max = 30, message = "TratativaId tamanho máximo de 30 caracteres")
	private String txTratativaId;

	@Column(name = "tx_assunto")
	// @NotEmpty(message = "Assunto campo obrigatório!")
	@Size(max = 255, message = "Assunto tamanho máximo de 255 caracteres")
	private String txAssunto;

	@Column(name = "tx_carteira")
	// @NotEmpty(message = "Carteira campo obrigatório!")
	@Size(max = 45, message = "Carteira tamanho máximo de 45 caracteres")
	private String txCarteira;

	@Column(name = "tx_cod_segurado")
	// @NotEmpty(message = "CodSegurado campo obrigatório!")
	@Size(max = 45, message = "CodSegurado tamanho máximo de 45 caracteres")
	private String txCodSegurado;

	@Column(name = "dt_data_incio_vpp")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataIncioVpp;

	@Column(name = "dt_data_nascimento")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataNascimento;

	@Column(name = "dt_data_solicitacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataSolicitacao;

	@Column(name = "tx_empresa")
	// @NotEmpty(message = "Empresa campo obrigatório!")
	@Size(max = 100, message = "Empresa tamanho máximo de 100 caracteres")
	private String txEmpresa;

	@Column(name = "tx_estado")
	// @NotEmpty(message = "Estado campo obrigatório!")
	@Size(max = 45, message = "Estado tamanho máximo de 45 caracteres")
	private String txEstado;

	@Column(name = "tx_mensagem")
	// @NotEmpty(message = "Mensagem campo obrigatório!")
	@Size(max = 16777215, message = "Mensagem tamanho máximo de 16777215 caracteres")
	private String txMensagem;

	@Column(name = "tx_nome_segurado")
	// @NotEmpty(message = "NomeSegurado campo obrigatório!")
	@Size(max = 45, message = "NomeSegurado tamanho máximo de 45 caracteres")
	private String txNomeSegurado;

	@Column(name = "tx_num_vpp")
	// @NotEmpty(message = "NumVpp campo obrigatório!")
	@Size(max = 45, message = "NumVpp tamanho máximo de 45 caracteres")
	private String txNumVpp;

	@Column(name = "tx_prestador")
	// @NotEmpty(message = "Prestador campo obrigatório!")
	@Size(max = 45, message = "Prestador tamanho máximo de 45 caracteres")
	private String txPrestador;

	@Column(name = "tx_prod_saude")
	// @NotEmpty(message = "ProdSaude campo obrigatório!")
	@Size(max = 45, message = "ProdSaude tamanho máximo de 45 caracteres")
	private String txProdSaude;

	@Column(name = "tx_sexo")
	// @NotEmpty(message = "Sexo campo obrigatório!")
	@Size(max = 45, message = "Sexo tamanho máximo de 45 caracteres")
	private String txSexo;

	@Column(name = "tx_sla_tratativa")
	// @NotEmpty(message = "SlaTratativa campo obrigatório!")
	@Size(max = 45, message = "SlaTratativa tamanho máximo de 45 caracteres")
	private String txSlaTratativa;

	@Column(name = "tx_tel_celular")
	// @NotEmpty(message = "TelCelular campo obrigatório!")
	@Size(max = 45, message = "TelCelular tamanho máximo de 45 caracteres")
	private String txTelCelular;

	@Column(name = "tx_tel_cobranca")
	// @NotEmpty(message = "TelCobranca campo obrigatório!")
	@Size(max = 45, message = "TelCobranca tamanho máximo de 45 caracteres")
	private String txTelCobranca;

	@Column(name = "tx_tel_comercial")
	// @NotEmpty(message = "TelComercial campo obrigatório!")
	@Size(max = 45, message = "TelComercial tamanho máximo de 45 caracteres")
	private String txTelComercial;

	@Column(name = "tx_tel_contato")
	// @NotEmpty(message = "TelContato campo obrigatório!")
	@Size(max = 45, message = "TelContato tamanho máximo de 45 caracteres")
	private String txTelContato;

	@Column(name = "tx_tel_contato2")
	// @NotEmpty(message = "TelContato2 campo obrigatório!")
	@Size(max = 45, message = "TelContato2 tamanho máximo de 45 caracteres")
	private String txTelContato2;

	@Column(name = "tx_tel_contato3")
	// @NotEmpty(message = "TelContato3 campo obrigatório!")
	@Size(max = 45, message = "TelContato3 tamanho máximo de 45 caracteres")
	private String txTelContato3;

	@Column(name = "tx_tel_prestador")
	// @NotEmpty(message = "TelPrestador campo obrigatório!")
	@Size(max = 45, message = "TelPrestador tamanho máximo de 45 caracteres")
	private String txTelPrestador;

	@Column(name = "tx_tel_resencial")
	// @NotEmpty(message = "TelResencial campo obrigatório!")
	@Size(max = 45, message = "TelResencial tamanho máximo de 45 caracteres")
	private String txTelResencial;

	@Column(name = "cd_status_tratativa")
	// @NotNull(message = "Status campo obrigatório!")
	private Integer cdStatusTratativa;

	@Column(name = "dt_novo_contato")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtNovoContato;

	@Column(name = "cd_usuario")
	// @NotNull(message = "Status campo obrigatório!")
	private Integer cdUsuario;

	@Transient
	private String txUsuario;

	@Column(name = "dt_sla_tratativa")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtSlaTratativa;

	@Column(name = "dt_encerramento_tratativa")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtEncerramentoTratativa;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {

		if (!Validator.isBlankOrNull(txAssunto))
			txAssunto = txAssunto.toUpperCase();
		if (!Validator.isBlankOrNull(txCarteira))
			txCarteira = txCarteira.toUpperCase();
		if (!Validator.isBlankOrNull(txCodSegurado))
			txCodSegurado = txCodSegurado.toUpperCase();
		if (!Validator.isBlankOrNull(txEmpresa))
			txEmpresa = txEmpresa.toUpperCase();
		if (!Validator.isBlankOrNull(txEstado))
			txEstado = txEstado.toUpperCase();
		if (!Validator.isBlankOrNull(txMensagem))
			txMensagem = txMensagem.toUpperCase();
		if (!Validator.isBlankOrNull(txNomeSegurado))
			txNomeSegurado = txNomeSegurado.toUpperCase();
		if (!Validator.isBlankOrNull(txNumVpp))
			txNumVpp = txNumVpp.toUpperCase();
		if (!Validator.isBlankOrNull(txPrestador))
			txPrestador = txPrestador.toUpperCase();
		if (!Validator.isBlankOrNull(txProdSaude))
			txProdSaude = txProdSaude.toUpperCase();
		if (!Validator.isBlankOrNull(txSexo))
			txSexo = txSexo.toUpperCase();
		if (!Validator.isBlankOrNull(txSlaTratativa))
			txSlaTratativa = txSlaTratativa.toUpperCase();
		if (!Validator.isBlankOrNull(txTelCelular))
			txTelCelular = txTelCelular.toUpperCase();
		if (!Validator.isBlankOrNull(txTelCobranca))
			txTelCobranca = txTelCobranca.toUpperCase();
		if (!Validator.isBlankOrNull(txTelComercial))
			txTelComercial = txTelComercial.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato))
			txTelContato = txTelContato.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato2))
			txTelContato2 = txTelContato2.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato3))
			txTelContato3 = txTelContato3.toUpperCase();
		if (!Validator.isBlankOrNull(txTelPrestador))
			txTelPrestador = txTelPrestador.toUpperCase();
		if (!Validator.isBlankOrNull(txTelResencial))
			txTelResencial = txTelResencial.toUpperCase();
		if (!Validator.isBlankOrNull(txCpfSegurado))
			txCpfSegurado = txCpfSegurado.toUpperCase();
		if (!Validator.isBlankOrNull(cdStatusTratativa)) {
			if (cdStatusTratativa == 2 && Validator.isBlankOrNull(dtEncerramentoTratativa)) {
				dtEncerramentoTratativa = new Date();
			}
		}
	}

}
