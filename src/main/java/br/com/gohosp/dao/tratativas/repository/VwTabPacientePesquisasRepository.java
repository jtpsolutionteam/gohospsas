package br.com.gohosp.dao.tratativas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.tratativas.VwTabPacienteObj;
import br.com.gohosp.dao.tratativas.VwTabPacientePesquisasObj;



public interface VwTabPacientePesquisasRepository extends JpaRepository<VwTabPacientePesquisasObj, Integer> {

	VwTabPacientePesquisasObj findByTxCpfSegurado(String txCpfSegurado);
	
	List<VwTabPacientePesquisasObj> findByCdStatusPaciente(Integer cdStatusPaciente);

	@Query("select distinct t from VwTabPacientePesquisasObj t where t.cdStatusTratativa is null and t.cdSequencia = 1 and t.dtNovoContato is null")
	List<VwTabPacientePesquisasObj> findNovasTratativasSaldoQuery();
	
	@Query("select distinct t from VwTabPacientePesquisasObj t where t.cdStatusTratativa is null and t.cdSequencia = 1 and t.dtNovoContato is null order by t.dtDataSolicitacao desc, t.cdPaciente")
	List<VwTabPacientePesquisasObj> findNovasTratativasQuery();
	
	@Query("select distinct t from VwTabPacientePesquisasObj t where t.cdStatusTratativa is null and t.cdTipo = 1 and t.cdSequencia in(1,2) and t.dtNovoContato is null and date(t.dtSlaTratativa) < date(now()) order by t.dtDataSolicitacao desc, t.cdPaciente")
	List<VwTabPacientePesquisasObj> findNovasTratativasSlaVencidoQuery();

	@Query("select distinct t from VwTabPacientePesquisasObj t where t.cdStatusTratativa = 1 and t.vlSlaPrimContato between 1 and 5 and t.cdTipo = 2 order by t.dtPrimContato, t.cdPaciente")
	List<VwTabPacientePesquisasObj> findSla5DiasQuery();
 
	@Query("select distinct t from VwTabPacientePesquisasObj t where t.cdStatusTratativa = 1 and t.vlSlaPrimContato between 6 and 10 and t.cdTipo = 2 order by t.dtPrimContato, t.cdPaciente")
	List<VwTabPacientePesquisasObj> findSla10DiasQuery();
	
	@Query("select distinct t from VwTabPacientePesquisasObj t where t.cdStatusTratativa = 1 and t.vlSlaPrimContato > 10 and t.cdTipo = 2 order by t.dtPrimContato, t.cdPaciente")
	List<VwTabPacientePesquisasObj> findSla10DiasMaisQuery();
	
	@Query("select distinct t from VwTabPacientePesquisasObj t where t.cdStatusTratativa = ?1 order by t.dtDataSolicitacao desc")
	List<VwTabPacientePesquisasObj> findByCdStatusTratativaQuery(Integer cdStatusTratativa);
	
	@Query("select distinct t from VwTabPacientePesquisasObj t where t.cdTipo = 1 and t.cdSequencia > 1 and t.cdStatusTratativa < 2 order by t.dtDataSolicitacao, t.cdPaciente, t.cdStatusTratativa")
	List<VwTabPacientePesquisasObj> findNovaRespostaSulamericaQuery();

	@Query("select distinct t from VwTabPacientePesquisasObj t where t.cdStatusTratativa = 1 and t.dtNovoContato is null  order by t.dtDataSolicitacao, t.cdPaciente")
	List<VwTabPacientePesquisasObj> findAbertoSemNovoContatoQuery();
	
	@Query("select distinct t from VwTabPacientePesquisasObj t where t.cdStatusTratativa = 1 and date(t.dtNovoContato) <= date(now()) order by t.dtNovoContato, t.cdPaciente")
	List<VwTabPacientePesquisasObj> findNovoContatoQuery();

	@Query("select distinct t from VwTabPacientePesquisasObj t where t.cdStatusTratativa = 1 and date(t.dtNovoContato) > date(now()) order by t.dtNovoContato, t.cdPaciente")
	List<VwTabPacientePesquisasObj> findNovoContatoPosteriorQuery();

}