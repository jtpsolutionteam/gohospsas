package br.com.gohosp.dao.tratativas;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.cadastros.varios.TabInternacoesAnoObj;
import br.com.gohosp.dao.cadastros.varios.TabLocalPacienteObj;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "vw_tab_retorno_consulta_tratativa", schema = Constants.SCHEMA)
public class VwTabRetornoConsultaTratativaObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_codigo")
	// @NotNull(message = "Codigo campo obrigatório!")
	private Integer cdCodigo;

	@Column(name = "tx_tratativa_id")
	// @NotEmpty(message = "TratativaId campo obrigatório!")
	@Size(max = 30, message = "TratativaId tamanho máximo de 30 caracteres")
	private String txTratativaId;

	@Column(name = "cd_sequencia")
	// @NotNull(message = "Sequencia campo obrigatório!")
	private Integer cdSequencia;

	@Column(name = "tx_msg_id")
	// @NotEmpty(message = "MsgId campo obrigatório!")
	@Size(max = 45, message = "MsgId tamanho máximo de 45 caracteres")
	private String txMsgId;

	@Column(name = "tx_assunto")
	// @NotEmpty(message = "Assunto campo obrigatório!")
	@Size(max = 255, message = "Assunto tamanho máximo de 255 caracteres")
	private String txAssunto;

	@Column(name = "tx_carteira")
	// @NotEmpty(message = "Carteira campo obrigatório!")
	@Size(max = 45, message = "Carteira tamanho máximo de 45 caracteres")
	private String txCarteira;

	@Column(name = "tx_cod_segurado")
	// @NotEmpty(message = "CodSegurado campo obrigatório!")
	@Size(max = 45, message = "CodSegurado tamanho máximo de 45 caracteres")
	private String txCodSegurado;

	@Column(name = "dt_data_incio_vpp")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataIncioVpp;

	@Column(name = "dt_data_nascimento")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataNascimento;

	@Column(name = "dt_data_solicitacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataSolicitacao;

	@Column(name = "tx_empresa")
	// @NotEmpty(message = "Empresa campo obrigatório!")
	@Size(max = 100, message = "Empresa tamanho máximo de 100 caracteres")
	private String txEmpresa;

	@Column(name = "tx_estado")
	// @NotEmpty(message = "Estado campo obrigatório!")
	@Size(max = 45, message = "Estado tamanho máximo de 45 caracteres")
	private String txEstado;

	@Column(name = "tx_mensagem")
	// @NotEmpty(message = "Mensagem campo obrigatório!")
	private String txMensagem;

	@Column(name = "tx_nome_segurado")
	// @NotEmpty(message = "NomeSegurado campo obrigatório!")
	@Size(max = 45, message = "NomeSegurado tamanho máximo de 45 caracteres")
	private String txNomeSegurado;
	
	@Column(name = "tx_cpf_segurado")
	// @NotEmpty(message = "CPF Segurado campo obrigatório!")
	@Size(max = 20, message = "CPF Segurado tamanho máximo de 20 caracteres")
	private String txCpfSegurado;

	@Column(name = "tx_num_vpp")
	// @NotEmpty(message = "NumVpp campo obrigatório!")
	@Size(max = 45, message = "NumVpp tamanho máximo de 45 caracteres")
	private String txNumVpp;

	@Column(name = "tx_prestador")
	// @NotEmpty(message = "Prestador campo obrigatório!")
	@Size(max = 45, message = "Prestador tamanho máximo de 45 caracteres")
	private String txPrestador;

	@Column(name = "tx_prod_saude")
	// @NotEmpty(message = "ProdSaude campo obrigatório!")
	@Size(max = 45, message = "ProdSaude tamanho máximo de 45 caracteres")
	private String txProdSaude;

	@Column(name = "tx_sexo")
	// @NotEmpty(message = "Sexo campo obrigatório!")
	@Size(max = 45, message = "Sexo tamanho máximo de 45 caracteres")
	private String txSexo;

	@Column(name = "tx_sla_tratativa")
	// @NotEmpty(message = "SlaTratativa campo obrigatório!")
	@Size(max = 45, message = "SlaTratativa tamanho máximo de 45 caracteres")
	private String txSlaTratativa;

	@Column(name = "tx_tel_celular")
	// @NotEmpty(message = "TelCelular campo obrigatório!")
	@Size(max = 45, message = "TelCelular tamanho máximo de 45 caracteres")
	private String txTelCelular;

	@Column(name = "tx_tel_cobranca")
	// @NotEmpty(message = "TelCobranca campo obrigatório!")
	@Size(max = 45, message = "TelCobranca tamanho máximo de 45 caracteres")
	private String txTelCobranca;

	@Column(name = "tx_tel_comercial")
	// @NotEmpty(message = "TelComercial campo obrigatório!")
	@Size(max = 45, message = "TelComercial tamanho máximo de 45 caracteres")
	private String txTelComercial;

	@Column(name = "tx_tel_contato")
	// @NotEmpty(message = "TelContato campo obrigatório!")
	@Size(max = 45, message = "TelContato tamanho máximo de 45 caracteres")
	private String txTelContato;

	@Column(name = "tx_tel_contato2")
	// @NotEmpty(message = "TelContato2 campo obrigatório!")
	@Size(max = 45, message = "TelContato2 tamanho máximo de 45 caracteres")
	private String txTelContato2;

	@Column(name = "tx_tel_contato3")
	// @NotEmpty(message = "TelContato3 campo obrigatório!")
	@Size(max = 45, message = "TelContato3 tamanho máximo de 45 caracteres")
	private String txTelContato3;

	@Column(name = "tx_tel_prestador")
	// @NotEmpty(message = "TelPrestador campo obrigatório!")
	@Size(max = 45, message = "TelPrestador tamanho máximo de 45 caracteres")
	private String txTelPrestador;

	@Column(name = "tx_tel_resencial")
	// @NotEmpty(message = "TelResencial campo obrigatório!")
	@Size(max = 45, message = "TelResencial tamanho máximo de 45 caracteres")
	private String txTelResencial;

	@Column(name = "ck_confirm_recebimento")
	// @NotNull(message = "ckConfirmRecebimento campo obrigatório!")
	private Integer ckConfirmRecebimento;

	@Column(name = "ck_encerrado")
	// @NotNull(message = "ckEncerrado campo obrigatório!")
	private Integer ckEncerrado;

	@Column(name = "tx_nome_responsavel")
	// @NotEmpty(message = "NomeResponsavel campo obrigatório!")
	@Size(max = 255, message = "NomeResponsavel tamanho máximo de 255 caracteres")
	private String txNomeResponsavel;

	@Column(name = "tx_email_responsavel")
	// @NotEmpty(message = "EmailResponsavel campo obrigatório!")
	@Size(max = 255, message = "EmailResponsavel tamanho máximo de 255 caracteres")
	private String txEmailResponsavel;

	@Column(name = "cd_resposta_definitiva")
	// @NotNull(message = "RespostaDefinitiva campo obrigatório!")
	private Integer cdRespostaDefinitiva;

	@Column(name = "tx_tel_hospital")
	// @NotEmpty(message = "TelHospital campo obrigatório!")
	@Size(max = 30, message = "TelHospital tamanho máximo de 30 caracteres")
	private String txTelHospital;

	@Column(name = "tx_cond_triagem")
	// @NotEmpty(message = "CondTriagem campo obrigatório!")
	@Size(max = 45, message = "CondTriagem tamanho máximo de 45 caracteres")
	private String txCondTriagem;

	@Column(name = "tx_realizou_contato")
	// @NotEmpty(message = "RealizouContato campo obrigatório!")
	@Size(max = 3, message = "RealizouContato tamanho máximo de 3 caracteres")
	private String txRealizouContato;

	@Column(name = "dt_prim_contato")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtPrimContato;

	@Column(name = "tx_desfecho")
	// @NotEmpty(message = "Desfecho campo obrigatório!")
	@Size(max = 45, message = "Desfecho tamanho máximo de 45 caracteres")
	private String txDesfecho;
	
	@Column(name = "dt_desfecho") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDesfecho;

	@Column(name = "tx_medico")
	// @NotEmpty(message = "Medico campo obrigatório!")
	@Size(max = 356, message = "Medico tamanho máximo de 356 caracteres")
	private String txMedico;

	@Column(name = "tx_nome_contato")
	// @NotEmpty(message = "NomeContato campo obrigatório!")
	@Size(max = 256, message = "NomeContato tamanho máximo de 256 caracteres")
	private String txNomeContato;

	@Column(name = "tx_grau_parentesco")
	// @NotEmpty(message = "GrauParentesco campo obrigatório!")
	@Size(max = 45, message = "GrauParentesco tamanho máximo de 45 caracteres")
	private String txGrauParentesco;

	@Column(name = "dt_internacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtInternacao;

	@Column(name = "tx_motivo_nao_contato")
	// @NotEmpty(message = "MotivoNaoContato campo obrigatório!")
	@Size(max = 45, message = "MotivoNaoContato tamanho máximo de 45 caracteres")
	private String txMotivoNaoContato;

	@Column(name = "tx_tipo_internacao")
	// @NotEmpty(message = "TipoInternacao campo obrigatório!")
	@Size(max = 45, message = "TipoInternacao tamanho máximo de 45 caracteres")
	private String txTipoInternacao;

	@Column(name = "tx_cid")
	// @NotEmpty(message = "Cid campo obrigatório!")
	@Size(max = 30, message = "Cid tamanho máximo de 30 caracteres")
	private String txCid;

	@Column(name = "tx_desc_cid")
	// @NotEmpty(message = "DescCid campo obrigatório!")
	@Size(max = 256, message = "DescCid tamanho máximo de 256 caracteres")
	private String txDescCid;

	@Column(name = "tx_motivo_alta")
	// @NotEmpty(message = "MotivoAlta campo obrigatório!")
	@Size(max = 45, message = "MotivoAlta tamanho máximo de 45 caracteres")
	private String txMotivoAlta;

	@Column(name = "tx_tipo_assistencia")
	// @NotEmpty(message = "TipoAssistencia campo obrigatório!")
	@Size(max = 45, message = "TipoAssistencia tamanho máximo de 45 caracteres")
	private String txTipoAssistencia;

	@Column(name = "tx_nome_usuario")
	// @NotEmpty(message = "NomeUsuario campo obrigatório!")
	@Size(max = 255, message = "NomeUsuario tamanho máximo de 255 caracteres")
	private String txNomeUsuario;

	@Column(name = "tx_mensagem_resposta")
	// @NotEmpty(message = "MensagemResposta campo obrigatório!")
	//@Size(max = 2147483647, message = "MensagemResposta tamanho máximo de 2147483647 caracteres")
	private String txMensagemResposta;

	@Column(name = "ck_enviar_atualizacao")
	// @NotNull(message = "ckEnviarAtualizacao campo obrigatório!")
	private Integer ckEnviarAtualizacao;

	@Column(name = "tx_tipo_acomodacao")
	// @NotEmpty(message = "TipoAcomodacao campo obrigatório!")
	@Size(max = 255, message = "TipoAcomodacao tamanho máximo de 255 caracteres")
	private String txTipoAcomodacao;

	@Column(name = "dt_criacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCriacao;

	@Column(name = "dt_ultimo_envio")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtUltimoEnvio;

	@Column(name = "dt_confirm_recebimento")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtConfirmRecebimento;

	@Column(name = "tx_cid2")
	// @NotEmpty(message = "Cid2 campo obrigatório!")
	@Size(max = 30, message = "Cid2 tamanho máximo de 30 caracteres")
	private String txCid2;

	@Column(name = "tx_desc_cid_2")
	// @NotEmpty(message = "DescCid2 campo obrigatório!")
	@Size(max = 256, message = "DescCid2 tamanho máximo de 256 caracteres")
	private String txDescCid2;

	@Column(name = "tx_cid3")
	// @NotEmpty(message = "Cid3 campo obrigatório!")
	@Size(max = 30, message = "Cid3 tamanho máximo de 30 caracteres")
	private String txCid3;

	@Column(name = "tx_desc_cid3")
	// @NotEmpty(message = "DescCid3 campo obrigatório!")
	@Size(max = 256, message = "DescCid3 tamanho máximo de 256 caracteres")
	private String txDescCid3;

	@Column(name = "cd_nivel_consciencia")
	// @NotNull(message = "NivelConsciencia campo obrigatório!")
	private Integer cdNivelConsciencia;

	@Column(name = "tx_nivel_consciencia")
	// @NotEmpty(message = "NivelConsciencia campo obrigatório!")
	@Size(max = 200, message = "NivelConsciencia tamanho máximo de 200 caracteres")
	private String txNivelConsciencia;

	@Column(name = "cd_sinais_vitais")
	// @NotNull(message = "SinaisVitais campo obrigatório!")
	private Integer cdSinaisVitais;

	@Column(name = "tx_sinais_vitais")
	// @NotEmpty(message = "SinaisVitais campo obrigatório!")
	@Size(max = 200, message = "SinaisVitais tamanho máximo de 200 caracteres")
	private String txSinaisVitais;

	@Column(name = "cd_cc_geral")
	// @NotNull(message = "CcGeral campo obrigatório!")
	private Integer cdCcGeral;

	@Column(name = "tx_cc_geral")
	// @NotEmpty(message = "CcGeral campo obrigatório!")
	@Size(max = 200, message = "CcGeral tamanho máximo de 200 caracteres")
	private String txCcGeral;

	@Column(name = "cd_cc_oxigenoterapia")
	// @NotNull(message = "CcOxigenoterapia campo obrigatório!")
	private Integer cdCcOxigenoterapia;

	@Column(name = "tx_cc_oxigenoterapia")
	// @NotEmpty(message = "CcOxigenoterapia campo obrigatório!")
	@Size(max = 200, message = "CcOxigenoterapia tamanho máximo de 200 caracteres")
	private String txCcOxigenoterapia;

	@Column(name = "ck_cc_asp_vias_aereas")
	// @NotNull(message = "ckCcAspViasAereas campo obrigatório!")
	private Integer ckCcAspViasAereas;

	@Column(name = "cd_cc_aps_vias_aereas")
	// @NotNull(message = "CcApsViasAereas campo obrigatório!")
	private Integer cdCcApsViasAereas;

	@Column(name = "tx_cc_aps_vias_aereas")
	// @NotEmpty(message = "CcApsViasAereas campo obrigatório!")
	@Size(max = 200, message = "CcApsViasAereas tamanho máximo de 200 caracteres")
	private String txCcApsViasAereas;

	@Column(name = "ck_cc_acesso_venoso")
	// @NotNull(message = "ckCcAcessoVenoso campo obrigatório!")
	private Integer ckCcAcessoVenoso;

	@Column(name = "cd_cc_acesso_venoso")
	// @NotNull(message = "CcAcessoVenoso campo obrigatório!")
	private Integer cdCcAcessoVenoso;

	@Column(name = "tx_cc_acesso_venoso")
	// @NotEmpty(message = "CcAcessoVenoso campo obrigatório!")
	@Size(max = 200, message = "CcAcessoVenoso tamanho máximo de 200 caracteres")
	private String txCcAcessoVenoso;

	@Column(name = "cd_cc_alimentacao")
	// @NotNull(message = "CcAlimentacao campo obrigatório!")
	private Integer cdCcAlimentacao;

	@Column(name = "tx_cc_alimentacao")
	// @NotEmpty(message = "CcAlimentacao campo obrigatório!")
	@Size(max = 200, message = "CcAlimentacao tamanho máximo de 200 caracteres")
	private String txCcAlimentacao;

	@Column(name = "cd_cc_ef_normal")
	// @NotEmpty(message = "CcEfNormal campo obrigatório!")
	@Size(max = 3, message = "CcEfNormal tamanho máximo de 3 caracteres")
	private String cdCcEfNormal;

	@Column(name = "cd_cc_ef_ausencia")
	// @NotEmpty(message = "CcEfAusencia campo obrigatório!")
	@Size(max = 3, message = "CcEfAusencia tamanho máximo de 3 caracteres")
	private String cdCcEfAusencia;

	@Column(name = "cd_cc_ef_controle")
	// @NotEmpty(message = "CcEfControle campo obrigatório!")
	@Size(max = 3, message = "CcEfControle tamanho máximo de 3 caracteres")
	private String cdCcEfControle;

	@Column(name = "cd_cc_ef_colostomia")
	// @NotEmpty(message = "CcEfColostomia campo obrigatório!")
	@Size(max = 3, message = "CcEfColostomia tamanho máximo de 3 caracteres")
	private String cdCcEfColostomia;

	@Column(name = "cd_cc_ef_cistostomia")
	// @NotEmpty(message = "CcEfCistostomia campo obrigatório!")
	@Size(max = 3, message = "CcEfCistostomia tamanho máximo de 3 caracteres")
	private String cdCcEfCistostomia;

	@Column(name = "cd_cc_ef_nefrostomia")
	// @NotEmpty(message = "CcEfNefrostomia campo obrigatório!")
	@Size(max = 3, message = "CcEfNefrostomia tamanho máximo de 3 caracteres")
	private String cdCcEfNefrostomia;

	@Column(name = "cd_cc_mu_habituais")
	// @NotEmpty(message = "CcMuHabituais campo obrigatório!")
	@Size(max = 3, message = "CcMuHabituais tamanho máximo de 3 caracteres")
	private String cdCcMuHabituais;

	@Column(name = "cd_cc_mu_novos")
	// @NotEmpty(message = "CcMuNovos campo obrigatório!")
	@Size(max = 3, message = "CcMuNovos tamanho máximo de 3 caracteres")
	private String cdCcMuNovos;

	@Column(name = "cd_cc_mu_parenteral")
	// @NotEmpty(message = "CcMuParenteral campo obrigatório!")
	@Size(max = 3, message = "CcMuParenteral tamanho máximo de 3 caracteres")
	private String cdCcMuParenteral;

	@Column(name = "cd_cc_mu_hipodermoclise")
	// @NotEmpty(message = "CcMuHipodermoclise campo obrigatório!")
	@Size(max = 3, message = "CcMuHipodermoclise tamanho máximo de 3 caracteres")
	private String cdCcMuHipodermoclise;

	@Column(name = "tx_cc_mu_obs")
	// @NotEmpty(message = "CcMuObs campo obrigatório!")
	@Size(max = 16777215, message = "CcMuObs tamanho máximo de 16777215 caracteres")
	private String txCcMuObs;

	@Column(name = "ck_cc_me_anticoagulantes")
	// @NotNull(message = "ckCcMeAnticoagulantes campo obrigatório!")
	private Integer ckCcMeAnticoagulantes;

	@Column(name = "cd_cc_me_anticoagulantes")
	// @NotNull(message = "CcMeAnticoagulantes campo obrigatório!")
	private Integer cdCcMeAnticoagulantes;

	@Column(name = "tx_cc_me_anticoagulantes")
	// @NotEmpty(message = "CcMeAnticoagulantes campo obrigatório!")
	@Size(max = 200, message = "CcMeAnticoagulantes tamanho máximo de 200 caracteres")
	private String txCcMeAnticoagulantes;

	@Column(name = "ck_cc_me_antibiotico")
	// @NotNull(message = "ckCcMeAntibiotico campo obrigatório!")
	private Integer ckCcMeAntibiotico;

	@Column(name = "cd_cc_me_antibiotico")
	// @NotNull(message = "CcMeAntibiotico campo obrigatório!")
	private Integer cdCcMeAntibiotico;

	@Column(name = "tx_cc_me_antibiotico")
	// @NotEmpty(message = "CcMeAntibiotico campo obrigatório!")
	@Size(max = 200, message = "CcMeAntibiotico tamanho máximo de 200 caracteres")
	private String txCcMeAntibiotico;

	@Column(name = "ck_cc_me_quimioterapicos")
	// @NotNull(message = "ckCcMeQuimioterapicos campo obrigatório!")
	private Integer ckCcMeQuimioterapicos;

	@Column(name = "cd_cc_me_quimioterapicos")
	// @NotNull(message = "CcMeQuimioterapicos campo obrigatório!")
	private Integer cdCcMeQuimioterapicos;

	@Column(name = "tx_cc_me_quimioterapicos")
	// @NotEmpty(message = "CcMeQuimioterapicos campo obrigatório!")
	@Size(max = 200, message = "CcMeQuimioterapicos tamanho máximo de 200 caracteres")
	private String txCcMeQuimioterapicos;

	@Column(name = "dt_cc_me_inicio_quimioterapicos")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCcMeInicioQuimioterapicos;

	@Column(name = "dt_cc_me_fim_quimioterapicos")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCcMeFimQuimioterapicos;

	@Column(name = "ck_cc_me_imunoglobulinas")
	// @NotNull(message = "ckCcMeImunoglobulinas campo obrigatório!")
	private Integer ckCcMeImunoglobulinas;

	@Column(name = "cd_cc_me_imunoglobulinas")
	// @NotNull(message = "CcMeImunoglobulinas campo obrigatório!")
	private Integer cdCcMeImunoglobulinas;

	@Column(name = "tx_cc_me_imunoglobulinas")
	// @NotEmpty(message = "CcMeImunoglobulinas campo obrigatório!")
	@Size(max = 200, message = "CcMeImunoglobulinas tamanho máximo de 200 caracteres")
	private String txCcMeImunoglobulinas;

	@Column(name = "dt_cc_me_inicio_imunoglobulinas")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCcMeInicioImunoglobulinas;

	@Column(name = "dt_cc_me_fim_imunoglobulinas")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCcMeFimImunoglobulinas;

	@Column(name = "ck_cc_me_imunobiologico")
	// @NotNull(message = "ckCcMeImunobiologico campo obrigatório!")
	private Integer ckCcMeImunobiologico;

	@Column(name = "cd_cc_me_imunobiologico")
	// @NotNull(message = "CcMeImunobiologico campo obrigatório!")
	private Integer cdCcMeImunobiologico;

	@Column(name = "tx_cc_me_imunobiologico")
	// @NotEmpty(message = "CcMeImunobiologico campo obrigatório!")
	@Size(max = 200, message = "CcMeImunobiologico tamanho máximo de 200 caracteres")
	private String txCcMeImunobiologico;

	@Column(name = "dt_cc_me_inicio_imunobiologico")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCcMeInicioImunobiologico;

	@Column(name = "dt_cc_me_fim_imunobiologico")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCcMeFimImunobiologico;

	@Column(name = "ck_cc_radioterapia")
	// @NotNull(message = "ckCcRadioterapia campo obrigatório!")
	private Integer ckCcRadioterapia;

	@Column(name = "cd_cc_radioterapia")
	// @NotNull(message = "CcRadioterapia campo obrigatório!")
	private Integer cdCcRadioterapia;

	@Column(name = "tx_cc_radioterapia")
	// @NotEmpty(message = "CcRadioterapia campo obrigatório!")
	@Size(max = 200, message = "CcRadioterapia tamanho máximo de 200 caracteres")
	private String txCcRadioterapia;

	@Column(name = "dt_cc_inicio_radioterapia")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCcInicioRadioterapia;

	@Column(name = "dt_cc_fim_radioterapia")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCcFimRadioterapia;

	@Column(name = "ck_cc_vos")
	// @NotNull(message = "ckCcVos campo obrigatório!")
	private Integer ckCcVos;

	@Column(name = "cd_cc_vos")
	// @NotNull(message = "CcVos campo obrigatório!")
	private Integer cdCcVos;

	@Column(name = "tx_cc_vos")
	// @NotEmpty(message = "CcVos campo obrigatório!")
	@Size(max = 200, message = "CcVos tamanho máximo de 200 caracteres")
	private String txCcVos;

	@Column(name = "tx_cc_vos_obs")
	// @NotEmpty(message = "CcVosObs campo obrigatório!")
	@Size(max = 16777215, message = "CcVosObs tamanho máximo de 16777215 caracteres")
	private String txCcVosObs;

	@Column(name = "ck_cc_eq_multiprofissional")
	// @NotNull(message = "ckCcEqMultiprofissional campo obrigatório!")
	private Integer ckCcEqMultiprofissional;

	@Column(name = "cd_cc_eq_multiprofissional")
	// @NotNull(message = "CcEqMultiprofissional campo obrigatório!")
	private Integer cdCcEqMultiprofissional;

	@Column(name = "tx_cc_eq_multiprofissional")
	// @NotEmpty(message = "CcEqMultiprofissional campo obrigatório!")
	@Size(max = 200, message = "CcEqMultiprofissional tamanho máximo de 200 caracteres")
	private String txCcEqMultiprofissional;

	@Column(name = "ck_cc_ohb")
	// @NotNull(message = "ckCcOhb campo obrigatório!")
	private Integer ckCcOhb;

	@Column(name = "cd_cc_ohb")
	// @NotNull(message = "CcOhb campo obrigatório!")
	private Integer cdCcOhb;

	@Column(name = "tx_cc_ohb")
	// @NotEmpty(message = "CcOhb campo obrigatório!")
	@Size(max = 200, message = "CcOhb tamanho máximo de 200 caracteres")
	private String txCcOhb;

	@Column(name = "cd_cc_ohb_externa")
	// @NotNull(message = "CcOhbExterna campo obrigatório!")
	private Integer cdCcOhbExterna;

	@Column(name = "tx_cc_ohb_externa")
	// @NotEmpty(message = "CcOhbExterna campo obrigatório!")
	@Size(max = 200, message = "CcOhbExterna tamanho máximo de 200 caracteres")
	private String txCcOhbExterna;

	@Column(name = "cd_cc_imppressao_geral")
	// @NotNull(message = "CcImppressaoGeral campo obrigatório!")
	private Integer cdCcImppressaoGeral;

	@Column(name = "tx_cc_imppressao_geral")
	// @NotEmpty(message = "CcImppressaoGeral campo obrigatório!")
	@Size(max = 200, message = "CcImppressaoGeral tamanho máximo de 200 caracteres")
	private String txCcImppressaoGeral;

	@Column(name = "ck_pt_medico_acredita")
	// @NotNull(message = "ckPtMedicoAcredita campo obrigatório!")
	private Integer ckPtMedicoAcredita;

	@Column(name = "ck_pt_medico_deacordo")
	// @NotNull(message = "ckPtMedicoDeacordo campo obrigatório!")
	private Integer ckPtMedicoDeacordo;

	@Column(name = "ck_pt_medico_proximas_etapas")
	// @NotNull(message = "ckPtMedicoProximasEtapas campo obrigatório!")
	private Integer ckPtMedicoProximasEtapas;

	@Column(name = "tx_pt_medico_proximas_etapas")
	// @NotEmpty(message = "PtMedicoProximasEtapas campo obrigatório!")
	@Size(max = 16777215, message = "PtMedicoProximasEtapas tamanho máximo de 16777215 caracteres")
	private String txPtMedicoProximasEtapas;

	@Column(name = "cd_pt_medico_expectativas")
	// @NotNull(message = "PtMedicoExpectativas campo obrigatório!")
	private Integer cdPtMedicoExpectativas;

	@Column(name = "tx_pt_medico_expectativas")
	// @NotEmpty(message = "PtMedicoExpectativas campo obrigatório!")
	@Size(max = 200, message = "PtMedicoExpectativas tamanho máximo de 200 caracteres")
	private String txPtMedicoExpectativas;

	@Column(name = "ck_pt_medico_algum_procedimento")
	// @NotNull(message = "ckPtMedicoAlgumProcedimento campo obrigatório!")
	private Integer ckPtMedicoAlgumProcedimento;

	@Column(name = "tx_pt_medico_algum_procedimento")
	// @NotEmpty(message = "PtMedicoAlgumProcedimento campo obrigatório!")
	@Size(max = 16777215, message = "PtMedicoAlgumProcedimento tamanho máximo de 16777215 caracteres")
	private String txPtMedicoAlgumProcedimento;

	@Column(name = "ck_pt_medico_ciencia_recursos")
	// @NotNull(message = "ckPtMedicoCienciaRecursos campo obrigatório!")
	private Integer ckPtMedicoCienciaRecursos;

	@Column(name = "cd_pt_medico_ciencia_recursos")
	// @NotNull(message = "PtMedicoCienciaRecursos campo obrigatório!")
	private Integer cdPtMedicoCienciaRecursos;

	@Column(name = "tx_pt_medico_ciencia_recursos")
	// @NotEmpty(message = "PtMedicoCienciaRecursos campo obrigatório!")
	@Size(max = 200, message = "PtMedicoCienciaRecursos tamanho máximo de 200 caracteres")
	private String txPtMedicoCienciaRecursos;

	@Column(name = "ck_ds_algum_apontamento")
	// @NotNull(message = "ckDsAlgumApontamento campo obrigatório!")
	private Integer ckDsAlgumApontamento;

	@Column(name = "tx_ds_algum_apontamento")
	// @NotEmpty(message = "DsAlgumApontamento campo obrigatório!")
	@Size(max = 16777215, message = "DsAlgumApontamento tamanho máximo de 16777215 caracteres")
	private String txDsAlgumApontamento;

	@Column(name = "ck_ds_esposo")
	// @NotEmpty(message = "ckDsEsposo campo obrigatório!")
	@Size(max = 3, message = "ckDsEsposo tamanho máximo de 3 caracteres")
	private String ckDsEsposo;

	@Column(name = "ck_ds_filho")
	// @NotEmpty(message = "ckDsFilho campo obrigatório!")
	@Size(max = 3, message = "ckDsFilho tamanho máximo de 3 caracteres")
	private String ckDsFilho;

	@Column(name = "ck_ds_pai")
	// @NotEmpty(message = "ckDsPai campo obrigatório!")
	@Size(max = 3, message = "ckDsPai tamanho máximo de 3 caracteres")
	private String ckDsPai;

	@Column(name = "ck_ds_mae")
	// @NotEmpty(message = "ckDsMae campo obrigatório!")
	@Size(max = 3, message = "ckDsMae tamanho máximo de 3 caracteres")
	private String ckDsMae;

	@Column(name = "ck_ds_irmao")
	// @NotEmpty(message = "ckDsIrmao campo obrigatório!")
	@Size(max = 3, message = "ckDsIrmao tamanho máximo de 3 caracteres")
	private String ckDsIrmao;

	@Column(name = "ck_ds_sogro")
	// @NotEmpty(message = "ckDsSogro campo obrigatório!")
	@Size(max = 3, message = "ckDsSogro tamanho máximo de 3 caracteres")
	private String ckDsSogro;

	@Column(name = "ck_ds_outros")
	// @NotEmpty(message = "ckDsOutros campo obrigatório!")
	@Size(max = 3, message = "ckDsOutros tamanho máximo de 3 caracteres")
	private String ckDsOutros;

	@Column(name = "tx_ds_outros")
	// @NotEmpty(message = "DsOutros campo obrigatório!")
	@Size(max = 2000, message = "DsOutros tamanho máximo de 2000 caracteres")
	private String txDsOutros;

	@Column(name = "ck_ds_cuidador")
	// @NotNull(message = "ckDsCuidador campo obrigatório!")
	private Integer ckDsCuidador;

	@Column(name = "cd_ds_cuidador")
	// @NotNull(message = "DsCuidador campo obrigatório!")
	private Integer cdDsCuidador;

	@Column(name = "tx_ds_cuidador")
	// @NotNull(message = "DsCuidador campo obrigatório!")
	private String txDsCuidador;

	
	@Column(name = "cd_ds_idade")
	// @NotNull(message = "DsIdade campo obrigatório!")
	private Integer cdDsIdade;

	@Column(name = "ck_ds_pendencia_familiar")
	// @NotNull(message = "ckDsPendenciaFamiliar campo obrigatório!")
	private Integer ckDsPendenciaFamiliar;

	@Column(name = "tx_ds_pendencia_familiar")
	// @NotEmpty(message = "DsPendenciaFamiliar campo obrigatório!")
	@Size(max = 16777215, message = "DsPendenciaFamiliar tamanho máximo de 16777215 caracteres")
	private String txDsPendenciaFamiliar;

	@Column(name = "dt_cc_ohb_inicio")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCcOhbInicio;

	@Column(name = "dt_cc_ohb_fim")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCcOhbFim;

	@Column(name = "tx_ds_cuidador_parentesco")
	// @NotEmpty(message = "DsCuidadorParentesco campo obrigatório!")
	@Size(max = 45, message = "DsCuidadorParentesco tamanho máximo de 45 caracteres")
	private String txDsCuidadorParentesco;

	@Column(name = "dt_cancelamento")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCancelamento;

	@Column(name = "cd_usuario_cancelamento")
	// @NotNull(message = "UsuarioCancelamento campo obrigatório!")
	private Integer cdUsuarioCancelamento;

	@Column(name = "cd_cc_ef_sva")
	// @NotEmpty(message = "CcEfSva campo obrigatório!")
	@Size(max = 3, message = "CcEfSva tamanho máximo de 3 caracteres")
	private String cdCcEfSva;

	@Column(name = "cd_cc_ef_svd")
	// @NotEmpty(message = "CcEfSvd campo obrigatório!")
	@Size(max = 3, message = "CcEfSvd tamanho máximo de 3 caracteres")
	private String cdCcEfSvd;
	
	@Column(name = "tx_questionario") 
	private String txQuestionario; 

	@Column(name = "vl_qtde_dias_novo_contato") 
	private Integer vlQtdeDiasNovoContato; 

	
	@Column(name = "dt_novo_contato")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtNovoContato;

	@Column(name = "ck_totalizador") 
	private Integer ckTotalizador; 

	@Column(name = "cd_status") 
	private Integer cdStatus; 
	
	@Column(name = "tx_status") 
	private String txStatus; 
	
	@Column(name = "cd_tipo") 
	private Integer cdTipo; 
	
	@Transient
	private String txRespostaSimplificada;
	
	@Column(name = "vl_idade") 
	private Integer vlIdade;
	
	@Column(name = "cd_cc_covid19") 
	private Integer cdCcCovid19; 

	@Column(name = "cd_prob_encontrados") 
	private Integer cdProbEncontrados; 
	
	@Column(name = "tx_cc_covid19") 
	private String txCcCovid19; 

	@Column(name = "tx_prob_encontrados") 
	private String txProbEncontrados; 
	
	@Column(name = "cd_pt_medico_homecare") 
	//@NotEmpty(message = "PtMedicoHomecare campo obrigatório!")
	@Size(max = 3, message = "PtMedicoHomecare tamanho máximo de 3 caracteres") 
	private String cdPtMedicoHomecare; 
	 
	@Column(name = "cd_pt_medico_atb") 
	//@NotEmpty(message = "PtMedicoAtb campo obrigatório!")
	@Size(max = 3, message = "PtMedicoAtb tamanho máximo de 3 caracteres") 
	private String cdPtMedicoAtb; 
	 
	@Column(name = "cd_pt_medico_reabilitacao") 
	//@NotEmpty(message = "PtMedicoReabilitacao campo obrigatório!")
	@Size(max = 3, message = "PtMedicoReabilitacao tamanho máximo de 3 caracteres") 
	private String cdPtMedicoReabilitacao; 
	 
	@Column(name = "cd_pt_medico_cuidados_casa") 
	//@NotEmpty(message = "PtMedicoCuidadosCasa campo obrigatório!")
	@Size(max = 3, message = "PtMedicoCuidadosCasa tamanho máximo de 3 caracteres") 
	private String cdPtMedicoCuidadosCasa; 
	 
	@Column(name = "cd_pt_medico_sem_indicacao_hospitalar") 
	//@NotEmpty(message = "PtMedicoSemIndicacaoHospitalar campo obrigatório!")
	@Size(max = 3, message = "PtMedicoSemIndicacaoHospitalar tamanho máximo de 3 caracteres") 
	private String cdPtMedicoSemIndicacaoHospitalar; 
	 
	@Column(name = "ck_pt_medico_fidelizado") 
	//@NotNull(message = "ckPtMedicoFidelizado campo obrigatório!")
	private Integer ckPtMedicoFidelizado; 
	 
	@Column(name = "ck_pt_medico_indicar_acompanhamento") 
	//@NotNull(message = "ckPtMedicoIndicarAcompanhamento campo obrigatório!")
	private Integer ckPtMedicoIndicarAcompanhamento; 
	
	@Column(name = "ck_pode_atender") 
	//@NotNull(message = "ckPtMedicoIndicarAcompanhamento campo obrigatório!")
	private Integer ckPodeAtender; 
	
	@ManyToOne
	@JoinColumn(name = "cd_paciente_hospital_casa") 
	//@NotNull(message = "ckPtMedicoIndicarAcompanhamento campo obrigatório!")
	private TabLocalPacienteObj tabLocalPacienteObj;

	
	@Column(name = "cd_paciente_cuidador_periodo_integral") 
	//@NotNull(message = "ckPtMedicoIndicarAcompanhamento campo obrigatório!")
	private Integer cdPacienteCuidadorPeriodoIntegral; 
		
	@ManyToOne
	@JoinColumn(name = "cd_paciente_internacoes_ultimo_ano") 
	//@NotNull(message = "PacienteInternacoesUltimoAno campo obrigatório!")
	private TabInternacoesAnoObj tabInternacoesAnoObj;
	
	@Column(name = "dt_covid_resultado") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtCovidResultado; 
	
	@Column(name = "cd_nivel_consciencia_antes") 
	//@NotNull(message = "NivelConsciencia campo obrigatório!")
	private Integer cdNivelConscienciaAntes; 
	
	@Column(name = "cd_paciente_desconforto_posicao") 
	//@NotNull(message = "PacienteDesconfortoPosicao campo obrigatório!")
	private Integer cdPacienteDesconfortoPosicao; 
	
	@Column(name = "cd_paciente_pele_contato_urina") 
	//@NotNull(message = "PacienteDesconfortoPosicao campo obrigatório!")
	private Integer cdPacientePeleContatoUrina; 

	@Column(name = "cd_tipo_lesao_pele") 
	//@NotNull(message = "TipoLesaoPele campo obrigatório!")
	private Integer cdTipoLesaoPele;
	
	@Column(name = "cd_paciente_anda_cama") 
	//@NotNull(message = "PacienteAndaCama campo obrigatório!")
	private Integer cdPacienteAndaCama; 
	 
	@Column(name = "cd_paciente_acamado_cadeira_rodas") 
	//@NotNull(message = "PacienteAcamadoCadeiraRodas campo obrigatório!")
	private Integer cdPacienteAcamadoCadeiraRodas; 
	 
	@Column(name = "cd_paciente_cama_cadeira_imovel") 
	//@NotNull(message = "PacienteCamaCadeiraImovel campo obrigatório!")
	private Integer cdPacienteCamaCadeiraImovel; 
	 
	@Column(name = "cd_paciente_movimento_cama") 
	//@NotNull(message = "PacienteMovimentoCama campo obrigatório!")
	private Integer cdPacienteMovimentoCama; 
	 
	@Column(name = "cd_paciente_tipo_banho") 
	//@NotNull(message = "PacienteTipoBanho campo obrigatório!")
	private Integer cdPacienteTipoBanho;
	
	@Column(name = "cd_paciente_alimentacao_qualidade") 
	//@NotNull(message = "PacienteAlimentacaoQualidade campo obrigatório!")
	private Integer cdPacienteAlimentacaoQualidade; 
	 
	@Column(name = "cd_paciente_alimentacao_como_atualmente") 
	//@NotNull(message = "PacienteAlimentacaoComoAtualmente campo obrigatório!")
	private Integer cdPacienteAlimentacaoComoAtualmente; 
	 
	@Column(name = "cd_paciente_alimentacao_estado_nutricional") 
	//@NotNull(message = "PacienteAlimentacaoEstadoNutricional campo obrigatório!")
	private Integer cdPacienteAlimentacaoEstadoNutricional; 
	 
	@Column(name = "cd_paciente_alimentacao_tipo_medicamentos") 
	//@NotNull(message = "PacienteAlimentacaoTipoMedicamentos campo obrigatório!")
	private Integer cdPacienteAlimentacaoTipoMedicamentos; 

	@Column(name = "cd_paciente_tipo_alimentacao_parenteral") 
	//@NotNull(message = "PacienteTipoAlimentacaoParenteral campo obrigatório!")
	private Integer cdPacienteTipoAlimentacaoParenteral; 
	 
	@Column(name = "cd_paciente_modo_vestir_atualmente") 
	//@NotNull(message = "PacienteModoVestirAtualmente campo obrigatório!")
	private Integer cdPacienteModoVestirAtualmente; 
	 
	@Column(name = "cd_paciente_modo_banheiro_atualmente") 
	//@NotNull(message = "PacienteModoBanheiroAtualmente campo obrigatório!")
	private Integer cdPacienteModoBanheiroAtualmente; 
	 
	@Column(name = "cd_paciente_vontade_banheiro_atualmente") 
	//@NotNull(message = "PacienteVontadeBanheiroAtualmente campo obrigatório!")
	private Integer cdPacienteVontadeBanheiroAtualmente; 
	 
	@Column(name = "cd_paciente_alimentar_atualmente") 
	//@NotNull(message = "PacienteAlimentarAtualmente campo obrigatório!")
	private Integer cdPacienteAlimentarAtualmente; 

	@Column(name = "cd_paciente_aspirado_sonda") 
	//@NotNull(message = "PacienteAspiradoSonda campo obrigatório!")
	private Integer cdPacienteAspiradoSonda; 
	 
	@Column(name = "cd_paciente_aspirado_boca_nariz") 
	//@NotNull(message = "PacienteAspiradoBocaNariz campo obrigatório!")
	private Integer cdPacienteAspiradoBocaNariz; 
	 
	@Column(name = "ck_paciente_aparelho_respiracao") 
	//@NotNull(message = "ckPacienteAparelhoRespiracao campo obrigatório!")
	private Integer ckPacienteAparelhoRespiracao; 
	 
	@Column(name = "cd_paciente_aparelho_respiracao") 
	//@NotNull(message = "PacienteAparelhoRespiracao campo obrigatório!")
	private Integer cdPacienteAparelhoRespiracao; 
	 
	@Column(name = "cd_paciente_exercicios_ventilatorios") 
	//@NotNull(message = "PacienteExerciciosVentilatorios campo obrigatório!")
	private Integer cdPacienteExerciciosVentilatorios; 

	@Column(name = "cd_paciente_utilizando_oxigenio") 
	//@NotNull(message = "PacienteUtilizandoOxigenio campo obrigatório!")
	private Integer cdPacienteUtilizandoOxigenio; 
	 
	@Column(name = "cd_paciente_tipo_oxigenio") 
	//@NotNull(message = "PacienteTipoOxigenio campo obrigatório!")
	private Integer cdPacienteTipoOxigenio; 
	 
	@Column(name = "cd_paciente_tipo_oxigenio_antes") 
	//@NotNull(message = "PacienteTipoOxigenioAntes campo obrigatório!")
	private Integer cdPacienteTipoOxigenioAntes; 
	 
	@Column(name = "ck_paciente_medicacao_veia") 
	//@NotNull(message = "ckPacienteMedicacaoVeia campo obrigatório!")
	private Integer ckPacienteMedicacaoVeia; 
	 
	@Column(name = "cd_paciente_medicacao_veia") 
	//@NotNull(message = "PacienteMedicacaoVeia campo obrigatório!")
	private Integer cdPacienteMedicacaoVeia; 
	 
	@Column(name = "cd_paciente_medicacao_administradas") 
	//@NotNull(message = "PacienteMedicacaoAdministradas campo obrigatório!")
	private Integer cdPacienteMedicacaoAdministradas;
	
	@Column(name = "cd_paciente_pendencia_tratamento") 
	//@NotNull(message = "PacientePendenciaTratamento campo obrigatório!")
	private Integer cdPacientePendenciaTratamento; 
	 
	@Column(name = "cd_paciente_necessidades_apos_alta") 
	//@NotNull(message = "PacienteNecessidadesAposAlta campo obrigatório!")
	private Integer cdPacienteNecessidadesAposAlta; 
	 
	@Column(name = "ck_paciente_atendimento_casa") 
	//@NotNull(message = "ckPacienteAtendimentoCasa campo obrigatório!")
	private Integer ckPacienteAtendimentoCasa; 
	 
	@Column(name = "cd_paciente_empresa_homecare") 
	//@NotNull(message = "ckPacienteEmpresaHomecare campo obrigatório!")
	private Integer cdPacienteEmpresaHomecare; 
	 
	@Column(name = "ck_paciente_medico_regularmente") 
	//@NotNull(message = "ckPacienteMedicoRegularmente campo obrigatório!")
	private Integer ckPacienteMedicoRegularmente; 
	 
	@Column(name = "ck_paciente_medico_perto_residencia") 
	//@NotNull(message = "ckPacienteMedicoPertoResidencia campo obrigatório!")
	private Integer ckPacienteMedicoPertoResidencia; 
	 
	@Column(name = "ck_paciente_operadora_indicar_medico") 
	//@NotNull(message = "ckPacienteOperadoraIndicarMedico campo obrigatório!")
	private Integer ckPacienteOperadoraIndicarMedico; 
	 
	@Column(name = "ck_paciente_alguma_duvida") 
	//@NotNull(message = "ckPacienteAlgumaDuvida campo obrigatório!")
	private Integer ckPacienteAlgumaDuvida;
	
	@Column(name = "cd_paciente_katz") 
	private Integer cdPacienteKatz;


	
	@PostLoad
	private void prePostLoad() {
		
		if (txMensagemResposta != null) {
		  if (txMensagemResposta.length() > 100) {
			txRespostaSimplificada = txMensagemResposta.substring(0,100)+"...";
		  }else {
			txRespostaSimplificada = txMensagemResposta;
		  }
		}
	}
	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTratativaId))
			txTratativaId = txTratativaId.toUpperCase();
		if (!Validator.isBlankOrNull(txMsgId))
			txMsgId = txMsgId.toUpperCase();
		if (!Validator.isBlankOrNull(txAssunto))
			txAssunto = txAssunto.toUpperCase();
		if (!Validator.isBlankOrNull(txCarteira))
			txCarteira = txCarteira.toUpperCase();
		if (!Validator.isBlankOrNull(txCodSegurado))
			txCodSegurado = txCodSegurado.toUpperCase();
		if (!Validator.isBlankOrNull(txEmpresa))
			txEmpresa = txEmpresa.toUpperCase();
		if (!Validator.isBlankOrNull(txEstado))
			txEstado = txEstado.toUpperCase();
		if (!Validator.isBlankOrNull(txMensagem))
			txMensagem = txMensagem.toUpperCase();
		if (!Validator.isBlankOrNull(txNomeSegurado))
			txNomeSegurado = txNomeSegurado.toUpperCase();
		if (!Validator.isBlankOrNull(txNumVpp))
			txNumVpp = txNumVpp.toUpperCase();
		if (!Validator.isBlankOrNull(txPrestador))
			txPrestador = txPrestador.toUpperCase();
		if (!Validator.isBlankOrNull(txProdSaude))
			txProdSaude = txProdSaude.toUpperCase();
		if (!Validator.isBlankOrNull(txSexo))
			txSexo = txSexo.toUpperCase();
		if (!Validator.isBlankOrNull(txSlaTratativa))
			txSlaTratativa = txSlaTratativa.toUpperCase();
		if (!Validator.isBlankOrNull(txTelCelular))
			txTelCelular = txTelCelular.toUpperCase();
		if (!Validator.isBlankOrNull(txTelCobranca))
			txTelCobranca = txTelCobranca.toUpperCase();
		if (!Validator.isBlankOrNull(txTelComercial))
			txTelComercial = txTelComercial.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato))
			txTelContato = txTelContato.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato2))
			txTelContato2 = txTelContato2.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato3))
			txTelContato3 = txTelContato3.toUpperCase();
		if (!Validator.isBlankOrNull(txTelPrestador))
			txTelPrestador = txTelPrestador.toUpperCase();
		if (!Validator.isBlankOrNull(txTelResencial))
			txTelResencial = txTelResencial.toUpperCase();
		if (!Validator.isBlankOrNull(txNomeResponsavel))
			txNomeResponsavel = txNomeResponsavel.toUpperCase();
		if (!Validator.isBlankOrNull(txEmailResponsavel))
			txEmailResponsavel = txEmailResponsavel.toUpperCase();
		if (!Validator.isBlankOrNull(txTelHospital))
			txTelHospital = txTelHospital.toUpperCase();
		if (!Validator.isBlankOrNull(txCondTriagem))
			txCondTriagem = txCondTriagem.toUpperCase();
		if (!Validator.isBlankOrNull(txRealizouContato))
			txRealizouContato = txRealizouContato.toUpperCase();
		if (!Validator.isBlankOrNull(txDesfecho))
			txDesfecho = txDesfecho.toUpperCase();
		if (!Validator.isBlankOrNull(txMedico))
			txMedico = txMedico.toUpperCase();
		if (!Validator.isBlankOrNull(txNomeContato))
			txNomeContato = txNomeContato.toUpperCase();
		if (!Validator.isBlankOrNull(txGrauParentesco))
			txGrauParentesco = txGrauParentesco.toUpperCase();
		if (!Validator.isBlankOrNull(txMotivoNaoContato))
			txMotivoNaoContato = txMotivoNaoContato.toUpperCase();
		if (!Validator.isBlankOrNull(txTipoInternacao))
			txTipoInternacao = txTipoInternacao.toUpperCase();
		if (!Validator.isBlankOrNull(txCid))
			txCid = txCid.toUpperCase();
		if (!Validator.isBlankOrNull(txDescCid))
			txDescCid = txDescCid.toUpperCase();
		if (!Validator.isBlankOrNull(txMotivoAlta))
			txMotivoAlta = txMotivoAlta.toUpperCase();
		if (!Validator.isBlankOrNull(txTipoAssistencia))
			txTipoAssistencia = txTipoAssistencia.toUpperCase();
		if (!Validator.isBlankOrNull(txNomeUsuario))
			txNomeUsuario = txNomeUsuario.toUpperCase();
		if (!Validator.isBlankOrNull(txMensagemResposta))
			txMensagemResposta = txMensagemResposta.toUpperCase();
		if (!Validator.isBlankOrNull(txTipoAcomodacao))
			txTipoAcomodacao = txTipoAcomodacao.toUpperCase();
		if (!Validator.isBlankOrNull(txCid2))
			txCid2 = txCid2.toUpperCase();
		if (!Validator.isBlankOrNull(txDescCid2))
			txDescCid2 = txDescCid2.toUpperCase();
		if (!Validator.isBlankOrNull(txCid3))
			txCid3 = txCid3.toUpperCase();
		if (!Validator.isBlankOrNull(txDescCid3))
			txDescCid3 = txDescCid3.toUpperCase();
		if (!Validator.isBlankOrNull(txNivelConsciencia))
			txNivelConsciencia = txNivelConsciencia.toUpperCase();
		if (!Validator.isBlankOrNull(txSinaisVitais))
			txSinaisVitais = txSinaisVitais.toUpperCase();
		if (!Validator.isBlankOrNull(txCcGeral))
			txCcGeral = txCcGeral.toUpperCase();
		if (!Validator.isBlankOrNull(txCcOxigenoterapia))
			txCcOxigenoterapia = txCcOxigenoterapia.toUpperCase();
		if (!Validator.isBlankOrNull(txCcApsViasAereas))
			txCcApsViasAereas = txCcApsViasAereas.toUpperCase();
		if (!Validator.isBlankOrNull(txCcAcessoVenoso))
			txCcAcessoVenoso = txCcAcessoVenoso.toUpperCase();
		if (!Validator.isBlankOrNull(txCcAlimentacao))
			txCcAlimentacao = txCcAlimentacao.toUpperCase();
		if (!Validator.isBlankOrNull(cdCcEfNormal))
			cdCcEfNormal = cdCcEfNormal.toUpperCase();
		if (!Validator.isBlankOrNull(cdCcEfAusencia))
			cdCcEfAusencia = cdCcEfAusencia.toUpperCase();
		if (!Validator.isBlankOrNull(cdCcEfControle))
			cdCcEfControle = cdCcEfControle.toUpperCase();
		if (!Validator.isBlankOrNull(cdCcEfColostomia))
			cdCcEfColostomia = cdCcEfColostomia.toUpperCase();
		if (!Validator.isBlankOrNull(cdCcEfCistostomia))
			cdCcEfCistostomia = cdCcEfCistostomia.toUpperCase();
		if (!Validator.isBlankOrNull(cdCcEfNefrostomia))
			cdCcEfNefrostomia = cdCcEfNefrostomia.toUpperCase();
		if (!Validator.isBlankOrNull(cdCcMuHabituais))
			cdCcMuHabituais = cdCcMuHabituais.toUpperCase();
		if (!Validator.isBlankOrNull(cdCcMuNovos))
			cdCcMuNovos = cdCcMuNovos.toUpperCase();
		if (!Validator.isBlankOrNull(cdCcMuParenteral))
			cdCcMuParenteral = cdCcMuParenteral.toUpperCase();
		if (!Validator.isBlankOrNull(cdCcMuHipodermoclise))
			cdCcMuHipodermoclise = cdCcMuHipodermoclise.toUpperCase();
		if (!Validator.isBlankOrNull(txCcMuObs))
			txCcMuObs = txCcMuObs.toUpperCase();
		if (!Validator.isBlankOrNull(txCcMeAnticoagulantes))
			txCcMeAnticoagulantes = txCcMeAnticoagulantes.toUpperCase();
		if (!Validator.isBlankOrNull(txCcMeAntibiotico))
			txCcMeAntibiotico = txCcMeAntibiotico.toUpperCase();
		if (!Validator.isBlankOrNull(txCcMeQuimioterapicos))
			txCcMeQuimioterapicos = txCcMeQuimioterapicos.toUpperCase();
		if (!Validator.isBlankOrNull(txCcMeImunoglobulinas))
			txCcMeImunoglobulinas = txCcMeImunoglobulinas.toUpperCase();
		if (!Validator.isBlankOrNull(txCcMeImunobiologico))
			txCcMeImunobiologico = txCcMeImunobiologico.toUpperCase();
		if (!Validator.isBlankOrNull(txCcRadioterapia))
			txCcRadioterapia = txCcRadioterapia.toUpperCase();
		if (!Validator.isBlankOrNull(txCcVos))
			txCcVos = txCcVos.toUpperCase();
		if (!Validator.isBlankOrNull(txCcVosObs))
			txCcVosObs = txCcVosObs.toUpperCase();
		if (!Validator.isBlankOrNull(txCcEqMultiprofissional))
			txCcEqMultiprofissional = txCcEqMultiprofissional.toUpperCase();
		if (!Validator.isBlankOrNull(txCcOhb))
			txCcOhb = txCcOhb.toUpperCase();
		if (!Validator.isBlankOrNull(txCcOhbExterna))
			txCcOhbExterna = txCcOhbExterna.toUpperCase();
		if (!Validator.isBlankOrNull(txCcImppressaoGeral))
			txCcImppressaoGeral = txCcImppressaoGeral.toUpperCase();
		if (!Validator.isBlankOrNull(txPtMedicoProximasEtapas))
			txPtMedicoProximasEtapas = txPtMedicoProximasEtapas.toUpperCase();
		if (!Validator.isBlankOrNull(txPtMedicoExpectativas))
			txPtMedicoExpectativas = txPtMedicoExpectativas.toUpperCase();
		if (!Validator.isBlankOrNull(txPtMedicoAlgumProcedimento))
			txPtMedicoAlgumProcedimento = txPtMedicoAlgumProcedimento.toUpperCase();
		if (!Validator.isBlankOrNull(txPtMedicoCienciaRecursos))
			txPtMedicoCienciaRecursos = txPtMedicoCienciaRecursos.toUpperCase();
		if (!Validator.isBlankOrNull(txDsAlgumApontamento))
			txDsAlgumApontamento = txDsAlgumApontamento.toUpperCase();
		if (!Validator.isBlankOrNull(ckDsEsposo))
			ckDsEsposo = ckDsEsposo.toUpperCase();
		if (!Validator.isBlankOrNull(ckDsFilho))
			ckDsFilho = ckDsFilho.toUpperCase();
		if (!Validator.isBlankOrNull(ckDsPai))
			ckDsPai = ckDsPai.toUpperCase();
		if (!Validator.isBlankOrNull(ckDsMae))
			ckDsMae = ckDsMae.toUpperCase();
		if (!Validator.isBlankOrNull(ckDsIrmao))
			ckDsIrmao = ckDsIrmao.toUpperCase();
		if (!Validator.isBlankOrNull(ckDsSogro))
			ckDsSogro = ckDsSogro.toUpperCase();
		if (!Validator.isBlankOrNull(ckDsOutros))
			ckDsOutros = ckDsOutros.toUpperCase();
		if (!Validator.isBlankOrNull(txDsOutros))
			txDsOutros = txDsOutros.toUpperCase();
		if (!Validator.isBlankOrNull(txDsPendenciaFamiliar))
			txDsPendenciaFamiliar = txDsPendenciaFamiliar.toUpperCase();
		if (!Validator.isBlankOrNull(txDsCuidadorParentesco))
			txDsCuidadorParentesco = txDsCuidadorParentesco.toUpperCase();
		if (!Validator.isBlankOrNull(cdCcEfSva))
			cdCcEfSva = cdCcEfSva.toUpperCase();
		if (!Validator.isBlankOrNull(cdCcEfSvd))
			cdCcEfSvd = cdCcEfSvd.toUpperCase();
	}

	

}
