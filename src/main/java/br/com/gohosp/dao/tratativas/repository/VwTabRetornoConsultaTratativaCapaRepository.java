package br.com.gohosp.dao.tratativas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaObj;
import br.com.gohosp.dao.tratativas.VwTabRetornoConsultaTratativaCapaObj;



public interface VwTabRetornoConsultaTratativaCapaRepository extends JpaRepository<VwTabRetornoConsultaTratativaCapaObj, Integer> {


	@Query("select t from VwTabRetornoConsultaTratativaCapaObj t where t.txMensagemTratativa like '%#FINALIZA%' and date(t.dtDataFinalizaSulamerica) = date(now())")
	List<VwTabRetornoConsultaTratativaCapaObj> findByFinalizaDiarioQuery();

	@Query("select t from VwTabRetornoConsultaTratativaCapaObj t where t.txMensagemTratativa like '%#FINALIZA%' and month(t.dtDataFinalizaSulamerica) = month(now())")
	List<VwTabRetornoConsultaTratativaCapaObj> findByFinalizaMensalQuery();

	@Query("select t from VwTabRetornoConsultaTratativaCapaObj t where t.txMensagemTratativa like '%#FINALIZA%' and date(t.dtDataFinalizaSulamerica) = date(now()) and t.vlQtdeContatos is null")
	List<VwTabRetornoConsultaTratativaCapaObj> findByFinalizaDiarioSemContatoQuery();

	@Query("select t from VwTabRetornoConsultaTratativaCapaObj t where t.txMensagemTratativa like '%#FINALIZA%' and month(t.dtDataFinalizaSulamerica) = month(now()) and t.vlQtdeContatos is null")
	List<VwTabRetornoConsultaTratativaCapaObj> findByFinalizaMensalSemContatoQuery();
	
	

}