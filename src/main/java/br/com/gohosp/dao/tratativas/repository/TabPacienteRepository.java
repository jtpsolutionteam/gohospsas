package br.com.gohosp.dao.tratativas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.tratativas.TabPacienteObj;



public interface TabPacienteRepository extends JpaRepository<TabPacienteObj, Integer> {

	TabPacienteObj findByTxCpfSegurado(String txCpfSegurado);
	
	List<TabPacienteObj> findByCdStatusPaciente(Integer cdStatusPaciente);

}