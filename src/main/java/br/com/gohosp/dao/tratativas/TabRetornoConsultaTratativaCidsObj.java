package br.com.gohosp.dao.tratativas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.cadastros.cid.TabCidSubcategoriaObj;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_retorno_consulta_tratativa_cids", schema = Constants.SCHEMA)
public class TabRetornoConsultaTratativaCidsObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_codigo_cid")
	// @NotNull(message = "CodigoCid campo obrigatório!")
	private Integer cdCodigoCid;

	@Column(name = "cd_codigo")
	// @NotNull(message = "Codigo campo obrigatório!")
	private Integer cdCodigo;

	@Column(name = "cd_tipo")
	// @NotNull(message = "Tipo campo obrigatório!")
	private Integer cdTipo;

	@ManyToOne
	@JoinColumn(name = "cd_subcategoria")
	// @NotNull(message = "Subcategoria campo obrigatório!")
	private TabCidSubcategoriaObj tabCidSubcategoriaObj;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

}
