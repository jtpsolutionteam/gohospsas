package br.com.gohosp.dao.tratativas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.tratativas.TabEnviosObj;



public interface TabEnviosRepository extends JpaRepository<TabEnviosObj, Integer> {



}