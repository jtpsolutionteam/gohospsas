package br.com.gohosp.dao.tratativas;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.cadastros.varios.TabInternacoesAnoObj;
import br.com.gohosp.dao.cadastros.varios.TabLocalPacienteObj;
import br.com.gohosp.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_retorno_consulta_tratativa", schema = Constants.SCHEMA)
public class TabRetornoConsultaTratativaObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_codigo")
	// @NotNull(message = "Codigo campo obrigatório!")
	private Integer cdCodigo;

	@Column(name = "tx_tratativa_id")
	// @NotEmpty(message = "TratativaId campo obrigatório!")
	@Size(max = 30, message = "TratativaId tamanho máximo de 30 caracteres")
	private String txTratativaId;

	@Column(name = "tx_assunto")
	// @NotEmpty(message = "Assunto campo obrigatório!")
	@Size(max = 255, message = "Assunto tamanho máximo de 45 caracteres")
	private String txAssunto;

	@Column(name = "tx_carteira")
	// @NotEmpty(message = "Carteirta campo obrigatório!")
	@Size(max = 45, message = "Carteira tamanho máximo de 45 caracteres")
	private String txCarteira;

	@Column(name = "tx_cod_segurado")
	// @NotEmpty(message = "CodSegurado campo obrigatório!")
	@Size(max = 45, message = "CodSegurado tamanho máximo de 45 caracteres")
	private String txCodSegurado;

	@Column(name = "dt_data_incio_vpp")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataIncioVpp;

	@Column(name = "dt_data_nascimento")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataNascimento;

	@Column(name = "dt_data_solicitacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataSolicitacao;

	@Column(name = "tx_empresa")
	// @NotEmpty(message = "Empresa campo obrigatório!")
	@Size(max = 100, message = "Empresa tamanho máximo de 100 caracteres")
	private String txEmpresa;

	@Column(name = "tx_estado")
	// @NotEmpty(message = "Estado campo obrigatório!")
	@Size(max = 45, message = "Estado tamanho máximo de 45 caracteres")
	private String txEstado;

	@Column(name = "tx_mensagem")
	// @NotEmpty(message = "Mensagem campo obrigatório!")
	@Size(max = 4000, message = "Mensagem tamanho máximo de 4000 caracteres")
	private String txMensagem;

	@Column(name = "tx_msg_id")
	// @NotEmpty(message = "MsgId campo obrigatório!")
	@Size(max = 45, message = "MsgId tamanho máximo de 45 caracteres")
	private String txMsgId;

	@Column(name = "tx_nome_segurado")
	// @NotEmpty(message = "NomeSegurado campo obrigatório!")
	@Size(max = 45, message = "NomeSegurado tamanho máximo de 45 caracteres")
	private String txNomeSegurado;
	
	
	@Column(name = "tx_cpf_segurado")
	// @NotEmpty(message = "CPF Segurado campo obrigatório!")
	@Size(max = 20, message = "CPF Segurado tamanho máximo de 20 caracteres")
	private String txCpfSegurado;
	

	@Column(name = "tx_num_vpp")
	// @NotEmpty(message = "NumVpp campo obrigatório!")
	@Size(max = 45, message = "NumVpp tamanho máximo de 45 caracteres")
	private String txNumVpp;

	@Column(name = "tx_prestador")
	// @NotEmpty(message = "Prestador campo obrigatório!")
	@Size(max = 45, message = "Prestador tamanho máximo de 45 caracteres")
	private String txPrestador;

	@Column(name = "tx_prod_saude")
	// @NotEmpty(message = "ProdSaude campo obrigatório!")
	@Size(max = 45, message = "ProdSaude tamanho máximo de 45 caracteres")
	private String txProdSaude;

	@Column(name = "tx_sexo")
	// @NotEmpty(message = "Sexo campo obrigatório!")
	@Size(max = 45, message = "Sexo tamanho máximo de 45 caracteres")
	private String txSexo;

	@Column(name = "tx_sla_tratativa")
	// @NotEmpty(message = "SlaTratativa campo obrigatório!")
	@Size(max = 45, message = "SlaTratativa tamanho máximo de 45 caracteres")
	private String txSlaTratativa;

	@Column(name = "tx_tel_celular")
	// @NotEmpty(message = "TelCelular campo obrigatório!")
	@Size(max = 45, message = "TelCelular tamanho máximo de 45 caracteres")
	private String txTelCelular;

	@Column(name = "tx_tel_cobranca")
	// @NotEmpty(message = "TelCobranca campo obrigatório!")
	@Size(max = 45, message = "TelCobranca tamanho máximo de 45 caracteres")
	private String txTelCobranca;

	@Column(name = "tx_tel_comercial")
	// @NotEmpty(message = "TelComercial campo obrigatório!")
	@Size(max = 45, message = "TelComercial tamanho máximo de 45 caracteres")
	private String txTelComercial;

	@Column(name = "tx_tel_contato")
	// @NotEmpty(message = "TelContato campo obrigatório!")
	@Size(max = 45, message = "TelContato tamanho máximo de 45 caracteres")
	private String txTelContato;

	@Column(name = "tx_tel_contato2")
	// @NotEmpty(message = "TelContato2 campo obrigatório!")
	@Size(max = 45, message = "TelContato2 tamanho máximo de 45 caracteres")
	private String txTelContato2;

	@Column(name = "tx_tel_contato3")
	// @NotEmpty(message = "TelContato3 campo obrigatório!")
	@Size(max = 45, message = "TelContato3 tamanho máximo de 45 caracteres")
	private String txTelContato3;

	@Column(name = "tx_tel_prestador")
	// @NotEmpty(message = "TelPrestador campo obrigatório!")
	@Size(max = 45, message = "TelPrestador tamanho máximo de 45 caracteres")
	private String txTelPrestador;

	@Column(name = "tx_tel_resencial")
	// @NotEmpty(message = "TelResencial campo obrigatório!")
	@Size(max = 45, message = "TelResencial tamanho máximo de 45 caracteres")
	private String txTelResencial;

	@Column(name = "ck_confirm_recebimento")
	// @NotNull(message = "ckConfirmRecebimento campo obrigatório!")
	private Integer ckConfirmRecebimento;

	@Column(name = "ck_encerrado")
	// @NotNull(message = "ckEncerrado campo obrigatório!")
	private Integer ckEncerrado;

	@Column(name = "tx_nome_responsavel")
	// @NotEmpty(message = "NomeResponsavel campo obrigatório!")
	@Size(max = 255, message = "NomeResponsavel tamanho máximo de 255 caracteres")
	private String txNomeResponsavel;

	@Column(name = "tx_email_responsavel")
	// @NotEmpty(message = "EmailResponsavel campo obrigatório!")
	@Size(max = 255, message = "EmailResponsavel tamanho máximo de 255 caracteres")
	private String txEmailResponsavel;

	@Column(name = "cd_resposta_definitiva")
	//@NotNull(message = "Resposta Definitiva campo obrigatório!")
	private Integer cdRespostaDefinitiva;

	@Column(name = "tx_tel_hospital")
	// @NotEmpty(message = "TelHospital campo obrigatório!")
	@Size(max = 30, message = "TelHospital tamanho máximo de 30 caracteres")
	private String txTelHospital;

	@Column(name = "tx_cond_triagem")
	// @NotEmpty(message = "CondTriagem campo obrigatório!")
	@Size(max = 45, message = "CondTriagem tamanho máximo de 45 caracteres")
	private String txCondTriagem;

	@Column(name = "tx_realizou_contato")
	// @NotEmpty(message = "RealizouContato campo obrigatório!")
	@Size(max = 3, message = "RealizouContato tamanho máximo de 3 caracteres")
	private String txRealizouContato;

	@Column(name = "dt_prim_contato")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtPrimContato;

	@Column(name = "tx_desfecho")
	// @NotEmpty(message = "Desfecho campo obrigatório!")
	@Size(max = 45, message = "Desfecho tamanho máximo de 45 caracteres")
	private String txDesfecho;
	
	@Column(name = "dt_desfecho") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDesfecho;

	@Column(name = "tx_medico")
	// @NotEmpty(message = "Medico campo obrigatório!")
	@Size(max = 356, message = "Medico tamanho máximo de 356 caracteres")
	private String txMedico;

	@Column(name = "tx_nome_contato")
	// @NotEmpty(message = "NomeContato campo obrigatório!")
	@Size(max = 256, message = "NomeContato tamanho máximo de 256 caracteres")
	private String txNomeContato;

	@Column(name = "tx_grau_parentesco")
	// @NotEmpty(message = "GrauParentesco campo obrigatório!")
	@Size(max = 45, message = "GrauParentesco tamanho máximo de 45 caracteres")
	private String txGrauParentesco;

	@Column(name = "dt_internacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtInternacao;

	@Column(name = "tx_motivo_nao_contato")
	// @NotEmpty(message = "MotivoNaoContato campo obrigatório!")
	@Size(max = 45, message = "MotivoNaoContato tamanho máximo de 45 caracteres")
	private String txMotivoNaoContato;

	@Column(name = "tx_tipo_internacao")
	// @NotEmpty(message = "TipoInternacao campo obrigatório!")
	@Size(max = 45, message = "TipoInternacao tamanho máximo de 45 caracteres")
	private String txTipoInternacao;

	@Column(name = "tx_cid")
	// @NotEmpty(message = "Cid campo obrigatório!")
	@Size(max = 200, message = "Cid tamanho máximo de 200 caracteres")
	private String txCid;

	@Column(name = "tx_desc_cid")
	// @NotEmpty(message = "DescCid campo obrigatório!")
	@Size(max = 256, message = "DescCid tamanho máximo de 256 caracteres")
	private String txDescCid;

	@Column(name = "tx_motivo_alta")
	// @NotEmpty(message = "MotivoAlta campo obrigatório!")
	@Size(max = 45, message = "MotivoAlta tamanho máximo de 45 caracteres")
	private String txMotivoAlta;

	@Column(name = "tx_tipo_assistencia")
	// @NotEmpty(message = "TipoAssistencia campo obrigatório!")
	@Size(max = 45, message = "TipoAssistencia tamanho máximo de 45 caracteres")
	private String txTipoAssistencia;

	@Column(name = "tx_nome_usuario")
	// @NotEmpty(message = "NomeUsuario campo obrigatório!")
	@Size(max = 255, message = "NomeUsuario tamanho máximo de 255 caracteres")
	private String txNomeUsuario;

	@Column(name = "tx_mensagem_resposta")
	private String txMensagemResposta;
	
	@Column(name = "ck_enviar_atualizacao")
	private Integer ckEnviarAtualizacao;
	
	@Column(name = "tx_tipo_acomodacao")
	private String txTipoAcomodacao;
	
	@Column(name = "dt_ultimo_envio")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtUltimoEnvio;
	
	@Column(name = "dt_confirm_recebimento")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtConfirmRecebimento;
	
	@Column(name = "cd_sequencia") 
	//@NotNull(message = "Sequencia campo obrigatório!")
	private Integer cdSequencia; 
	 
	@Column(name = "tx_cid2") 
	//@NotEmpty(message = "Cid2 campo obrigatório!")
	@Size(max = 200, message = "Cid2 tamanho máximo de 200 caracteres") 
	private String txCid2; 
	 
	@Column(name = "tx_desc_cid_2") 
	//@NotEmpty(message = "DescCid2 campo obrigatório!")
	@Size(max = 256, message = "DescCid2 tamanho máximo de 256 caracteres") 	
	private String txDescCid2; 
	 
	@Column(name = "tx_cid3") 
	//@NotEmpty(message = "Cid3 campo obrigatório!")
	@Size(max = 200, message = "Cid3 tamanho máximo de 200 caracteres") 
	private String txCid3; 
	 
	@Column(name = "tx_desc_cid3") 
	//@NotEmpty(message = "DescCid3 campo obrigatório!")
	@Size(max = 256, message = "DescCid3 tamanho máximo de 256 caracteres") 
	private String txDescCid3; 
	 
	@Column(name = "cd_nivel_consciencia") 
	//@NotNull(message = "NivelConsciencia campo obrigatório!")
	private Integer cdNivelConsciencia; 
	 
	@Column(name = "cd_sinais_vitais") 
	//@NotNull(message = "SinaisVitais campo obrigatório!")
	private Integer cdSinaisVitais; 
	 
	@Column(name = "cd_cc_geral") 
	//@NotNull(message = "CcGeral campo obrigatório!")
	private Integer cdCcGeral; 
	 
	@Column(name = "cd_cc_oxigenoterapia") 
	//@NotNull(message = "CcOxigenoterapia campo obrigatório!")
	private Integer cdCcOxigenoterapia; 
	 
	@Column(name = "ck_cc_asp_vias_aereas") 
	//@NotNull(message = "ckCcAspViasAereas campo obrigatório!")
	private Integer ckCcAspViasAereas; 
	 
	@Column(name = "cd_cc_aps_vias_aereas") 
	//@NotNull(message = "CcApsViasAereas campo obrigatório!")
	private Integer cdCcApsViasAereas; 
	 
	@Column(name = "ck_cc_acesso_venoso") 
	//@NotNull(message = "ckCcAcessoVenoso campo obrigatório!")
	private Integer ckCcAcessoVenoso; 
	 
	@Column(name = "cd_cc_acesso_venoso") 
	//@NotNull(message = "CcAcessoVenoso campo obrigatório!")
	private Integer cdCcAcessoVenoso; 
	 
	@Column(name = "cd_cc_alimentacao") 
	//@NotNull(message = "CcAlimentacao campo obrigatório!")
	private Integer cdCcAlimentacao; 
	 
	@Column(name = "cd_cc_ef_normal") 
	//@NotNull(message = "CcEfNormal campo obrigatório!")
	private String cdCcEfNormal; 
	 
	@Column(name = "cd_cc_ef_ausencia") 
	//@NotNull(message = "CcEfAusencia campo obrigatório!")
	private String cdCcEfAusencia; 
	 
	@Column(name = "cd_cc_ef_controle") 
	//@NotNull(message = "CcEfControle campo obrigatório!")
	private String cdCcEfControle; 
	 
	@Column(name = "cd_cc_ef_colostomia") 
	//@NotNull(message = "CcEfColostomia campo obrigatório!")
	private String cdCcEfColostomia; 
	 
	@Column(name = "cd_cc_ef_cistostomia") 
	//@NotNull(message = "CcEfCistostomia campo obrigatório!")
	private String cdCcEfCistostomia; 
	 
	@Column(name = "cd_cc_ef_nefrostomia") 
	//@NotNull(message = "CcEfNefrostomia campo obrigatório!")
	private String cdCcEfNefrostomia; 
	
	@Column(name = "cd_cc_ef_sva") 
	//@NotNull(message = "cdCcEfSva campo obrigatório!")
	private String cdCcEfSva; 
	
	@Column(name = "cd_cc_ef_svd") 
	//@NotNull(message = "cdCcEfSvd campo obrigatório!")
	private String cdCcEfSvd; 
	
	 
	@Column(name = "cd_cc_mu_habituais") 
	//@NotNull(message = "CcMuHabituais campo obrigatório!")
	private String cdCcMuHabituais; 
	 
	@Column(name = "cd_cc_mu_novos") 
	//@NotNull(message = "CcMuNovos campo obrigatório!")
	private String cdCcMuNovos; 
	 
	@Column(name = "cd_cc_mu_parenteral") 
	//@NotNull(message = "CcMuParenteral campo obrigatório!")
	private String cdCcMuParenteral; 
	 
	@Column(name = "cd_cc_mu_hipodermoclise") 
	//@NotNull(message = "CcMuHipodermoclise campo obrigatório!")
	private String cdCcMuHipodermoclise; 
	 
	@Column(name = "tx_cc_mu_obs") 
	//@NotEmpty(message = "CcMuObs campo obrigatório!")
	@Size(max = 16777215, message = "CcMuObs tamanho máximo de 16777215 caracteres") 
	private String txCcMuObs; 
	 
	@Column(name = "ck_cc_me_anticoagulantes") 
	//@NotNull(message = "ckCcMeAnticoagulantes campo obrigatório!")
	private Integer ckCcMeAnticoagulantes; 
	 
	@Column(name = "cd_cc_me_anticoagulantes") 
	//@NotNull(message = "CcMeAnticoagulantes campo obrigatório!")
	private Integer cdCcMeAnticoagulantes; 
	 
	@Column(name = "ck_cc_me_antibiotico") 
	//@NotNull(message = "ckCcMeAntibiotico campo obrigatório!")
	private Integer ckCcMeAntibiotico; 
	 
	@Column(name = "cd_cc_me_antibiotico") 
	//@NotNull(message = "CcMeAntibiotico campo obrigatório!")
	private Integer cdCcMeAntibiotico; 
	 
	@Column(name = "ck_cc_me_quimioterapicos") 
	//@NotNull(message = "ckCcMeQuimioterapicos campo obrigatório!")
	private Integer ckCcMeQuimioterapicos; 
	 
	@Column(name = "cd_cc_me_quimioterapicos") 
	//@NotNull(message = "CcMeQuimioterapicos campo obrigatório!")
	private Integer cdCcMeQuimioterapicos; 
	 
	@Column(name = "dt_cc_me_inicio_quimioterapicos") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtCcMeInicioQuimioterapicos; 
	 
	@Column(name = "dt_cc_me_fim_quimioterapicos") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtCcMeFimQuimioterapicos; 
	 
	@Column(name = "ck_cc_me_imunoglobulinas") 
	//@NotNull(message = "ckCcMeImunoglobulinas campo obrigatório!")
	private Integer ckCcMeImunoglobulinas; 
	 
	@Column(name = "cd_cc_me_imunoglobulinas") 
	//@NotNull(message = "CcMeImunoglobulinas campo obrigatório!")
	private Integer cdCcMeImunoglobulinas; 
	 
	@Column(name = "dt_cc_me_inicio_imunoglobulinas") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtCcMeInicioImunoglobulinas; 
	 
	@Column(name = "dt_cc_me_fim_imunoglobulinas") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtCcMeFimImunoglobulinas; 
	 
	@Column(name = "ck_cc_me_imunobiologico") 
	//@NotNull(message = "ckCcMeImunobiologico campo obrigatório!")
	private Integer ckCcMeImunobiologico; 
	 
	@Column(name = "cd_cc_me_imunobiologico") 
	//@NotNull(message = "CcMeImunobiologico campo obrigatório!")
	private Integer cdCcMeImunobiologico; 
	 
	@Column(name = "dt_cc_me_inicio_imunobiologico") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtCcMeInicioImunobiologico; 
	 
	@Column(name = "dt_cc_me_fim_imunobiologico") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtCcMeFimImunobiologico; 
	 
	@Column(name = "ck_cc_radioterapia") 
	//@NotNull(message = "ckCcRadioterapia campo obrigatório!")
	private Integer ckCcRadioterapia; 
	 
	@Column(name = "cd_cc_radioterapia") 
	//@NotNull(message = "CcRadioterapia campo obrigatório!")
	private Integer cdCcRadioterapia; 
	 
	@Column(name = "dt_cc_inicio_radioterapia") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtCcInicioRadioterapia; 
	 
	@Column(name = "dt_cc_fim_radioterapia") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtCcFimRadioterapia; 
	 
	@Column(name = "ck_cc_vos") 
	//@NotNull(message = "ckCcVos campo obrigatório!")
	private Integer ckCcVos; 
	 
	@Column(name = "cd_cc_vos") 
	//@NotNull(message = "CcVos campo obrigatório!")
	private Integer cdCcVos; 
	 
	@Column(name = "tx_cc_vos_obs") 
	//@NotEmpty(message = "CcVosObs campo obrigatório!")
	@Size(max = 16777215, message = "CcVosObs tamanho máximo de 16777215 caracteres") 
	private String txCcVosObs; 
	 
	@Column(name = "ck_cc_eq_multiprofissional") 
	//@NotNull(message = "ckCcEqMultiprofissional campo obrigatório!")
	private Integer ckCcEqMultiprofissional; 
	 
	@Column(name = "cd_cc_eq_multiprofissional") 
	//@NotNull(message = "CcEqMultiprofissional campo obrigatório!")
	private Integer cdCcEqMultiprofissional; 
	 
	@Column(name = "tx_cc_eq_multiprofissional") 
	//@NotEmpty(message = "CcEqMultiprofissional campo obrigatório!")
	@Size(max = 16777215, message = "CcEqMultiprofissional tamanho máximo de 16777215 caracteres") 
	private String txCcEqMultiprofissional; 
	 
	@Column(name = "ck_cc_ohb") 
	//@NotNull(message = "ckCcOhb campo obrigatório!")
	private Integer ckCcOhb; 
	 
	@Column(name = "cd_cc_ohb") 
	//@NotNull(message = "CcOhb campo obrigatório!")
	private Integer cdCcOhb; 
	 
	@Column(name = "cd_cc_ohb_externa") 
	//@NotNull(message = "CcOhbExterna campo obrigatório!")
	private Integer cdCcOhbExterna; 

	@Column(name = "dt_cc_ohb_inicio") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtCcOhbInicio; 

	@Column(name = "dt_cc_ohb_fim") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtCcOhbFim; 
	
	
	@Column(name = "cd_cc_imppressao_geral") 
	//@NotNull(message = "CcImppressaoGeral campo obrigatório!")
	private Integer cdCcImppressaoGeral; 
	 
	@Column(name = "ck_pt_medico_acredita") 
	//@NotNull(message = "ckPtMedicoAcredita campo obrigatório!")
	private Integer ckPtMedicoAcredita; 
	 
	@Column(name = "ck_pt_medico_deacordo") 
	//@NotNull(message = "ckPtMedicoDeacordo campo obrigatório!")
	private Integer ckPtMedicoDeacordo; 
	 
	@Column(name = "ck_pt_medico_proximas_etapas") 
	//@NotNull(message = "ckPtMedicoProximasEtapas campo obrigatório!")
	private Integer ckPtMedicoProximasEtapas; 
	 
	@Column(name = "tx_pt_medico_proximas_etapas") 
	//@NotEmpty(message = "PtMedicoProximasEtapas campo obrigatório!")
	@Size(max = 16777215, message = "PtMedicoProximasEtapas tamanho máximo de 16777215 caracteres") 
	private String txPtMedicoProximasEtapas; 
	 
	@Column(name = "cd_pt_medico_expectativas") 
	//@NotNull(message = "PtMedicoExpectativas campo obrigatório!")
	private Integer cdPtMedicoExpectativas; 
	 
	@Column(name = "ck_pt_medico_algum_procedimento") 
	//@NotNull(message = "ckPtMedicoAlgumProcedimento campo obrigatório!")
	private Integer ckPtMedicoAlgumProcedimento; 
	 
	@Column(name = "tx_pt_medico_algum_procedimento") 
	//@NotEmpty(message = "PtMedicoAlgumProcedimento campo obrigatório!")
	@Size(max = 16777215, message = "PtMedicoAlgumProcedimento tamanho máximo de 16777215 caracteres") 
	private String txPtMedicoAlgumProcedimento; 
	 
	@Column(name = "ck_pt_medico_ciencia_recursos") 
	//@NotNull(message = "ckPtMedicoCienciaRecursos campo obrigatório!")
	private Integer ckPtMedicoCienciaRecursos; 
	 
	@Column(name = "cd_pt_medico_ciencia_recursos") 
	//@NotNull(message = "PtMedicoCienciaRecursos campo obrigatório!")
	private Integer cdPtMedicoCienciaRecursos; 
	 
	@Column(name = "ck_ds_algum_apontamento") 
	//@NotNull(message = "ckDsAlgumApontamento campo obrigatório!")
	private Integer ckDsAlgumApontamento; 
	 
	@Column(name = "tx_ds_algum_apontamento") 
	//@NotEmpty(message = "DsAlgumApontamento campo obrigatório!")
	@Size(max = 16777215, message = "DsAlgumApontamento tamanho máximo de 16777215 caracteres") 
	private String txDsAlgumApontamento; 
	 
	@Column(name = "ck_ds_esposo") 
	//@NotNull(message = "ckDsEsposo campo obrigatório!")
	private String ckDsEsposo; 
	 
	@Column(name = "ck_ds_filho") 
	//@NotNull(message = "ckDsFilho campo obrigatório!")
	private String ckDsFilho; 
	 
	@Column(name = "ck_ds_pai") 
	//@NotNull(message = "ckDsPai campo obrigatório!")
	private String ckDsPai; 
	 
	@Column(name = "ck_ds_mae") 
	//@NotNull(message = "ckDsMae campo obrigatório!")
	private String ckDsMae; 
	 
	@Column(name = "ck_ds_irmao") 
	//@NotNull(message = "ckDsIrmao campo obrigatório!")
	private String ckDsIrmao; 
	 
	@Column(name = "ck_ds_sogro") 
	//@NotNull(message = "ckDsSogro campo obrigatório!")
	private String ckDsSogro; 
	 
	@Column(name = "ck_ds_outros") 
	//@NotNull(message = "ckDsOutros campo obrigatório!")
	private String ckDsOutros; 
	 
	@Column(name = "tx_ds_outros") 
	//@NotEmpty(message = "DsOutros campo obrigatório!")
	@Size(max = 2000, message = "DsOutros tamanho máximo de 2000 caracteres") 
	private String txDsOutros; 
	 
	@Column(name = "ck_ds_cuidador") 
	//@NotNull(message = "ckDsCuidador campo obrigatório!")
	private Integer ckDsCuidador; 
	 
	@Column(name = "cd_ds_cuidador") 
	//@NotNull(message = "DsCuidador campo obrigatório!")
	private Integer cdDsCuidador; 
	 
	@Column(name = "cd_ds_idade") 
	//@NotNull(message = "DsIdade campo obrigatório!")
	private Integer cdDsIdade; 

	@Column(name = "tx_ds_cuidador_parentesco") 
	@Size(max = 45, message = "DsPendenciaFamiliar tamanho máximo de 16777215 caracteres") 
	private String txDsCuidadorParentesco; 
	
	@Column(name = "ck_ds_pendencia_familiar") 
	//@NotNull(message = "ckDsPendenciaFamiliar campo obrigatório!")
	private Integer ckDsPendenciaFamiliar; 
	 
	@Column(name = "tx_ds_pendencia_familiar") 
	//@NotEmpty(message = "DsPendenciaFamiliar campo obrigatório!")
	//@Size(max = 16777215, message = "DsPendenciaFamiliar tamanho máximo de 16777215 caracteres") 
	private String txDsPendenciaFamiliar; 

	@Column(name = "dt_cancelamento")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtCancelamento;
	
	@Column(name = "cd_usuario_cancelamento") 
	private Integer cdUsuarioCancelamento; 
	
	@Column(name = "tx_questionario") 
	private String txQuestionario; 

	@Column(name = "vl_qtde_dias_novo_contato") 
	private Integer vlQtdeDiasNovoContato; 

	
	@Column(name = "dt_novo_contato")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtNovoContato;
	
	
	@Column(name = "ck_totalizador") 
	private Integer ckTotalizador; 
	
	@Column(name = "cd_status") 
	private Integer cdStatus; 
	
	@Column(name = "cd_tipo") 
	private Integer cdTipo; 

	@Column(name = "cd_cc_covid19") 
	private Integer cdCcCovid19; 

	@Column(name = "cd_prob_encontrados") 
	private Integer cdProbEncontrados; 

	@Column(name = "cd_pt_medico_homecare") 
	//@NotEmpty(message = "PtMedicoHomecare campo obrigatório!")
	@Size(max = 3, message = "PtMedicoHomecare tamanho máximo de 3 caracteres") 
	private String cdPtMedicoHomecare; 
	 
	@Column(name = "cd_pt_medico_atb") 
	//@NotEmpty(message = "PtMedicoAtb campo obrigatório!")
	@Size(max = 3, message = "PtMedicoAtb tamanho máximo de 3 caracteres") 
	private String cdPtMedicoAtb; 
	 
	@Column(name = "cd_pt_medico_reabilitacao") 
	//@NotEmpty(message = "PtMedicoReabilitacao campo obrigatório!")
	@Size(max = 3, message = "PtMedicoReabilitacao tamanho máximo de 3 caracteres") 
	private String cdPtMedicoReabilitacao; 
	 
	@Column(name = "cd_pt_medico_cuidados_casa") 
	//@NotEmpty(message = "PtMedicoCuidadosCasa campo obrigatório!")
	@Size(max = 3, message = "PtMedicoCuidadosCasa tamanho máximo de 3 caracteres") 
	private String cdPtMedicoCuidadosCasa; 
	 
	@Column(name = "cd_pt_medico_sem_indicacao_hospitalar") 
	//@NotEmpty(message = "PtMedicoSemIndicacaoHospitalar campo obrigatório!")
	@Size(max = 3, message = "PtMedicoSemIndicacaoHospitalar tamanho máximo de 3 caracteres") 
	private String cdPtMedicoSemIndicacaoHospitalar; 
	 
	@Column(name = "ck_pt_medico_fidelizado") 
	//@NotNull(message = "ckPtMedicoFidelizado campo obrigatório!")
	private Integer ckPtMedicoFidelizado; 
	 
	@Column(name = "ck_pt_medico_indicar_acompanhamento") 
	//@NotNull(message = "ckPtMedicoIndicarAcompanhamento campo obrigatório!")
	private Integer ckPtMedicoIndicarAcompanhamento; 

	@Column(name = "ck_pode_atender") 
	//@NotNull(message = "ckPtMedicoIndicarAcompanhamento campo obrigatório!")
	private Integer ckPodeAtender; 
	
	@Column(name = "cd_paciente_hospital_casa") 
	//@NotNull(message = "ckPtMedicoIndicarAcompanhamento campo obrigatório!")
	private Integer  cdPacienteHospitalCasa;
	
	@Column(name = "cd_paciente_cuidador_periodo_integral") 
	//@NotNull(message = "ckPtMedicoIndicarAcompanhamento campo obrigatório!")
	private Integer cdPacienteCuidadorPeriodoIntegral; 
	
	
	@Column(name = "cd_paciente_internacoes_ultimo_ano") 
	//@NotNull(message = "PacienteInternacoesUltimoAno campo obrigatório!")
	private Integer  cdPacienteInternacoesUltimoAno; 

	@Column(name = "dt_covid_resultado") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtCovidResultado; 
	
	@Column(name = "cd_nivel_consciencia_antes") 
	//@NotNull(message = "NivelConsciencia campo obrigatório!")
	private Integer cdNivelConscienciaAntes; 
	
	@Column(name = "cd_paciente_desconforto_posicao") 
	//@NotNull(message = "PacienteDesconfortoPosicao campo obrigatório!")
	private Integer cdPacienteDesconfortoPosicao; 

	@Column(name = "cd_paciente_pele_contato_urina") 
	//@NotNull(message = "PacienteDesconfortoPosicao campo obrigatório!")
	private Integer cdPacientePeleContatoUrina; 
	
	@Column(name = "cd_tipo_lesao_pele") 
	//@NotNull(message = "TipoLesaoPele campo obrigatório!")
	private Integer cdTipoLesaoPele;
	
	@Column(name = "cd_paciente_anda_cama") 
	//@NotNull(message = "PacienteAndaCama campo obrigatório!")
	private Integer cdPacienteAndaCama; 
	 
	@Column(name = "cd_paciente_acamado_cadeira_rodas") 
	//@NotNull(message = "PacienteAcamadoCadeiraRodas campo obrigatório!")
	private Integer cdPacienteAcamadoCadeiraRodas; 
	 
	@Column(name = "cd_paciente_cama_cadeira_imovel") 
	//@NotNull(message = "PacienteCamaCadeiraImovel campo obrigatório!")
	private Integer cdPacienteCamaCadeiraImovel; 
	 
	@Column(name = "cd_paciente_movimento_cama") 
	//@NotNull(message = "PacienteMovimentoCama campo obrigatório!")
	private Integer cdPacienteMovimentoCama; 
	 
	@Column(name = "cd_paciente_tipo_banho") 
	//@NotNull(message = "PacienteTipoBanho campo obrigatório!")
	private Integer cdPacienteTipoBanho; 
	
	@Column(name = "cd_paciente_alimentacao_qualidade") 
	//@NotNull(message = "PacienteAlimentacaoQualidade campo obrigatório!")
	private Integer cdPacienteAlimentacaoQualidade; 
	 
	@Column(name = "cd_paciente_alimentacao_como_atualmente") 
	//@NotNull(message = "PacienteAlimentacaoComoAtualmente campo obrigatório!")
	private Integer cdPacienteAlimentacaoComoAtualmente; 
	 
	@Column(name = "cd_paciente_alimentacao_estado_nutricional") 
	//@NotNull(message = "PacienteAlimentacaoEstadoNutricional campo obrigatório!")
	private Integer cdPacienteAlimentacaoEstadoNutricional; 
	 
	@Column(name = "cd_paciente_alimentacao_tipo_medicamentos") 
	//@NotNull(message = "PacienteAlimentacaoTipoMedicamentos campo obrigatório!")
	private Integer cdPacienteAlimentacaoTipoMedicamentos; 

	@Column(name = "cd_paciente_tipo_alimentacao_parenteral") 
	//@NotNull(message = "PacienteTipoAlimentacaoParenteral campo obrigatório!")
	private Integer cdPacienteTipoAlimentacaoParenteral; 
	 
	@Column(name = "cd_paciente_modo_vestir_atualmente") 
	//@NotNull(message = "PacienteModoVestirAtualmente campo obrigatório!")
	private Integer cdPacienteModoVestirAtualmente; 
	 
	@Column(name = "cd_paciente_modo_banheiro_atualmente") 
	//@NotNull(message = "PacienteModoBanheiroAtualmente campo obrigatório!")
	private Integer cdPacienteModoBanheiroAtualmente; 
	 
	@Column(name = "cd_paciente_vontade_banheiro_atualmente") 
	//@NotNull(message = "PacienteVontadeBanheiroAtualmente campo obrigatório!")
	private Integer cdPacienteVontadeBanheiroAtualmente; 
	 
	@Column(name = "cd_paciente_alimentar_atualmente") 
	//@NotNull(message = "PacienteAlimentarAtualmente campo obrigatório!")
	private Integer cdPacienteAlimentarAtualmente; 

	@Column(name = "cd_paciente_aspirado_sonda") 
	//@NotNull(message = "PacienteAspiradoSonda campo obrigatório!")
	private Integer cdPacienteAspiradoSonda; 
	 
	@Column(name = "cd_paciente_aspirado_boca_nariz") 
	//@NotNull(message = "PacienteAspiradoBocaNariz campo obrigatório!")
	private Integer cdPacienteAspiradoBocaNariz; 
	 
	@Column(name = "ck_paciente_aparelho_respiracao") 
	//@NotNull(message = "ckPacienteAparelhoRespiracao campo obrigatório!")
	private Integer ckPacienteAparelhoRespiracao; 
	 
	@Column(name = "cd_paciente_aparelho_respiracao") 
	//@NotNull(message = "PacienteAparelhoRespiracao campo obrigatório!")
	private Integer cdPacienteAparelhoRespiracao; 
	 
	@Column(name = "cd_paciente_exercicios_ventilatorios") 
	//@NotNull(message = "PacienteExerciciosVentilatorios campo obrigatório!")
	private Integer cdPacienteExerciciosVentilatorios; 
	
	@Column(name = "cd_paciente_utilizando_oxigenio") 
	//@NotNull(message = "PacienteUtilizandoOxigenio campo obrigatório!")
	private Integer cdPacienteUtilizandoOxigenio; 
	 
	@Column(name = "cd_paciente_tipo_oxigenio") 
	//@NotNull(message = "PacienteTipoOxigenio campo obrigatório!")
	private Integer cdPacienteTipoOxigenio; 
	 
	@Column(name = "cd_paciente_tipo_oxigenio_antes") 
	//@NotNull(message = "PacienteTipoOxigenioAntes campo obrigatório!")
	private Integer cdPacienteTipoOxigenioAntes; 
	 
	@Column(name = "ck_paciente_medicacao_veia") 
	//@NotNull(message = "ckPacienteMedicacaoVeia campo obrigatório!")
	private Integer ckPacienteMedicacaoVeia; 
	 
	@Column(name = "cd_paciente_medicacao_veia") 
	//@NotNull(message = "PacienteMedicacaoVeia campo obrigatório!")
	private Integer cdPacienteMedicacaoVeia; 
	 
	@Column(name = "cd_paciente_medicacao_administradas") 
	//@NotNull(message = "PacienteMedicacaoAdministradas campo obrigatório!")
	private Integer cdPacienteMedicacaoAdministradas;
	
	@Column(name = "cd_paciente_pendencia_tratamento") 
	//@NotNull(message = "PacientePendenciaTratamento campo obrigatório!")
	private Integer cdPacientePendenciaTratamento; 
	 
	@Column(name = "cd_paciente_necessidades_apos_alta") 
	//@NotNull(message = "PacienteNecessidadesAposAlta campo obrigatório!")
	private Integer cdPacienteNecessidadesAposAlta; 
	 
	@Column(name = "ck_paciente_atendimento_casa") 
	//@NotNull(message = "ckPacienteAtendimentoCasa campo obrigatório!")
	private Integer ckPacienteAtendimentoCasa; 
	 
	@Column(name = "cd_paciente_empresa_homecare") 
	//@NotNull(message = "ckPacienteEmpresaHomecare campo obrigatório!")
	private Integer cdPacienteEmpresaHomecare; 
	 
	@Column(name = "ck_paciente_medico_regularmente") 
	//@NotNull(message = "ckPacienteMedicoRegularmente campo obrigatório!")
	private Integer ckPacienteMedicoRegularmente; 
	 
	@Column(name = "ck_paciente_medico_perto_residencia") 
	//@NotNull(message = "ckPacienteMedicoPertoResidencia campo obrigatório!")
	private Integer ckPacienteMedicoPertoResidencia; 
	 
	@Column(name = "ck_paciente_operadora_indicar_medico") 
	//@NotNull(message = "ckPacienteOperadoraIndicarMedico campo obrigatório!")
	private Integer ckPacienteOperadoraIndicarMedico; 
	 
	@Column(name = "ck_paciente_alguma_duvida") 
	//@NotNull(message = "ckPacienteAlgumaDuvida campo obrigatório!")
	private Integer ckPacienteAlgumaDuvida; 
	
	@Column(name = "cd_paciente_katz") 
	private Integer cdPacienteKatz;
	


	
	@Transient
	private Integer vlQtde; 

	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txAssunto))
			txAssunto = txAssunto.toUpperCase();
		if (!Validator.isBlankOrNull(txCodSegurado))
			txCodSegurado = txCodSegurado.toUpperCase();
		if (!Validator.isBlankOrNull(txEmpresa))
			txEmpresa = txEmpresa.toUpperCase();
		if (!Validator.isBlankOrNull(txEstado))
			txEstado = txEstado.toUpperCase();
		if (!Validator.isBlankOrNull(txMensagem))
			txMensagem = txMensagem.toUpperCase();
		if (!Validator.isBlankOrNull(txNomeSegurado))
			txNomeSegurado = txNomeSegurado.toUpperCase();
		if (!Validator.isBlankOrNull(txNumVpp))
			txNumVpp = txNumVpp.toUpperCase();
		if (!Validator.isBlankOrNull(txPrestador))
			txPrestador = txPrestador.toUpperCase();
		if (!Validator.isBlankOrNull(txProdSaude))
			txProdSaude = txProdSaude.toUpperCase();
		if (!Validator.isBlankOrNull(txSexo))
			txSexo = txSexo.toUpperCase();
		if (!Validator.isBlankOrNull(txSlaTratativa))
			txSlaTratativa = txSlaTratativa.toUpperCase();
		if (!Validator.isBlankOrNull(txTelCelular))
			txTelCelular = txTelCelular.toUpperCase();
		if (!Validator.isBlankOrNull(txTelCobranca))
			txTelCobranca = txTelCobranca.toUpperCase();
		if (!Validator.isBlankOrNull(txTelComercial))
			txTelComercial = txTelComercial.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato))
			txTelContato = txTelContato.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato2))
			txTelContato2 = txTelContato2.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato3))
			txTelContato3 = txTelContato3.toUpperCase();
		if (!Validator.isBlankOrNull(txTelPrestador))
			txTelPrestador = txTelPrestador.toUpperCase();
		if (!Validator.isBlankOrNull(txTelResencial))
			txTelResencial = txTelResencial.toUpperCase();
		if (!Validator.isBlankOrNull(txNomeResponsavel))
			txNomeResponsavel = txNomeResponsavel.toUpperCase();
		if (!Validator.isBlankOrNull(txEmailResponsavel))
			txEmailResponsavel = txEmailResponsavel.toUpperCase();
		if (!Validator.isBlankOrNull(txTelHospital))
			txTelHospital = txTelHospital.toUpperCase();
		if (!Validator.isBlankOrNull(txCondTriagem))
			txCondTriagem = txCondTriagem.toUpperCase();
		if (!Validator.isBlankOrNull(txRealizouContato))
			txRealizouContato = txRealizouContato.toUpperCase();
		if (!Validator.isBlankOrNull(txDesfecho))
			txDesfecho = txDesfecho.toUpperCase();
		if (!Validator.isBlankOrNull(txMedico))
			txMedico = txMedico.toUpperCase();
		if (!Validator.isBlankOrNull(txNomeContato))
			txNomeContato = txNomeContato.toUpperCase();
		if (!Validator.isBlankOrNull(txMotivoNaoContato))
			txMotivoNaoContato = txMotivoNaoContato.toUpperCase();
		if (!Validator.isBlankOrNull(txTipoInternacao))
			txTipoInternacao = txTipoInternacao.toUpperCase();
		if (!Validator.isBlankOrNull(txCid))
			txCid = txCid.toUpperCase();
		if (!Validator.isBlankOrNull(txDescCid))
			txDescCid = txDescCid.toUpperCase();
		if (!Validator.isBlankOrNull(txMotivoAlta))
			txMotivoAlta = txMotivoAlta.toUpperCase();
		if (!Validator.isBlankOrNull(txTipoAssistencia))
			txTipoAssistencia = txTipoAssistencia.toUpperCase();
		if (!Validator.isBlankOrNull(txNomeUsuario))
			txNomeUsuario = txNomeUsuario.toUpperCase();
	}

	
}
