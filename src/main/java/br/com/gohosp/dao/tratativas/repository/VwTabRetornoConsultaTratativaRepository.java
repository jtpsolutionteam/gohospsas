package br.com.gohosp.dao.tratativas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.tratativas.VwTabRetornoConsultaTratativaObj;



public interface VwTabRetornoConsultaTratativaRepository extends JpaRepository<VwTabRetornoConsultaTratativaObj, Integer> {

	List<VwTabRetornoConsultaTratativaObj> findBytxTratativaIdAndDtCancelamentoIsNullOrderByCdCodigoAsc(String txTratativaId);

}