package br.com.gohosp.dao.tratativas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.tratativas.VwTabPacienteObj;



public interface VwTabPacienteRepository extends JpaRepository<VwTabPacienteObj, Integer> {

	VwTabPacienteObj findByTxCpfSegurado(String txCpfSegurado);
	
	List<VwTabPacienteObj> findByCdStatusPaciente(Integer cdStatusPaciente);

	@Query("select distinct t from VwTabPacienteObj t where t.cdStatusTratativa is null and t.dtNovoContato is null")
	List<VwTabPacienteObj> findNovasTratativasSaldoQuery();
	
	@Query("select distinct t from VwTabPacienteObj t join fetch t.listaTratativas listaTratativas where t.cdStatusTratativa is null and t.dtNovoContato is null order by t.dtDataSolicitacao desc, t.cdPaciente")
	List<VwTabPacienteObj> findNovasTratativasQuery();
	
	@Query("select distinct t from VwTabPacienteObj t join fetch t.listaTratativas listaTratativas where t.cdStatusTratativa is null and t.cdTipo = 1 and t.cdSequencia in(1,2) and t.dtNovoContato is null and date(t.dtSlaTratativa) < date(now()) order by t.dtDataSolicitacao desc, t.cdPaciente")
	List<VwTabPacienteObj> findNovasTratativasSlaVencidoQuery();

	@Query("select distinct t from VwTabPacienteObj t join fetch t.listaTratativas listaTratativas where t.cdStatusTratativa = 1 and t.vlSlaPrimContato between 1 and 5 and t.cdTipo = 2 order by t.dtPrimContato, t.cdPaciente")
	List<VwTabPacienteObj> findSla5DiasQuery();
 
	@Query("select distinct t from VwTabPacienteObj t join fetch t.listaTratativas listaTratativas where t.cdStatusTratativa = 1 and t.cdCcCovid19 is not null order by t.dtPrimContato, t.cdPaciente")
	List<VwTabPacienteObj> findCovidEmAbertoQuery();

	
	@Query("select distinct t from VwTabPacienteObj t join fetch t.listaTratativas listaTratativas where t.cdStatusTratativa = 1 and t.vlSlaPrimContato between 6 and 10 and t.cdTipo = 2 order by t.dtPrimContato, t.cdPaciente")
	List<VwTabPacienteObj> findSla10DiasQuery();
	
	@Query("select distinct t from VwTabPacienteObj t join fetch t.listaTratativas listaTratativas where t.cdStatusTratativa = 1 and t.vlSlaPrimContato > 10 and t.cdTipo = 2 order by t.dtPrimContato, t.cdPaciente")
	List<VwTabPacienteObj> findSla10DiasMaisQuery();
	
	@Query("select distinct t from VwTabPacienteObj t join fetch t.listaTratativas listaTratativas where t.cdStatusTratativa = ?1 order by t.dtDataSolicitacao desc")
	List<VwTabPacienteObj> findByCdStatusTratativaQuery(Integer cdStatusTratativa);
	
	@Query("select distinct t from VwTabPacienteObj t join fetch t.listaTratativas listaTratativas where t.cdTipo = 1 and t.cdSequencia > 1 and t.cdStatusTratativa < 2 order by t.dtDataSolicitacao, t.cdPaciente, t.cdStatusTratativa")
	List<VwTabPacienteObj> findNovaRespostaSulamericaQuery();

	@Query("select distinct t from VwTabPacienteObj t join fetch t.listaTratativas listaTratativas where t.cdStatusTratativa = 1 and t.dtNovoContato is null  order by t.dtDataSolicitacao, t.cdPaciente")
	List<VwTabPacienteObj> findAbertoSemNovoContatoQuery();
	
	@Query("select distinct t from VwTabPacienteObj t join fetch t.listaTratativas listaTratativas where t.cdStatusTratativa = 1 and date(t.dtNovoContato) <= date(now()) order by t.dtNovoContato, t.cdPaciente")
	List<VwTabPacienteObj> findNovoContatoQuery();

	@Query("select distinct t from VwTabPacienteObj t join fetch t.listaTratativas listaTratativas where t.cdStatusTratativa = 1 and date(t.dtNovoContato) > date(now()) order by t.dtNovoContato, t.cdPaciente")
	List<VwTabPacienteObj> findNovoContatoPosteriorQuery();

}