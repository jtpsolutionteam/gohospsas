package br.com.gohosp.dao.tratativas.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaObj;



public interface TabRetornoConsultaTratativaRepository extends JpaRepository<TabRetornoConsultaTratativaObj, Integer> {


	List<TabRetornoConsultaTratativaObj> findByCkConfirmRecebimento(Integer ckConfirmRecebimento);
	 
	
	List<TabRetornoConsultaTratativaObj> findByCkEncerradoAndDtCancelamentoIsNullOrderByDtDataSolicitacaoDesc(Integer ckEncerrado);
	
	List<TabRetornoConsultaTratativaObj> findByCkEnviarAtualizacao(Integer ckEnviarAtualizacao);
	
	List<TabRetornoConsultaTratativaObj> findByCdCodigo(Integer cdCodigo);
	
	List<TabRetornoConsultaTratativaObj> findByTxRealizouContatoAndCkEncerrado(String txRealizouContato, Integer ckEncerrado);

	@Query("select t from TabRetornoConsultaTratativaObj t where t.dtNovoContato is not null and t.dtCancelamento is null order by t.dtDataSolicitacao")
	List<TabRetornoConsultaTratativaObj> findByTratativasTodosNovosContatosQuery();
	
	@Query("select t from TabRetornoConsultaTratativaObj t where date(t.dtNovoContato) <= date(now()) and t.dtCancelamento is null order by t.dtNovoContato")
	List<TabRetornoConsultaTratativaObj> findByTratativasNovoContatoQuery();

	@Query("select t from TabRetornoConsultaTratativaObj t where date(t.dtNovoContato) > date(now()) and t.dtCancelamento is null order by t.dtNovoContato")
	List<TabRetornoConsultaTratativaObj> findByTratativasContatoPosteriorQuery();

	
	@Query("select t from TabRetornoConsultaTratativaObj t where t.ckEncerrado = 0 and t.cdSequencia > 1 and t.dtNovoContato is null and t.dtCancelamento is null order by t.dtDataSolicitacao desc")
	List<TabRetornoConsultaTratativaObj> findByTratativasAlertaRespostaQuery();
	
	//@Query("select t from TabRetornoConsultaTratativaObj t where t.ckEncerrado = 0 and t.txRealizouContato is null and t.cdSequencia = 1 and t.dtCancelamento is null")
	@Query("select t from TabRetornoConsultaTratativaObj t where t.ckEncerrado = 0 and t.cdSequencia = 1 and t.dtNovoContato is null and t.dtCancelamento is null order by t.dtDataSolicitacao desc")
	List<TabRetornoConsultaTratativaObj> findByTratativasNovasQuery();
	
	@Query("select t from TabRetornoConsultaTratativaObj t where t.txTratativaId = ?1 and t.cdSequencia not in(?2) and t.dtCancelamento is null")
	List<TabRetornoConsultaTratativaObj> findByTratativasNotInSequenciaQuery(String txTratativaId, Integer cdSequencia);

	@Query("select t from TabRetornoConsultaTratativaObj t where t.txTratativaId = ?1 and t.cdSequencia not in(?2) and t.ckEncerrado = 0 and t.dtCancelamento is null")
	List<TabRetornoConsultaTratativaObj> findByTratativasNotInSequenciaNotEncerradoQuery(String txTratativaId, Integer cdSequencia);

	@Query("select t from TabRetornoConsultaTratativaObj t where t.txTratativaId = ?1 and t.cdSequencia < ?2 and t.ckEncerrado = 0 and t.dtCancelamento is null")
	List<TabRetornoConsultaTratativaObj> findByTratativasMenorSequenciaNotEncerradoQuery(String txTratativaId, Integer cdSequencia);
	
	List<TabRetornoConsultaTratativaObj> findBytxTratativaIdAndDtCancelamentoIsNull(String txTratativaId);
	
	List<TabRetornoConsultaTratativaObj> findBytxTratativaIdAndCdTipoAndDtCancelamentoIsNullOrderByCdCodigoAsc(String txTratativaId, Integer cdTipo);
	
	List<TabRetornoConsultaTratativaObj> findByDtCancelamentoIsNotNull();
	
	TabRetornoConsultaTratativaObj findByTxTratativaIdAndCdSequencia(String txTratativaId, Integer cdSequencia);
	
	TabRetornoConsultaTratativaObj findByTxTratativaIdAndTxMsgId(String txTratativaId, String txMsgId);
	
	List<TabRetornoConsultaTratativaObj> findByTxTratativaIdAndTxMsgIdOrderByTxTratativaIdAsc(String txTratativaId, String txMsgId);
	
	@Query("select t from TabRetornoConsultaTratativaObj t where month(t.dtDataSolicitacao) = month(?1) and t.cdStatus in(1,4,7) and t.dtCancelamento is null order by t.dtDataSolicitacao")
	List<TabRetornoConsultaTratativaObj> findByTratativasDataSolicitacaoMonthQuery(Date dtDataSolicitacao);

	@Query("select t from TabRetornoConsultaTratativaObj t where t.txMensagem like '%#FINALIZA%' and date(t.dtDataSolicitacao) = date(now())")
	List<TabRetornoConsultaTratativaObj> findByFinalizaDiarioQuery();

	@Query("select t from TabRetornoConsultaTratativaObj t where t.txMensagem like '%#FINALIZA%' and month(t.dtDataSolicitacao) = month(now())")
	List<TabRetornoConsultaTratativaObj> findByFinalizaMensalQuery();

}