package br.com.gohosp.dao.tratativas;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "tab_envios", schema = Constants.SCHEMA)
public class TabEnviosObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_envio")
	// @NotNull(message = "Envio campo obrigatório!")
	private Integer cdEnvio;

	@Column(name = "tx_tratativa_id")
	// @NotEmpty(message = "TratativaId campo obrigatório!")
	@Size(max = 45, message = "TratativaId tamanho máximo de 45 caracteres")
	private String txTratativaId;

	@Column(name = "dt_envio")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtEnvio;

	@Column(name = "tx_usuario")
	// @NotEmpty(message = "Usuario campo obrigatório!")
	@Size(max = 45, message = "Usuario tamanho máximo de 45 caracteres")
	private String txUsuario;
	
	@Column(name = "ck_tipo")	
	private Integer ckTipo;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
		if (!Validator.isBlankOrNull(txUsuario))
			txUsuario = txUsuario.toUpperCase();
	}

	public Integer getCdEnvio() {
		return cdEnvio;
	}

	public void setCdEnvio(Integer cdEnvio) {
		this.cdEnvio = cdEnvio;
	}

	public String getTxTratativaId() {
		return txTratativaId;
	}

	public void setTxTratativaId(String txTratativaId) {
		this.txTratativaId = txTratativaId;
	}

	public Date getDtEnvio() {
		return dtEnvio;
	}

	public void setDtEnvio(Date dtEnvio) {
		this.dtEnvio = dtEnvio;
	}

	public String getTxUsuario() {
		return txUsuario;
	}

	public void setTxUsuario(String txUsuario) {
		this.txUsuario = txUsuario;
	}

	public Integer getCkTipo() {
		return ckTipo;
	}

	public void setCkTipo(Integer ckTipo) {
		this.ckTipo = ckTipo;
	}
	
	

}
