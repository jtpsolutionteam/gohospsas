package br.com.gohosp.dao.tratativas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaCidsObj;

public interface TabRetornoConsultaTratativaCidsRepository
		extends JpaRepository<TabRetornoConsultaTratativaCidsObj, Integer> {
	List<TabRetornoConsultaTratativaCidsObj> findByCdCodigoAndCdTipo(Integer cdCodigo, Integer cdTipo);
}