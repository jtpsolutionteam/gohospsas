package br.com.gohosp.dao.tratativas.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaTotalizadorObj;



public interface TabRetornoConsultaTratativaTotalizadorRepository extends JpaRepository<TabRetornoConsultaTratativaTotalizadorObj, Integer> {

}