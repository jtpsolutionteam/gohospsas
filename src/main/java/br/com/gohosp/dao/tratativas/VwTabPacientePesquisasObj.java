package br.com.gohosp.dao.tratativas;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "vw_tab_paciente", schema = Constants.SCHEMA)
public class VwTabPacientePesquisasObj {

	@Id	
	@Column(name = "tx_cpf_segurado")
	// @NotEmpty(message = "CpfSegurado campo obrigatório!")
	@Size(max = 20, message = "CpfSegurado tamanho máximo de 20 caracteres")
	private String txCpfSegurado;
	
	@Column(name = "cd_paciente")
	// @NotNull(message = "Paciente campo obrigatório!")
	private Integer cdPaciente;

	@Column(name = "tx_carteira")
	// @NotEmpty(message = "Carteira campo obrigatório!")
	@Size(max = 45, message = "Carteira tamanho máximo de 45 caracteres")
	private String txCarteira;

	@Column(name = "tx_cod_segurado")
	// @NotEmpty(message = "CodSegurado campo obrigatório!")
	@Size(max = 45, message = "CodSegurado tamanho máximo de 45 caracteres")
	private String txCodSegurado;

	@Column(name = "dt_data_nascimento")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataNascimento;

	@Column(name = "tx_nome_segurado")
	// @NotEmpty(message = "NomeSegurado campo obrigatório!")
	@Size(max = 45, message = "NomeSegurado tamanho máximo de 45 caracteres")
	private String txNomeSegurado;

	@Column(name = "tx_prod_saude")
	// @NotEmpty(message = "ProdSaude campo obrigatório!")
	@Size(max = 45, message = "ProdSaude tamanho máximo de 45 caracteres")
	private String txProdSaude;

	@Column(name = "tx_sexo")
	// @NotEmpty(message = "Sexo campo obrigatório!")
	@Size(max = 45, message = "Sexo tamanho máximo de 45 caracteres")
	private String txSexo;

	@Column(name = "tx_tel_celular")
	// @NotEmpty(message = "TelCelular campo obrigatório!")
	@Size(max = 45, message = "TelCelular tamanho máximo de 45 caracteres")
	private String txTelCelular;

	@Column(name = "tx_tel_cobranca")
	// @NotEmpty(message = "TelCobranca campo obrigatório!")
	@Size(max = 45, message = "TelCobranca tamanho máximo de 45 caracteres")
	private String txTelCobranca;

	@Column(name = "tx_tel_comercial")
	// @NotEmpty(message = "TelComercial campo obrigatório!")
	@Size(max = 45, message = "TelComercial tamanho máximo de 45 caracteres")
	private String txTelComercial;

	@Column(name = "tx_tel_contato")
	// @NotEmpty(message = "TelContato campo obrigatório!")
	@Size(max = 45, message = "TelContato tamanho máximo de 45 caracteres")
	private String txTelContato;

	@Column(name = "tx_tel_contato2")
	// @NotEmpty(message = "TelContato2 campo obrigatório!")
	@Size(max = 45, message = "TelContato2 tamanho máximo de 45 caracteres")
	private String txTelContato2;

	@Column(name = "tx_tel_contato3")
	// @NotEmpty(message = "TelContato3 campo obrigatório!")
	@Size(max = 45, message = "TelContato3 tamanho máximo de 45 caracteres")
	private String txTelContato3;

	@Column(name = "tx_tel_prestador")
	// @NotEmpty(message = "TelPrestador campo obrigatório!")
	@Size(max = 45, message = "TelPrestador tamanho máximo de 45 caracteres")
	private String txTelPrestador;

	@Column(name = "tx_tel_resencial")
	// @NotEmpty(message = "TelResencial campo obrigatório!")
	@Size(max = 45, message = "TelResencial tamanho máximo de 45 caracteres")
	private String txTelResencial;
	
	@Column(name = "cd_status_paciente")
	// @NotNull(message = "Status campo obrigatório!")
	private Integer cdStatusPaciente;
	
	@Column(name = "dt_ultima_atualizacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtUltimaAtualizacao;
	
	@Column(name = "dt_novo_contato")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtNovoContato;
	
	@Column(name = "cd_status_tratativa")
	// @NotNull(message = "Status campo obrigatório!")
	private Integer cdStatusTratativa;

	@Column(name = "cd_sequencia")
	// @NotNull(message = "Status campo obrigatório!")
	private Integer cdSequencia;
	
	@Column(name = "cd_tipo")
	// @NotNull(message = "Status campo obrigatório!")
	private Integer cdTipo;
	
	@Column(name = "dt_data_solicitacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataSolicitacao;
	
	@Column(name = "tx_prestador")
	private String txPrestador;

	@Column(name = "dt_sla_tratativa")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtSlaTratativa;
	
	@Column(name = "dt_prim_contato")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtPrimContato;

	@Column(name = "vl_sla_solicitado")
	private String vlSlaSolicitado;
	
	@Column(name = "vl_sla_atualizado")
	private String vlSlaAtualizado;
	
	@Column(name = "vl_sla_prim_contato")
	private String vlSlaPrimContato;
	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txCarteira))
			txCarteira = txCarteira.toUpperCase();
		if (!Validator.isBlankOrNull(txCodSegurado))
			txCodSegurado = txCodSegurado.toUpperCase();
		if (!Validator.isBlankOrNull(txNomeSegurado))
			txNomeSegurado = txNomeSegurado.toUpperCase();
		if (!Validator.isBlankOrNull(txProdSaude))
			txProdSaude = txProdSaude.toUpperCase();
		if (!Validator.isBlankOrNull(txSexo))
			txSexo = txSexo.toUpperCase();
		if (!Validator.isBlankOrNull(txTelCelular))
			txTelCelular = txTelCelular.toUpperCase();
		if (!Validator.isBlankOrNull(txTelCobranca))
			txTelCobranca = txTelCobranca.toUpperCase();
		if (!Validator.isBlankOrNull(txTelComercial))
			txTelComercial = txTelComercial.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato))
			txTelContato = txTelContato.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato2))
			txTelContato2 = txTelContato2.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato3))
			txTelContato3 = txTelContato3.toUpperCase();
		if (!Validator.isBlankOrNull(txTelPrestador))
			txTelPrestador = txTelPrestador.toUpperCase();
		if (!Validator.isBlankOrNull(txTelResencial))
			txTelResencial = txTelResencial.toUpperCase();
		if (!Validator.isBlankOrNull(txCpfSegurado))
			txCpfSegurado = txCpfSegurado.toUpperCase();
	}

	public Integer getCdPaciente() {
		return cdPaciente;
	}

	public void setCdPaciente(Integer cdPaciente) {
		this.cdPaciente = cdPaciente;
	}

	public String getTxCarteira() {
		return txCarteira;
	}

	public void setTxCarteira(String txCarteira) {
		this.txCarteira = txCarteira;
	}

	public String getTxCodSegurado() {
		return txCodSegurado;
	}

	public void setTxCodSegurado(String txCodSegurado) {
		this.txCodSegurado = txCodSegurado;
	}

	public Date getDtDataNascimento() {
		return dtDataNascimento;
	}

	public void setDtDataNascimento(Date dtDataNascimento) {
		this.dtDataNascimento = dtDataNascimento;
	}

	public String getTxNomeSegurado() {
		return txNomeSegurado;
	}

	public void setTxNomeSegurado(String txNomeSegurado) {
		this.txNomeSegurado = txNomeSegurado;
	}

	public String getTxProdSaude() {
		return txProdSaude;
	}

	public void setTxProdSaude(String txProdSaude) {
		this.txProdSaude = txProdSaude;
	}

	public String getTxSexo() {
		return txSexo;
	}

	public void setTxSexo(String txSexo) {
		this.txSexo = txSexo;
	}

	public String getTxTelCelular() {
		return txTelCelular;
	}

	public void setTxTelCelular(String txTelCelular) {
		this.txTelCelular = txTelCelular;
	}

	public String getTxTelCobranca() {
		return txTelCobranca;
	}

	public void setTxTelCobranca(String txTelCobranca) {
		this.txTelCobranca = txTelCobranca;
	}

	public String getTxTelComercial() {
		return txTelComercial;
	}

	public void setTxTelComercial(String txTelComercial) {
		this.txTelComercial = txTelComercial;
	}

	public String getTxTelContato() {
		return txTelContato;
	}

	public void setTxTelContato(String txTelContato) {
		this.txTelContato = txTelContato;
	}

	public String getTxTelContato2() {
		return txTelContato2;
	}

	public void setTxTelContato2(String txTelContato2) {
		this.txTelContato2 = txTelContato2;
	}

	public String getTxTelContato3() {
		return txTelContato3;
	}

	public void setTxTelContato3(String txTelContato3) {
		this.txTelContato3 = txTelContato3;
	}

	public String getTxTelPrestador() {
		return txTelPrestador;
	}

	public void setTxTelPrestador(String txTelPrestador) {
		this.txTelPrestador = txTelPrestador;
	}

	public String getTxTelResencial() {
		return txTelResencial;
	}

	public void setTxTelResencial(String txTelResencial) {
		this.txTelResencial = txTelResencial;
	}

	public String getTxCpfSegurado() {
		return txCpfSegurado;
	}

	public void setTxCpfSegurado(String txCpfSegurado) {
		this.txCpfSegurado = txCpfSegurado;
	}

	

	public Integer getCdStatusPaciente() {
		return cdStatusPaciente;
	}

	public void setCdStatusPaciente(Integer cdStatusPaciente) {
		this.cdStatusPaciente = cdStatusPaciente;
	}

	public Date getDtUltimaAtualizacao() {
		return dtUltimaAtualizacao;
	}

	public void setDtUltimaAtualizacao(Date dtUltimaAtualizacao) {
		this.dtUltimaAtualizacao = dtUltimaAtualizacao;
	}



	public Date getDtNovoContato() {
		return dtNovoContato;
	}

	public void setDtNovoContato(Date dtNovoContato) {
		this.dtNovoContato = dtNovoContato;
	}

	public Integer getCdStatusTratativa() {
		return cdStatusTratativa;
	}

	public void setCdStatusTratativa(Integer cdStatusTratativa) {
		this.cdStatusTratativa = cdStatusTratativa;
	}

	public Integer getCdSequencia() {
		return cdSequencia;
	}

	public void setCdSequencia(Integer cdSequencia) {
		this.cdSequencia = cdSequencia;
	}

	public Integer getCdTipo() {
		return cdTipo;
	}

	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	public Date getDtDataSolicitacao() {
		return dtDataSolicitacao;
	}

	public void setDtDataSolicitacao(Date dtDataSolicitacao) {
		this.dtDataSolicitacao = dtDataSolicitacao;
	}

	public String getTxPrestador() {
		return txPrestador;
	}

	public void setTxPrestador(String txPrestador) {
		this.txPrestador = txPrestador;
	}

	public Date getDtSlaTratativa() {
		return dtSlaTratativa;
	}

	public void setDtSlaTratativa(Date dtSlaTratativa) {
		this.dtSlaTratativa = dtSlaTratativa;
	}

	public String getVlSlaSolicitado() {
		return vlSlaSolicitado;
	}

	public void setVlSlaSolicitado(String vlSlaSolicitado) {
		this.vlSlaSolicitado = vlSlaSolicitado;
	}

	public String getVlSlaAtualizado() {
		return vlSlaAtualizado;
	}

	public void setVlSlaAtualizado(String vlSlaAtualizado) {
		this.vlSlaAtualizado = vlSlaAtualizado;
	}

	public String getVlSlaPrimContato() {
		return vlSlaPrimContato;
	}

	public void setVlSlaPrimContato(String vlSlaPrimContato) {
		this.vlSlaPrimContato = vlSlaPrimContato;
	}

	public Date getDtPrimContato() {
		return dtPrimContato;
	}

	public void setDtPrimContato(Date dtPrimContato) {
		this.dtPrimContato = dtPrimContato;
	}


	
	
	
}
