package br.com.gohosp.dao.tratativas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaCapaObj;



public interface TabRetornoConsultaTratativaCapaRepository extends JpaRepository<TabRetornoConsultaTratativaCapaObj, Integer> {

	TabRetornoConsultaTratativaCapaObj findByTxTratativaId(String txTratativaId);

}