package br.com.gohosp.dao;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.util.logErro.JTPLogErroBean;

@Service
public class TransactionUtilBean {
	
	 @Autowired
	 private JTPLogErroBean logErroBean;
	
	 @PersistenceContext
	 private EntityManager entityManager;
	
	 /*
	@SuppressWarnings("unchecked")
	public List<VwTabImportacaoObj> teste() {
		 
		 //entityManager.createQuery("").getResultList();
		 return entityManager.createQuery("select txNref, txSref from VwTabImportacaoObj order by txNref").getResultList();
		 
	 }*/


	@Transactional
	public Integer queryInsertUpdate(String queryText) {
		
	  try {	
	    Query query = entityManager.createQuery(queryText);
	    int rowsUpdated = query.executeUpdate();
	    //System.out.println("entities Updated: " + rowsUpdated);
	    //entityManager.close();
	    return rowsUpdated;
		 
	  }catch (Exception ex) {
		  logErroBean.addLog(getClass().getName(),ex.getMessage(),ex); 
		  return 0;
	  }
	
	 }
	
	@Transactional
	public Integer queryInsertUpdate(String queryText, HashMap hashParameters) {
		
	  try {	
	    Query query = entityManager.createQuery(queryText);
	    
	    Set set = hashParameters.entrySet();
		Iterator i = set.iterator();
		
		while(i.hasNext()){
			Map.Entry entrada = (Map.Entry)i.next();
		    query.setParameter(entrada.getKey().toString(), entrada.getValue()!=null?entrada.getValue().toString():"");
		}
	    
	    int rowsUpdated = query.executeUpdate();
	    System.out.println("entities Updated: " + rowsUpdated);
	    //entityManager.close();
	    return rowsUpdated;
	    
	  }catch (Exception ex) {
		logErroBean.addLog(getClass().getName(),ex.getMessage(),ex); 
		return 0;
	  }

	}

	
	public List querySelect(String queryText, HashMap hashParameters) {

		try {	
		    Query query = entityManager.createQuery(queryText);
		    
		    Set set = hashParameters.entrySet();
			Iterator i = set.iterator();
			
			while(i.hasNext()){
				Map.Entry entrada = (Map.Entry)i.next();
			    query.setParameter(entrada.getKey().toString(), entrada.getValue()!=null?entrada.getValue().toString():"");
			}
		    
		    List listResult = query.getResultList();
		    //entityManager.close();
		    return listResult;
		    
		  }catch (Exception ex) {
			logErroBean.addLog(getClass().getName(),ex.getMessage(),ex); 
			return null;
		  }
		}

	
	public List querySelect(String queryText) {
		
		  try {	
		    Query query = entityManager.createQuery(queryText);
		    
		    List listResult = query.getResultList();
		    //entityManager.close();
		    return listResult;
		    
		  }catch (Exception ex) {
			logErroBean.addLog(getClass().getName(),ex.getMessage(),ex); 
			return null;
			
		  }

		}
	
	
	public List queryNativeSelect(String queryText) {
		
		  try {	
		    Query query = entityManager.createNativeQuery(queryText);
		    
		    List listResult = query.getResultList();
		    //entityManager.close();
		    return listResult;
		    
		  }catch (Exception ex) {
			logErroBean.addLog(getClass().getName(),ex.getMessage(),ex); 
			return null;
			
		  }

		}

	public List<?> querySelect(String queryText, Class<?> objClass) {
		
		  try {	
		    TypedQuery<?> query = entityManager.createQuery(queryText, objClass);
		    List<?> listResult = query.getResultList();
		    //entityManager.close();
		    return listResult;
		    
		  }catch (Exception ex) {
			logErroBean.addLog(getClass().getName(),ex.getMessage(),ex); 
			return null;
			
		  }

		}
	
}
