package br.com.gohosp.dao.faturamento;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "tab_precos", schema = Constants.SCHEMA)
public class TabPrecosObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_preco")
	//@NotNull(message = "Preco campo obrigatório!")
	private Integer cdPreco;

	@Column(name = "tx_descricao")
	@NotEmpty(message = "Descrição campo obrigatório!")
	@Size(max = 45, message = "Descricao tamanho máximo de 45 caracteres")
	private String txDescricao;

	@Column(name = "vl_de")
	@NotNull(message = "De campo obrigatório!")
	private Integer vlDe;

	@Column(name = "vl_ate")
	@NotNull(message = "Até campo obrigatório!")
	private Integer vlAte;

	@Column(name = "vl_valor")
	@NotNull(message = "Valor campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlValor;

	@Column(name = "cd_tipo")
	@NotNull(message = "Tipo campo obrigatório!")
	private Integer cdTipo;
	
	@Column(name = "tx_tipo_operacao")
	@NotNull(message = "Tipo da Operação campo obrigatório!")
	private String txTipoOperacao;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txDescricao))
			txDescricao = txDescricao.toUpperCase();
	}

	public Integer getCdPreco() {
		return cdPreco;
	}

	public void setCdPreco(Integer cdPreco) {
		this.cdPreco = cdPreco;
	}

	public String getTxDescricao() {
		return txDescricao;
	}

	public void setTxDescricao(String txDescricao) {
		this.txDescricao = txDescricao;
	}

	public Integer getVlDe() {
		return vlDe;
	}

	public void setVlDe(Integer vlDe) {
		this.vlDe = vlDe;
	}

	public Integer getVlAte() {
		return vlAte;
	}

	public void setVlAte(Integer vlAte) {
		this.vlAte = vlAte;
	}

	public BigDecimal getVlValor() {
		return vlValor;
	}

	public void setVlValor(BigDecimal vlValor) {
		this.vlValor = vlValor;
	}

	public Integer getCdTipo() {
		return cdTipo;
	}

	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	public String getTxTipoOperacao() {
		return txTipoOperacao;
	}

	public void setTxTipoOperacao(String txTipoOperacao) {
		this.txTipoOperacao = txTipoOperacao;
	}

	
	

}
