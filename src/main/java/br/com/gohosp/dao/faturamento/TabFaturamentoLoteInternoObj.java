package br.com.gohosp.dao.faturamento;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.gohosp.Constants;

@Entity
@Table(name = "tab_faturamento_lote_interno", schema = Constants.SCHEMA)
public class TabFaturamentoLoteInternoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_lote_interno")
	// @NotNull(message = "LoteInterno campo obrigatório!")
	private Integer cdLoteInterno;

	@Column(name = "dt_criacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCriacao;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

	public Integer getCdLoteInterno() {
		return cdLoteInterno;
	}

	public void setCdLoteInterno(Integer cdLoteInterno) {
		this.cdLoteInterno = cdLoteInterno;
	}

	public Date getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

}
