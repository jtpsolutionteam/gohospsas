package br.com.gohosp.dao.faturamento;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "vw_tab_faturamento", schema = Constants.SCHEMA)
public class VwTabFaturamentoObj {

	@Id
	@GeneratedValue
	@Column(name = "tx_tratativa_id")
	// @NotEmpty(message = "TratativaId campo obrigatório!")
	@Size(max = 30, message = "TratativaId tamanho máximo de 30 caracteres")
	private String txTratativaId;

	@Column(name = "tx_nome_segurado")
	// @NotEmpty(message = "NomeSegurado campo obrigatório!")
	@Size(max = 45, message = "NomeSegurado tamanho máximo de 45 caracteres")
	private String txNomeSegurado;

	@Column(name = "tx_cpf_segurado")
	// @NotEmpty(message = "CpfSegurado campo obrigatório!")
	@Size(max = 20, message = "CpfSegurado tamanho máximo de 20 caracteres")
	private String txCpfSegurado;
	
	@Column(name = "tx_num_vpp")
	private String txNumVpp;
	
	@Column(name = "tx_cod_segurado")
	private String txCodSegurado;

	@Column(name = "dt_data")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtData;

	@Column(name = "vl_total")
	// @NotNull(message = "Total campo obrigatório!")
	private Integer vlTotal;
	
	@Transient	
	private Integer dtStatusFaturamento;
	
	

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTratativaId))
			txTratativaId = txTratativaId.toUpperCase();
		if (!Validator.isBlankOrNull(txNomeSegurado))
			txNomeSegurado = txNomeSegurado.toUpperCase();
		if (!Validator.isBlankOrNull(txCpfSegurado))
			txCpfSegurado = txCpfSegurado.toUpperCase();
	}

	public String getTxTratativaId() {
		return txTratativaId;
	}

	public void setTxTratativaId(String txTratativaId) {
		this.txTratativaId = txTratativaId;
	}

	public String getTxNomeSegurado() {
		return txNomeSegurado;
	}

	public void setTxNomeSegurado(String txNomeSegurado) {
		this.txNomeSegurado = txNomeSegurado;
	}

	public String getTxCpfSegurado() {
		return txCpfSegurado;
	}

	public void setTxCpfSegurado(String txCpfSegurado) {
		this.txCpfSegurado = txCpfSegurado;
	}

	public Date getDtData() {
		return dtData;
	}

	public void setDtData(Date dtData) {
		this.dtData = dtData;
	}

	public Integer getVlTotal() {
		return vlTotal;
	}

	public void setVlTotal(Integer vlTotal) {
		this.vlTotal = vlTotal;
	}

	public String getTxNumVpp() {
		return txNumVpp;
	}

	public void setTxNumVpp(String txNumVpp) {
		this.txNumVpp = txNumVpp;
	}

	public String getTxCodSegurado() {
		return txCodSegurado;
	}

	public void setTxCodSegurado(String txCodSegurado) {
		this.txCodSegurado = txCodSegurado;
	}

	public Integer getDtStatusFaturamento() {
		return dtStatusFaturamento;
	}

	public void setDtStatusFaturamento(Integer dtStatusFaturamento) {
		this.dtStatusFaturamento = dtStatusFaturamento;
	}

}
