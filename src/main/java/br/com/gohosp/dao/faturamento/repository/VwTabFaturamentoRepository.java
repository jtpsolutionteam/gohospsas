package br.com.gohosp.dao.faturamento.repository;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.faturamento.VwTabFaturamentoObj;

public interface VwTabFaturamentoRepository extends JpaRepository<VwTabFaturamentoObj, Integer> {

	@Query("select t from VwTabFaturamentoObj t where month(t.dtData) = month(?1) order by dtData")
	List<VwTabFaturamentoObj> findByDtFaturamentoQuery(Date dtFaturamento);

}