package br.com.gohosp.dao.faturamento;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "tab_faturamento", schema = Constants.SCHEMA)
public class TabFaturamentoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_faturamento")
	// @NotNull(message = "Faturamento campo obrigatório!")
	private Integer cdFaturamento;

	@Column(name = "cd_tratativa")
	// @NotNull(message = "Tratativa campo obrigatório!")
	private Integer cdTratativa;

	@Column(name = "dt_enviado")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtEnviado;

	@Column(name = "dt_autorizado")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtAutorizado;

	@Column(name = "dt_validado")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtValidado;

	@Column(name = "dt_faturado")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtFaturado;

	@Column(name = "tx_lote")
	// @NotEmpty(message = "Lote campo obrigatório!")
	@Size(max = 10, message = "Lote tamanho máximo de 10 caracteres")
	private String txLote;

	@Column(name = "cd_tipo")
	// @NotNull(message = "Tipo campo obrigatório!")
	private Integer cdTipo;
	
	@Column(name = "cd_tabela_preco")
	private Integer cdTabelaPreco;

	
	@Column(name = "vl_valor")
	@NotNull(message = "Valor campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlValor;
	
	@Column(name = "tx_senha_autorizacao")
	private String txSenhaAutorizacao;
	
	@Column(name = "cd_lote_interno")
	private Integer cdLoteInterno;
	

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txLote))
			txLote = txLote.toUpperCase();
	}

	public Integer getCdFaturamento() {
		return cdFaturamento;
	}

	public void setCdFaturamento(Integer cdFaturamento) {
		this.cdFaturamento = cdFaturamento;
	}

	public Integer getCdTratativa() {
		return cdTratativa;
	}

	public void setCdTratativa(Integer cdTratativa) {
		this.cdTratativa = cdTratativa;
	}

	public Date getDtEnviado() {
		return dtEnviado;
	}

	public void setDtEnviado(Date dtEnviado) {
		this.dtEnviado = dtEnviado;
	}

	public Date getDtAutorizado() {
		return dtAutorizado;
	}

	public void setDtAutorizado(Date dtAutorizado) {
		this.dtAutorizado = dtAutorizado;
	}

	public Date getDtValidado() {
		return dtValidado;
	}

	public void setDtValidado(Date dtValidado) {
		this.dtValidado = dtValidado;
	}

	public Date getDtFaturado() {
		return dtFaturado;
	}

	public void setDtFaturado(Date dtFaturado) {
		this.dtFaturado = dtFaturado;
	}

	public String getTxLote() {
		return txLote;
	}

	public void setTxLote(String txLote) {
		this.txLote = txLote;
	}

	public Integer getCdTipo() {
		return cdTipo;
	}

	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	

	public Integer getCdTabelaPreco() {
		return cdTabelaPreco;
	}

	public void setCdTabelaPreco(Integer cdTabelaPreco) {
		this.cdTabelaPreco = cdTabelaPreco;
	}

	public BigDecimal getVlValor() {
		return vlValor;
	}

	public void setVlValor(BigDecimal vlValor) {
		this.vlValor = vlValor;
	}

	public String getTxSenhaAutorizacao() {
		return txSenhaAutorizacao;
	}

	public void setTxSenhaAutorizacao(String txSenhaAutorizacao) {
		this.txSenhaAutorizacao = txSenhaAutorizacao;
	}

	public Integer getCdLoteInterno() {
		return cdLoteInterno;
	}

	public void setCdLoteInterno(Integer cdLoteInterno) {
		this.cdLoteInterno = cdLoteInterno;
	}

	
	

}
