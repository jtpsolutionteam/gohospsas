package br.com.gohosp.dao.faturamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.faturamento.TabFaturamentoLoteInternoObj;



public interface TabFaturamentoLoteInternoRepository extends JpaRepository<TabFaturamentoLoteInternoObj, Integer> {



}