package br.com.gohosp.dao.faturamento;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "vw_tab_faturamento_v3", schema = Constants.SCHEMA)
public class VwTabFaturamentoV3Obj {

	@Id
	@GeneratedValue
	@Column(name = "cd_faturamento") 
	//@NotNull(message = "Faturamento campo obrigatório!")
	private Integer cdFaturamento;

	@Column(name = "cd_tratativa")
	// @NotNull(message = "Tratativa campo obrigatório!")
	private Integer cdTratativa;
	
	
	@Column(name = "tx_tratativa_id")
	// @NotEmpty(message = "TratativaId campo obrigatório!")
	@Size(max = 30, message = "TratativaId tamanho máximo de 30 caracteres")
	private String txTratativaId;

	@Column(name = "tx_assunto")
	// @NotEmpty(message = "Assunto campo obrigatório!")
	@Size(max = 255, message = "Assunto tamanho máximo de 255 caracteres")
	private String txAssunto;

	@Column(name = "tx_carteira")
	// @NotEmpty(message = "Carteira campo obrigatório!")
	@Size(max = 45, message = "Carteira tamanho máximo de 45 caracteres")
	private String txCarteira;

	@Column(name = "tx_cod_segurado")
	// @NotEmpty(message = "CodSegurado campo obrigatório!")
	@Size(max = 45, message = "CodSegurado tamanho máximo de 45 caracteres")
	private String txCodSegurado;

	@Column(name = "dt_data_incio_vpp")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataIncioVpp;

	@Column(name = "dt_data_nascimento")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataNascimento;

	@Column(name = "dt_data_solicitacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataSolicitacao;

	@Column(name = "tx_empresa")
	// @NotEmpty(message = "Empresa campo obrigatório!")
	@Size(max = 100, message = "Empresa tamanho máximo de 100 caracteres")
	private String txEmpresa;

	@Column(name = "tx_estado")
	// @NotEmpty(message = "Estado campo obrigatório!")
	@Size(max = 45, message = "Estado tamanho máximo de 45 caracteres")
	private String txEstado;

	@Column(name = "tx_mensagem")
	// @NotEmpty(message = "Mensagem campo obrigatório!")
	@Size(max = 16777215, message = "Mensagem tamanho máximo de 16777215 caracteres")
	private String txMensagem;

	@Column(name = "tx_nome_segurado")
	// @NotEmpty(message = "NomeSegurado campo obrigatório!")
	@Size(max = 45, message = "NomeSegurado tamanho máximo de 45 caracteres")
	private String txNomeSegurado;

	@Column(name = "tx_num_vpp")
	// @NotEmpty(message = "NumVpp campo obrigatório!")
	@Size(max = 45, message = "NumVpp tamanho máximo de 45 caracteres")
	private String txNumVpp;

	@Column(name = "tx_prestador")
	// @NotEmpty(message = "Prestador campo obrigatório!")
	@Size(max = 45, message = "Prestador tamanho máximo de 45 caracteres")
	private String txPrestador;

	@Column(name = "tx_prod_saude")
	// @NotEmpty(message = "ProdSaude campo obrigatório!")
	@Size(max = 45, message = "ProdSaude tamanho máximo de 45 caracteres")
	private String txProdSaude;

	@Column(name = "tx_sexo")
	// @NotEmpty(message = "Sexo campo obrigatório!")
	@Size(max = 45, message = "Sexo tamanho máximo de 45 caracteres")
	private String txSexo;

	@Column(name = "tx_sla_tratativa")
	// @NotEmpty(message = "SlaTratativa campo obrigatório!")
	@Size(max = 45, message = "SlaTratativa tamanho máximo de 45 caracteres")
	private String txSlaTratativa;

	@Column(name = "tx_tel_celular")
	// @NotEmpty(message = "TelCelular campo obrigatório!")
	@Size(max = 45, message = "TelCelular tamanho máximo de 45 caracteres")
	private String txTelCelular;

	@Column(name = "tx_tel_cobranca")
	// @NotEmpty(message = "TelCobranca campo obrigatório!")
	@Size(max = 45, message = "TelCobranca tamanho máximo de 45 caracteres")
	private String txTelCobranca;

	@Column(name = "tx_tel_comercial")
	// @NotEmpty(message = "TelComercial campo obrigatório!")
	@Size(max = 45, message = "TelComercial tamanho máximo de 45 caracteres")
	private String txTelComercial;

	@Column(name = "tx_tel_contato")
	// @NotEmpty(message = "TelContato campo obrigatório!")
	@Size(max = 45, message = "TelContato tamanho máximo de 45 caracteres")
	private String txTelContato;

	@Column(name = "tx_tel_contato2")
	// @NotEmpty(message = "TelContato2 campo obrigatório!")
	@Size(max = 45, message = "TelContato2 tamanho máximo de 45 caracteres")
	private String txTelContato2;

	@Column(name = "tx_tel_contato3")
	// @NotEmpty(message = "TelContato3 campo obrigatório!")
	@Size(max = 45, message = "TelContato3 tamanho máximo de 45 caracteres")
	private String txTelContato3;

	@Column(name = "tx_tel_prestador")
	// @NotEmpty(message = "TelPrestador campo obrigatório!")
	@Size(max = 45, message = "TelPrestador tamanho máximo de 45 caracteres")
	private String txTelPrestador;

	@Column(name = "tx_tel_resencial")
	// @NotEmpty(message = "TelResencial campo obrigatório!")
	@Size(max = 45, message = "TelResencial tamanho máximo de 45 caracteres")
	private String txTelResencial;

	@Column(name = "tx_cpf_segurado")
	// @NotEmpty(message = "CpfSegurado campo obrigatório!")
	@Size(max = 20, message = "CpfSegurado tamanho máximo de 20 caracteres")
	private String txCpfSegurado;

	@Column(name = "cd_status_tratativa")
	// @NotNull(message = "StatusTratativa campo obrigatório!")
	private Integer cdStatusTratativa;

	@Column(name = "dt_novo_contato")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtNovoContato;

	@Column(name = "cd_usuario")
	// @NotNull(message = "Usuario campo obrigatório!")
	private Integer cdUsuario;

	@Column(name = "vl_qtde_contatos")
	private Integer vlQtdeContatos;
	
	 
	 
	@Column(name = "dt_enviado") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtEnviado; 
	 
	@Column(name = "dt_autorizado") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtAutorizado; 
	 
	@Column(name = "dt_validado") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtValidado; 
	 
	@Column(name = "dt_faturado") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtFaturado; 
	 
	@Column(name = "tx_lote") 
	//@NotEmpty(message = "Lote campo obrigatório!")
	@Size(max = 10, message = "Lote tamanho máximo de 10 caracteres") 
	private String txLote; 
	 
	@Column(name = "cd_tipo") 
	//@NotNull(message = "Tipo campo obrigatório!")
	private Integer cdTipo; 
	
	@Column(name = "cd_tabela_preco")
	private Integer cdTabelaPreco;
	
	@Column(name = "vl_valor")
	@NotNull(message = "Valor campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlValor;
	
	@Column(name = "tx_tabela_preco")
	private String txTabelaPreco;
	
	@Transient	
	private String dtStatusFaturamento;
	
	@Transient
	@Column(name = "dt_data")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtData;
	
	@Column(name = "dt_ultimo_envio")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtUltimoEnvio;
	
	@Column(name = "tx_senha_autorizacao")
	private String txSenhaAutorizacao;
	
	@Column(name = "dt_prim_contato")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtPrimContato;
	
	@Column(name = "cd_lote_interno")
	private Integer cdLoteInterno;
	
	@Column(name = "tx_tipo_operacao")
	private String txTipoOperacao;


	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTratativaId))
			txTratativaId = txTratativaId.toUpperCase();
		if (!Validator.isBlankOrNull(txAssunto))
			txAssunto = txAssunto.toUpperCase();
		if (!Validator.isBlankOrNull(txCarteira))
			txCarteira = txCarteira.toUpperCase();
		if (!Validator.isBlankOrNull(txCodSegurado))
			txCodSegurado = txCodSegurado.toUpperCase();
		if (!Validator.isBlankOrNull(txEmpresa))
			txEmpresa = txEmpresa.toUpperCase();
		if (!Validator.isBlankOrNull(txEstado))
			txEstado = txEstado.toUpperCase();
		if (!Validator.isBlankOrNull(txMensagem))
			txMensagem = txMensagem.toUpperCase();
		if (!Validator.isBlankOrNull(txNomeSegurado))
			txNomeSegurado = txNomeSegurado.toUpperCase();
		if (!Validator.isBlankOrNull(txNumVpp))
			txNumVpp = txNumVpp.toUpperCase();
		if (!Validator.isBlankOrNull(txPrestador))
			txPrestador = txPrestador.toUpperCase();
		if (!Validator.isBlankOrNull(txProdSaude))
			txProdSaude = txProdSaude.toUpperCase();
		if (!Validator.isBlankOrNull(txSexo))
			txSexo = txSexo.toUpperCase();
		if (!Validator.isBlankOrNull(txSlaTratativa))
			txSlaTratativa = txSlaTratativa.toUpperCase();
		if (!Validator.isBlankOrNull(txTelCelular))
			txTelCelular = txTelCelular.toUpperCase();
		if (!Validator.isBlankOrNull(txTelCobranca))
			txTelCobranca = txTelCobranca.toUpperCase();
		if (!Validator.isBlankOrNull(txTelComercial))
			txTelComercial = txTelComercial.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato))
			txTelContato = txTelContato.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato2))
			txTelContato2 = txTelContato2.toUpperCase();
		if (!Validator.isBlankOrNull(txTelContato3))
			txTelContato3 = txTelContato3.toUpperCase();
		if (!Validator.isBlankOrNull(txTelPrestador))
			txTelPrestador = txTelPrestador.toUpperCase();
		if (!Validator.isBlankOrNull(txTelResencial))
			txTelResencial = txTelResencial.toUpperCase();
		if (!Validator.isBlankOrNull(txCpfSegurado))
			txCpfSegurado = txCpfSegurado.toUpperCase();
	}


	public Integer getCdTratativa() {
		return cdTratativa;
	}


	public void setCdTratativa(Integer cdTratativa) {
		this.cdTratativa = cdTratativa;
	}


	public String getTxTratativaId() {
		return txTratativaId;
	}


	public void setTxTratativaId(String txTratativaId) {
		this.txTratativaId = txTratativaId;
	}


	public String getTxAssunto() {
		return txAssunto;
	}


	public void setTxAssunto(String txAssunto) {
		this.txAssunto = txAssunto;
	}


	public String getTxCarteira() {
		return txCarteira;
	}


	public void setTxCarteira(String txCarteira) {
		this.txCarteira = txCarteira;
	}


	public String getTxCodSegurado() {
		return txCodSegurado;
	}


	public void setTxCodSegurado(String txCodSegurado) {
		this.txCodSegurado = txCodSegurado;
	}


	public Date getDtDataIncioVpp() {
		return dtDataIncioVpp;
	}


	public void setDtDataIncioVpp(Date dtDataIncioVpp) {
		this.dtDataIncioVpp = dtDataIncioVpp;
	}


	public Date getDtDataNascimento() {
		return dtDataNascimento;
	}


	public void setDtDataNascimento(Date dtDataNascimento) {
		this.dtDataNascimento = dtDataNascimento;
	}


	public Date getDtDataSolicitacao() {
		return dtDataSolicitacao;
	}


	public void setDtDataSolicitacao(Date dtDataSolicitacao) {
		this.dtDataSolicitacao = dtDataSolicitacao;
	}


	public String getTxEmpresa() {
		return txEmpresa;
	}


	public void setTxEmpresa(String txEmpresa) {
		this.txEmpresa = txEmpresa;
	}


	public String getTxEstado() {
		return txEstado;
	}


	public void setTxEstado(String txEstado) {
		this.txEstado = txEstado;
	}


	public String getTxMensagem() {
		return txMensagem;
	}


	public void setTxMensagem(String txMensagem) {
		this.txMensagem = txMensagem;
	}


	public String getTxNomeSegurado() {
		return txNomeSegurado;
	}


	public void setTxNomeSegurado(String txNomeSegurado) {
		this.txNomeSegurado = txNomeSegurado;
	}


	public String getTxNumVpp() {
		return txNumVpp;
	}


	public void setTxNumVpp(String txNumVpp) {
		this.txNumVpp = txNumVpp;
	}


	public String getTxPrestador() {
		return txPrestador;
	}


	public void setTxPrestador(String txPrestador) {
		this.txPrestador = txPrestador;
	}


	public String getTxProdSaude() {
		return txProdSaude;
	}


	public void setTxProdSaude(String txProdSaude) {
		this.txProdSaude = txProdSaude;
	}


	public String getTxSexo() {
		return txSexo;
	}


	public void setTxSexo(String txSexo) {
		this.txSexo = txSexo;
	}


	public String getTxSlaTratativa() {
		return txSlaTratativa;
	}


	public void setTxSlaTratativa(String txSlaTratativa) {
		this.txSlaTratativa = txSlaTratativa;
	}


	public String getTxTelCelular() {
		return txTelCelular;
	}


	public void setTxTelCelular(String txTelCelular) {
		this.txTelCelular = txTelCelular;
	}


	public String getTxTelCobranca() {
		return txTelCobranca;
	}


	public void setTxTelCobranca(String txTelCobranca) {
		this.txTelCobranca = txTelCobranca;
	}


	public String getTxTelComercial() {
		return txTelComercial;
	}


	public void setTxTelComercial(String txTelComercial) {
		this.txTelComercial = txTelComercial;
	}


	public String getTxTelContato() {
		return txTelContato;
	}


	public void setTxTelContato(String txTelContato) {
		this.txTelContato = txTelContato;
	}


	public String getTxTelContato2() {
		return txTelContato2;
	}


	public void setTxTelContato2(String txTelContato2) {
		this.txTelContato2 = txTelContato2;
	}


	public String getTxTelContato3() {
		return txTelContato3;
	}


	public void setTxTelContato3(String txTelContato3) {
		this.txTelContato3 = txTelContato3;
	}


	public String getTxTelPrestador() {
		return txTelPrestador;
	}


	public void setTxTelPrestador(String txTelPrestador) {
		this.txTelPrestador = txTelPrestador;
	}


	public String getTxTelResencial() {
		return txTelResencial;
	}


	public void setTxTelResencial(String txTelResencial) {
		this.txTelResencial = txTelResencial;
	}


	public String getTxCpfSegurado() {
		return txCpfSegurado;
	}


	public void setTxCpfSegurado(String txCpfSegurado) {
		this.txCpfSegurado = txCpfSegurado;
	}


	public Integer getCdStatusTratativa() {
		return cdStatusTratativa;
	}


	public void setCdStatusTratativa(Integer cdStatusTratativa) {
		this.cdStatusTratativa = cdStatusTratativa;
	}


	public Date getDtNovoContato() {
		return dtNovoContato;
	}


	public void setDtNovoContato(Date dtNovoContato) {
		this.dtNovoContato = dtNovoContato;
	}


	public Integer getCdUsuario() {
		return cdUsuario;
	}


	public void setCdUsuario(Integer cdUsuario) {
		this.cdUsuario = cdUsuario;
	}


	public Integer getVlQtdeContatos() {
		return vlQtdeContatos;
	}


	public void setVlQtdeContatos(Integer vlQtdeContatos) {
		this.vlQtdeContatos = vlQtdeContatos;
	}


	public Integer getCdFaturamento() {
		return cdFaturamento;
	}


	public void setCdFaturamento(Integer cdFaturamento) {
		this.cdFaturamento = cdFaturamento;
	}


	public Date getDtEnviado() {
		return dtEnviado;
	}


	public void setDtEnviado(Date dtEnviado) {
		this.dtEnviado = dtEnviado;
	}


	public Date getDtAutorizado() {
		return dtAutorizado;
	}


	public void setDtAutorizado(Date dtAutorizado) {
		this.dtAutorizado = dtAutorizado;
	}


	public Date getDtValidado() {
		return dtValidado;
	}


	public void setDtValidado(Date dtValidado) {
		this.dtValidado = dtValidado;
	}


	public Date getDtFaturado() {
		return dtFaturado;
	}


	public void setDtFaturado(Date dtFaturado) {
		this.dtFaturado = dtFaturado;
	}


	public String getTxLote() {
		return txLote;
	}


	public void setTxLote(String txLote) {
		this.txLote = txLote;
	}


	public Integer getCdTipo() {
		return cdTipo;
	}


	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}


	


	public Date getDtData() {
		return dtData;
	}


	public void setDtData(Date dtData) {
		this.dtData = dtData;
	}


	public String getDtStatusFaturamento() {
		return dtStatusFaturamento;
	}


	public void setDtStatusFaturamento(String dtStatusFaturamento) {
		this.dtStatusFaturamento = dtStatusFaturamento;
	}


	public Date getDtUltimoEnvio() {
		return dtUltimoEnvio;
	}


	public void setDtUltimoEnvio(Date dtUltimoEnvio) {
		this.dtUltimoEnvio = dtUltimoEnvio;
	}


	public Integer getCdTabelaPreco() {
		return cdTabelaPreco;
	}


	public void setCdTabelaPreco(Integer cdTabelaPreco) {
		this.cdTabelaPreco = cdTabelaPreco;
	}


	public BigDecimal getVlValor() {
		return vlValor;
	}


	public void setVlValor(BigDecimal vlValor) {
		this.vlValor = vlValor;
	}


	public String getTxSenhaAutorizacao() {
		return txSenhaAutorizacao;
	}


	public void setTxSenhaAutorizacao(String txSenhaAutorizacao) {
		this.txSenhaAutorizacao = txSenhaAutorizacao;
	}


	public String getTxTabelaPreco() {
		return txTabelaPreco;
	}


	public void setTxTabelaPreco(String txTabelaPreco) {
		this.txTabelaPreco = txTabelaPreco;
	}


	public Date getDtPrimContato() {
		return dtPrimContato;
	}


	public void setDtPrimContato(Date dtPrimContato) {
		this.dtPrimContato = dtPrimContato;
	}




	public String getTxTipoOperacao() {
		return txTipoOperacao;
	}


	public void setTxTipoOperacao(String txTipoOperacao) {
		this.txTipoOperacao = txTipoOperacao;
	}


	public Integer getCdLoteInterno() {
		return cdLoteInterno;
	}


	public void setCdLoteInterno(Integer cdLoteInterno) {
		this.cdLoteInterno = cdLoteInterno;
	}




}
