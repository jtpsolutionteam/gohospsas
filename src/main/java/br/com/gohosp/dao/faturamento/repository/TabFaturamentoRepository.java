package br.com.gohosp.dao.faturamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.faturamento.TabFaturamentoObj;



public interface TabFaturamentoRepository extends JpaRepository<TabFaturamentoObj, Integer> {

	TabFaturamentoObj findByCdTratativaAndCdTipo(Integer cdTratativa, Integer cdTipo);

	TabFaturamentoObj findByCdTratativaAndCdTabelaPreco(Integer cdTratativa, Integer cdTabelaPreco);
}