package br.com.gohosp.dao.faturamento.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.faturamento.VwTabFaturamentoV2Obj;



public interface VwTabFaturamentoV2Repository extends JpaRepository<VwTabFaturamentoV2Obj, Integer> {

	@Query("select t from VwTabFaturamentoV2Obj t where month(t.dtDataSolicitacao) = month(?1) and year(t.dtDataSolicitacao) = year(?1) and t.vlQtdeContatos > 0 order by dtDataSolicitacao")
	List<VwTabFaturamentoV2Obj> findByDtFaturamentoQuery(Date dtFaturamento);

	@Query("select t from VwTabFaturamentoV2Obj t where month(t.dtDataSolicitacao) = month(?1) and year(t.dtDataSolicitacao) = year(?1) and t.dtEnviado is null and t.vlQtdeContatos > 0 order by dtDataSolicitacao")
	List<VwTabFaturamentoV2Obj> findByPEnviarQuery(Date dtFaturamento);

	@Query("select t from VwTabFaturamentoV2Obj t where month(t.dtDataSolicitacao) = month(?1) and year(t.dtDataSolicitacao) = year(?1) and t.dtEnviado is not null and t.dtAutorizado is null and t.vlQtdeContatos > 0 order by dtDataSolicitacao")
	List<VwTabFaturamentoV2Obj> findByPAutorizarQuery(Date dtFaturamento);

	@Query("select t from VwTabFaturamentoV2Obj t where month(t.dtDataSolicitacao) = month(?1) and year(t.dtDataSolicitacao) = year(?1) and t.dtAutorizado is not null and t.dtValidado is null and t.vlQtdeContatos > 0 order by dtDataSolicitacao")
	List<VwTabFaturamentoV2Obj> findByPValidarQuery(Date dtFaturamento);

	@Query("select t from VwTabFaturamentoV2Obj t where month(t.dtDataSolicitacao) = month(?1) and year(t.dtDataSolicitacao) = year(?1) and t.dtValidado is not null and t.txSenhaAutorizacao is not null and t.dtFaturado is null and t.vlQtdeContatos > 0 and t.vlValor is not null order by dtDataSolicitacao")
	List<VwTabFaturamentoV2Obj> findByPFaturarQuery(Date dtFaturamento);

	@Query("select t from VwTabFaturamentoV2Obj t where month(t.dtDataSolicitacao) = month(?1) and year(t.dtDataSolicitacao) = year(?1) and t.dtFaturado is not null order by dtDataSolicitacao")
	List<VwTabFaturamentoV2Obj> findByFaturadoQuery(Date dtFaturamento);

	@Query("select t from VwTabFaturamentoV2Obj t where t.cdLoteInterno = ?1 and t.dtPrimContato is not null")
	List<VwTabFaturamentoV2Obj> findByLoteInternoQuery(Integer cdLoteInterno);
	
}