package br.com.gohosp.dao.faturamento.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gohosp.dao.faturamento.TabPrecosObj;



public interface TabPrecosRepository extends JpaRepository<TabPrecosObj, Integer> {

	@Query("select t from TabPrecosObj t where vlDe >= 1 and vlDe <= ?1  ")
	List<TabPrecosObj> findByPrecosQuery(Integer vlDias);

}