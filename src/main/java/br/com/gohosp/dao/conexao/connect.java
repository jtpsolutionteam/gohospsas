package br.com.gohosp.dao.conexao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.gohosp.Constants;

public class connect implements Serializable {
	  
public static Connection getConnectionSimpleMySql() {
	 Connection conn = null;
	 
	 try { 
	  String driver = "com.mysql.jdbc.Driver";
	    String url = "jdbc:mysql://vip-gohospsas:3306/gohospsas?autoReconnect=true";
	    String username = "gohospsas";
	    String password = "gohosp#$2019";

	    Class.forName(driver);
	    conn = DriverManager.getConnection(url, username, password);
	  }
	  catch (SQLException e) {

	    e.printStackTrace();
	  }
	  catch (Exception e) {
		  e.printStackTrace();
	  }
		    
		    return conn;
   } 

public ResultSet conexao_mysql_resultSet(Connection conn, String StrSql, String State){
    ResultSet rsDados_oracle = null;		 		  
	  
	try{
	  PreparedStatement ptConsulta_oracle = conn.prepareStatement(StrSql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
          
	  if (Constants.LOG_SHOW)
	    System.out.println(StrSql);
	      
	  if (State.equals("Select")){
	    rsDados_oracle = ptConsulta_oracle.executeQuery();
	  }else if (State.equals("Update")){
	    ptConsulta_oracle.executeUpdate();
	  }		      
	}catch (Exception e){
		e.printStackTrace();
	}
	  
    return rsDados_oracle;
  }	  

public static Connection getConnectionSimpleOracle() {
	 Connection conn = null;
	 
	 try { 
	  String driver = "oracle.jdbc.driver.OracleDriver";
	  
	    //String url = "jdbc:oracle:thin:@10.10.90.21:1521:FIORDEH";
	    String url = "jdbc:oracle:thin:@fiorde.cb5e502rvknf.sa-east-1.rds.amazonaws.com:1521:FIORDE";
	    String username = "fiorde";
	    String password = "frdadmin2008";

	    Class.forName(driver);
	    conn = DriverManager.getConnection(url, username, password);
	  }
	  catch (SQLException e) {
	     e.printStackTrace();
	  }
	  catch (Exception e) {
		  e.printStackTrace();
	  }
		    
	    return conn;
  } 
}