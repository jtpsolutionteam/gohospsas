package br.com.gohosp.dao.util.log.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.util.log.TabLogObj;




public interface TabLogRepository extends JpaRepository<TabLogObj, Integer> {

	List<TabLogObj> findByTxServiceAndTxCampoAndTxRefOrderByDtDataDesc(String txService, String txCampo, String txRef);
	
	
	
}
