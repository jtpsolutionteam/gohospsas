package br.com.gohosp.dao.util.logerro;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.gohosp.Constants;

@Entity
@Table(name = "tab_log_erro", schema = Constants.SCHEMA)
public class TabLogErroObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_log_erro")
	// @NotNull(message = "LogErro campo obrigatório!")
	private Integer cdLogErro;

	@Column(name = "dt_log")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtLog;

	@Column(name = "tx_servico")
	// @NotEmpty(message = "Servico campo obrigatório!")
	@Size(max = 200, message = "Servico tamanho máximo de 200 caracteres")
	private String txServico;

	@Column(name = "tx_mensagem")
	// @NotEmpty(message = "Mensagem campo obrigatório!")
	@Size(max = 1000, message = "Mensagem tamanho máximo de 1000 caracteres")
	private String txMensagem;

	@Column(name = "tx_erro")
	// @NotEmpty(message = "Erro campo obrigatório!")
	@Size(max = 2147483647, message = "Erro tamanho máximo de 2147483647 caracteres")
	private String txErro;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}

	public Integer getCdLogErro() {
		return cdLogErro;
	}

	public void setCdLogErro(Integer cdLogErro) {
		this.cdLogErro = cdLogErro;
	}

	public Date getDtLog() {
		return dtLog;
	}

	public void setDtLog(Date dtLog) {
		this.dtLog = dtLog;
	}

	public String getTxServico() {
		return txServico;
	}

	public void setTxServico(String txServico) {
		this.txServico = txServico;
	}

	public String getTxMensagem() {
		return txMensagem;
	}

	public void setTxMensagem(String txMensagem) {
		this.txMensagem = txMensagem;
	}

	public String getTxErro() {
		return txErro;
	}

	public void setTxErro(String txErro) {
		this.txErro = txErro;
	}
	
	

}
