package br.com.gohosp.dao.util.logerro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.util.logerro.TabLogErroObj;

public interface TabLogErroRepository extends JpaRepository<TabLogErroObj, Integer> {



}