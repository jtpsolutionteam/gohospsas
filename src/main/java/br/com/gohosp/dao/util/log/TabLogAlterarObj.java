package br.com.gohosp.dao.util.log;

import java.util.Date;

public class TabLogAlterarObj {

	private Integer cd_codigo;
	private Integer cd_codigo_linha_grid;
	private Integer cd_folder;
	private Integer cd_usuario;
	private String tx_campo;
	private String tx_ref;
	private Integer cd_tipo;
	private String tx_valor;
	private Date dt_data;
	public Integer getCd_codigo_linha_grid() {
		return cd_codigo_linha_grid;
	}
	public void setCd_codigo_linha_grid(Integer cd_codigo_linha_grid) {
		this.cd_codigo_linha_grid = cd_codigo_linha_grid;
	}
	public Integer getCd_folder() {
		return cd_folder;
	}
	public void setCd_folder(Integer cd_folder) {
		this.cd_folder = cd_folder;
	}
	public Integer getCd_usuario() {
		return cd_usuario;
	}
	public void setCd_usuario(Integer cd_usuario) {
		this.cd_usuario = cd_usuario;
	}
	public String getTx_campo() {
		return tx_campo;
	}
	public void setTx_campo(String tx_campo) {
		this.tx_campo = tx_campo;
	}
	public String getTx_ref() {
		return tx_ref;
	}
	public void setTx_ref(String tx_ref) {
		this.tx_ref = tx_ref;
	}
	public Integer getCd_tipo() {
		return cd_tipo;
	}
	public void setCd_tipo(Integer cd_tipo) {
		this.cd_tipo = cd_tipo;
	}
	public String getTx_valor() {
		return tx_valor;
	}
	public void setTx_valor(String tx_valor) {
		this.tx_valor = tx_valor;
	}
	public Date getDt_data() {
		return dt_data;
	}
	public void setDt_data(Date dt_data) {
		this.dt_data = dt_data;
	}
	public Integer getCd_codigo() {
		return cd_codigo;
	}
	public void setCd_codigo(Integer cd_codigo) {
		this.cd_codigo = cd_codigo;
	}

	
	
	
}
