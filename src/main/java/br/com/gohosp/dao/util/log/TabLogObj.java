package br.com.gohosp.dao.util.log;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.gohosp.Constants;

@Entity
@Table(name="tab_log", schema=Constants.SCHEMA)
public class TabLogObj {

	@Id @GeneratedValue
	@Column(name = "cd_codigo")
	private Integer cdCodigo;	
	
	@Column(name = "cd_codigo_linha_grid")
	private Integer cdCodigoLinhaGrid;
	
	@Column(name = "cd_folder")
	private Integer cdFolder;
	
	@Column(name = "cd_usuario")
	private Integer cdUsuario;
	
	@Column(name = "tx_campo")
	private String txCampo;
	
	@Column(name = "tx_ref")
	private String txRef;
	
	@Column(name = "cd_tipo")
	private Integer cdTipo;
	
	@Column(name = "tx_valor")
	private String txValor;
	
	@Column(name = "tx_service")
	private String txService;
	
	@Column(name = "dt_data")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	//@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone="GMT-3")
	private Date dtData;

	public Integer getCdCodigo() {
		return cdCodigo;
	}

	public void setCdCodigo(Integer cdCodigo) {
		this.cdCodigo = cdCodigo;
	}

	public Integer getCdCodigoLinhaGrid() {
		return cdCodigoLinhaGrid;
	}

	public void setCdCodigoLinhaGrid(Integer cdCodigoLinhaGrid) {
		this.cdCodigoLinhaGrid = cdCodigoLinhaGrid;
	}

	public Integer getCdFolder() {
		return cdFolder;
	}

	public void setCdFolder(Integer cdFolder) {
		this.cdFolder = cdFolder;
	}

	public Integer getCdUsuario() {
		return cdUsuario;
	}

	public void setCdUsuario(Integer cdUsuario) {
		this.cdUsuario = cdUsuario;
	}

	public String getTxCampo() {
		return txCampo;
	}

	public void setTxCampo(String txCampo) {
		this.txCampo = txCampo;
	}

	public String getTxRef() {
		return txRef;
	}

	public void setTxRef(String txRef) {
		this.txRef = txRef;
	}

	public Integer getCdTipo() {
		return cdTipo;
	}

	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	public String getTxValor() {
		return txValor;
	}

	public void setTxValor(String txValor) {
		this.txValor = txValor;
	}

	public String getTxService() {
		return txService;
	}

	public void setTxService(String txService) {
		this.txService = txService;
	}

	public Date getDtData() {
		return dtData;
	}

	public void setDtData(Date dtData) {
		this.dtData = dtData;
	}
	
	
	
}
