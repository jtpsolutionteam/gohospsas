package br.com.gohosp.dao.relatorios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.com.gohosp.Constants;

@Entity
@Table(name = "tab_relatorio_grupo_acesso", schema = Constants.SCHEMA)
public class TabRelatorioGrupoAcessoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_codigo")
	// @NotNull(message = "Codigo campo obrigatório!")
	private Integer cdCodigo;

	@Column(name = "cd_relatorio")
	@NotNull(message = "Relatorio campo obrigatório!")
	private Integer cdRelatorio;

	@Column(name = "cd_grupo_acesso")
	@NotNull(message = "GrupoAcesso campo obrigatório!")
	private Integer cdGrupoAcesso;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

	public Integer getCdCodigo() {
		return cdCodigo;
	}

	public void setCdCodigo(Integer cdCodigo) {
		this.cdCodigo = cdCodigo;
	}

	public Integer getCdRelatorio() {
		return cdRelatorio;
	}

	public void setCdRelatorio(Integer cdRelatorio) {
		this.cdRelatorio = cdRelatorio;
	}

	public Integer getCdGrupoAcesso() {
		return cdGrupoAcesso;
	}

	public void setCdGrupoAcesso(Integer cdGrupoAcesso) {
		this.cdGrupoAcesso = cdGrupoAcesso;
	}

}
