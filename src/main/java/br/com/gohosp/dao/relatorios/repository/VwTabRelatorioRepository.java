package br.com.gohosp.dao.relatorios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.relatorios.vw.VwTabRelatorioObj;



public interface VwTabRelatorioRepository extends JpaRepository<VwTabRelatorioObj, Integer> {

   List<VwTabRelatorioObj> findByTxServiceAndCkAtivoOrderByTxTituloAsc(String txService, Integer ckAtivo);
	
   List<VwTabRelatorioObj> findByTxServiceOrderByTxTituloAsc(String txService);
   
   List<VwTabRelatorioObj> findByCdRelatorioAndTxService(Integer cdRelatorio, String txService);
   
   VwTabRelatorioObj findByTxPesquisar(String txPesquisar);
   
   List<VwTabRelatorioObj> findByTxUrlPesquisarContaining(String txUrlPesquisar);

}