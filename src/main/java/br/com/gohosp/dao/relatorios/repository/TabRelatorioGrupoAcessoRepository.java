package br.com.gohosp.dao.relatorios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.relatorios.TabRelatorioGrupoAcessoObj;



public interface TabRelatorioGrupoAcessoRepository extends JpaRepository<TabRelatorioGrupoAcessoObj, Integer> {



}