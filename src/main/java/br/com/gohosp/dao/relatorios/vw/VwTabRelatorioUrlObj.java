package br.com.gohosp.dao.relatorios.vw;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.gohosp.Constants;

@Entity
@Table(name = "vw_tab_relatorio_url", schema = Constants.SCHEMA)
public class VwTabRelatorioUrlObj {

	@Id
	@GeneratedValue
	@Column(name = "tx_codigo")
	// @NotEmpty(message = "Codigo campo obrigatório!")
	@Size(max = 100, message = "Codigo tamanho máximo de 100 caracteres")
	private String txCodigo;

	@Column(name = "tx_filtro")
	// @NotEmpty(message = "Filtro campo obrigatório!")
	@Size(max = 2147483647, message = "Filtro tamanho máximo de 2147483647 caracteres")
	private String txFiltro;

	@Column(name = "ck_agendamento")
	// @NotNull(message = "ckAgendamento campo obrigatório!")
	private Integer ckAgendamento;

	@Column(name = "cd_relatorio")
	// @NotNull(message = "Relatorio campo obrigatório!")
	private Integer cdRelatorio;

	@Column(name = "dt_criacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCriacao;

	@Column(name = "tx_service")
	// @NotEmpty(message = "Service campo obrigatório!")
	@Size(max = 45, message = "Service tamanho máximo de 45 caracteres")
	private String txService;

	@Column(name = "tx_titulo")
	// @NotEmpty(message = "Titulo campo obrigatório!")
	@Size(max = 100, message = "Titulo tamanho máximo de 100 caracteres")
	private String txTitulo;

	@Column(name = "tx_quebra1")
	// @NotEmpty(message = "Quebra1 campo obrigatório!")
	@Size(max = 45, message = "Quebra1 tamanho máximo de 45 caracteres")
	private String txQuebra1;

	@Column(name = "tx_quebra2")
	// @NotEmpty(message = "Quebra2 campo obrigatório!")
	@Size(max = 45, message = "Quebra2 tamanho máximo de 45 caracteres")
	private String txQuebra2;

	@Column(name = "ck_ativo")
	// @NotNull(message = "ckAtivo campo obrigatório!")
	private Integer ckAtivo;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}

	public String getTxCodigo() {
		return txCodigo;
	}

	public void setTxCodigo(String txCodigo) {
		this.txCodigo = txCodigo;
	}

	public String getTxFiltro() {
		return txFiltro;
	}

	public void setTxFiltro(String txFiltro) {
		this.txFiltro = txFiltro;
	}

	public Integer getCkAgendamento() {
		return ckAgendamento;
	}

	public void setCkAgendamento(Integer ckAgendamento) {
		this.ckAgendamento = ckAgendamento;
	}

	public Integer getCdRelatorio() {
		return cdRelatorio;
	}

	public void setCdRelatorio(Integer cdRelatorio) {
		this.cdRelatorio = cdRelatorio;
	}

	public Date getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	public String getTxService() {
		return txService;
	}

	public void setTxService(String txService) {
		this.txService = txService;
	}

	public String getTxTitulo() {
		return txTitulo;
	}

	public void setTxTitulo(String txTitulo) {
		this.txTitulo = txTitulo;
	}

	public String getTxQuebra1() {
		return txQuebra1;
	}

	public void setTxQuebra1(String txQuebra1) {
		this.txQuebra1 = txQuebra1;
	}

	public String getTxQuebra2() {
		return txQuebra2;
	}

	public void setTxQuebra2(String txQuebra2) {
		this.txQuebra2 = txQuebra2;
	}

	public Integer getCkAtivo() {
		return ckAtivo;
	}

	public void setCkAtivo(Integer ckAtivo) {
		this.ckAtivo = ckAtivo;
	}

}
