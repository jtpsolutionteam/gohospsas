package br.com.gohosp.dao.relatorios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.gohosp.Constants;

@Entity
@Table(name = "tab_relatorio_campos", schema = Constants.SCHEMA)
public class TabRelatorioCamposObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_codigo")
	// @NotNull(message = "Codigo campo obrigatório!")
	private Integer cdCodigo;

	@Column(name = "cd_relatorio")
	@NotNull(message = "Relatorio campo obrigatório!")
	private Integer cdRelatorio;

	@Column(name = "tx_objcampo")
	@NotEmpty(message = "Campo campo obrigatório!")
	@Size(max = 45, message = "Objcampo tamanho máximo de 45 caracteres")
	private String txObjcampo;

	@Column(name = "cd_ordem")
	@NotNull(message = "Ordem campo obrigatório!")
	private Integer cdOrdem;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

	public Integer getCdCodigo() {
		return cdCodigo;
	}

	public void setCdCodigo(Integer cdCodigo) {
		this.cdCodigo = cdCodigo;
	}

	public Integer getCdRelatorio() {
		return cdRelatorio;
	}

	public void setCdRelatorio(Integer cdRelatorio) {
		this.cdRelatorio = cdRelatorio;
	}

	public String getTxObjcampo() {
		return txObjcampo;
	}

	public void setTxObjcampo(String txObjcampo) {
		this.txObjcampo = txObjcampo;
	}

	public Integer getCdOrdem() {
		return cdOrdem;
	}

	public void setCdOrdem(Integer cdOrdem) {
		this.cdOrdem = cdOrdem;
	}

}
