package br.com.gohosp.dao.relatorios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.relatorios.TabRelatorioObj;



public interface TabRelatorioRepository extends JpaRepository<TabRelatorioObj, Integer> {



}