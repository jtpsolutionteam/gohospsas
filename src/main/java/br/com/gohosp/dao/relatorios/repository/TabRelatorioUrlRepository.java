package br.com.gohosp.dao.relatorios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.relatorios.TabRelatorioUrlObj;



public interface TabRelatorioUrlRepository extends JpaRepository<TabRelatorioUrlObj, String> {



}