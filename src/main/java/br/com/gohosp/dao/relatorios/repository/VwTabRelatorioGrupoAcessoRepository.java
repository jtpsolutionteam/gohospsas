package br.com.gohosp.dao.relatorios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.relatorios.vw.VwTabRelatorioGrupoAcessoObj;



public interface VwTabRelatorioGrupoAcessoRepository extends JpaRepository<VwTabRelatorioGrupoAcessoObj, Integer> {

	List<VwTabRelatorioGrupoAcessoObj> findByCdRelatorioOrderByTxGrupoAcessoAsc(Integer cdRelatorio);
	

}