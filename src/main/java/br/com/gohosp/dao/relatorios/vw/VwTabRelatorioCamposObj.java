package br.com.gohosp.dao.relatorios.vw;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;

@Entity
@Table(name = "vw_tab_relatorio_campos", schema = Constants.SCHEMA)
public class VwTabRelatorioCamposObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_codigo")
	// @NotNull(message = "Codigo campo obrigatório!")
	private Integer cdCodigo;

	@Column(name = "cd_relatorio")
	// @NotNull(message = "Relatorio campo obrigatório!")
	private Integer cdRelatorio;

	@Column(name = "tx_objcampo")
	// @NotEmpty(message = "Objcampo campo obrigatório!")
	@Size(max = 45, message = "Objcampo tamanho máximo de 45 caracteres")
	private String txObjcampo;

	@Column(name = "cd_ordem")
	// @NotNull(message = "Ordem campo obrigatório!")
	private Integer cdOrdem;

	@Column(name = "tx_service")
	// @NotEmpty(message = "Service campo obrigatório!")
	@Size(max = 45, message = "Service tamanho máximo de 45 caracteres")
	private String txService;

	@Column(name = "tx_quebra1")
	// @NotEmpty(message = "Quebra1 campo obrigatório!")
	@Size(max = 45, message = "Quebra1 tamanho máximo de 45 caracteres")
	private String txQuebra1;

	@Column(name = "tx_quebra2")
	// @NotEmpty(message = "Quebra2 campo obrigatório!")
	@Size(max = 45, message = "Quebra2 tamanho máximo de 45 caracteres")
	private String txQuebra2;

	@Column(name = "tx_campo")
	// @NotEmpty(message = "Campo campo obrigatório!")
	@Size(max = 45, message = "Campo tamanho máximo de 45 caracteres")
	private String txCampo;

	@Column(name = "tx_tipo_campo")
	// @NotEmpty(message = "TipoCampo campo obrigatório!")
	@Size(max = 100, message = "TipoCampo tamanho máximo de 100 caracteres")
	private String txTipoCampo;

	@Column(name = "tx_aux")
	// @NotEmpty(message = "Aux campo obrigatório!")
	@Size(max = 1000, message = "Aux tamanho máximo de 1000 caracteres")
	private String txAux;

	@Column(name = "vl_size")
	// @NotNull(message = "Size campo obrigatório!")
	private Integer vlSize;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}

	public Integer getCdCodigo() {
		return cdCodigo;
	}

	public void setCdCodigo(Integer cdCodigo) {
		this.cdCodigo = cdCodigo;
	}

	public Integer getCdRelatorio() {
		return cdRelatorio;
	}

	public void setCdRelatorio(Integer cdRelatorio) {
		this.cdRelatorio = cdRelatorio;
	}

	public String getTxObjcampo() {
		return txObjcampo;
	}

	public void setTxObjcampo(String txObjcampo) {
		this.txObjcampo = txObjcampo;
	}

	public Integer getCdOrdem() {
		return cdOrdem;
	}

	public void setCdOrdem(Integer cdOrdem) {
		this.cdOrdem = cdOrdem;
	}

	public String getTxService() {
		return txService;
	}

	public void setTxService(String txService) {
		this.txService = txService;
	}

	public String getTxQuebra1() {
		return txQuebra1;
	}

	public void setTxQuebra1(String txQuebra1) {
		this.txQuebra1 = txQuebra1;
	}

	public String getTxQuebra2() {
		return txQuebra2;
	}

	public void setTxQuebra2(String txQuebra2) {
		this.txQuebra2 = txQuebra2;
	}

	public String getTxCampo() {
		return txCampo;
	}

	public void setTxCampo(String txCampo) {
		this.txCampo = txCampo;
	}

	public String getTxTipoCampo() {
		return txTipoCampo;
	}

	public void setTxTipoCampo(String txTipoCampo) {
		this.txTipoCampo = txTipoCampo;
	}

	public String getTxAux() {
		return txAux;
	}

	public void setTxAux(String txAux) {
		this.txAux = txAux;
	}

	public Integer getVlSize() {
		return vlSize;
	}

	public void setVlSize(Integer vlSize) {
		this.vlSize = vlSize;
	}

}
