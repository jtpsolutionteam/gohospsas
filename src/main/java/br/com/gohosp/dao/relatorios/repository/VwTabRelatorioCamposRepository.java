package br.com.gohosp.dao.relatorios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.relatorios.vw.VwTabRelatorioCamposObj;



public interface VwTabRelatorioCamposRepository extends JpaRepository<VwTabRelatorioCamposObj, Integer> {

  List<VwTabRelatorioCamposObj> findByCdRelatorioOrderByCdOrdemAsc(Integer cdRelatorio);
  
  VwTabRelatorioCamposObj findByCdRelatorioAndTxObjcampo(Integer cdRelatorio, String txObjcampo);

}