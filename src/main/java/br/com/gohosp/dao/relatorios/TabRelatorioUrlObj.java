package br.com.gohosp.dao.relatorios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.gohosp.Constants;

@Entity
@Table(name = "tab_relatorio_url", schema = Constants.SCHEMA)
public class TabRelatorioUrlObj {

	@Id	
	@Column(name = "tx_codigo")
	// @NotEmpty(message = "Codigo campo obrigatório!")
	@Size(max = 30, message = "Codigo tamanho máximo de 30 caracteres")
	private String txCodigo;

	@Column(name = "tx_filtro")
	@NotEmpty(message = "Filtro campo obrigatório!")
	@Size(max = 2147483647, message = "Filtro tamanho máximo de 2147483647 caracteres")
	private String txFiltro;

	@Column(name = "ck_agendamento")
	// @NotNull(message = "ckAgendamento campo obrigatório!")
	private Integer ckAgendamento;

	@Column(name = "cd_relatorio")
	@NotNull(message = "Relatorio campo obrigatório!")
	private Integer cdRelatorio;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {		
	}

	public String getTxCodigo() {
		return txCodigo;
	}

	public void setTxCodigo(String txCodigo) {
		this.txCodigo = txCodigo;
	}

	public String getTxFiltro() {
		return txFiltro;
	}

	public void setTxFiltro(String txFiltro) {
		this.txFiltro = txFiltro;
	}

	public Integer getCkAgendamento() {
		return ckAgendamento;
	}

	public void setCkAgendamento(Integer ckAgendamento) {
		this.ckAgendamento = ckAgendamento;
	}

	public Integer getCdRelatorio() {
		return cdRelatorio;
	}

	public void setCdRelatorio(Integer cdRelatorio) {
		this.cdRelatorio = cdRelatorio;
	}

}
