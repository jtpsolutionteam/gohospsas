package br.com.gohosp.dao.relatorios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.relatorios.vw.VwTabRelatorioUrlObj;



public interface VwTabRelatorioUrlRepository extends JpaRepository<VwTabRelatorioUrlObj, String> {



}