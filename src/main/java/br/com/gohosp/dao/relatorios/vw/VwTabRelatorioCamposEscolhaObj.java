package br.com.gohosp.dao.relatorios.vw;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "vw_tab_relatorio_campos_escolha", schema = Constants.SCHEMA)
public class VwTabRelatorioCamposEscolhaObj {

	@Id
	@Column(name = "cd_codigo")
	// @NotNull(message = "Codigo campo obrigatório!")
	private Integer cdCodigo;

	@Column(name = "tx_obj_campo")
	// @NotEmpty(message = "ObjCampo campo obrigatório!")
	@Size(max = 100, message = "ObjCampo tamanho máximo de 100 caracteres")
	private String txObjCampo;

	@Column(name = "tx_service_rel")
	// @NotEmpty(message = "ServiceRel campo obrigatório!")
	@Size(max = 45, message = "ServiceRel tamanho máximo de 45 caracteres")
	private String txServiceRel;


	@Column(name = "cd_relatorio")
	// @NotNull(message = "Relatorio campo obrigatório!")
	private Integer cdRelatorio;

	@Column(name = "tx_objcampo")
	// @NotEmpty(message = "Objcampo campo obrigatório!")
	@Size(max = 45, message = "Objcampo tamanho máximo de 45 caracteres")
	private String txObjcampo;

	@Column(name = "cd_ordem")
	// @NotNull(message = "Ordem campo obrigatório!")
	private Integer cdOrdem;

	@Column(name = "tx_campo")
	private String txCampo;

	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txObjCampo))
			txObjCampo = txObjCampo.toUpperCase();
		if (!Validator.isBlankOrNull(txServiceRel))
			txServiceRel = txServiceRel.toUpperCase();
		if (!Validator.isBlankOrNull(txObjcampo))
			txObjcampo = txObjcampo.toUpperCase();
	}

	public Integer getCdCodigo() {
		return cdCodigo;
	}

	public void setCdCodigo(Integer cdCodigo) {
		this.cdCodigo = cdCodigo;
	}

	public String getTxObjCampo() {
		return txObjCampo;
	}

	public void setTxObjCampo(String txObjCampo) {
		this.txObjCampo = txObjCampo;
	}

	public String getTxServiceRel() {
		return txServiceRel;
	}

	public void setTxServiceRel(String txServiceRel) {
		this.txServiceRel = txServiceRel;
	}

	public Integer getCdRelatorio() {
		return cdRelatorio;
	}

	public void setCdRelatorio(Integer cdRelatorio) {
		this.cdRelatorio = cdRelatorio;
	}

	public String getTxObjcampo() {
		return txObjcampo;
	}

	public void setTxObjcampo(String txObjcampo) {
		this.txObjcampo = txObjcampo;
	}

	public Integer getCdOrdem() {
		return cdOrdem;
	}

	public void setCdOrdem(Integer cdOrdem) {
		this.cdOrdem = cdOrdem;
	}

	public String getTxCampo() {
		return txCampo;
	}

	public void setTxCampo(String txCampo) {
		this.txCampo = txCampo;
	}

}
