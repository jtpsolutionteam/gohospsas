package br.com.gohosp.dao.relatorios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gohosp.dao.relatorios.vw.VwTabRelatorioCamposEscolhaObj;



public interface VwTabRelatorioCamposEscolhaRepository extends JpaRepository<VwTabRelatorioCamposEscolhaObj, Integer> {

	
	List<VwTabRelatorioCamposEscolhaObj> findByTxServiceRelOrderByTxCampoAsc(String txServiceRel);
	
	List<VwTabRelatorioCamposEscolhaObj> findByTxServiceRelAndCdRelatorioAndTxObjcampoIsNullOrderByTxCampoAsc(String txServiceRel, Integer cdRelatorio);
	
	List<VwTabRelatorioCamposEscolhaObj> findByCdRelatorioAndTxServiceRelAndTxObjcampoIsNotNullOrderByCdOrdemAsc(Integer cdRelatorio, String txServiceRel);

}