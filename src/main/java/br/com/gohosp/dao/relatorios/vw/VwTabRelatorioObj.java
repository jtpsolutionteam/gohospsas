package br.com.gohosp.dao.relatorios.vw;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.gohosp.Constants;
import br.com.gohosp.util.Validator;

@Entity
@Table(name = "vw_tab_relatorio", schema = Constants.SCHEMA)
public class VwTabRelatorioObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_relatorio")
	// @NotNull(message = "Relatorio campo obrigatório!")
	private Integer cdRelatorio;

	@Column(name = "tx_service")
	// @NotEmpty(message = "Service campo obrigatório!")
	@Size(max = 45, message = "Service tamanho máximo de 45 caracteres")
	private String txService;

	@Column(name = "tx_titulo")
	// @NotEmpty(message = "Titulo campo obrigatório!")
	@Size(max = 100, message = "Titulo tamanho máximo de 100 caracteres")
	private String txTitulo;

	@Column(name = "tx_quebra1")
	// @NotEmpty(message = "Quebra1 campo obrigatório!")
	@Size(max = 45, message = "Quebra1 tamanho máximo de 45 caracteres")
	private String txQuebra1;

	@Column(name = "tx_quebra2")
	// @NotEmpty(message = "Quebra2 campo obrigatório!")
	@Size(max = 45, message = "Quebra2 tamanho máximo de 45 caracteres")
	private String txQuebra2;

	@Column(name = "dt_criacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtCriacao;

	@Column(name = "cd_usuario_inclusao")
	// @NotNull(message = "UsuarioInclusao campo obrigatório!")
	private Integer cdUsuarioInclusao;

	@Column(name = "ck_ativo")
	// @NotNull(message = "ckAtivo campo obrigatório!")
	private Integer ckAtivo;

	@Column(name = "tx_usuario_inclusao")
	// @NotEmpty(message = "UsuarioInclusao campo obrigatório!")
	@Size(max = 200, message = "UsuarioInclusao tamanho máximo de 200 caracteres")
	private String txUsuarioInclusao;

	@Column(name = "tx_pesquisar")
	private String txPesquisar;
	
	@Column(name = "tx_url_pesquisar")
	private String txUrlPesquisar;
	
	@Transient
	private String txServiceRel;

	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txService))
			txService = txService.toUpperCase();
		if (!Validator.isBlankOrNull(txTitulo))
			txTitulo = txTitulo.toUpperCase();
		if (!Validator.isBlankOrNull(txQuebra1))
			txQuebra1 = txQuebra1.toUpperCase();
		if (!Validator.isBlankOrNull(txQuebra2))
			txQuebra2 = txQuebra2.toUpperCase();
		if (!Validator.isBlankOrNull(txUsuarioInclusao))
			txUsuarioInclusao = txUsuarioInclusao.toUpperCase();
	}

	public Integer getCdRelatorio() {
		return cdRelatorio;
	}

	public void setCdRelatorio(Integer cdRelatorio) {
		this.cdRelatorio = cdRelatorio;
	}

	public String getTxService() {
		return txService;
	}

	public void setTxService(String txService) {
		this.txService = txService;
	}

	public String getTxTitulo() {
		return txTitulo;
	}

	public void setTxTitulo(String txTitulo) {
		this.txTitulo = txTitulo;
	}

	public String getTxQuebra1() {
		return txQuebra1;
	}

	public void setTxQuebra1(String txQuebra1) {
		this.txQuebra1 = txQuebra1;
	}

	public String getTxQuebra2() {
		return txQuebra2;
	}

	public void setTxQuebra2(String txQuebra2) {
		this.txQuebra2 = txQuebra2;
	}

	public Date getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	public Integer getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	public void setCdUsuarioInclusao(Integer cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	public Integer getCkAtivo() {
		return ckAtivo;
	}

	public void setCkAtivo(Integer ckAtivo) {
		this.ckAtivo = ckAtivo;
	}

	public String getTxUsuarioInclusao() {
		return txUsuarioInclusao;
	}

	public void setTxUsuarioInclusao(String txUsuarioInclusao) {
		this.txUsuarioInclusao = txUsuarioInclusao;
	}

	public String getTxPesquisar() {
		return txPesquisar;
	}

	public void setTxPesquisar(String txPesquisar) {
		this.txPesquisar = txPesquisar;
	}

	public String getTxUrlPesquisar() {
		return txUrlPesquisar;
	}

	public void setTxUrlPesquisar(String txUrlPesquisar) {
		this.txUrlPesquisar = txUrlPesquisar;
	}

	public String getTxServiceRel() {
		return txServiceRel;
	}

	public void setTxServiceRel(String txServiceRel) {
		this.txServiceRel = txServiceRel;
	}

}
