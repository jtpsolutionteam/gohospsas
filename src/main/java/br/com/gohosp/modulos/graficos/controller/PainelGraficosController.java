package br.com.gohosp.modulos.graficos.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.TransactionUtilBean;
import br.com.gohosp.modulos.graficos.obj.TabGraficoModelo1Obj;
import br.com.gohosp.util.GeneralParser;
import br.com.gohosp.util.graficos.GraficosService;
import br.com.gohosp.util.graficos.TabGraficoLinhasObj;

@Controller
@RequestMapping("/graficos")
public class PainelGraficosController {

	@Autowired
	private TransactionUtilBean transactionUtilBean;
	
	private String txUrlTela = Constants.TEMPLATE_PATH_GRAFICOS+"/graficomodelo1";

	
	@GetMapping
	public ModelAndView show() {
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		
		return mv;
		
	}
	
	@RequestMapping("/consultar/{txGrafico}")
	public @ResponseBody String consultar(@PathVariable String txGrafico) {

		String txReturn = "";
		
		if (txGrafico.equals("totaltratativames")) {
			
			String StrSql = "select count(*) as vlQtde, txNomeUsuario as txNome from TabRetornoConsultaTratativaObj " + 
					"where month(date(dtUltimoEnvio)) = month(now()) and  year(date(dtUltimoEnvio)) = year(now())" + 
					"group by  month(date(dtUltimoEnvio)), year(date(dtUltimoEnvio)), txNomeUsuario";
			List lst = transactionUtilBean.querySelect(StrSql);
			
			GraficosService gr = new GraficosService();
			
			List<TabGraficoLinhasObj> lstLinhas = new ArrayList<TabGraficoLinhasObj>();
			for (int i=0; i<=lst.size()-1; i++) {

				Object[] objects = (Object[]) lst.get(i);
				
				TabGraficoLinhasObj TabLinhas = new TabGraficoLinhasObj();
				TabLinhas.setVlValorNumber(GeneralParser.parseInt(objects[0].toString()));
				TabLinhas.setTxLabel(objects[1].toString());
				TabLinhas.setTxValor(objects[0].toString());
				lstLinhas.add(TabLinhas);
			}
			
			txReturn = gr.getJsonGrafico("Nome", "Quantidade", "number", lstLinhas);
		}
		
		if (txGrafico.equals("totaltratativadia")) {
			
			String StrSql = "select count(*) as vlQtde, txNomeUsuario as txNome from TabRetornoConsultaTratativaObj " + 
					"where date(dtUltimoEnvio) = date(now()) " + 
					"group by  month(date(dtUltimoEnvio)), year(date(dtUltimoEnvio)), txNomeUsuario";
			List lst = transactionUtilBean.querySelect(StrSql);
			
			GraficosService gr = new GraficosService();
			
			List<TabGraficoLinhasObj> lstLinhas = new ArrayList<TabGraficoLinhasObj>();
			for (int i=0; i<=lst.size()-1; i++) {

				Object[] objects = (Object[]) lst.get(i);
				
				TabGraficoLinhasObj TabLinhas = new TabGraficoLinhasObj();
				TabLinhas.setVlValorNumber(GeneralParser.parseInt(objects[0].toString()));
				TabLinhas.setTxLabel(objects[1].toString());
				TabLinhas.setTxValor(objects[0].toString());
				lstLinhas.add(TabLinhas);
			}
			
			txReturn = gr.getJsonGrafico("Nome", "Quantidade", "number", lstLinhas);
		}
		
		
		return txReturn;
		
	}	
	
	
}
