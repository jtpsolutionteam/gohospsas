package br.com.gohosp.modulos.dashboard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.dao.TransactionUtilBean;
import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaObj;
import br.com.gohosp.dao.tratativas.VwTabRetornoConsultaTratativaCapaObj;
import br.com.gohosp.dao.tratativas.repository.TabRetornoConsultaTratativaRepository;
import br.com.gohosp.dao.tratativas.repository.VwTabRetornoConsultaTratativaCapaRepository;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.util.GeneralParser;


@Controller
public class DashboardController extends allController {
	
	
	@Autowired
	private TransactionUtilBean transactionUtilBean;
	
	@Autowired
	private TabRetornoConsultaTratativaRepository tabRetornoConsultaTratativaRepository;

	@Autowired
	private VwTabRetornoConsultaTratativaCapaRepository vwTabRetornoConsultaTratativaCapaRepository;


	@RequestMapping("/dashboard")
	public ModelAndView dashboard() {
		
		ModelAndView mv = new ModelAndView("dashboard/dashboard");

		
		List l = transactionUtilBean.queryNativeSelect("select  cd_cc_covid19, count(*) from vw_tab_tratativas_covid where cd_cc_covid19 is not null group by cd_cc_covid19");
		
		Integer vlTotal = 0;
		
		for (int i=0; i<=l.size()-1; i++) {
			
			Object obj = l.get(i);
			
			Object[] objects=(Object[])obj;
			
			System.out.println(objects[0]);
			System.out.println(objects[1]);
			
			vlTotal += GeneralParser.parseInt(objects[1].toString());
			
			mv.addObject("cdCovid"+objects[0], objects[1]);
			
		}
		
		mv.addObject("vlTotalCovid", vlTotal);
		
		
		List<VwTabRetornoConsultaTratativaCapaObj> lstFinalizaDiario = vwTabRetornoConsultaTratativaCapaRepository.findByFinalizaDiarioQuery();
		mv.addObject("vlTotalFinalizaDiario", lstFinalizaDiario.size());

		List<VwTabRetornoConsultaTratativaCapaObj> lstFinalizaMensal = vwTabRetornoConsultaTratativaCapaRepository.findByFinalizaMensalQuery();
		mv.addObject("vlTotalFinalizaMensal", lstFinalizaMensal.size());

		List<VwTabRetornoConsultaTratativaCapaObj> lstFinalizaDiarioSemContato = vwTabRetornoConsultaTratativaCapaRepository.findByFinalizaDiarioSemContatoQuery();
		mv.addObject("vlTotalFinalizaDiarioSemContato", lstFinalizaDiarioSemContato.size());

		List<VwTabRetornoConsultaTratativaCapaObj> lstFinalizaMensalSemContato = vwTabRetornoConsultaTratativaCapaRepository.findByFinalizaMensalSemContatoQuery();
		mv.addObject("vlTotalFinalizaMensalSemContato", lstFinalizaMensalSemContato.size());

		
		return mv;
	}
	
	
	
}
