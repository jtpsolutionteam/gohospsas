package br.com.gohosp.modulos.tratativastestes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.Constants;
import br.com.gohosp.modulos.tratativas.service.TratativasService;
import br.com.gohosp.modulos.util.geralcontroller.allController;


@Controller
@RequestMapping("/tratativastestes")
public class TratativasTestesController extends allController {

	@Autowired
	private TratativasService tratativasService;
	

	
	@RequestMapping("/consultartratativas")
	public @ResponseBody String consultartratativas() {
		
		tratativasService.verificaTratativas();
		
		return "OK";
		
	}
	
	
	@RequestMapping("/confirmartratativas")
	public @ResponseBody String confirmartratativas() {
		
		tratativasService.confirmaRecebimentoTratativas();
		
		return "OK";
		
	}
	


	@RequestMapping("/atualizatratativas")
	public @ResponseBody String atualizatratativas() {
		
		tratativasService.atualizarTratativa();
		
		return "OK";
		
	}
	
}
