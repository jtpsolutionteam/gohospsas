package br.com.gohosp.modulos.cadastros.usuario.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.cadastros.usuario.TabUsuarioObj;
import br.com.gohosp.dao.cadastros.usuario.repository.TabUsuarioRepository;
import br.com.gohosp.dao.util.log.TabLogObj;
import br.com.gohosp.exceptions.ErrosConstraintException;
import br.com.gohosp.modulos.cadastros.usuario.bean.TabFiltroUsuarioBean;
import br.com.gohosp.modulos.cadastros.usuario.bean.TabFiltroUsuarioObj;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.log.LogBean;
import br.com.gohosp.util.logErro.JTPLogErroBean;

@Service
public class TabUsuarioService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabUsuarioRepository  tabRepository;
	

	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;

	
	@Autowired 
	private TabFiltroUsuarioBean tabFiltroService;
	
	
	
	
	public List<TabUsuarioObj> listar() {
		return tabRepository.findAll();
		
	}
	
	/* Modelo tela filho 
	
		public List<VwTabUsuarioObj> listar(String CampoRelacionamento) {

		DadosUser user  = tabUsuarioService.DadosUsuario();

		return tabVwRepository.findByTxNrefQuery(CampoRelacionamento, user.getCdGrupoAcesso());
		
	}
	*/
	
	public Page<TabUsuarioObj> telapesquisar(TabFiltroUsuarioObj tabFiltroObj, Pageable pageable) {

		Page<TabUsuarioObj> Tab = tabFiltroService.filtrar(tabFiltroObj, pageable);
			
		return Tab;

	}

	public TabUsuarioObj gravar(TabUsuarioObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabUsuarioObj tAtual = new TabUsuarioObj();
		TabUsuarioObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdUsuario() != null && !Validator.isBlankOrNull(Tab.getCdUsuario())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdUsuario());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabUsuarioObj consultar(Integer CdUsuario) {
	 TabUsuarioObj Tab = tabRepository.findOne(CdUsuario);
	 
	   return Tab;
	 
	}

	
	/* Se tela tiver excluir
		public void excluir(Integer CdUsuario) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();

		   TabUsuarioObj Tab = tabRepository.findOne(CdUsuario);
		   Tab.setDtCancelamento(new Date());
		   Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		   tabRepository.save(Tab);
			 
		}
		
	*/	
	
	
	

	
}
