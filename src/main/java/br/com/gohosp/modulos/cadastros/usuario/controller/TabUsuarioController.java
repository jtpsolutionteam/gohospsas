package br.com.gohosp.modulos.cadastros.usuario.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.administracao.TabGrupoVisaoObj;
import br.com.gohosp.dao.cadastros.usuario.TabUsuarioObj;
import br.com.gohosp.dao.cadastros.varios.TabCuidadorTipoObj;
import br.com.gohosp.dao.cadastros.varios.repository.TabCuidadorTipoRepository;
import br.com.gohosp.modulos.administrador.service.TabGrupoVisaoService;
import br.com.gohosp.modulos.cadastros.usuario.bean.TabFiltroUsuarioObj;
import br.com.gohosp.modulos.cadastros.usuario.service.TabUsuarioService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.GeraPass;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.paginacao.PageWrapper;
import br.com.gohosp.util.regras.RegrasBean;

@Controller
@RequestMapping("/usuario")
public class TabUsuarioController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabUsuarioService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_CADASTROS+"/usuario/usuario";
	
	@GetMapping
	public ModelAndView pesquisa(TabFiltroUsuarioObj tabFiltroUsuarioObj, Errors errors, @PageableDefault(size = 20) Pageable pageable, HttpServletRequest httpServletRequest) {
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject("tabFiltroUsuarioObj", tabFiltroUsuarioObj);
		
		PageWrapper<TabUsuarioObj> TabView = new PageWrapper<>(tabService.telapesquisar(tabFiltroUsuarioObj, pageable),httpServletRequest);
		
		if (TabView.getPage() != null) {	
		  mv.addObject("listadados",TabView);
		  mv.addObject(new TabUsuarioObj());
		}else {	
		  mv.addObject(new TabUsuarioObj());	
		  mv.addObject("txMensagem", Constants.FILTRO_NAO_EXISTE); 	
		}
		
		return mv;
	}
 	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo() {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabUsuarioObj());
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public ModelAndView gravar(@Validated TabUsuarioObj tabUsuarioObj, Errors erros) {
		ModelAndView mv = new ModelAndView(txUrlTela);
		
		if (erros.hasErrors()) {
		  mv.addObject("listaErros", erros.getAllErrors());
		  return mv;
		}
		
		if (tabUsuarioObj.getCdUsuario() != null) {
		  TabUsuarioObj TabViewSimples = tabService.consultar(tabUsuarioObj.getCdUsuario());
		  if (TabViewSimples == null) {
			  tabUsuarioObj.setTxSenha(new GeraPass().BCryptPass(tabUsuarioObj.getTxSenha()));
		  }else {
			  //System.out.println(TabViewSimples.getTxSenha());
			  //System.out.println(tabUsuarioObj.getTxSenha());
			  
			  if (Validator.isBlankOrNull(tabUsuarioObj.getTxSenha())) {
				  tabUsuarioObj.setTxSenha(TabViewSimples.getTxSenha());				   
			  }else {
				  tabUsuarioObj.setTxSenha(new GeraPass().BCryptPass(tabUsuarioObj.getTxSenha()));
			  }
		  }		  
		}else {
			tabUsuarioObj.setTxSenha(new GeraPass().BCryptPass(tabUsuarioObj.getTxSenha()));
		}
		
	    TabUsuarioObj Tab = tabService.gravar(tabUsuarioObj);
	   
	    TabUsuarioObj TabView = tabService.consultar(Tab.getCdUsuario());
				
		mv.addObject("tabUsuarioObj",TabView);
		mv.addObject("txMensagem", Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
		mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_SUCCESS);
		return mv;
	}
   
	/*
	@RequestMapping(value = "/pesquisar/{txPesquisar}", method = RequestMethod.GET)
	public ModelAndView pesquisar(@PathVariable String txPesquisar) {
	
	   TabUsuarioObj TabView = tabService.pesquisa(txPesquisar);

		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabUsuarioObj",TabView);
		}else {
		  mv.addObject(new TabUsuarioObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		
		return mv;
	}*/
	
	@RequestMapping(value = "/consultar/{cdUsuario}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdUsuario) {
	
	   TabUsuarioObj TabView = tabService.consultar(cdUsuario);
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabUsuarioObj",TabView);
		}else {
		  mv.addObject(new TabUsuarioObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	/*
	@RequestMapping(value = "/lkpesquisar")
	public @ResponseBody List<VwTabUsuarioObj> lkpesquisar(String txUrlPesquisar) {
		return tabService.lkpesquisar(txUrlPesquisar);
		
	}*/
	
	@Autowired
	private TabGrupoVisaoService tabGrupoVisaoService;
	
	@ModelAttribute("selectcdgrupovisao")	
	public List<TabGrupoVisaoObj> selectcdgrupovisao() {
		return tabGrupoVisaoService.listar();
	}
	
}
