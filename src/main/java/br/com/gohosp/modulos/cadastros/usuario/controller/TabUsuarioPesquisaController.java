package br.com.gohosp.modulos.cadastros.usuario.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.cadastros.usuario.TabUsuarioObj;
import br.com.gohosp.modulos.cadastros.usuario.bean.TabFiltroUsuarioObj;
import br.com.gohosp.modulos.cadastros.usuario.service.TabUsuarioService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.paginacao.PageWrapper;

@Controller
@RequestMapping("/usuariopesquisa")
public class TabUsuarioPesquisaController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabUsuarioService tabService;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_CADASTROS+"/usuario/usuariopesquisa";
	
	@GetMapping
	public ModelAndView pesquisa(TabFiltroUsuarioObj tabUsuarioObj, Errors errors, @PageableDefault(size = 20) Pageable pageable, HttpServletRequest httpServletRequest) {
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject("tabUsuarioObj", tabUsuarioObj);
		
		PageWrapper<TabUsuarioObj> TabView = new PageWrapper<>(tabService.telapesquisar(tabUsuarioObj, pageable),httpServletRequest);
		
		if (TabView.getTotal() > 0) {					
		 mv.addObject("listadados",TabView);
		}else {
		  mv.addObject("mensagem", Constants.FILTRO_NAO_EXISTE); 	
		}
		
		return mv;
	}
	
	//Modelo
	/*
	@ModelAttribute("selectgrupoacesso")
	public List<TabGrupoAcessoObj> selectgrupoacesso() {
		return tabGrupoAcessoService.listar();
	}*/

	
	
}
