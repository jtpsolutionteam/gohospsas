package br.com.gohosp.modulos.cadastros.sid.service;

import java.io.BufferedReader;
import java.io.FileReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.cadastros.cid.TabCidCapituloObj;
import br.com.gohosp.dao.cadastros.cid.TabCidCategoriaObj;
import br.com.gohosp.dao.cadastros.cid.TabCidGrupoObj;
import br.com.gohosp.dao.cadastros.cid.TabCidSubcategoriaObj;
import br.com.gohosp.dao.cadastros.cid.repository.TabCidCapituloRepository;
import br.com.gohosp.dao.cadastros.cid.repository.TabCidCategoriaRepository;
import br.com.gohosp.dao.cadastros.cid.repository.TabCidGrupoRepository;
import br.com.gohosp.dao.cadastros.cid.repository.TabCidSubcategoriaRepository;
import br.com.gohosp.util.GeneralParser;

@Service
public class LerCidCsvService {

	
	@Autowired
	private TabCidCapituloRepository tabSidCapituloRepository;
	
	@Autowired
	private TabCidGrupoRepository tabSidGrupoRepository;
	
	@Autowired
	private TabCidCategoriaRepository tabCidCategoriaRepository;
	
	@Autowired
	private TabCidSubcategoriaRepository tabCidSubCategoriaRepository;
	
	public void LerCidCapituloCSV() {
		
	    try {
	      FileReader arq = new FileReader("/temp/cid/TB_CFG_CID_CAPITULOS_CICP.csv");
	      BufferedReader lerArq = new BufferedReader(arq);
	 
	      String linha = lerArq.readLine(); // lê a primeira linha
	      StringBuilder str = new StringBuilder();
	      
	      
	   // a variável "linha" recebe o valor "null" quando o processo
	  	// de repetição atingir o final do arquivo texto
	  	      while (linha != null) {
	  	        //System.out.printf("%s\n", linha);
	  	 
	  	        linha = lerArq.readLine(); // lê da segunda até a última linha
	  	        //System.out.println(linha);
	  	        
	  	        try {
	  	          
	  	          if (linha != null) {
		  	         String[] strLinha = linha.split("\\;");
		  	         TabCidCapituloObj Tab = new TabCidCapituloObj();
		  	         Tab.setCdCapitulo(GeneralParser.parseInt(strLinha[0]));
		  	         Tab.setTxDescricao(strLinha[1]);
		  	         Tab.setTxDescricaoAbreviada(strLinha[2]);
		  	         Tab.setTxCategoriaInicial(strLinha[3]);
		  	         Tab.setTxCategoriaFinal(strLinha[4]);
		  	         Tab.setDtCriacao(GeneralParser.parseDate("yyyy-MM-dd HH:mm:ss.SSS", strLinha[5]));
		  	         tabSidCapituloRepository.save(Tab);
	  	          }	
	  	        	//System.out.println(linha);
	  	        }catch (Exception ex) {
	  	        	ex.printStackTrace();
	  	        }
	  	        
	  	      }
	  	 
	  	      arq.close();
	      
	    }catch (Exception ex) {
	    	ex.printStackTrace();
	    }
	}
	
	
	public void LerCidGrupoCSV() {
		
	    try {
	      FileReader arq = new FileReader("/temp/cid/TB_CFG_CID_GRUPOS_CIGR.csv");
	      BufferedReader lerArq = new BufferedReader(arq);
	 
	      String linha = lerArq.readLine(); // lê a primeira linha
	      StringBuilder str = new StringBuilder();
	      
	      
	   // a variável "linha" recebe o valor "null" quando o processo
	  	// de repetição atingir o final do arquivo texto
	  	      while (linha != null) {
	  	        //System.out.printf("%s\n", linha);
	  	 
	  	        linha = lerArq.readLine(); // lê da segunda até a última linha
	  	        //System.out.println(linha);
	  	        
	  	        try {
	  	          
	  	          if (linha != null) {
		  	         String[] strLinha = linha.split("\\;");
		  	         if (strLinha.length > 7) {
			  	         TabCidGrupoObj Tab = new TabCidGrupoObj();
			  	         Tab.setCdCapitulo(GeneralParser.parseInt(strLinha[0]));
			  	         Tab.setCdGrupo(GeneralParser.parseInt(strLinha[1]));
			  	         Tab.setTxDescricao(strLinha[2]);
			  	         Tab.setTxDescricaoAbreviada(strLinha[3]+"\"");
			  	         Tab.setTxCategoriaInicial(strLinha[5]);
			  	         Tab.setTxCategoriaFinal(strLinha[6]);
			  	         Tab.setDtCriacao(GeneralParser.parseDate("yyyy-MM-dd HH:mm:ss.SSS", strLinha[7]));
			  	         tabSidGrupoRepository.save(Tab);
		  	         }else {
			  	         TabCidGrupoObj Tab = new TabCidGrupoObj();
			  	         Tab.setCdCapitulo(GeneralParser.parseInt(strLinha[0]));
			  	         Tab.setCdGrupo(GeneralParser.parseInt(strLinha[1]));
			  	         Tab.setTxDescricao(strLinha[2]);
			  	         Tab.setTxDescricaoAbreviada(strLinha[3]+"\"");
			  	         Tab.setTxCategoriaInicial(strLinha[4]);
			  	         Tab.setTxCategoriaFinal(strLinha[5]);
			  	         Tab.setDtCriacao(GeneralParser.parseDate("yyyy-MM-dd HH:mm:ss.SSS", strLinha[6]));
			  	         tabSidGrupoRepository.save(Tab);
		  	         }
		  	         
	  	          }	
	  	        	//System.out.println(linha);
	  	        }catch (Exception ex) {
	  	        	ex.printStackTrace();
	  	        }
	  	        
	  	      }
	  	 
	  	      arq.close();
	      
	    }catch (Exception ex) {
	    	ex.printStackTrace();
	    }
	} 

	
	public void LerCidCategoriaCSV() {
		
	    try {
	      FileReader arq = new FileReader("/temp/cid/TB_CFG_CID_CATEGORIAS_CICA.csv");
	      BufferedReader lerArq = new BufferedReader(arq);
	 
	      String linha = lerArq.readLine(); // lê a primeira linha
	      StringBuilder str = new StringBuilder();
	      
	      
	   // a variável "linha" recebe o valor "null" quando o processo
	  	// de repetição atingir o final do arquivo texto
	  	      while (linha != null) {
	  	        //System.out.printf("%s\n", linha);
	  	 
	  	        linha = lerArq.readLine(); // lê da segunda até a última linha
	  	        //System.out.println(linha);
	  	        
	  	        try {
	  	          
	  	          if (linha != null) {
		  	         String[] strLinha = linha.split("\\;");
		  	         if (strLinha.length > 7) {
			  	         TabCidCategoriaObj Tab = new TabCidCategoriaObj();
			  	         Tab.setCdCategoria(GeneralParser.parseInt(strLinha[0]));
			  	         Tab.setCdGrupo(GeneralParser.parseInt(strLinha[1]));			  	      
			  	         Tab.setTxCategoria(strLinha[2]);
			  	         Tab.setTxDescricao(strLinha[3]);
			  	         Tab.setTxDescricaoAbreviada(strLinha[4]+"\"");
			  	         Tab.setTxClassificacao(strLinha[5]);
			  	         Tab.setTxExcluido(strLinha[6]);
			  	         Tab.setTxReferencia(strLinha[8]);
			  	         Tab.setDtCriacao(GeneralParser.parseDate("yyyy-MM-dd HH:mm:ss.SSS", strLinha[9]));
			  	         tabCidCategoriaRepository.save(Tab);
		  	         }else {
		  	        	 System.out.println(linha);
		  	         }
		  	         
	  	          }	
	  	        	//System.out.println(linha);
	  	        }catch (Exception ex) {
	  	        	ex.printStackTrace();
	  	        }
	  	        
	  	      }
	  	 
	  	      arq.close();
	      
	    }catch (Exception ex) {
	    	ex.printStackTrace();
	    }
	} 

	public void LerCidSubCategoriaCSV() {
		
	    try {
	      FileReader arq = new FileReader("/temp/cid/TB_CFG_CID_SUBCATEGORIA_CISC.csv");
	      BufferedReader lerArq = new BufferedReader(arq);
	 
	      String linha = lerArq.readLine(); // lê a primeira linha
	      StringBuilder str = new StringBuilder();
	      
	      
	   // a variável "linha" recebe o valor "null" quando o processo
	  	// de repetição atingir o final do arquivo texto
	  	      while (linha != null) {
	  	        //System.out.printf("%s\n", linha);
	  	 
	  	        linha = lerArq.readLine(); // lê da segunda até a última linha
	  	        //System.out.println(linha);
	  	        
	  	        try {
	  	          
	  	          if (linha != null) {
		  	         String[] strLinha = linha.split("\\;");
		  	         if (strLinha.length > 7) {
		  	        	 /*
		  	        	System.out.println("ID_CD_SUBCATEGORIA "+strLinha[0]);
		  	        	System.out.println("ID_CD_CATEGORIA "+strLinha[1]);
		  	        	System.out.println("IN_SUB_CATEGORIA "+strLinha[2]);
		  	        	System.out.println("IN_CLASSFICACAO "+strLinha[3]);
		  	        	System.out.println("IN_SEXO_RESTRIT "+strLinha[4]);
		  	        	System.out.println("IN_CAUSA_OBTIO "+strLinha[5]);
		  	        	System.out.println("DS_REFERENCIA "+strLinha[6]);
		  	        	System.out.println("DS_DESCRICAO "+strLinha[7]);
		  	        	System.out.println("DS_DESCRICAO_ABREV "+strLinha[8]);	
		  	        	System.out.println("IN_EXCLUIDOS "+strLinha[9]);
		  	        	System.out.println("DH_CADASTRO "+strLinha[10]);
		  	        	System.out.println("DH_CADASTRO "+strLinha[11]);
		  	        	 */
		  	        	 
			  	         TabCidSubcategoriaObj Tab = new TabCidSubcategoriaObj();
			  	         Tab.setCdSubcategoria(GeneralParser.parseInt(strLinha[0]));
			  	         Tab.setCdCategoria(GeneralParser.parseInt(strLinha[1]));			  	      
			  	         Tab.setTxSubcategoria(strLinha[2]);
			  	         Tab.setTxClassificacao(strLinha[3]);
			  	         Tab.setTxSexoRestrit(strLinha[4]);
			  	         Tab.setTxCausaObito(strLinha[5]);
			  	         Tab.setTxReferencia(strLinha[6]);
			  	         Tab.setTxDescricao(strLinha[7]);
			  	         Tab.setTxDescricaoAbreviada(strLinha[8]);			  	         
			  	         Tab.setTxExcluido(strLinha[9]);			  	         
			  	         Tab.setDtCriacao(GeneralParser.parseDate("yyyy-MM-dd HH:mm:ss.SSS", strLinha[11]));
			  	         tabCidSubCategoriaRepository.save(Tab);
			  	         
			  	         
		  	         }else {
		  	        	 System.out.println(linha);
		  	         }
		  	         
	  	          }	
	  	        	//System.out.println(linha);
	  	        }catch (Exception ex) {
	  	        	ex.printStackTrace();
	  	        }
	  	        
	  	      }
	  	 
	  	      arq.close();
	      
	    }catch (Exception ex) {
	    	ex.printStackTrace();
	    }
	} 

	
}
