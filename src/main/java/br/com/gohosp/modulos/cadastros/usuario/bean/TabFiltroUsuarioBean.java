package br.com.gohosp.modulos.cadastros.usuario.bean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.gohosp.dao.cadastros.usuario.TabUsuarioObj;
import br.com.gohosp.security.UsuarioBean;

@Service
public class TabFiltroUsuarioBean {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private UsuarioBean tabDadosUsuariosBean;
  
  @SuppressWarnings("unchecked")
  @Transactional(readOnly = true)
  public Page<TabUsuarioObj> filtrar(TabFiltroUsuarioObj  filtro, Pageable pageable) {
	
	  Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabUsuarioObj.class);
	  
	  int paginaAtual = pageable.getPageNumber();
	  int totalRegistroPagina = pageable.getPageSize();
	  int primeiroRegistro = paginaAtual * totalRegistroPagina;
	  
	  criteria.setFirstResult(primeiroRegistro);
	  criteria.setMaxResults(totalRegistroPagina);
	  
	  adicionarFiltro(filtro, criteria);
		
	  return new PageImpl<>(criteria.list(), pageable, total(filtro));
	  
  }
	
  private Long total(TabFiltroUsuarioObj filtro) {
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabUsuarioObj.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
}
  
  private void adicionarFiltro(TabFiltroUsuarioObj filtro, Criteria criteria) {

	  if (filtro != null) {
		  
		  if (!StringUtils.isEmpty(filtro.getTxApelidoFiltro())) {
			  criteria.add(Restrictions.ilike("txApelido", "%"+filtro.getTxApelidoFiltro()+"%"));
		  }
		  
	  }
  }

}
 