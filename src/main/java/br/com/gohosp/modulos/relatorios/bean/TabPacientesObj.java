package br.com.gohosp.modulos.relatorios.bean;

public class TabPacientesObj {


	private String txCid2;
	

	private Integer vlQtde;
	
	private Integer vlQtdeTotal;
	
	private Integer vlIdade;
	
	private String txStatus;
	
	private String txHospital;
	
	private String txPaciente;
	
	private String txTratativas;
	
	private String txSexo;
	
	private double vlPorcent;
	
	private String dtDataSolicitacao;
	
	private String dtPrimContato;

	private String dtUltimoEnvio;
	
	private String txPacienteCpf;
	
	private String txNumVpp;
	
	private String txUsuario;

	public String getTxCid2() {
		return txCid2;
	}

	public void setTxCid2(String txCid2) {
		this.txCid2 = txCid2;
	}

	public Integer getVlQtde() {
		return vlQtde;
	}

	public void setVlQtde(Integer vlQtde) {
		this.vlQtde = vlQtde;
	}

	public String getTxStatus() {
		return txStatus;
	}

	public void setTxStatus(String txStatus) {
		this.txStatus = txStatus;
	}

	public String getTxHospital() {
		return txHospital;
	}

	public void setTxHospital(String txHospital) {
		this.txHospital = txHospital;
	}

	public String getTxPaciente() {
		return txPaciente;
	}

	public void setTxPaciente(String txPaciente) {
		this.txPaciente = txPaciente;
	}

	public Integer getVlIdade() {
		return vlIdade;
	}

	public void setVlIdade(Integer vlIdade) {
		this.vlIdade = vlIdade;
	}

	public String getTxTratativas() {
		return txTratativas;
	}

	public void setTxTratativas(String txTratativas) {
		this.txTratativas = txTratativas;
	}

	public String getTxSexo() {
		return txSexo;
	}

	public void setTxSexo(String txSexo) {
		this.txSexo = txSexo;
	}

	public Integer getVlQtdeTotal() {
		return vlQtdeTotal;
	}

	public void setVlQtdeTotal(Integer vlQtdeTotal) {
		this.vlQtdeTotal = vlQtdeTotal;
	}

	public double getVlPorcent() {
		return vlPorcent;
	}

	public void setVlPorcent(double vlPorcent) {
		this.vlPorcent = vlPorcent;
	}



	public String getTxPacienteCpf() {
		return txPacienteCpf;
	}

	public void setTxPacienteCpf(String txPacienteCpf) {
		this.txPacienteCpf = txPacienteCpf;
	}

	public String getTxNumVpp() {
		return txNumVpp;
	}

	public void setTxNumVpp(String txNumVpp) {
		this.txNumVpp = txNumVpp;
	}

	public String getDtDataSolicitacao() {
		return dtDataSolicitacao;
	}

	public void setDtDataSolicitacao(String dtDataSolicitacao) {
		this.dtDataSolicitacao = dtDataSolicitacao;
	}

	public String getTxUsuario() {
		return txUsuario;
	}

	public void setTxUsuario(String txUsuario) {
		this.txUsuario = txUsuario;
	}

	public String getDtPrimContato() {
		return dtPrimContato;
	}

	public void setDtPrimContato(String dtPrimContato) {
		this.dtPrimContato = dtPrimContato;
	}

	public String getDtUltimoEnvio() {
		return dtUltimoEnvio;
	}

	public void setDtUltimoEnvio(String dtUltimoEnvio) {
		this.dtUltimoEnvio = dtUltimoEnvio;
	}
	
	
}
