package br.com.gohosp.modulos.relatorios.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

public class TabMultiRelatoriosObj {

	
	
	private String txDataFiltro;
	
	@NotNull(message = "Data Inicial obrigatória!")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataInicialFiltro;
	
	@NotNull(message = "Data Final obrigatória!")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataFinalFiltro;
	
	@NotNull(message = "Relatório obrigatório!")
	private Integer cdRelatorioFiltro;


	public String getTxDataFiltro() {
		return txDataFiltro;
	}


	public void setTxDataFiltro(String txDataFiltro) {
		this.txDataFiltro = txDataFiltro;
	}


	public Date getDtDataInicialFiltro() {
		return dtDataInicialFiltro;
	}


	public void setDtDataInicialFiltro(Date dtDataInicialFiltro) {
		this.dtDataInicialFiltro = dtDataInicialFiltro;
	}


	public Date getDtDataFinalFiltro() {
		return dtDataFinalFiltro;
	}


	public void setDtDataFinalFiltro(Date dtDataFinalFiltro) {
		this.dtDataFinalFiltro = dtDataFinalFiltro;
	}


	public Integer getCdRelatorioFiltro() {
		return cdRelatorioFiltro;
	}


	public void setCdRelatorioFiltro(Integer cdRelatorioFiltro) {
		this.cdRelatorioFiltro = cdRelatorioFiltro;
	}
	

}
