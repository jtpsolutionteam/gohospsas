package br.com.gohosp.modulos.relatorios.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.TransactionUtilBean;
import br.com.gohosp.modulos.relatorios.bean.TabMultiRelatoriosObj;
import br.com.gohosp.modulos.relatorios.bean.TabPacientesObj;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralParser;

@Controller
@RequestMapping("/multirelatorios")
public class MultiRelatoriosController extends allController {

	@Autowired
	private UsuarioBean tabUsuarioService;

	@Autowired
	private TransactionUtilBean transactionUtilBean;

	private String txUrlTela = Constants.TEMPLATE_PATH_RELATORIOS + "/multirelatorios";

	@GetMapping
	public ModelAndView listartratativas(@Validated TabMultiRelatoriosObj tabMultiRelatoriosObj, Errors erros,
			HttpServletRequest request) {

		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject("tabMultiRelatoriosObj", tabMultiRelatoriosObj);

		if (erros.hasErrors()) {
			mv.addObject("listaErros", erros.getAllErrors());
			return mv;
		}

		if (tabMultiRelatoriosObj.getCdRelatorioFiltro() != null) {
			switch (tabMultiRelatoriosObj.getCdRelatorioFiltro()) {
			case 1: {
				mv.addObject("listdados", listPacientesCid(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesCid", "OK");
				mv.addObject("tituloRelatorio", "Pacientes X CID");
				break;
			}
			case 2: {
				mv.addObject("listdados", listPacientesCidStatus(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesCidStatus", "OK");
				mv.addObject("tituloRelatorio", "Pacientes X CID X Status");
				break;
			}

			case 3: {
				mv.addObject("listdados", listPacientesCidHospital(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesCidHospital", "OK");
				mv.addObject("tituloRelatorio", "Pacientes X CID X Hospital");
				break;
			}

			case 4: {
				mv.addObject("listdados", listPacientesReinternaram(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesReinternaram", "OK");
				mv.addObject("tituloRelatorio", "Pacientes que Reinternaram");
				break;
			}

			case 5: {
				mv.addObject("listdados", listPacientesReinternaramHospital(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesReinternaramHospital", "OK");
				mv.addObject("tituloRelatorio", "Pacientes que Reinternaram X Hospital");
				break;
			}

			case 6: {
				mv.addObject("listdados", listPacientesIdadeCidStatus(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesIdadeCidStatus", "OK");
				mv.addObject("tituloRelatorio", "Idade X CID X Status");
				break;
			}

			case 7: {
				mv.addObject("listdados", listPacientesStatusPorcentagemQuantidade(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesStatusPorcentagemQuantidade", "OK");
				mv.addObject("tituloRelatorio", "Status em % e quantidade");
				break;
			}

			case 8: {
				mv.addObject("listdados", listPacientesHomemMulher(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesHomemMulher", "OK");
				mv.addObject("tituloRelatorio", "Homem X Mulher");
				break;
			}

			case 9: {
				mv.addObject("listdados", listPacientesHomemMulherStatus(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesHomemMulherStatus", "OK");
				mv.addObject("tituloRelatorio", "Homem X Mulher X Status");
				break;
			}

			case 10: {
				mv.addObject("listdados", listPacientesStatusSulamerica(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesStatusSulamerica", "OK");
				mv.addObject("tituloRelatorio", "Status Automáticos SAS");
				break;
			}

			case 11: {
				mv.addObject("listdados", listPacientesTratativasNovas(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesTratativasNovas", "OK");
				mv.addObject("tituloRelatorio", "novas Tratativas");
				break;
			}

			case 12: {
				mv.addObject("listdados", listPacientesStatusPorcentagemQuantidadeUsuario(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesStatusPorcentagemQuantidadeUsuario", "OK");
				mv.addObject("tituloRelatorio", "Usuário X Status em % e quantidade");
				break;
			}

			case 13: {
				mv.addObject("listdados", listPacientesTratativasmaisde30(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesTratativasmaisde30", "OK");
				mv.addObject("tituloRelatorio", "Pacientes mais de 30 dias");
				break;
			}

			case 14: {
				mv.addObject("listdados", listPacientesStatusPorcentagemQuantidadeUsuario2(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesStatusPorcentagemQuantidadeUsuario2", "OK");
				mv.addObject("tituloRelatorio", "Status X Usuário em % e quantidade");
				break;
			}

			case 15: {
				mv.addObject("listdados", listPacientesStatusPorcentagemQuantidadeUsuarioV1(tabMultiRelatoriosObj));
				mv.addObject("ckPacientesStatusPorcentagemQuantidadeUsuario2", "OK");
				mv.addObject("tituloRelatorio", "Status X Usuário em % e quantidade - V1");
				break;
			}

			}
		}

		return mv;
	}

	private List<TabPacientesObj> listPacientesCid(TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select count(*) as vlQtde, txCid2 from TabRetornoConsultaTratativaObj where date(dtDataSolicitacao)  between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' and cdTipo = 2 and txCid2 is not null and txCid2 <> '' group by txCid2";
		List list = transactionUtilBean.querySelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			tab.setTxCid2(objects[1] != null ? objects[1].toString() : "");
			tab.setVlQtde(GeneralParser.parseInt(objects[0].toString()));
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

	private List<TabPacientesObj> listPacientesCidStatus(TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select count(*) as vlQtde, txCid2, txStatus from VwTabRetornoConsultaTratativaObj where date(dtDataSolicitacao)  between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' and cdTipo = 2 and txCid2 is not null and txCid2 <> '' and txStatus is not null and txStatus <> ''  group by txCid2, txStatus";
		List list = transactionUtilBean.querySelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			tab.setTxCid2(objects[1] != null ? objects[1].toString() : "");
			tab.setVlQtde(GeneralParser.parseInt(objects[0].toString()));
			tab.setTxStatus(objects[2] != null ? objects[2].toString() : "");
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

	private List<TabPacientesObj> listPacientesCidHospital(TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select count(*) as vlQtde, txCid2, txPrestador from VwTabRetornoConsultaTratativaObj where date(dtDataSolicitacao)  between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' and cdTipo = 2 and txCid2 is not null and txCid2 <> '' group by txCid2, txPrestador";
		List list = transactionUtilBean.querySelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			tab.setTxCid2(objects[1] != null ? objects[1].toString() : "");
			tab.setVlQtde(GeneralParser.parseInt(objects[0].toString()));
			tab.setTxHospital(objects[2] != null ? objects[2].toString() : "");
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

	private List<TabPacientesObj> listPacientesReinternaram(TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select t.tx_nome_segurado, " + 
				"(select count(*) " + 
				"from tab_retorno_consulta_tratativa_capa tt " + 
				"where tt.tx_cpf_segurado = t.tx_cpf_segurado and date(tt.dt_data_solicitacao) <= '"+GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))+"' " + 
				"group by tt.tx_cpf_segurado having count(*) > 1 " + 
				"order by tt.tx_nome_segurado) as vl_qtde, " + 
				"(select dt_data_solicitacao " + 
				"from tab_retorno_consulta_tratativa_capa tt " + 
				"where tt.tx_cpf_segurado = t.tx_cpf_segurado " + 
				"order by tt.dt_data_solicitacao limit 1) as dt_internacao " + 
				"from tab_retorno_consulta_tratativa_capa t where date(t.dt_data_solicitacao) between '"+GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))+"' and '"+GeneralParser.parseDateSQL(
								GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))+"' " + 
				"group by t.tx_cpf_segurado having vl_qtde > 0 order by t.tx_nome_segurado";
		
		
		List list = transactionUtilBean.queryNativeSelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			tab.setVlQtde(GeneralParser.parseInt(objects[1].toString()));
			tab.setTxPaciente(objects[0] != null ? objects[0].toString() : "");
			String dt = GeneralParser.format_dateBR2(GeneralParser.parseDateMySql(objects[2].toString()));
			tab.setDtDataSolicitacao(dt);
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

	private List<TabPacientesObj> listPacientesReinternaramHospital(TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select t.tx_nome_segurado, t.tx_prestador, " + 
				"(select count(*) " + 
				"from tab_retorno_consulta_tratativa_capa tt " + 
				"where tt.tx_cpf_segurado = t.tx_cpf_segurado and tt.tx_prestador = t.tx_prestador and date(tt.dt_data_solicitacao) < '"+GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))+"' " + 
				"group by tt.tx_cpf_segurado, tt.tx_prestador having count(*) >=1 " + 
				"order by tt.tx_nome_segurado) as vl_qtde, " + 
				"(select dt_data_solicitacao " + 
				"from tab_retorno_consulta_tratativa_capa tt " + 
				"where tt.tx_cpf_segurado = t.tx_cpf_segurado and tt.tx_prestador = t.tx_prestador " + 
				"order by tt.dt_data_solicitacao limit 1) as dt_internacao " + 
				"from tab_retorno_consulta_tratativa_capa t where date(t.dt_data_solicitacao) between '"+GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))+"' and '"+GeneralParser.parseDateSQL(
								GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))+"' " + 
				"group by t.tx_cpf_segurado, t.tx_prestador having vl_qtde > 0 order by t.tx_nome_segurado";

		
		/*
		String StrSql = "select count(*) as vlQtde, txNomeSegurado, txPrestador from TabRetornoConsultaTratativaCapaObj "
				+ "where date(dtDataSolicitacao) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' group by txCpfSegurado, txPrestador having count(*) > 1 order by txNomeSegurado"; */
		List list = transactionUtilBean.queryNativeSelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			tab.setTxPaciente(objects[0] != null ? objects[0].toString() : "");
			tab.setTxHospital(objects[1] != null ? objects[1].toString() : "");
			tab.setVlQtde(GeneralParser.parseInt(objects[2].toString()));
			String dt = GeneralParser.format_dateBR2(GeneralParser.parseDateMySql(objects[3].toString()));
			tab.setDtDataSolicitacao(dt);
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

	private List<TabPacientesObj> listPacientesIdadeCidStatus(TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select vl_idade, tx_cid2, tx_status, count(*) as vlQtde, "
				+ "fn_tratativas_idade_status(vl_idade, cd_status, tx_cid2, '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "', '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "') as tx_tratativas " + "from vw_tab_retorno_consulta_tratativa "
				+ "where date(dt_data_solicitacao) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' and cd_tipo = 2 and txCid2 is not null and txCid2 <> '' group by vl_idade, tx_cid2, tx_status ";
		List list = transactionUtilBean.queryNativeSelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			tab.setVlIdade(GeneralParser.parseInt(objects[0].toString()));
			tab.setTxCid2(objects[1] != null ? objects[1].toString() : "");
			tab.setTxStatus(objects[2] != null ? objects[2].toString() : "");
			tab.setVlQtde(GeneralParser.parseInt(objects[3].toString()));
			tab.setTxTratativas(objects[4] != null ? objects[4].toString() : "");
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

	private List<TabPacientesObj> listPacientesHomemMulher(TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select count(*) as vlQtde, txSexo  from TabRetornoConsultaTratativaCapaObj  where date(dtDataSolicitacao) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' group by txSexo";
		List list = transactionUtilBean.querySelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			tab.setVlQtde(GeneralParser.parseInt(objects[0].toString()));
			tab.setTxSexo(objects[1].equals("M") ? "Masculino" : "Feminino");
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

	private List<TabPacientesObj> listPacientesHomemMulherStatus(TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select count(*) as vlQtde, txSexo, txStatus  from VwTabRetornoConsultaTratativaCapaObj  where date(dtDataSolicitacao) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ " and cdTipo = 2' and tx_status is not null and tx_status <> '' group by txSexo, txStatus";
		List list = transactionUtilBean.querySelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			tab.setVlQtde(GeneralParser.parseInt(objects[0].toString()));
			tab.setTxSexo(objects[1].equals("M") ? "Masculino" : "Feminino");
			tab.setTxStatus(objects[2] != null ? objects[2].toString() : "");
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

	private List<TabPacientesObj> listPacientesStatusPorcentagemQuantidade(
			TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select count(*) as vlQtde, " + "tx_status, "
				+ "(select count(*) from vw_tab_retorno_consulta_tratativa where date(dt_ultimo_envio) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' and cd_tipo = 2 and dt_cancelamento is null) as vlQtdeTotal "
				+ "from vw_tab_retorno_consulta_tratativa " + "where date(dt_ultimo_envio) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' and cd_tipo = 2 and dt_cancelamento group by tx_status";
		List list = transactionUtilBean.queryNativeSelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			double vlQtde = GeneralParser.parseInt(objects[0].toString());
			tab.setVlQtde((int) vlQtde);
			tab.setTxStatus(objects[1] != null ? objects[1].toString() : "");
			double vlTotal = GeneralParser.parseInt(objects[2].toString());
			double vlPorcent = ((vlQtde / vlTotal) * 100);
			tab.setVlPorcent(GeneralParser.arredondaValor(vlPorcent, 2));
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

	private List<TabPacientesObj> listPacientesStatusPorcentagemQuantidadeUsuario(
			TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select count(*) as vlQtde, " + "tx_status, "
				+ "(select count(*) from vw_tab_retorno_consulta_tratativa where date(dt_criacao) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' and cd_tipo = 2 and dt_cancelamento is null) as vlQtdeTotal, "
				+ "tx_nome_usuario from vw_tab_retorno_consulta_tratativa " + "where date(dt_criacao) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' and cd_tipo = 2 and tx_status is not null and tx_status <> '' and dt_cancelamento is null group by tx_nome_usuario,tx_status";
		List list = transactionUtilBean.queryNativeSelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			double vlQtde = GeneralParser.parseInt(objects[0].toString());
			tab.setVlQtde((int) vlQtde);
			tab.setTxStatus(objects[1] != null ? objects[1].toString() : "");
			double vlTotal = GeneralParser.parseInt(objects[2].toString());
			double vlPorcent = ((vlQtde / vlTotal) * 100);
			tab.setVlPorcent(GeneralParser.arredondaValor(vlPorcent, 2));
			tab.setTxUsuario(objects[3] != null ? objects[3].toString() : "");
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

	private List<TabPacientesObj> listPacientesStatusPorcentagemQuantidadeUsuario2(
			TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select count(*) as vlQtde, t.tx_status, (select count(*) from vw_tab_retorno_consulta_tratativa t2 where date(t2.dt_criacao) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' and t2.cd_tipo = 2 and  t2.tx_status = t.tx_status and t2.dt_cancelamento is null) as vlQtdeTotal, tx_nome_usuario from vw_tab_retorno_consulta_tratativa t where date(t.dt_criacao) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' and t.cd_tipo = 2 and tx_status is not null and tx_status <> '' and t.dt_cancelamento is null group by t.tx_status,t.tx_nome_usuario";
		List list = transactionUtilBean.queryNativeSelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			double vlQtde = GeneralParser.parseInt(objects[0].toString());
			tab.setVlQtde((int) vlQtde);
			tab.setTxStatus(objects[1] != null ? objects[1].toString() : "");
			double vlTotal = GeneralParser.parseInt(objects[2].toString());
			double vlPorcent = 0;
			if (vlTotal > 0) {
				vlPorcent = ((vlQtde / vlTotal) * 100);
			}
			tab.setVlPorcent(GeneralParser.arredondaValor(vlPorcent, 2));
			tab.setTxUsuario(objects[3] != null ? objects[3].toString() : "");
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

	private List<TabPacientesObj> listPacientesStatusPorcentagemQuantidadeUsuarioV1(
			TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select count(*) as vlQtde, t.tx_status, (select count(*) from vw_tab_retorno_consulta_tratativa t2 where date(t2.dt_criacao) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' and t2.cd_tipo = 1 and  t2.tx_status = t.tx_status and t2.dt_cancelamento is null) as vlQtdeTotal, tx_nome_usuario from vw_tab_retorno_consulta_tratativa t where date(t.dt_criacao) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' and t.cd_tipo = 1 and tx_status is not null and tx_status <> '' and t.dt_cancelamento is null group by t.tx_status,t.tx_nome_usuario";
		List list = transactionUtilBean.queryNativeSelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			double vlQtde = GeneralParser.parseInt(objects[0].toString());
			tab.setVlQtde((int) vlQtde);
			tab.setTxStatus(objects[1] != null ? objects[1].toString() : "");
			double vlTotal = GeneralParser.parseInt(objects[2].toString());
			double vlPorcent = 0;
			if (vlTotal > 0) {
				vlPorcent = ((vlQtde / vlTotal) * 100);
			}
			tab.setVlPorcent(GeneralParser.arredondaValor(vlPorcent, 2));
			tab.setTxUsuario(objects[3] != null ? objects[3].toString() : "");
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

	private List<TabPacientesObj> listPacientesStatusSulamerica(TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select count(*) as vlQtde, " + "tx_status " + "from vw_tab_retorno_consulta_tratativa "
				+ "where date(dt_data_solicitacao) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' and cd_status in(8,9) group by tx_status";
		List list = transactionUtilBean.queryNativeSelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			double vlQtde = GeneralParser.parseInt(objects[0].toString());
			tab.setVlQtde((int) vlQtde);
			tab.setTxStatus(objects[1] != null ? objects[1].toString() : "");
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

	private List<TabPacientesObj> listPacientesTratativasNovas(TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select tx_nome_segurado, tx_cpf_segurado, tx_num_vpp, dt_data_solicitacao "
				+ "from tab_retorno_consulta_tratativa_capa " + "where date(dt_data_solicitacao) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' order by tx_nome_segurado";
		List list = transactionUtilBean.queryNativeSelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			tab.setTxPaciente(objects[0].toString());
			tab.setTxPacienteCpf(objects[1].toString());
			tab.setTxNumVpp(objects[2].toString());
			tab.setDtDataSolicitacao(
					GeneralParser.format_dateBR2(GeneralParser.parseDate("yyyy-MM-dd", objects[3].toString())));
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

	private List<TabPacientesObj> listPacientesTratativasmaisde30(TabMultiRelatoriosObj tabMultiRelatoriosObj) {

		String StrSql = "select tx_nome_segurado, dt_data_solicitacao, dt_prim_contato, dt_ultimo_envio, "
				+ "DATEDIFF(date(dt_ultimo_envio), date(dt_prim_contato)) vl_qtde_dias "
				+ "from vw_tab_faturamento_v2 where date(dt_data_solicitacao) between '"
				+ GeneralParser.parseDateSQL(
						GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataInicialFiltro()))
				+ "' and '"
				+ GeneralParser.parseDateSQL(GeneralParser.format_dateBR2(tabMultiRelatoriosObj.getDtDataFinalFiltro()))
				+ "' and cd_status_tratativa = 2 having vl_qtde_dias > 30 order by dt_data_solicitacao";
		List list = transactionUtilBean.queryNativeSelect(StrSql);

		List<TabPacientesObj> listTabPacienteCid = new ArrayList<TabPacientesObj>();

		for (int i = 0; i <= list.size() - 1; i++) {

			Object[] objects = (Object[]) list.get(i);

			TabPacientesObj tab = new TabPacientesObj();
			tab.setTxPaciente(objects[0].toString());
			tab.setDtDataSolicitacao(
					GeneralParser.format_dateBR2(GeneralParser.parseDate("yyyy-MM-dd", objects[1].toString())));
			tab.setDtPrimContato(
					GeneralParser.format_dateBR2(GeneralParser.parseDate("yyyy-MM-dd", objects[2].toString())));
			tab.setDtUltimoEnvio(
					GeneralParser.format_dateBR2(GeneralParser.parseDate("yyyy-MM-dd", objects[3].toString())));
			tab.setVlQtde(GeneralParser.parseInt(objects[4].toString()));
			listTabPacienteCid.add(tab);

		}

		return listTabPacienteCid;

	}

}
