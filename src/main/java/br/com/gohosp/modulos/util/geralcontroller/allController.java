package br.com.gohosp.modulos.util.geralcontroller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.administracao.TabTelasObj;
import br.com.gohosp.dao.administracao.vw.VwTabBotaoObj;
import br.com.gohosp.dao.administracao.vw.VwTabCamposObj;
import br.com.gohosp.dao.administracao.vw.VwTabMenuObj;
import br.com.gohosp.modulos.administrador.service.TabBotaoService;
import br.com.gohosp.modulos.administrador.service.TabCamposService;
import br.com.gohosp.modulos.administrador.service.TabMenuService;
import br.com.gohosp.modulos.administrador.service.TabTelasService;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;

public class allController {


	@Autowired
	private UsuarioBean tabUsuariosService;
	
	@Autowired
	private TabTelasService tabTelasService;

	@Autowired
	private TabMenuService tabMenuService;
	
	
	@ModelAttribute("listMenus")
	public List<VwTabMenuObj> listMenus() {
		
		DadosUser user = tabUsuariosService.DadosUsuario();
		
		List<VwTabMenuObj> list = tabMenuService.listar(user.getCdGrupoVisao());
		return list;
		
	}
	
	/*
	@ModelAttribute("listMenus")
	public List<TabMenuObj> listMenus() {
		
		
		List<TabMenuObj> list = tabMenuService.listar();
		return list;
		
	}
	*/
	// Dados da Tela/Usuário

	@ModelAttribute("nomeSistema")
	public String NomeSistema() {
		return Constants.NOME_SISTEMA_LOCAL;
	}	
	
	@ModelAttribute("pathtela")
	public String pathtela() {
		
		String txTela = "Sem cadastro - "+getClass().getSimpleName();
		TabTelasObj Tab = tabTelasService.consultarController(getClass().getSimpleName());
		if (Tab != null) {
			txTela = Tab.getTxTela();
		}		
		return txTela;
	}
	
	
	@ModelAttribute("voltarTratativa")
	public String voltarTratativa(HttpServletRequest request) {
		
		if (request.getSession().getAttribute("cdTipoTratativa")!= null) {
			return request.getSession().getAttribute("cdTipoTratativa").toString();
		}else {
			return "2";
		}
		
	}

	@ModelAttribute("txService")
	public String txService() {

		return getClass().getSimpleName().replace("Controller", "Service");
	}

	@ModelAttribute("txUsuario")
	public String txNomeUsuario() {
		DadosUser user = tabUsuariosService.DadosUsuario();
		return user.getTxApelido().toUpperCase();
	}
	
	
	
	@ModelAttribute("cdGrupoVisao")
	public Integer cdGrupoVisao() {
		DadosUser user = tabUsuariosService.DadosUsuario();
		return user.getCdGrupoVisao();
	}

	// Dados de permissões
	@Autowired
	private TabBotaoService tabBotaoService;

	@ModelAttribute("listPermissaoBotao")
	public @ResponseBody List<VwTabBotaoObj> listPermissaoBotao() {
		DadosUser user = tabUsuariosService.DadosUsuario();
		
		return tabBotaoService.pesquisaPermissoes(user.getCdGrupoVisao(), getClass().getSimpleName());
	}

	@Autowired
	private TabCamposService tabCamposService;

	@ModelAttribute("listPermissaoCampos")
	public @ResponseBody List<VwTabCamposObj> listPermissaoCampos() {
		DadosUser user = tabUsuariosService.DadosUsuario();

		return tabCamposService.pesquisaPermissoes(user.getCdGrupoVisao(), getClass().getSimpleName());
	}


	
}
