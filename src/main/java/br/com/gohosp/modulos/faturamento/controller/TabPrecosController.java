package br.com.gohosp.modulos.faturamento.controller;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.cadastros.varios.TabPrecosTipoObj;
import br.com.gohosp.dao.cadastros.varios.TabStatusObj;
import br.com.gohosp.dao.cadastros.varios.repository.TabStatusRepository;
import br.com.gohosp.dao.faturamento.TabPrecosObj;
import br.com.gohosp.dao.faturamento.VwTabPrecosObj;
import br.com.gohosp.mensagens.TabRetornoSucessoObj;
import br.com.gohosp.modulos.cadastros.varios.service.TabPrecosTipoService;
import br.com.gohosp.modulos.faturamento.service.TabPrecosService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.regras.RegrasBean;

@Controller
@RequestMapping("/tabelaprecos")
public class TabPrecosController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabPrecosService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_FATURAMENTO+"/tabelaprecos";
 	
 	
 	@GetMapping
	@Transactional(readOnly = true)
	public ModelAndView pesquisa() {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabPrecosObj());
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(VwTabPrecosObj.class);
		
		adicionarFiltro(criteria);
		
		mv.addObject("listadados", retornoFiltro());
		
		return mv;
	}

	
 	private List<?> retornoFiltro() {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(VwTabPrecosObj.class);
		
		adicionarFiltro(criteria);
 		
 		return criteria.list();
 	}
 	
 	
	private void adicionarFiltro(Criteria criteria) {
		
		  boolean ckFiltro = false;
		  /*
			  if (!ckFiltro) {
				  criteria.add(Restrictions.eq("Campo", "-1"));
			  }
			*/  
			  criteria.addOrder(Order.asc("cdPreco"));
		  
	  }
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabPrecosObj tabPrecosObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabPrecosObj Tab = tabService.gravar(tabPrecosObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo(HttpServletRequest httpServletRequest) {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabPrecosObj());
		
		mv.addObject("listadados",retornoFiltro());
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{cdPreco}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdPreco,
								   HttpServletRequest httpServletRequest) {
	
	   VwTabPrecosObj TabView = tabService.consultar(cdPreco);
	    	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		 	
		  mv.addObject("tabPrecosObj",TabView);
		  mv.addObject("listadados",retornoFiltro());
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject(new TabPrecosObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	@Autowired
	private TabPrecosTipoService tabPrecosTipoService;
	
	@ModelAttribute("selectcdprecotipo")	
	public List<TabPrecosTipoObj> selectcdprecotipo() {
		return tabPrecosTipoService.listar();
	}
}
