package br.com.gohosp.modulos.faturamento.controller;


import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.Constants;
import br.com.gohosp.ans.AnsService;
import br.com.gohosp.dao.cadastros.varios.TabStatusObj;
import br.com.gohosp.dao.cadastros.varios.repository.TabStatusRepository;
import br.com.gohosp.dao.faturamento.TabFaturamentoLoteInternoObj;
import br.com.gohosp.dao.faturamento.TabFaturamentoObj;
import br.com.gohosp.dao.faturamento.VwTabFaturamentoV2Obj;
import br.com.gohosp.dao.faturamento.repository.TabFaturamentoLoteInternoRepository;
import br.com.gohosp.dao.faturamento.repository.VwTabFaturamentoV3Repository;
import br.com.gohosp.modulos.faturamento.bean.FaturamentoBean;
import br.com.gohosp.modulos.faturamento.service.FaturamentoService;
import br.com.gohosp.modulos.tratativas.service.TratativasService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralParser;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.Validator;

@Controller
@RequestMapping("/faturamento")
public class FaturamentoController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private FaturamentoService tabService;
	
	@Autowired
	private FaturamentoBean faturamentoBean;
	
	@Autowired
	private TratativasService tratativasService;
	
	@Autowired
	private VwTabFaturamentoV3Repository vwTabFaturamentoRepository;
	
	@Autowired
	private TabFaturamentoLoteInternoRepository tabFaturamentoLoteInternoRepository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	private String txUrlTela = Constants.TEMPLATE_PATH_FATURAMENTO+"/faturamento";
	
	
	@GetMapping	
	public ModelAndView show(VwTabFaturamentoV2Obj tabFiltro, HttpServletRequest request) {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		
		if (tabFiltro.getDtData() == null) {
			Date dtData = GeneralParser.parseDate(request.getParameter("dtData"));
			tabFiltro.setDtData(dtData);
		}
		
		mv = filtros(tabFiltro);
		
		return mv;
	}

	
	@RequestMapping("/gravarcondicao/{cdTratativa}/{cdCondicao}/{txAcao}/{txAux}")	
	public @ResponseBody String gravarcondicao(@PathVariable String cdTratativa, @PathVariable Integer cdCondicao, @PathVariable String txAcao, @PathVariable String txAux) {

		if (Validator.isBlankOrNullUndefined(txAux)) {
			txAux = null;
		}
		
		//System.out.println(cdTratativa);

		String[] txCodTratativa = cdTratativa.split("-");
		
		Integer cdFaturamento = null;
		
		if (Validator.isValidInteger(txCodTratativa[1])) {
			cdFaturamento = GeneralParser.parseInt(txCodTratativa[1]);
		}
		
		TabFaturamentoObj Tab = null;
		if (cdFaturamento == null) {
			Tab =  new TabFaturamentoObj();
		}else {
			Tab = tabService.consultar(cdFaturamento);
		}
		
		Tab.setCdFaturamento(cdFaturamento);
    	Tab.setCdTipo(1);
    	Tab.setCdTratativa(GeneralParser.parseInt(txCodTratativa[0]));
    	if (!Validator.isBlankOrNull(txAux)) {
    	  Tab.setTxSenhaAutorizacao(txAux);
    	}
		
		if (cdCondicao == 1) { //Enviado
		    if (txAcao.equals("on")) {
		    	Tab.setDtEnviado(new Date());
		    }else {
		    	Tab.setDtEnviado(null);
		    }
		}else if (cdCondicao == 2) { //Autorizado
		    if (txAcao.equals("on")) {
		    	Tab.setDtAutorizado(new Date());
		    }else {
		    	Tab.setDtAutorizado(null);
		    }			
		}else if (cdCondicao == 3) { //Validado
		    if (txAcao.equals("on")) {
		    	Tab.setDtValidado(new Date());
		    }else {
		    	Tab.setDtValidado(null);
		    }
		}else { //Faturado
			
		}
		
		tabService.gravar(Tab);
		
		return "OK";
	}

	
	private ModelAndView filtros(VwTabFaturamentoV2Obj tabFiltro) {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		
		if (tabFiltro.getDtStatusFaturamento() != null) {		
			if (tabFiltro.getDtStatusFaturamento().equals("dtEnviado")) {
				faturamentoBean.gerarFaturamento(tabFiltro.getDtData());
				mv.addObject("listFaturamento", vwTabFaturamentoRepository.findByPEnviarQuery(tabFiltro.getDtData()));				
			} else if (tabFiltro.getDtStatusFaturamento().equals("dtAutorizado")) {
				faturamentoBean.gerarFaturamento(tabFiltro.getDtData());
				mv.addObject("listFaturamento", vwTabFaturamentoRepository.findByPAutorizarQuery(tabFiltro.getDtData()));			
			} else if (tabFiltro.getDtStatusFaturamento().equals("dtValidado")) {
				faturamentoBean.gerarFaturamento(tabFiltro.getDtData());
				mv.addObject("listFaturamento", vwTabFaturamentoRepository.findByPValidarQuery(tabFiltro.getDtData()));			
			} else if (tabFiltro.getDtStatusFaturamento().equals("dtFaturadoAutorizado")) {
				faturamentoBean.gerarFaturamento(tabFiltro.getDtData());
				mv.addObject("ckCheckboxAll", "OK");
				mv.addObject("listFaturamento", vwTabFaturamentoRepository.findByPFaturarAutorizadoQuery(tabFiltro.getDtData()));								
			}else if (tabFiltro.getDtStatusFaturamento().equals("dtFaturado")) {
				faturamentoBean.gerarFaturamento(tabFiltro.getDtData());
				mv.addObject("ckCheckboxAll", "OK");
				mv.addObject("listFaturamento", vwTabFaturamentoRepository.findByPFaturarQuery(tabFiltro.getDtData()));								
			}else if (tabFiltro.getDtStatusFaturamento().equals("dtFaturadoOK")) {
				mv.addObject("ckCheckboxAll", "OK");
				mv.addObject("listFaturamento", vwTabFaturamentoRepository.findByFaturadoQuery(tabFiltro.getDtData()));
			}else {
				mv.addObject("listFaturamento", tabService.listaFaturamento(tabFiltro.getDtData()));
			}
			
			
		}else {
			   mv.addObject("listFaturamento", tabService.listaFaturamento(tabFiltro.getDtData()));
		}
		
		if (!Validator.isBlankOrNull(tabFiltro.getDtData())) {
		  mv.addObject("dtDataGerar", GeneralParser.format_dateBR2(tabFiltro.getDtData()));
		}
		
		return mv;
	}
	 
	
	@Autowired
	AnsService ansService;
	

	@RequestMapping("/gerarxml")
	public @ResponseBody String  reportDeliveryStatusPendingExcel(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
	    Integer cdLoteInterno = validarlotexml(request);		
		String xml = ansService.gerarXml(cdLoteInterno);
		return xml;
	} 
	


	@RequestMapping(value = "/gerarlote", method = RequestMethod.POST)	
	public ModelAndView gerarlote(HttpServletRequest request) {
		
		Date dtData = GeneralParser.parseDate(request.getParameter("dtDataGerar"));
		
	    validarlotexml(request);
		
		//Monta o filtro		
		VwTabFaturamentoV2Obj vwTabFaturamentoV2Obj = new VwTabFaturamentoV2Obj();
		vwTabFaturamentoV2Obj.setDtData(dtData);
		
		ModelAndView mv = new ModelAndView("redirect:/faturamento?dtData="+GeneralParser.format_dateBR2(dtData)+"&dtStatusFaturamento=dtFaturadoOK");		
		mv.addObject("vwTabFaturamentoV2Obj",vwTabFaturamentoV2Obj);
		
		
		return mv;
	}

	
	public Integer validarlotexml(HttpServletRequest request) {
		
		Date dtData = GeneralParser.parseDate(request.getParameter("dtDataGerar"));
		
		Enumeration<?> en = request.getParameterNames();

		String tx = "";
		Date dtFaturado = new Date();
		boolean ckGeraLote = false;
		Integer cdLoteInterno = 0;
		while (en.hasMoreElements()) {
			String paramName = (String) en.nextElement();
			String paramValue = request.getParameter(paramName);
			
			if (paramName.contains("All")) {
				String txLote = request.getParameter("txLoteNovo");
				
				if (Validator.isBlankOrNull(txLote)) {
				 if (!ckGeraLote) {
					  ckGeraLote = true;
					  TabFaturamentoLoteInternoObj tabLoteInterno = new TabFaturamentoLoteInternoObj();
					  tabLoteInterno.setDtCriacao(new Date());
					  cdLoteInterno = tabFaturamentoLoteInternoRepository.save(tabLoteInterno).getCdLoteInterno();  
				 }	
			   }
				
			  Integer cdFaturamento = GeneralParser.parseInt(GeneralUtil.TiraNaonumero(paramName));			  
			  TabFaturamentoObj tabFaturamento = tabService.consultar(cdFaturamento);
			  tabFaturamento.setDtFaturado(dtFaturado);
			  if (Validator.isBlankOrNull(txLote)) {
				 tabFaturamento.setCdLoteInterno(cdLoteInterno); 
			  }else {
				 tabFaturamento.setTxLote(txLote); 
			  }			  				  
		      tabService.gravar(tabFaturamento);
			}
		}
		
		System.out.println(tx);

		
		return cdLoteInterno;
	}
	
	
	
	
		@Autowired
		private TabStatusRepository tabStatusRepository;
		
		@ModelAttribute("selectcdstatus")	
		public List<TabStatusObj> selectcdstatus() {
			return tabStatusRepository.findAll();
		}

		
}
