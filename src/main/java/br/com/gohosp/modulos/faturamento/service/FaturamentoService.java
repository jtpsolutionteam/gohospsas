package br.com.gohosp.modulos.faturamento.service;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.faturamento.TabFaturamentoObj;
import br.com.gohosp.dao.faturamento.VwTabFaturamentoV3Obj;
import br.com.gohosp.dao.faturamento.repository.TabFaturamentoRepository;
import br.com.gohosp.dao.faturamento.repository.VwTabFaturamentoV3Repository;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.log.LogBean;
import br.com.gohosp.util.logErro.JTPLogErroBean;

@Service
public class FaturamentoService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private VwTabFaturamentoV3Repository  vwTabFaturamentoV3Repository;
		
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;
	
	@Autowired
	private TabFaturamentoRepository tabRepository;

	public List<VwTabFaturamentoV3Obj> listaFaturamento(Date dtFaturamento) {
		
		return vwTabFaturamentoV3Repository.findByDtFaturamentoQuery(dtFaturamento);
	}

	public TabFaturamentoObj gravar(TabFaturamentoObj Tab) {
		
		return tabRepository.save(Tab);
	}

	public TabFaturamentoObj consultar2(Integer cdTratativa,  Integer cdTipo) {
		
		return tabRepository.findByCdTratativaAndCdTipo(cdTratativa, cdTipo);
	}
	
	public TabFaturamentoObj consultar(Integer cdFaturamento) {
		
		return tabRepository.findOne(cdFaturamento);
	}
	
	public List<VwTabFaturamentoV3Obj> consultarLoteInterno(Integer cdLoteInterno) {
		
		return vwTabFaturamentoV3Repository.findByLoteInternoQuery(cdLoteInterno);
	}
	
}
