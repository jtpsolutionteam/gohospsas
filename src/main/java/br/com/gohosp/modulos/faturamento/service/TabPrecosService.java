package br.com.gohosp.modulos.faturamento.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.faturamento.TabPrecosObj;
import br.com.gohosp.dao.faturamento.VwTabPrecosObj;
import br.com.gohosp.dao.faturamento.repository.TabPrecosRepository;
import br.com.gohosp.dao.faturamento.repository.VwTabPrecosRepository;
import br.com.gohosp.dao.util.log.TabLogObj;
import br.com.gohosp.exceptions.ErrosConstraintException;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.log.LogBean;
import br.com.gohosp.util.logErro.JTPLogErroBean;

@Service
public class TabPrecosService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabPrecosRepository  tabRepository;
	
	@Autowired
	private VwTabPrecosRepository  tabVwRepository;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;

	
	public List<VwTabPrecosObj> listar() {
		return tabVwRepository.findAll();
		
	}
	


	public TabPrecosObj gravar(TabPrecosObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabPrecosObj tAtual = new TabPrecosObj();
		TabPrecosObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdPreco() != null && !Validator.isBlankOrNull(Tab.getCdPreco())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdPreco());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdPreco()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdPreco()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public VwTabPrecosObj consultar(Integer CdPreco) {
	 VwTabPrecosObj Tab = tabVwRepository.findOne(CdPreco);
	 
	   return Tab;
	 
	}

	
	/* Se tela tiver excluir
		public void excluir(Integer CdPreco) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();

		   TabPrecosObj Tab = tabRepository.findOne(CdPreco);
		   Tab.setDtCancelamento(new Date());
		   Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		   tabRepository.save(Tab);
			 
		}
		
	*/	
	

}
