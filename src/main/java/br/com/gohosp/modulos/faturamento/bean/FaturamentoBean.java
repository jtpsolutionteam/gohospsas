package br.com.gohosp.modulos.faturamento.bean;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.TransactionUtilBean;
import br.com.gohosp.dao.faturamento.TabFaturamentoObj;
import br.com.gohosp.dao.faturamento.TabPrecosObj;
import br.com.gohosp.dao.faturamento.VwTabFaturamentoV2Obj;
import br.com.gohosp.dao.faturamento.VwTabFaturamentoV3Obj;
import br.com.gohosp.dao.faturamento.repository.TabFaturamentoRepository;
import br.com.gohosp.dao.faturamento.repository.TabPrecosRepository;
import br.com.gohosp.dao.faturamento.repository.VwTabFaturamentoV2Repository;
import br.com.gohosp.dao.faturamento.repository.VwTabFaturamentoV3Repository;
import br.com.gohosp.util.GeneralParser;
import br.com.gohosp.util.Validator;

@Service
public class FaturamentoBean {

	@Autowired
	private VwTabFaturamentoV2Repository vwTabFaturamentoV2Repository;
	
	@Autowired
	private VwTabFaturamentoV3Repository vwTabFaturamentoV3Repository;
	
	@Autowired
	private TabFaturamentoRepository tabFaturamentoRepository;
	
	@Autowired
	private TabPrecosRepository tabPrecosRepository;
	
	@Autowired
	private TransactionUtilBean transactionUtilBean;
	
	//@Scheduled(cron="0 0 20 ? * MON-FRI")
	public void gerarFaturamento(Date dtFaturamento) {
		
		List<VwTabFaturamentoV3Obj> list = vwTabFaturamentoV3Repository.findByDtFaturamentoQuery(dtFaturamento);
		
		for (VwTabFaturamentoV3Obj atual : list) {
			
			if (atual.getCdStatusTratativa() != null) {
				if (!Validator.isBlankOrNull(atual.getDtUltimoEnvio()) && atual.getVlQtdeContatos() > 0) {
					
					FaturaTratativa(atual.getCdTratativa(), atual.getCdTabelaPreco(), atual.getDtDataSolicitacao(), atual.getDtUltimoEnvio());
					
				}
			}
		}
		
		
	}
	
	
	
	private void FaturaTratativa(Integer cdTratativa, Integer cdTabelaPreco, Date dtSolicitacao, Date dtUltimoEnvio) {
		
		try {
				
			Integer vlDias = GeneralParser.DiferencaEntreDatas(dtSolicitacao, dtUltimoEnvio);
			
			if (vlDias == 0) {
				vlDias = 1;
			}
			
			List<TabPrecosObj> listPrecos = tabPrecosRepository.findByPrecosQuery(vlDias);
			
			Integer vlTot = 1;
			for (TabPrecosObj atual : listPrecos) {

				//Verifica se já existe o lançamento no Faturamento
				TabFaturamentoObj tabFaturamento = tabFaturamentoRepository.findByCdTratativaAndCdTabelaPreco(cdTratativa, atual.getCdPreco());
				
				if (tabFaturamento != null)  {					
					tabFaturamento.setVlValor(atual.getVlValor());
					tabFaturamento.setCdTabelaPreco(atual.getCdPreco());
					tabFaturamentoRepository.save(tabFaturamento);
					
				}else {
					tabFaturamento = new TabFaturamentoObj();
					tabFaturamento.setCdTratativa(cdTratativa);
					tabFaturamento.setCdTabelaPreco(atual.getCdPreco());
					tabFaturamento.setCdTipo(vlTot);
					tabFaturamento.setVlValor(atual.getVlValor());
					tabFaturamentoRepository.save(tabFaturamento);
					
				}
				
				vlTot++;
			}
			
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		
	}
	

}
