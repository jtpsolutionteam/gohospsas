package br.com.gohosp.modulos.tratativas.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.tratativas.TabPacienteObj;
import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaCapaObj;
import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaObj;
import br.com.gohosp.dao.tratativas.repository.TabPacienteRepository;
import br.com.gohosp.dao.tratativas.repository.TabRetornoConsultaTratativaCapaRepository;
import br.com.gohosp.modulos.faturamento.bean.FaturamentoBean;
import br.com.gohosp.modulos.tratativas.service.TabRetornoConsultaTratativaService;
import br.com.gohosp.modulos.tratativas.service.TratativasService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;


@Controller
@RequestMapping("/tratativas")
public class TratativasController extends allController {

	@Autowired
	private TratativasService tratativasService;
	
	@Autowired
	private TabRetornoConsultaTratativaService tabRetornoConsultaTratativaService;
	
	@Autowired
	private UsuarioBean tabUsuarioService;

	@Autowired
	private FaturamentoBean  faturamentoBean;

	private String txUrlTela = Constants.TEMPLATE_PATH_TRATATIVAS+"/listatratativas";
	
	@RequestMapping("/listartratativas/{cdTipo}")
	public ModelAndView listartratativas(@PathVariable Integer cdTipo, HttpServletRequest request) {
		
		ModelAndView mv = new ModelAndView(txUrlTela);		
		mv.addObject("listatratativas", tratativasService.listar(cdTipo));
		mv.addObject("cdTipo", cdTipo);
		
		//tratativasService.transferir(cdTipo);

		if (cdTipo == 1) {  //Encerrado			
			mv.addObject("pathtela", "Tratativas - Contatos Realizados");
			mv.addObject("classContatoRealizado", "blue bold");
			request.getSession().setAttribute("cdTipoTratativa", 1);
		}else if (cdTipo == 2) {  // novas
			mv.addObject("pathtela", "Tratativas - Novas");	
			mv.addObject("classNovas", "blue bold");
			request.getSession().setAttribute("cdTipoTratativa", 2);
		}else if (cdTipo == 3) { // Alerta Resposta
			mv.addObject("pathtela", "Tratativas - Alerta resposta");
			mv.addObject("classSemContato", "blue bold");
			request.getSession().setAttribute("cdTipoTratativa", 3);
		}else if (cdTipo == 4) { // Inativos
			mv.addObject("pathtela", "Tratativas - Contato Realizado");	
			mv.addObject("classInativos", "blue bold");	
			request.getSession().setAttribute("cdTipoTratativa", 4);
		}else if (cdTipo == 5) { // Novo Contato
			mv.addObject("pathtela", "Tratativas - Novo Contato");	
			mv.addObject("classNovoContato", "blue bold");	
			request.getSession().setAttribute("cdTipoTratativa", 5);
		}else if (cdTipo == 6) { // Contato Posterior
			mv.addObject("pathtela", "Tratativas - Novo Contato Posterior");	
			mv.addObject("classNovoContatoPosterior", "blue bold");	
			request.getSession().setAttribute("cdTipoTratativa", 6);
		}
		

		//Total Contato Realizado
		List<TabRetornoConsultaTratativaObj> listEncerradas = tratativasService.listar(1);
		mv.addObject("vlTotComContato", listEncerradas.size());
		
		//Total Novas
		List<TabRetornoConsultaTratativaObj> listNovas = tratativasService.listar(2);
		mv.addObject("vlTotNovas", listNovas.size());

		//Total Alerta Resposta
		List<TabRetornoConsultaTratativaObj> listSemContato = tratativasService.listar(3);
		mv.addObject("vlTotSemContato", listSemContato.size());
		
		//Total Inativos
		List<TabRetornoConsultaTratativaObj> listInativos = tratativasService.listar(4);
		mv.addObject("vlTotInativos", listInativos.size());

		//Total Novo contato
		List<TabRetornoConsultaTratativaObj> listNovoContato = tratativasService.listar(5);
		mv.addObject("vlTotNovoContato", listNovoContato.size());
		
		//Total Novo contato Posterior
		List<TabRetornoConsultaTratativaObj> listNovoContatoPosterior = tratativasService.listar(6);
		mv.addObject("vlTotNovoContatoPosterior", listNovoContatoPosterior.size());

		//
	//	faturamentoBean.gerarFaturamento();
		
		
		return mv;
	}
	

	
	@RequestMapping("/dadostratativa/{cdTipo}/{cdCodigo}")
	public ModelAndView dadostratativa(@PathVariable Integer cdTipo, @PathVariable Integer cdCodigo) {
		
		ModelAndView mv = new ModelAndView(txUrlTela);		
		mv.addObject("listatratativas", tratativasService.listar(cdTipo));
		mv.addObject("cdTipo", cdTipo);

		mv.addObject("txEdit", "edit");

		
		return mv;
	}


	@RequestMapping("/excluir/{cdTipo}/{cdCodigo}")
	public String excluir(@PathVariable Integer cdTipo, @PathVariable Integer cdCodigo) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();

		TabRetornoConsultaTratativaObj Tab = tabRetornoConsultaTratativaService.consultar(cdCodigo);
		Tab.setDtCancelamento(new Date());
		Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		tabRetornoConsultaTratativaService.gravar(Tab);
		
		return "redirect:/tratativas/listartratativas/"+cdTipo;
		
		//return mv;
	}
	
	@Autowired
	private TabPacienteRepository tabPacienteRepository;
	
	@Autowired
	private TabRetornoConsultaTratativaCapaRepository tabRetornoConsultaTratativaCapaRepository;
	
	@RequestMapping("/transferir/{cdTipo}/{cdCodigo}")
	public String transferir(@PathVariable Integer cdTipo, @PathVariable Integer cdCodigo) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();
		
		TabRetornoConsultaTratativaObj Tab = tabRetornoConsultaTratativaService.consultar(cdCodigo);
		TabRetornoConsultaTratativaCapaObj TabCapa = null;
		Integer cdTratativa = 0; 
		if (Tab != null) {
		
		TabPacienteObj TabPaciente = tabPacienteRepository.findByTxCpfSegurado(Tab.getTxCpfSegurado());
		
		if (TabPaciente == null) {
			
			TabPacienteObj TabPac = new TabPacienteObj();
			TabPac.setDtDataNascimento(Tab.getDtDataNascimento());
			TabPac.setTxCarteira(Tab.getTxCarteira());
			TabPac.setTxCodSegurado(Tab.getTxCodSegurado());
			TabPac.setTxCpfSegurado(Tab.getTxCpfSegurado());
			TabPac.setTxNomeSegurado(Tab.getTxNomeSegurado());
			TabPac.setTxProdSaude(Tab.getTxProdSaude());
			TabPac.setTxSexo(Tab.getTxSexo());
			TabPac.setTxTelCelular(Tab.getTxTelCelular());
			TabPac.setTxTelCobranca(Tab.getTxTelCobranca());
			TabPac.setTxTelComercial(Tab.getTxTelComercial());
			TabPac.setTxTelContato(Tab.getTxTelContato());
			TabPac.setTxTelContato2(Tab.getTxTelContato2());
			TabPac.setTxTelContato3(Tab.getTxTelContato3());
			TabPac.setTxTelPrestador(Tab.getTxTelPrestador());
			TabPac.setTxTelResencial(Tab.getTxTelResencial());
			TabPac.setCdStatusPaciente(1);
			TabPac.setDtUltimaAtualizacao(new Date());
			tabPacienteRepository.save(TabPac);
			
			//Capa
			TabCapa = new TabRetornoConsultaTratativaCapaObj();
			TabCapa.setTxTratativaId(Tab.getTxTratativaId());
			TabCapa.setDtDataIncioVpp(Tab.getDtDataIncioVpp());
			TabCapa.setDtDataNascimento(Tab.getDtDataNascimento());
			TabCapa.setDtDataSolicitacao(Tab.getDtDataSolicitacao());
			TabCapa.setTxAssunto(Tab.getTxAssunto());
			TabCapa.setTxCarteira(Tab.getTxCarteira());
			TabCapa.setTxCodSegurado(Tab.getTxCodSegurado());
			TabCapa.setTxCpfSegurado(Tab.getTxCpfSegurado());
			TabCapa.setTxEmpresa(Tab.getTxEmpresa());
			TabCapa.setTxEstado(Tab.getTxEstado());
			TabCapa.setTxNomeSegurado(Tab.getTxNomeSegurado());
			TabCapa.setTxNumVpp(Tab.getTxNumVpp());
			TabCapa.setTxPrestador(Tab.getTxPrestador());
			TabCapa.setTxProdSaude(Tab.getTxProdSaude());
			TabCapa.setTxSexo(Tab.getTxSexo());
			TabCapa.setTxSlaTratativa(Tab.getTxSlaTratativa());
			TabCapa.setTxTelCelular(Tab.getTxTelCelular());
			TabCapa.setTxTelCobranca(Tab.getTxTelCobranca());
			TabCapa.setTxTelComercial(Tab.getTxTelComercial());
			TabCapa.setTxTelContato(Tab.getTxTelContato());
			TabCapa.setTxTelContato2(Tab.getTxTelContato2());
			TabCapa.setTxTelContato3(Tab.getTxTelContato3());
			TabCapa.setTxTelPrestador(Tab.getTxTelPrestador());
			TabCapa.setTxTelResencial(Tab.getTxTelResencial());
			TabCapa.setDtNovoContato(Tab.getDtNovoContato());
			TabCapa.setCdStatusTratativa(1);
			TabCapa = tabRetornoConsultaTratativaCapaRepository.save(TabCapa);
			cdTratativa = TabCapa.getCdTratativa();
		}else {
			
			TabRetornoConsultaTratativaCapaObj TabCapaNew = tabRetornoConsultaTratativaCapaRepository.findByTxTratativaId(Tab.getTxTratativaId());
			
			if (TabCapaNew != null) {
				cdTratativa = TabCapaNew.getCdTratativa();	
			}else {
				//Capa
				TabCapa = new TabRetornoConsultaTratativaCapaObj();
				TabCapa.setTxTratativaId(Tab.getTxTratativaId());
				TabCapa.setDtDataIncioVpp(Tab.getDtDataIncioVpp());
				TabCapa.setDtDataNascimento(Tab.getDtDataNascimento());
				TabCapa.setDtDataSolicitacao(Tab.getDtDataSolicitacao());
				TabCapa.setTxAssunto(Tab.getTxAssunto());
				TabCapa.setTxCarteira(Tab.getTxCarteira());
				TabCapa.setTxCodSegurado(Tab.getTxCodSegurado());
				TabCapa.setTxCpfSegurado(Tab.getTxCpfSegurado());
				TabCapa.setTxEmpresa(Tab.getTxEmpresa());
				TabCapa.setTxEstado(Tab.getTxEstado());
				TabCapa.setTxNomeSegurado(Tab.getTxNomeSegurado());
				TabCapa.setTxNumVpp(Tab.getTxNumVpp());
				TabCapa.setTxPrestador(Tab.getTxPrestador());
				TabCapa.setTxProdSaude(Tab.getTxProdSaude());
				TabCapa.setTxSexo(Tab.getTxSexo());
				TabCapa.setTxSlaTratativa(Tab.getTxSlaTratativa());
				TabCapa.setTxTelCelular(Tab.getTxTelCelular());
				TabCapa.setTxTelCobranca(Tab.getTxTelCobranca());
				TabCapa.setTxTelComercial(Tab.getTxTelComercial());
				TabCapa.setTxTelContato(Tab.getTxTelContato());
				TabCapa.setTxTelContato2(Tab.getTxTelContato2());
				TabCapa.setTxTelContato3(Tab.getTxTelContato3());
				TabCapa.setTxTelPrestador(Tab.getTxTelPrestador());
				TabCapa.setTxTelResencial(Tab.getTxTelResencial());
				TabCapa.setDtNovoContato(Tab.getDtNovoContato());
				TabCapa.setCdStatusTratativa(1);
				TabCapa = tabRetornoConsultaTratativaCapaRepository.save(TabCapa);
				cdTratativa = TabCapa.getCdTratativa();

			}
			
		}
		
		}
		
		return "redirect:/tratativas/listartratativas/"+cdTipo;
		
		//return mv;
	}

}
