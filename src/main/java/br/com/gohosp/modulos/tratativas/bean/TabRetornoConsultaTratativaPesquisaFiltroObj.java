package br.com.gohosp.modulos.tratativas.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabRetornoConsultaTratativaPesquisaFiltroObj {

	
	private String txCpfSeguradoFiltro;
	
	private String txNomeSeguradoFiltro;
	
	private String txCodSeguradoFiltro;
	
	private String txDataFiltro;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataInicialFiltro;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDataFinalFiltro;
	
	private Integer ckEncerradoFiltro;
	
	private Integer cdStatusFiltro;
	
	private Integer cdStatusTratativaFiltro;
	
	private String txPrestadorFiltro;
	
	private Integer cdCcCovid19Filtro;

	
}
