package br.com.gohosp.modulos.tratativas.controller;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.cadastros.varios.TabStatusObj;
import br.com.gohosp.dao.cadastros.varios.repository.TabStatusRepository;
import br.com.gohosp.dao.tratativas.VwTabRetornoConsultaTratativaObj;
import br.com.gohosp.modulos.tratativas.bean.TabRetornoConsultaTratativaPesquisaFiltroObj;
import br.com.gohosp.modulos.tratativas.service.TratativaCapaService;
import br.com.gohosp.modulos.tratativas.service.TratativasService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralParser;

@Controller
@RequestMapping("/tratativapesquisav1")
public class TabRetornoConsultaTratativaPesquisaV1Controller extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TratativaCapaService tabService;
	
	@Autowired
	private TratativasService tratativasService;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	private String txUrlTela = Constants.TEMPLATE_PATH_TRATATIVAS+"/tratativapesquisav1";
	
	
	@GetMapping
	@Transactional(readOnly = true)
	public ModelAndView show(TabRetornoConsultaTratativaPesquisaFiltroObj tabFiltro) {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(VwTabRetornoConsultaTratativaObj.class);
		
		adicionarFiltro(tabFiltro, criteria);
		
		mv.addObject("listPesquisa", criteria.list());
		
		return mv;
	}

	
	private void adicionarFiltro(TabRetornoConsultaTratativaPesquisaFiltroObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  if (filtro != null) {
			  
			  if (!StringUtils.isEmpty(filtro.getTxCpfSeguradoFiltro())) {
				  criteria.add(Restrictions.ilike("txCpfSegurado", "%"+filtro.getTxCpfSeguradoFiltro()+"%"));
				  ckFiltro = true;
			  }
			  
			  if (!StringUtils.isEmpty(filtro.getTxPrestadorFiltro())) {
				  criteria.add(Restrictions.ilike("txPrestador", "%"+filtro.getTxPrestadorFiltro()+"%"));
				  ckFiltro = true;
			  }
			  
			  if (!StringUtils.isEmpty(filtro.getTxNomeSeguradoFiltro())) {
				  criteria.add(Restrictions.ilike("txNomeSegurado", "%"+filtro.getTxNomeSeguradoFiltro()+"%"));
				  ckFiltro = true;
			  }
			  
			  if (!StringUtils.isEmpty(filtro.getTxDataFiltro()) && !StringUtils.isEmpty(filtro.getDtDataInicialFiltro()) && !StringUtils.isEmpty(filtro.getDtDataFinalFiltro())) {
				  
				  if (filtro.getTxDataFiltro().equals("dtUltimoEnvio")) {
					
					  String dtInicial = GeneralParser.format_dateBR2(filtro.getDtDataInicialFiltro())+" 00:00:00";
					  String dtFinal = GeneralParser.format_dateBR2(filtro.getDtDataFinalFiltro())+" 23:59:59";
					  
					  criteria.add(Restrictions.between(filtro.getTxDataFiltro(),GeneralParser.parseDate("dd/MM/yyyy HH:mm:ss", dtInicial), GeneralParser.parseDate("dd/MM/yyyy HH:mm:ss",dtFinal)));
					  
				  }else {
				  
				  criteria.add(Restrictions.between(filtro.getTxDataFiltro(),filtro.getDtDataInicialFiltro(), filtro.getDtDataFinalFiltro()));
				  
				  }
				  
				  ckFiltro = true;
			  }
			  
			 
			  
			  if (!StringUtils.isEmpty(filtro.getCdStatusFiltro())) {
				  criteria.add(Restrictions.eq("cdStatus",filtro.getCdStatusFiltro()));
				  ckFiltro = true;
			  }
			  
			  
			  if (!ckFiltro) {
				  criteria.add(Restrictions.eq("txPrestador", "-1"));
			  }
			  
			  criteria.addOrder(Order.asc("dtDataSolicitacao"));
		  }
	  }
	

		@Autowired
		private TabStatusRepository tabStatusRepository;
		
		@ModelAttribute("selectcdstatus")	
		public List<TabStatusObj> selectcdstatus() {
			return tabStatusRepository.findAll();
		}

		
}
