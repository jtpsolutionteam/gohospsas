package br.com.gohosp.modulos.tratativas.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.TransactionUtilBean;
import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaObj;
import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaTotalizadorObj;
import br.com.gohosp.dao.tratativas.VwTabPacienteObj;
import br.com.gohosp.dao.tratativas.repository.TabRetornoConsultaTratativaTotalizadorRepository;
import br.com.gohosp.dao.tratativas.repository.VwTabPacientePesquisasRepository;
import br.com.gohosp.modulos.tratativas.service.TabPacientesService;
import br.com.gohosp.modulos.tratativas.service.TabRetornoConsultaTratativaService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;


@Controller
@RequestMapping("/pacientesv3")
public class TabPacientesV3Controller extends allController {

	
	
	@Autowired
	private TabRetornoConsultaTratativaService tabRetornoConsultaTratativaService;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private TabPacientesService pacienteService;
	
	@Autowired
	private TransactionUtilBean transactionUtilBean;
	
	@Autowired
	private VwTabPacientePesquisasRepository vwTabPacientePesquisasRepository;
	
	@Autowired
	private TabRetornoConsultaTratativaTotalizadorRepository tabRetornoConsultaTratativaTotalizadorRepository;


	private String txUrlTela = Constants.TEMPLATE_PATH_TRATATIVAS+"/listapacientesv3";
	
	@RequestMapping("/listarpacientes/{cdTipo}")
	public ModelAndView listartratativas(@PathVariable Integer cdTipo, HttpServletRequest request) {
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		
		List<VwTabPacienteObj> lst = pacienteService.listarPaciente(cdTipo);
		
		mv.addObject("listaPacientes", lst);
		mv.addObject("cdTipo", cdTipo);

		if (cdTipo == 5) {  //Encerrado			
			mv.addObject("pathtela", "Tratativas - Encerradas");
			mv.addObject("classEncerradas", "blue bold");
			request.getSession().setAttribute("cdTipoTratativa", 5);
		}else if (cdTipo == 1) {  // novas
			mv.addObject("pathtela", "Tratativas - Novas");	
			mv.addObject("classNovas", "blue bold");
			request.getSession().setAttribute("cdTipoTratativa", 1);
		}else if (cdTipo == 2) { // Alerta Resposta
			mv.addObject("pathtela", "Tratativas - Alerta resposta");
			mv.addObject("classRespostaSulamerica", "blue bold");
			request.getSession().setAttribute("cdTipoTratativa", 2);
		}else if (cdTipo == 6) { // Inativos
			mv.addObject("pathtela", "Tratativas - Cancelados");	
			mv.addObject("classCancelados", "blue bold");	
			request.getSession().setAttribute("cdTipoTratativa", 6);
		}else if (cdTipo == 3) { // Novo Contato
			mv.addObject("pathtela", "Tratativas - Novo Contato");	
			mv.addObject("classNovoContato", "blue bold");	
			request.getSession().setAttribute("cdTipoTratativa", 3);
		}else if (cdTipo == 4) { // Contato Posterior
			mv.addObject("pathtela", "Tratativas - Novo Contato Posterior");	
			mv.addObject("classNovoContatoPosterior", "blue bold");	
			request.getSession().setAttribute("cdTipoTratativa", 4);
		}else if (cdTipo == 7) { // Aberto sem novo contato
			mv.addObject("pathtela", "Tratativas - Aberto sem Novo Contato");	
			mv.addObject("classAbertoSemNovoContato", "blue bold");	
			request.getSession().setAttribute("cdTipoTratativa", 7);
		}else if (cdTipo == 8) { // Novas SLA Vencido
			mv.addObject("pathtela", "Tratativas - Novas SLA Vencidos");	
			mv.addObject("classNovasSlaVencidos", "blue bold");	
			request.getSession().setAttribute("cdTipoTratativa", 8);
		}else if (cdTipo == 9) { // SLA 5 Dias
			mv.addObject("pathtela", "Tratativas - SLA 5 dias");	
			mv.addObject("classSla5dias", "blue bold");	
			request.getSession().setAttribute("cdTipoTratativa", 9);
		}else if (cdTipo == 10) { // SLA > 5 Dias
			mv.addObject("pathtela", "Tratativas - SLA > 5 até 10 dias");	
			mv.addObject("classSla10dias", "blue bold");	
			request.getSession().setAttribute("cdTipoTratativa", 10);
		}else if (cdTipo == 9) { // SLA > 10 Dias
			mv.addObject("pathtela", "Tratativas - SLA > 10 dias");	
			mv.addObject("classSla10diasMais", "blue bold");	
			request.getSession().setAttribute("cdTipoTratativa", 11);
		}else if (cdTipo == 12) { // Covid
			mv.addObject("pathtela", "Tratativas - Covid aberto");	
			mv.addObject("classCovid", "blue bold");	
			request.getSession().setAttribute("cdTipoTratativa", 12);
		}

		List<TabRetornoConsultaTratativaTotalizadorObj> listTotais = tabRetornoConsultaTratativaTotalizadorRepository.findAll();
		
		for (TabRetornoConsultaTratativaTotalizadorObj atual : listTotais) {
			mv.addObject(atual.getTxTotal(), atual.getVlTotal());
		}
		
		
		return mv;
	}


	@RequestMapping("/excluir/{cdTipo}/{cdCodigo}")
	public String excluir(@PathVariable Integer cdTipo, @PathVariable Integer cdCodigo) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();

		TabRetornoConsultaTratativaObj Tab = tabRetornoConsultaTratativaService.consultar(cdCodigo);
		Tab.setDtCancelamento(new Date());
		Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		tabRetornoConsultaTratativaService.gravar(Tab);
		
		return "redirect:/tratativasv3/listartratativas/"+cdTipo;
		
		//return mv;
	}


	
}
