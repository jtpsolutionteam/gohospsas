package br.com.gohosp.modulos.tratativas.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.tratativas.TabPacienteObj;
import br.com.gohosp.dao.tratativas.VwTabPacienteObj;
import br.com.gohosp.dao.tratativas.VwTabPacientePesquisasObj;
import br.com.gohosp.dao.tratativas.repository.TabPacienteRepository;
import br.com.gohosp.dao.tratativas.repository.VwTabPacienteRepository;
import br.com.gohosp.dao.util.log.TabLogObj;
import br.com.gohosp.exceptions.ErrosConstraintException;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.log.LogBean;
import br.com.gohosp.util.logErro.JTPLogErroBean;

@Service
public class TabPacientesService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabPacienteRepository tabRepository;
	
	@Autowired
	private VwTabPacienteRepository vwTabRepository;
	
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;

	/*
	@Autowired 
	private TabFiltro.....Bean tabFiltroService;
	
	*/
	
	public List<VwTabPacienteObj> listarPaciente(Integer cdTipo) {
		
		List<VwTabPacienteObj> listaPacientes = null; 
		
		 if (cdTipo == 1) {
			 listaPacientes = vwTabRepository.findNovasTratativasQuery(); 		    
		 }else if (cdTipo == 2) {
			 listaPacientes = vwTabRepository.findNovaRespostaSulamericaQuery(); 
		 }else if (cdTipo == 3) {
			 listaPacientes = vwTabRepository.findNovoContatoQuery();  
		 }else if (cdTipo == 4) {
			 listaPacientes = vwTabRepository.findNovoContatoPosteriorQuery(); 
		 }else if (cdTipo == 5) {
			 listaPacientes = vwTabRepository.findByCdStatusTratativaQuery(2); 
		 }else if (cdTipo == 7) {
			 listaPacientes = vwTabRepository.findAbertoSemNovoContatoQuery();
		 }else if (cdTipo == 8) {
			 listaPacientes = vwTabRepository.findNovasTratativasSlaVencidoQuery();
		 }else if (cdTipo == 9) {
			 listaPacientes = vwTabRepository.findSla5DiasQuery();
		 }else if (cdTipo == 10) {
			 listaPacientes = vwTabRepository.findSla10DiasQuery();
		 }else if (cdTipo == 11) {
			 listaPacientes = vwTabRepository.findSla10DiasMaisQuery();
		 }else if (cdTipo == 12) {
			 listaPacientes = vwTabRepository.findCovidEmAbertoQuery();
		 }else {		 
			 listaPacientes = vwTabRepository.findByCdStatusTratativaQuery(3);
		 }
		
		return listaPacientes;
	}
	

	public TabPacienteObj gravar(TabPacienteObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabPacienteObj tAtual = new TabPacienteObj();
		TabPacienteObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdPaciente() != null && !Validator.isBlankOrNull(Tab.getCdPaciente())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdPaciente());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdPaciente()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdPaciente()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabPacienteObj consultar(Integer cdPaciente) {
		TabPacienteObj Tab = tabRepository.findOne(cdPaciente);
	 
	   return Tab;
	 
	}

	


	
	
	
}
