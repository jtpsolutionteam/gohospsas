package br.com.gohosp.modulos.tratativas.bean;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.TransactionUtilBean;
import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaTotalizadorObj;
import br.com.gohosp.dao.tratativas.VwTabPacienteObj;
import br.com.gohosp.dao.tratativas.VwTabPacientePesquisasObj;
import br.com.gohosp.dao.tratativas.repository.TabRetornoConsultaTratativaTotalizadorRepository;
import br.com.gohosp.dao.tratativas.repository.VwTabPacientePesquisasRepository;
import br.com.gohosp.modulos.tratativas.service.TabPacientesService;

@Service
public class TabTratativasTotaisBean {

	@Autowired
	private TabRetornoConsultaTratativaTotalizadorRepository tabRetornoConsultaTratativaTotalizadorRepository;
	
	@Autowired
	private VwTabPacientePesquisasRepository vwTabPacientePesquisasRepository;
	
	@Autowired
	private TabPacientesService pacienteService;
	
	@Autowired
	private TransactionUtilBean transactionUtilBean;
	

	@Scheduled(cron="0 0/10 6-23 * * MON-SAT")
	private void gerarTotais() {
     
		
		//Total Novas
		List<VwTabPacientePesquisasObj> listNovas = vwTabPacientePesquisasRepository.findNovasTratativasQuery();
		gravar("vlTotNovas", listNovas.size());

		//Total Alerta Resposta
		List<VwTabPacientePesquisasObj> listRespostaSulamerica = vwTabPacientePesquisasRepository.findNovaRespostaSulamericaQuery();
		gravar("vlTotRespostaSulamerica", listRespostaSulamerica.size());
		
		//Total Novo contato
		List<VwTabPacienteObj> listNovoContato = pacienteService.listarPaciente(3);
		gravar("vlTotNovoContato", listNovoContato.size());
		
		//Total Novo contato Posterior
		List<VwTabPacienteObj> listNovoContatoPosterior = pacienteService.listarPaciente(4);
		gravar("vlTotNovoContatoPosterior", listNovoContatoPosterior.size());
		
		//Total Encerrado
		List<VwTabPacienteObj> listEncerradas = pacienteService.listarPaciente(5);
		gravar("vlTotEncerradas", listEncerradas.size());
		
		//Total Cancelados
		List<VwTabPacienteObj> listCancelados = pacienteService.listarPaciente(6);
		gravar("vlTotCancelados", listCancelados.size());

		
		//Total Aberto Sem Novo contato
		List<VwTabPacientePesquisasObj> listAbertoSemNovoContato = vwTabPacientePesquisasRepository.findAbertoSemNovoContatoQuery();
		gravar("vlTotAbertoSemNovoContato", listAbertoSemNovoContato.size());
		
		//Total Nova Sla Vencidos
		List<VwTabPacientePesquisasObj> listNovasSlaVencidos = vwTabPacientePesquisasRepository.findNovasTratativasSlaVencidoQuery();
		gravar("vlTotNovasSlaVencidos", listNovasSlaVencidos.size());
		
		//Total Sla 5 dias
		List<VwTabPacienteObj> listSla5Dias = pacienteService.listarPaciente(9);		
		gravar("vlTotSla5dias", listSla5Dias.size());
		
		//Total Sla > 5 até 10 dias
		List<VwTabPacienteObj> listSla10Dias = pacienteService.listarPaciente(10);
		gravar("vlTotSla10dias", listSla10Dias.size());
		
		//Total Sla > 10 dias Mais
		List<VwTabPacienteObj> listSla10DiasMais = pacienteService.listarPaciente(11);
		gravar("vlTotSla10diasMais", listSla10DiasMais.size());

		//Total Covid em aberto
		List<VwTabPacienteObj> listCovid = pacienteService.listarPaciente(12);
		gravar("vlTotCovid", listCovid.size());
		
	}
	
	
	private void gravar(String txTotal, Integer vlTotal) {
		
	   	TabRetornoConsultaTratativaTotalizadorObj Tab = new TabRetornoConsultaTratativaTotalizadorObj();
	   	Tab.setTxTotal(txTotal);
	   	Tab.setVlTotal(vlTotal);
	   	tabRetornoConsultaTratativaTotalizadorRepository.save(Tab);
	   	
		
	}
	
	
}
