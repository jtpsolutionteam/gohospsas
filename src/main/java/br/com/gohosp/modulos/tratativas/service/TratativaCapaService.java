package br.com.gohosp.modulos.tratativas.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaCapaObj;
import br.com.gohosp.dao.tratativas.VwTabRetornoConsultaTratativaCapaObj;
import br.com.gohosp.dao.tratativas.repository.TabRetornoConsultaTratativaCapaRepository;
import br.com.gohosp.dao.tratativas.repository.VwTabRetornoConsultaTratativaCapaRepository;
import br.com.gohosp.dao.util.log.TabLogObj;
import br.com.gohosp.exceptions.ErrosConstraintException;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.log.LogBean;
import br.com.gohosp.util.logErro.JTPLogErroBean;

@Service
public class TratativaCapaService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private LogBean tabLogBeanService;
	
	@Autowired
	private TabRetornoConsultaTratativaCapaRepository tabRepository;
	
	@Autowired
	private VwTabRetornoConsultaTratativaCapaRepository tabVwRepository;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	
	public VwTabRetornoConsultaTratativaCapaObj consultar(Integer cdTratativa) {
		
		VwTabRetornoConsultaTratativaCapaObj Tab = tabVwRepository.findOne(cdTratativa);
		 
		 return Tab;
		 
	}
	
	public TabRetornoConsultaTratativaCapaObj consultarTratativa(Integer cdTratativa) {
		
		TabRetornoConsultaTratativaCapaObj Tab = tabRepository.findOne(cdTratativa);
		 
		 return Tab;
		 
	}
	
	
	public TabRetornoConsultaTratativaCapaObj gravar(TabRetornoConsultaTratativaCapaObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabRetornoConsultaTratativaCapaObj tAtual = new TabRetornoConsultaTratativaCapaObj();
		TabRetornoConsultaTratativaCapaObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdTratativa() != null && !Validator.isBlankOrNull(Tab.getCdTratativa())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdTratativa());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdTratativa()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdTratativa()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	
}
