package br.com.gohosp.modulos.tratativas.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaCapaObj;
import br.com.gohosp.dao.tratativas.repository.TabRetornoConsultaTratativaCapaRepository;
import br.com.gohosp.util.GeneralParser;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.Validator;

@Service
public class utilTratativasBean {

	@Autowired
	private TabRetornoConsultaTratativaCapaRepository tabRepository;
	
	
	public void acertaSla() {
		
		List<TabRetornoConsultaTratativaCapaObj> tList  = tabRepository.findAll();
		
		for (TabRetornoConsultaTratativaCapaObj atual : tList) {
			
			
			TabRetornoConsultaTratativaCapaObj Tab = tabRepository.findByTxTratativaId(atual.getTxTratativaId());
			
			if (Tab != null) {
				
				
				System.out.println(atual.getTxTratativaId());
				if (!Validator.isBlankOrNull(atual.getTxSlaTratativa())){
				   Tab.setDtSlaTratativa(GeneralParser.retornaDataPosteriorDiasCorridosUteis(atual.getDtDataSolicitacao(), GeneralParser.parseInt(GeneralUtil.TiraNaonumero(atual.getTxSlaTratativa()))));
				   tabRepository.save(Tab);
				}
				
			}
			
			
			
			
			
			
		}
		
		
	}
	
	
	
}
