package br.com.gohosp.modulos.tratativas.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaObj;
import br.com.gohosp.dao.tratativas.VwTabRetornoConsultaTratativaObj;
import br.com.gohosp.dao.tratativas.repository.TabRetornoConsultaTratativaRepository;
import br.com.gohosp.dao.tratativas.repository.VwTabRetornoConsultaTratativaRepository;
import br.com.gohosp.dao.util.log.TabLogObj;
import br.com.gohosp.exceptions.ErrosConstraintException;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.log.LogBean;
import br.com.gohosp.util.logErro.JTPLogErroBean;

@Service
public class TabRetornoConsultaTratativaService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabRetornoConsultaTratativaRepository  tabRepository;
	
	@Autowired
	private VwTabRetornoConsultaTratativaRepository  vwTabRepository;
	
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;

	/*
	@Autowired 
	private TabFiltro.....Bean tabFiltroService;
	
	*/
	
	

	public TabRetornoConsultaTratativaObj gravar(TabRetornoConsultaTratativaObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabRetornoConsultaTratativaObj tAtual = new TabRetornoConsultaTratativaObj();
		TabRetornoConsultaTratativaObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdCodigo() != null && !Validator.isBlankOrNull(Tab.getCdCodigo())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdCodigo());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdCodigo()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdCodigo()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabRetornoConsultaTratativaObj consultar(Integer CdCodigo) {
	 TabRetornoConsultaTratativaObj Tab = tabRepository.findOne(CdCodigo);
	 
	   return Tab;
	 
	}

	
	public VwTabRetornoConsultaTratativaObj consultarVw(Integer CdCodigo) {
		 VwTabRetornoConsultaTratativaObj Tab = vwTabRepository.findOne(CdCodigo);
		 
		   return Tab;
		 
		}

	/*
	public VwTabRetornoConsultaTratativaObj consultar(String txTratativaId, Integer cdSequencia) {
		 VwTabRetornoConsultaTratativaObj Tab = vwTabRepository.findOne(CdCodigo);
		 
		   return Tab;
		 
		}
	*/
	
	public List<TabRetornoConsultaTratativaObj> listarTratativaId(String txTratativaId) {
		 
		   List<TabRetornoConsultaTratativaObj> Tab = tabRepository.findBytxTratativaIdAndDtCancelamentoIsNull(txTratativaId);
		 
		   return Tab;
		 
		}
	
	public List<TabRetornoConsultaTratativaObj> listarTratativaId(String txTratativaId, Integer cdTipo) {
		 
		   List<TabRetornoConsultaTratativaObj> Tab = tabRepository.findBytxTratativaIdAndCdTipoAndDtCancelamentoIsNullOrderByCdCodigoAsc(txTratativaId, cdTipo);
		 
		   return Tab;
		 
		}
	
	public List<VwTabRetornoConsultaTratativaObj> listarTratativaIdView(String txTratativaId) {
		 
		   List<VwTabRetornoConsultaTratativaObj> Tab = vwTabRepository.findBytxTratativaIdAndDtCancelamentoIsNullOrderByCdCodigoAsc(txTratativaId);
		 
		   return Tab;
		 
	}
	
	public List<TabRetornoConsultaTratativaObj> listarTratativaIdNotInSequencia(String txTratativaId, Integer cdSequencia) {
		 
		   List<TabRetornoConsultaTratativaObj> Tab = tabRepository.findByTratativasNotInSequenciaQuery(txTratativaId, cdSequencia);
		 
		   return Tab;
		 
		}

	
	/* Se tela tiver excluir
		public void excluir(Integer CdCodigo) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();

		   TabRetornoConsultaTratativaObj Tab = tabRepository.findOne(CdCodigo);
		   Tab.setDtCancelamento(new Date());
		   Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		   tabRepository.save(Tab);
			 
		}
		
	*/	
	
	
}
