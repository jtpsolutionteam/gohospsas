package br.com.gohosp.modulos.tratativas.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.TransactionUtilBean;
import br.com.gohosp.dao.cadastros.varios.TabAcessoVenosoObj;
import br.com.gohosp.dao.cadastros.varios.TabAlimentacaoEstadoNutricionalObj;
import br.com.gohosp.dao.cadastros.varios.TabAlimentacaoFormaMedicamentosObj;
import br.com.gohosp.dao.cadastros.varios.TabAlimentacaoObj;
import br.com.gohosp.dao.cadastros.varios.TabAlimentacaoParenteralObj;
import br.com.gohosp.dao.cadastros.varios.TabAlimentacaoQualidadeObj;
import br.com.gohosp.dao.cadastros.varios.TabAlimentacaoTiposObj;
import br.com.gohosp.dao.cadastros.varios.TabAntibioticoObj;
import br.com.gohosp.dao.cadastros.varios.TabAnticoagulantesObj;
import br.com.gohosp.dao.cadastros.varios.TabAspiracoesViasAereasObj;
import br.com.gohosp.dao.cadastros.varios.TabCcGeralObj;
import br.com.gohosp.dao.cadastros.varios.TabCienciaRecursosObj;
import br.com.gohosp.dao.cadastros.varios.TabCovid19Obj;
import br.com.gohosp.dao.cadastros.varios.TabCuidadorTipoObj;
import br.com.gohosp.dao.cadastros.varios.TabCurativosObj;
import br.com.gohosp.dao.cadastros.varios.TabEmpresaHomecareObj;
import br.com.gohosp.dao.cadastros.varios.TabExpectativaAltaObj;
import br.com.gohosp.dao.cadastros.varios.TabImpressaoGeralObj;
import br.com.gohosp.dao.cadastros.varios.TabImunobiologicoObj;
import br.com.gohosp.dao.cadastros.varios.TabImunoglobulinasObj;
import br.com.gohosp.dao.cadastros.varios.TabInternacoesAnoObj;
import br.com.gohosp.dao.cadastros.varios.TabLocalPacienteObj;
import br.com.gohosp.dao.cadastros.varios.TabMedicacoesAdministradasObj;
import br.com.gohosp.dao.cadastros.varios.TabMultiprofissionalObj;
import br.com.gohosp.dao.cadastros.varios.TabNecessidadeAposAltaObj;
import br.com.gohosp.dao.cadastros.varios.TabNivelConscienciaObj;
import br.com.gohosp.dao.cadastros.varios.TabOhbExternaObj;
import br.com.gohosp.dao.cadastros.varios.TabOhbObj;
import br.com.gohosp.dao.cadastros.varios.TabOxigenoterapiaObj;
import br.com.gohosp.dao.cadastros.varios.TabPendenciaTratamentoObj;
import br.com.gohosp.dao.cadastros.varios.TabProbEncontradosObj;
import br.com.gohosp.dao.cadastros.varios.TabQuimioterapicosObj;
import br.com.gohosp.dao.cadastros.varios.TabRadioterapiaObj;
import br.com.gohosp.dao.cadastros.varios.TabSinaisVitaisObj;
import br.com.gohosp.dao.cadastros.varios.TabStatusObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoAcamadoCadeiraRodasObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoAndarCamaObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoAspiracaoBocaNarizObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoAspiracaoSondaObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoBanhoObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoCamaCadeiraImovelObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoContatoPeleUrinaObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoControleBanheiroObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoDesconfortoPosicaoObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoExeciciosVentilatoriosObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoIdaBanheiroObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoKatzObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoLesoesPeleObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoMovimentacaoCamaObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoUsoOxigenioObj;
import br.com.gohosp.dao.cadastros.varios.TabTipoVestirObj;
import br.com.gohosp.dao.cadastros.varios.TabUsoMedicacaoParentealObj;
import br.com.gohosp.dao.cadastros.varios.repository.TabAcessoVenosoRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabAlimentacaoEstadoNutricionalRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabAlimentacaoFormaMedicamentosRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabAlimentacaoParenteralRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabAlimentacaoQualidadeRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabAlimentacaoRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabAlimentacaoTiposRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabAntibioticoRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabAnticoagulantesRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabAspiracoesViasAereasRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabCcGeralRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabCienciaRecursosRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabCovid19Repository;
import br.com.gohosp.dao.cadastros.varios.repository.TabCuidadorTipoRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabCurativosRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabEmpresaHomecareRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabExpectativaAltaRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabImpressaoGeralRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabImunobiologicoRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabImunoglobulinasRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabInternacoesAnoRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabLocalPacienteRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabMedicacoesAdministradasRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabMultiprofissionalRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabNecessidadeAposAltaRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabNivelConscienciaRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabOhbExternaRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabOhbRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabOxigenoterapiaRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabPendenciaTratamentoRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabProbEncontradosRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabQuimioterapicosRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabRadioterapiaRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabSinaisVitaisRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabStatusRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoAcamadoCadeiraRodasRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoAndarCamaRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoAspiracaoBocaNarizRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoAspiracaoSondaRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoBanhoRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoCamaCadeiraImovelRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoContatoPeleUrinaRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoControleBanheiroRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoDesconfortoPosicaoRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoExeciciosVentilatoriosRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoIdaBanheiroRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoKatzRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoLesoesPeleRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoMovimentacaoCamaRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoUsoOxigenioRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabTipoVestirRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabUsoMedicacaoParentealRepository;
import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaCidsObj;
import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaObj;
import br.com.gohosp.dao.tratativas.VwTabRetornoConsultaTratativaObj;
import br.com.gohosp.dao.tratativas.repository.TabRetornoConsultaTratativaCidsRepository;
import br.com.gohosp.dao.tratativas.repository.TabRetornoConsultaTratativaTotalizadorRepository;
import br.com.gohosp.mensagens.TabRetornoSucessoObj;
import br.com.gohosp.modulos.tratativas.service.TabRetornoConsultaTratativaService;
import br.com.gohosp.modulos.tratativas.service.TratativasService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.regras.RegrasBean;

@Controller
@RequestMapping("/tratativav3")
public class TabRetornoConsultaTratativaV3Controller extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabRetornoConsultaTratativaService tabService;

	@Autowired
	private TabRetornoConsultaTratativaTotalizadorRepository tabRetornoConsultaTratativaTotalizadorRepository;

	@Autowired
	private TabRetornoConsultaTratativaCidsRepository tabRetornoConsultaTratativaCidsRepository;

	@Autowired
	private RegrasBean regrasBean;

	@Autowired
	private TratativasService tratativasService;

	@Autowired
	private TransactionUtilBean transactionUtilBean;

	private String txUrlTela = Constants.TEMPLATE_PATH_TRATATIVAS + "/tratativav3";

	@RequestMapping(value = "/novo")
	public ModelAndView novo() {

		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabRetornoConsultaTratativaObj());

		return mv;
	}

	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public ModelAndView gravar(@Validated TabRetornoConsultaTratativaObj tabRetornoConsultaTratativaObj, Errors erros,
			HttpServletRequest request) {

		DadosUser user = tabUsuariosService.DadosUsuario();

		ModelAndView mv = new ModelAndView(txUrlTela);

		if (erros.hasErrors()) {
			mv.addObject("listaErros", erros.getAllErrors());
			return mv;
		} else {
			List<ObjectError> error = new ArrayList<ObjectError>();
			error = regrasBean.verificaregras(getClass().getSimpleName(), request);
			if (!error.isEmpty()) {
				mv.addObject("listaErros", error);
				return mv;
			}
		}

		tabRetornoConsultaTratativaObj.setCkEnviarAtualizacao(1);
		// tabRetornoConsultaTratativaObj.setCkEncerrado(0);

		tabRetornoConsultaTratativaObj.setTxNomeUsuario(user.getTxApelido());

		/*
		 * if
		 * (Validator.isBlankOrNull(tabRetornoConsultaTratativaObj.getDtNovoContato())
		 * && tabRetornoConsultaTratativaObj.getVlQtdeDiasNovoContato() != null) {
		 * 
		 * tabRetornoConsultaTratativaObj.setDtNovoContato(GeneralParser.
		 * retornaDataPosteriorDiasCorridos(new Date(),
		 * tabRetornoConsultaTratativaObj.getVlQtdeDiasNovoContato()));
		 * 
		 * }
		 */

		TabRetornoConsultaTratativaObj Tab = tabService.gravar(tabRetornoConsultaTratativaObj);

		/*
		 * if (!Validator.isBlankOrNull(Tab.getDtNovoContato())) {
		 * limpaNovoContatoMenorSequencia(Tab.getTxTratativaId(), Tab.getCdSequencia());
		 * }
		 */

		VwTabRetornoConsultaTratativaObj TabView = tabService.consultarVw(Tab.getCdCodigo());

		String str = montaTextoQuestionario(TabView);
		String txMensagemSaudecare = tabRetornoConsultaTratativaObj.getTxMensagemResposta();
		tabRetornoConsultaTratativaObj.setTxQuestionario(str + "\n\n" + txMensagemSaudecare);
		Tab = tabService.gravar(tabRetornoConsultaTratativaObj);

		// tratativasService.atualizarTratativa(Tab.getCdCodigo());
		mv.addObject("listTratativaId", listTratativas(TabView.getTxTratativaId()));
		mv.addObject("tabRetornoConsultaTratativaObj", Tab);
		mv.addObject("txMensagem", Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
		mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_SUCCESS);
		return mv;
	}

	@RequestMapping(value = "/enviarsulamerica", method = RequestMethod.POST)
	public @ResponseBody String enviarsulamerica(
			@Validated TabRetornoConsultaTratativaObj tabRetornoConsultaTratativaObj, Errors erros,
			HttpServletRequest request) {

		DadosUser user = tabUsuariosService.DadosUsuario();

		ModelAndView mv = new ModelAndView(txUrlTela);

		tabRetornoConsultaTratativaObj.setCkEnviarAtualizacao(1);

		tabRetornoConsultaTratativaObj.setTxNomeUsuario(user.getTxApelido());
		tabRetornoConsultaTratativaObj.setDtUltimoEnvio(null);
		TabRetornoConsultaTratativaObj Tab = tabService.gravar(tabRetornoConsultaTratativaObj);

		/*
		 * if (!Validator.isBlankOrNull(Tab.getDtNovoContato())) {
		 * limpaNovoContatoMenorSequencia(Tab.getTxTratativaId(), Tab.getCdSequencia());
		 * }
		 */

		VwTabRetornoConsultaTratativaObj TabView = tabService.consultarVw(Tab.getCdCodigo());

		String str = montaTextoQuestionario(TabView);
		String txMensagemSaudecare = tabRetornoConsultaTratativaObj.getTxMensagemResposta();
		tabRetornoConsultaTratativaObj.setTxQuestionario(str + "\n\n" + txMensagemSaudecare);
		Tab = tabService.gravar(tabRetornoConsultaTratativaObj);

		String txOk = "";
		if (tabRetornoConsultaTratativaObj.getCdStatus() == null) {
			txOk = "Status campo obrigatório!";
		} else {

			txOk = tratativasService.atualizarTratativa(Tab.getCdCodigo());

			if (txOk.equals("OK")) {
				tabRetornoConsultaTratativaObj.setCkEncerrado(1);
				tabRetornoConsultaTratativaObj.setDtUltimoEnvio(new Date());
				Tab = tabService.gravar(tabRetornoConsultaTratativaObj);
			}

			// mv.addObject("listTratativaId", listTratativas(TabView.getTxTratativaId()));
			// mv.addObject("tabRetornoConsultaTratativaObj",Tab);
			// mv.addObject("txMensagem", Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
			// mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_SUCCESS);
		}
		return txOk;
	}

	private void limpaNovoContatoMenorSequencia(String txTratativaId, Integer cdSequencia) {

		transactionUtilBean.queryInsertUpdate(
				"update TabRetornoConsultaTratativaObj set dtNovoContato = null where txTratativaId = '" + txTratativaId
						+ "' and cdSequencia < " + cdSequencia);

	}

	@RequestMapping(value = "/consultar/{cdCodigo}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdCodigo) {

		ModelAndView mv = new ModelAndView(txUrlTela);

		TabRetornoConsultaTratativaObj TabView = tabService.consultar(cdCodigo);

		Integer vlRows = TabView.getTxMensagem().split("\n").length;
		mv.addObject("vlRows", vlRows);

		mv.addObject("listTratativaId", listTratativas(TabView.getTxTratativaId()));
		if (TabView != null) {
			mv.addObject("tabRetornoConsultaTratativaObj", TabView);
			mv.addObject("listacid1", listaCidsPrimario(cdCodigo));
			mv.addObject("listacid2", listaCidsSecundario(cdCodigo));
		} else {
			mv.addObject(new TabRetornoConsultaTratativaObj());
			mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}

	@RequestMapping(value = "/gravarcid", method = RequestMethod.POST)
	public @ResponseBody List<?> gravaCids(@RequestBody TabRetornoConsultaTratativaCidsObj Tab) {
		tabRetornoConsultaTratativaCidsRepository.save(Tab);
		List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();
		TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
		tSuccess.setCd_mensagem(1);
		tSuccess.setTx_mensagem(Constants.REGISTRO_EXCLUIDO_SUCESSO);
		success.add(tSuccess);
		return success;
	}

	@RequestMapping(value = "/excluircid/{cdCodigoCid}", method = RequestMethod.GET)
	public @ResponseBody List<?> excluirCid(@PathVariable Integer cdCodigoCid) {
		TabRetornoConsultaTratativaCidsObj tabRetornoConsultaTratativaCidsObj = new TabRetornoConsultaTratativaCidsObj();
		tabRetornoConsultaTratativaCidsObj.setCdCodigoCid(cdCodigoCid);
		tabRetornoConsultaTratativaCidsRepository.delete(tabRetornoConsultaTratativaCidsObj);
		List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();
		TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
		tSuccess.setCd_mensagem(1);
		tSuccess.setTx_mensagem(Constants.REGISTRO_EXCLUIDO_SUCESSO);
		success.add(tSuccess);
		return success;
	}

	public List<TabRetornoConsultaTratativaCidsObj> listaCidsPrimario(Integer cdCodigo) {
		return tabRetornoConsultaTratativaCidsRepository.findByCdCodigoAndCdTipo(cdCodigo, 1);
	}

	public List<TabRetornoConsultaTratativaCidsObj> listaCidsSecundario(Integer cdCodigo) {
		return tabRetornoConsultaTratativaCidsRepository.findByCdCodigoAndCdTipo(cdCodigo, 2);
	}

	@RequestMapping(value = "/liberarinativo", method = RequestMethod.POST)
	public String liberarinativo(TabRetornoConsultaTratativaObj tabRetornoConsultaTratativaObj) {

		DadosUser user = tabUsuariosService.DadosUsuario();

		TabRetornoConsultaTratativaObj Tab = tabService.consultar(tabRetornoConsultaTratativaObj.getCdCodigo());
		Tab.setDtCancelamento(null);
		Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		tabService.gravar(Tab);

		return "redirect:/tratativasv3/listartratativas/4";

		// return mv;
	}

	private List<TabRetornoConsultaTratativaObj> listTratativas(String txTratativaId) {

		List<TabRetornoConsultaTratativaObj> list = tabService.listarTratativaId(txTratativaId);

		return list;
	}

	private String montaTextoQuestionario(VwTabRetornoConsultaTratativaObj tab) {

		StringBuilder str = new StringBuilder();
		if (!Validator.isBlankOrNull(tab.getTxCid2()))
			str.append("Cid1: " + tab.getTxCid2() + "\n");

		if (!Validator.isBlankOrNull(tab.getTxCid3()))
			str.append("Cid2: " + tab.getTxCid3() + "\n");

		StringBuilder strStatusClinico = new StringBuilder();
		if (!Validator.isBlankOrNull(tab.getTxNivelConsciencia())) {
			strStatusClinico.append("Nível Consciência: " + tab.getTxNivelConsciencia() + "\n");
		}

		if (!Validator.isBlankOrNull(tab.getTxSinaisVitais())) {
			strStatusClinico.append("Sinais vitais / Exames Básicos: " + tab.getTxSinaisVitais() + "\n");
		}

		if (strStatusClinico.length() > 2) {
			str.append("\nSTATUS CLÍNICO" + "\n");
			str.append(strStatusClinico);
		}

		StringBuilder strContextoClinico = new StringBuilder();

		if (!Validator.isBlankOrNull(tab.getTxCcGeral())) {
			strContextoClinico.append("Geral: " + tab.getTxCcGeral() + "\n");
		}

		if (!Validator.isBlankOrNull(tab.getTxCcOxigenoterapia())) {
			strContextoClinico.append("Oxigenoterapia: " + tab.getTxCcOxigenoterapia() + "\n");
		}

		if (!Validator.isBlankOrNull(tab.getTxCcApsViasAereas())) {
			strContextoClinico.append("Aspirações de vias aéreas: " + tab.getTxCcApsViasAereas() + "\n");
		}

		if (!Validator.isBlankOrNull(tab.getTxCcAcessoVenoso())) {
			strContextoClinico.append("Acesso Venoso: " + tab.getTxCcAcessoVenoso() + "\n");
		}

		if (!Validator.isBlankOrNull(tab.getTxCcAlimentacao())) {
			strContextoClinico.append("Alimentação: " + tab.getTxCcAlimentacao() + "\n");
		}

		if (!Validator.isBlankOrNull(tab.getTxCcCovid19())) {
			strContextoClinico.append("Covid-19: " + tab.getTxCcCovid19() + "\n");
		}

		if (strContextoClinico.length() > 2) {
			str.append("\nCONTEXTO CLÍNICO" + "\n");
			str.append(strContextoClinico);
		}

		String ef = "";
		if (!Validator.isBlankOrNull(tab.getCdCcEfNormal()))
			if (tab.getCdCcEfNormal().contains("on"))
				ef += "Normal - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCdCcEfAusencia()))
			if (tab.getCdCcEfAusencia().contains("on"))
				ef += "Ausência de controle esfincteriano (fraldas) - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCdCcEfControle()))
			if (tab.getCdCcEfControle().contains("on"))
				ef += "Controle esfincteriano, com uso de fraldas - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCdCcEfColostomia()))
			if (tab.getCdCcEfColostomia().contains("on"))
				ef += "Colostomia - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCdCcEfCistostomia()))
			if (tab.getCdCcEfCistostomia().contains("on"))
				ef += "Cistostomia - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCdCcEfNefrostomia()))
			if (tab.getCdCcEfNefrostomia().contains("on"))
				ef += "Nefrostomia - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCdCcEfSva()))
			if (tab.getCdCcEfSva().contains("on"))
				ef += "SVA - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCdCcEfSva()))
			if (tab.getCdCcEfSva().contains("on"))
				ef += "SVD - " + "\n";

		if (ef.length() > 1) {
			str.append("\nELIMINAÇÕES FISIOLÓGICAS" + "\n");
			str.append(ef.substring(0, ef.length() - 3) + "\n");
		}

		String mu = "";
		if (!Validator.isBlankOrNull(tab.getCdCcMuHabituais()))
			if (tab.getCdCcMuHabituais().contains("on"))
				mu += "Habituais sem novos medicamentos - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCdCcMuNovos()))
			if (tab.getCdCcMuNovos().contains("on"))
				mu += "Novos medicamentos VO/SNE/GTT - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCdCcMuNovos()))
			if (tab.getCdCcMuNovos().contains("on"))
				mu += "Medicamentos via Parenteral - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCdCcMuHipodermoclise()))
			if (tab.getCdCcMuHipodermoclise().contains("on"))
				mu += "Medicamentos via Hipodermóclise - " + "\n";

		if (mu.length() > 1) {
			str.append("\nMEDICAMENTOS EM USO\n");
			str.append(mu.substring(0, mu.length() - 3) + "\n");
		}

		StringBuilder strMedicamentos = new StringBuilder();
		if (!Validator.isBlankOrNull(tab.getTxCcMeAnticoagulantes())) {
			strMedicamentos.append("Anticoagulantes: " + tab.getTxCcMeAnticoagulantes() + "\n");
		}

		if (!Validator.isBlankOrNull(tab.getTxCcMeAntibiotico())) {
			strMedicamentos.append("Antibiótico: " + tab.getTxCcMeAntibiotico() + "\n");
		}

		if (!Validator.isBlankOrNull(tab.getTxCcMeQuimioterapicos())) {
			strMedicamentos.append("Quimioterapicos: " + tab.getTxCcMeQuimioterapicos() + " - Início: "
					+ tab.getDtCcMeInicioQuimioterapicos() + " Fim: " + tab.getDtCcMeFimQuimioterapicos() + "\n");
		}

		if (!Validator.isBlankOrNull(tab.getTxCcMeImunoglobulinas())) {
			strMedicamentos.append("Imunoglobulinas: " + tab.getTxCcMeImunoglobulinas() + " - Início: "
					+ tab.getDtCcMeInicioImunoglobulinas() + " Fim: " + tab.getDtCcMeFimImunoglobulinas() + "\n");
		}

		if (!Validator.isBlankOrNull(tab.getTxCcMeImunobiologico())) {
			strMedicamentos.append("Imunobiologico: " + tab.getTxCcMeImunobiologico() + " - Início: "
					+ tab.getDtCcMeInicioImunobiologico() + " Fim: " + tab.getDtCcMeFimImunobiologico() + "\n");
		}

		if (strMedicamentos.length() > 2) {
			str.append("\nMEDICAMENTOS ESPECÍFICOS" + "\n");
			str.append(strMedicamentos);
		}

		StringBuilder strRadioteratia = new StringBuilder();
		if (!Validator.isBlankOrNull(tab.getTxCcRadioterapia()))
			strRadioteratia.append("Radioterapia Atual/Concomitante: " + tab.getTxCcRadioterapia() + " - Início: "
					+ tab.getDtCcInicioRadioterapia() + " Fim: " + tab.getDtCcFimRadioterapia() + "\n");

		if (!Validator.isBlankOrNull(tab.getTxCcVos()))
			strRadioteratia.append("Curativo: " + tab.getTxCcVos() + " - " + tab.getTxCcVosObs() + "\n");

		if (!Validator.isBlankOrNull(tab.getTxCcEqMultiprofissional()))
			strRadioteratia.append("Equipe Multiprofissional: " + tab.getTxCcEqMultiprofissional() + "\n");

		if (!Validator.isBlankOrNull(tab.getTxCcOhb()))
			strRadioteratia.append("OHB: " + tab.getTxCcOhb() + " - OHB Externa: " + tab.getTxCcOhbExterna()
					+ " - Início: " + tab.getDtCcOhbInicio() + " Fim: " + tab.getDtCcOhbFim() + "\n");

		if (!Validator.isBlankOrNull(tab.getTxCcImppressaoGeral()))
			strRadioteratia.append("Impressão Geral: " + tab.getTxCcImppressaoGeral() + "\n");

		if (strRadioteratia.length() > 2) {
			str.append("\nRADIOTERAPIA ATUAL / CONCOMITANTE" + "\n");
			str.append(strRadioteratia);
		}

		StringBuilder strProgramacao = new StringBuilder();
		if (!Validator.isBlankOrNull(tab.getCkPtMedicoAcredita()))
			if (tab.getCkPtMedicoAcredita() == 1) {
				strProgramacao.append("O médico acredita que há condições clínicas para a alta hospitalar? SIM" + "\n");
			} else if (tab.getCkPtMedicoAcredita() == 2) {
				strProgramacao.append("O médico acredita que há condições clínicas para a alta hospitalar? NÃO" + "\n");
			}

		if (!Validator.isBlankOrNull(tab.getCkPtMedicoDeacordo()))
			if (tab.getCkPtMedicoDeacordo() == 1) {
				strProgramacao.append("Está de acordo com a conduta médica para desospitalização? SIM" + "\n");
			} else if (tab.getCkPtMedicoDeacordo() == 2) {
				strProgramacao.append("Está de acordo com a conduta médica para desospitalização? NÃO" + "\n");
			}

		if (!Validator.isBlankOrNull(tab.getCkPtMedicoProximasEtapas()))
			if (tab.getCkPtMedicoProximasEtapas() == 1) {
				strProgramacao
						.append("Saberia dizer as próximas etapas para que possa receber alta hospitalar? SIM" + "\n");
				strProgramacao.append("Estapas: " + tab.getTxPtMedicoProximasEtapas() + "\n");
			} else if (tab.getCkPtMedicoProximasEtapas() == 2) {
				strProgramacao
						.append("Saberia dizer as próximas etapas para que possa receber alta hospitalar? NÃO" + "\n");
			}

		if (!Validator.isBlankOrNull(tab.getTxPtMedicoExpectativas()))
			strProgramacao.append("Expectativas para a alta hospitalar: " + tab.getTxPtMedicoExpectativas() + "\n");

		if (!Validator.isBlankOrNull(tab.getCkPtMedicoAlgumProcedimento()))
			if (tab.getCkPtMedicoAlgumProcedimento() == 1) {
				strProgramacao.append("Algum procedimento ou terapia ainda pendente para realização? SIM" + "\n");
				strProgramacao.append("Procedimento: " + tab.getTxPtMedicoAlgumProcedimento() + "\n");
			} else if (tab.getCkPtMedicoAlgumProcedimento() == 2) {
				strProgramacao.append("Algum procedimento ou terapia ainda pendente para realização? NÃO" + "\n");
			}

		if (!Validator.isBlankOrNull(tab.getCkPtMedicoCienciaRecursos()))
			if (tab.getCkPtMedicoCienciaRecursos() == 1) {
				strProgramacao.append(
						"Tem ciência dos recursos equipe multi/equipamento/mobiliários para desospitalização? SIM"
								+ "\n");
				strProgramacao.append("Ciência: " + tab.getTxPtMedicoCienciaRecursos() + "\n");
			} else if (tab.getCkPtMedicoCienciaRecursos() == 2) {
				strProgramacao.append(
						"Tem ciência dos recursos equipe multi/equipamento/mobiliários para desospitalização? NÃO"
								+ "\n");
			}

		if (strProgramacao.length() > 2) {
			str.append("\nPROGRAMAÇÃO TERAPÊUTICA" + "\n");
			str.append(strProgramacao);
		}

		String par = "";
		if (!Validator.isBlankOrNull(tab.getCkDsEsposo()))
			if (tab.getCkDsEsposo().contains("on"))
				par += "Esposo(a) - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCkDsFilho()))
			if (tab.getCkDsFilho().contains("on"))
				par += "Filho(a) - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCkDsPai()))
			if (tab.getCkDsPai().contains("on"))
				par += "Pai - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCkDsMae()))
			if (tab.getCkDsMae().contains("on"))
				par += "Mãe - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCkDsIrmao()))
			if (tab.getCkDsIrmao().contains("on"))
				par += "Irmão(ã) - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCkDsSogro()))
			if (tab.getCkDsSogro().contains("on"))
				par += "Sogro(a) - " + "\n";

		if (!Validator.isBlankOrNull(tab.getCkDsOutros()))
			if (tab.getCkDsOutros().contains("on"))
				par += "Outros - " + "\n";

		if (par.length() > 1) {
			str.append("\nNÚMERO DE PESSOAS COM QUEM RESIDE" + "\n");
			str.append(par.substring(0, par.length() - 3) + "\n");
		}

		if (!Validator.isBlankOrNull(tab.getCkDsCuidador()))
			if (tab.getCkDsCuidador() == 1) {
				str.append("Cuidador? SIM - Qual? " + tab.getTxDsCuidador() + " - Parentesco: "
						+ tab.getTxDsCuidadorParentesco() + "\n");
			} else if (tab.getCkDsCuidador() == 2) {
				str.append("Cuidador? NÃO" + "\n");
			}

		if (!Validator.isBlankOrNull(tab.getCkDsPendenciaFamiliar()))
			if (tab.getCkDsPendenciaFamiliar() == 1) {
				str.append("Pendência Familiar? SIM - Qual? " + tab.getTxDsPendenciaFamiliar() + "\n");
			} else if (!Validator.isBlankOrNull(tab.getCkDsCuidador())) {
				if (tab.getCkDsCuidador() == 2) {
					str.append("Pendência Familiar? NÃO" + "\n");
				}
			}

		if (!Validator.isBlankOrNull(tab.getTxProbEncontrados())) {
			str.append("\nPROBLEMAS ENCONTRADOS" + "\n");
			str.append(tab.getTxProbEncontrados() + "\n");
		}

		return str.toString();
	}

	// Modelo

	// Nivel de Consciencia
	@Autowired
	private TabNivelConscienciaRepository tabNivelConscienciaRepository;

	@ModelAttribute("selectcdnivelconsciencia")
	public List<TabNivelConscienciaObj> selectcdnivelconsciencia() {
		return tabNivelConscienciaRepository.findAll();
	}

	// Sinais Vitais
	@Autowired
	private TabSinaisVitaisRepository tabSinaisVitaisRepository;

	@ModelAttribute("selectcdsinaisvitais")
	public List<TabSinaisVitaisObj> selectcdsinaisvitais() {
		return tabSinaisVitaisRepository.findAll();
	}

	// GG Geral
	@Autowired
	private TabCcGeralRepository tabCcGeralRepository;

	@ModelAttribute("selectcdccgeral")
	public List<TabCcGeralObj> selectcdccgeral() {
		return tabCcGeralRepository.findAll();
	}

	@Autowired
	private TabOxigenoterapiaRepository TabOxigenoterapiaRepository;

	@ModelAttribute("selectcdoxigenoterapia")
	public List<TabOxigenoterapiaObj> selectcdoxigenoterapia() {
		return TabOxigenoterapiaRepository.findAll();
	}

	@Autowired
	private TabAspiracoesViasAereasRepository tabAspiracoesViasAereasRepository;

	@ModelAttribute("selectcdccapsviasaereas")
	public List<TabAspiracoesViasAereasObj> selectcdccapsviasaereas() {
		return tabAspiracoesViasAereasRepository.findAll();
	}

	@Autowired
	private TabAcessoVenosoRepository tabAcessoVenosoRepository;

	@ModelAttribute("selectcdccacessovenoso")
	public List<TabAcessoVenosoObj> selectcdccacessovenoso() {
		return tabAcessoVenosoRepository.findAll();
	}

	@Autowired
	private TabAlimentacaoRepository tabAlimentacaoRepository;

	@ModelAttribute("selectcdccalimentacao")
	public List<TabAlimentacaoObj> selectcdccalimentacao() {
		return tabAlimentacaoRepository.findAll();
	}

	@Autowired
	private TabAnticoagulantesRepository tabAnticoagulantesRepository;

	@ModelAttribute("selectcdccmeanticoagulantes")
	public List<TabAnticoagulantesObj> selectcdccmeanticoagulantes() {
		return tabAnticoagulantesRepository.findAll();
	}

	@Autowired
	private TabAntibioticoRepository tabAntibioticoRepository;

	@ModelAttribute("selectcdccmeantibiotico")
	public List<TabAntibioticoObj> selectcdccmeantibiotico() {
		return tabAntibioticoRepository.findAll();
	}

	@Autowired
	private TabQuimioterapicosRepository tabQuimioterapicosRepository;

	@ModelAttribute("selectcdccmequimioterapicos")
	public List<TabQuimioterapicosObj> selectcdccmequimioterapicos() {
		return tabQuimioterapicosRepository.findAll();
	}

	@Autowired
	private TabImunoglobulinasRepository tabImunoglobulinasRepository;

	@ModelAttribute("selectcdccmeimunoglobulinas")
	public List<TabImunoglobulinasObj> selectcdccmeimunoglobulinas() {
		return tabImunoglobulinasRepository.findAll();
	}

	@Autowired
	private TabImunobiologicoRepository tabImunobiologicoRepository;

	@ModelAttribute("selectcdccmeimunobiologico")
	public List<TabImunobiologicoObj> selectcdccmeimunobiologico() {
		return tabImunobiologicoRepository.findAll();
	}

	@Autowired
	private TabRadioterapiaRepository tabRadioterapiaRepository;

	@ModelAttribute("selectcdccradioterapia")
	public List<TabRadioterapiaObj> selectcdccradioterapia() {
		return tabRadioterapiaRepository.findAll();
	}

	@Autowired
	private TabCurativosRepository tabCurativosRepository;

	@ModelAttribute("selectcdccvos")
	public List<TabCurativosObj> selectcdccvos() {
		return tabCurativosRepository.findAll();
	}

	@Autowired
	private TabMultiprofissionalRepository tabMultiprofissionalRepository;

	@ModelAttribute("selectcdcceqmultiprofissional")
	public List<TabMultiprofissionalObj> selectcdcceqmultiprofissional() {
		return tabMultiprofissionalRepository.findAll();
	}

	@Autowired
	private TabOhbRepository tabOhbRepository;

	@ModelAttribute("selectcdccohb")
	public List<TabOhbObj> selectcdccohb() {
		return tabOhbRepository.findAll();
	}

	@Autowired
	private TabOhbExternaRepository tabOhbExternaRepository;

	@ModelAttribute("selectcdccohbexterna")
	public List<TabOhbExternaObj> selectcdccohbexterna() {
		return tabOhbExternaRepository.findAll();
	}

	@Autowired
	private TabImpressaoGeralRepository tabImpressaoGeralRepository;

	@ModelAttribute("selectcdccimppressaogeral")
	public List<TabImpressaoGeralObj> selectcdccimppressaogeral() {
		return tabImpressaoGeralRepository.findAll();
	}

	@Autowired
	private TabExpectativaAltaRepository tabExpectativaAltaRepository;

	@ModelAttribute("selectcdptmedicoexpectativas")
	public List<TabExpectativaAltaObj> selectcdptmedicoexpectativas() {
		return tabExpectativaAltaRepository.findAll();
	}

	@Autowired
	private TabCienciaRecursosRepository tabCienciaRecursosRepository;

	@ModelAttribute("selectcdptmedicocienciarecursos")
	public List<TabCienciaRecursosObj> selectcdptmedicocienciarecursos() {
		return tabCienciaRecursosRepository.findAll();
	}

	@Autowired
	private TabCuidadorTipoRepository tabCuidadorTipoRepository;

	@ModelAttribute("selectcddscuidador")
	public List<TabCuidadorTipoObj> selectcddscuidador() {
		return tabCuidadorTipoRepository.findAll();
	}

	@Autowired
	private TabStatusRepository tabStatusRepository;

	@ModelAttribute("selectcdstatus")
	public List<TabStatusObj> selectcdstatus() {
		return tabStatusRepository.findAll();
	}

	@Autowired
	private TabCovid19Repository tabCovid19Repository;

	@ModelAttribute("selectcdcovid")
	public List<TabCovid19Obj> selectcdcovid() {
		return tabCovid19Repository.findAll();
	}

	@Autowired
	private TabProbEncontradosRepository tabProbEncontradosRepository;

	@ModelAttribute("selectcdprobencontrados")
	public List<TabProbEncontradosObj> selectcdprobencontrados() {
		return tabProbEncontradosRepository.findAll();
	}

	@Autowired
	private TabLocalPacienteRepository tabLocalPacienteRepository;

	@ModelAttribute("selectcdlocalpaciente")
	public List<TabLocalPacienteObj> selectcdlocalpaciente() {
		return tabLocalPacienteRepository.findAll();
	}

	@Autowired
	private TabInternacoesAnoRepository tabInternacoesAnoRepository;

	@ModelAttribute("selectcdinternacoesano")
	public List<TabInternacoesAnoObj> selectcdinternacoesano() {
		return tabInternacoesAnoRepository.findAll();
	}

	@Autowired
	private TabTipoDesconfortoPosicaoRepository tabTipoDesconfortoPosicaoRepository;

	@ModelAttribute("selectcddesconfortoposicao")
	public List<TabTipoDesconfortoPosicaoObj> selectcddesconfortoposicao() {
		return tabTipoDesconfortoPosicaoRepository.findAll();
	}

	@Autowired
	private TabTipoContatoPeleUrinaRepository tabTipoContatoPeleUrinaRepository;

	@ModelAttribute("selectcdcontatopeleurina")
	public List<TabTipoContatoPeleUrinaObj> selectcdcontatopeleurina() {
		return tabTipoContatoPeleUrinaRepository.findAll();
	}

	@Autowired
	private TabTipoLesoesPeleRepository tabTipoLesoesPeleRepository;

	@ModelAttribute("selectcdtipolesoes")
	public List<TabTipoLesoesPeleObj> selectcdtipolesoes() {
		return tabTipoLesoesPeleRepository.findAll();
	}

	@Autowired
	private TabTipoAndarCamaRepository tabTipoAndarCamaRepository;

	@ModelAttribute("selectcdpacienteandacama")
	public List<TabTipoAndarCamaObj> selectcdpacienteandacama() {
		return tabTipoAndarCamaRepository.findAll();
	}

	@Autowired
	private TabTipoAcamadoCadeiraRodasRepository tabTipoAcamadoCadeiraRodasRepository;

	@ModelAttribute("selectcdpacienteacamadocadeirarodas")
	public List<TabTipoAcamadoCadeiraRodasObj> selectcdpacienteacamadocadeirarodas() {
		return tabTipoAcamadoCadeiraRodasRepository.findAll();
	}

	@Autowired
	private TabTipoCamaCadeiraImovelRepository tabTipoCamaCadeiraImovelRepository;

	@ModelAttribute("selectcdpacientecamacadeiraimovel")
	public List<TabTipoCamaCadeiraImovelObj> selectcdpacientecamacadeiraimovel() {
		return tabTipoCamaCadeiraImovelRepository.findAll();
	}

	@Autowired
	private TabTipoMovimentacaoCamaRepository tabTipoMovimentacaoCamaRepository;

	@ModelAttribute("selectcdpacientemovimentocama")
	public List<TabTipoMovimentacaoCamaObj> selectcdpacientemovimentocama() {
		return tabTipoMovimentacaoCamaRepository.findAll();
	}

	@Autowired
	private TabTipoBanhoRepository tabTipoBanhoRepository;

	@ModelAttribute("selectcdpacientetipobanho")
	public List<TabTipoBanhoObj> selectcdpacientetipobanho() {
		return tabTipoBanhoRepository.findAll();
	}

	@Autowired
	private TabAlimentacaoQualidadeRepository tabAlimentacaoQualidadeRepository;

	@ModelAttribute("selectcdpacientealimentacaoqualidade")
	public List<TabAlimentacaoQualidadeObj> selectcdpacientealimentacaoqualidade() {
		return tabAlimentacaoQualidadeRepository.findAll();
	}

	@Autowired
	private TabAlimentacaoTiposRepository tabAlimentacaoTiposRepository;

	@ModelAttribute("selectcdpacientealimentacaocomoatualmente")
	public List<TabAlimentacaoTiposObj> selectcdpacientealimentacaocomoatualmente() {
		return tabAlimentacaoTiposRepository.findAll();
	}

	@Autowired
	private TabAlimentacaoEstadoNutricionalRepository tabAlimentacaoEstadoNutricionalRepository;

	@ModelAttribute("selectcdpacientealimentacaoestadonutricional")
	public List<TabAlimentacaoEstadoNutricionalObj> selectcdpacientealimentacaoestadonutricional() {
		return tabAlimentacaoEstadoNutricionalRepository.findAll();
	}

	@Autowired
	private TabAlimentacaoFormaMedicamentosRepository tabAlimentacaoFormaMedicamentosRepository;

	@ModelAttribute("selectcdpacientealimentacaotipomedicamentos")
	public List<TabAlimentacaoFormaMedicamentosObj> selectcdpacientealimentacaotipomedicamentos() {
		return tabAlimentacaoFormaMedicamentosRepository.findAll();
	}

	@Autowired
	private TabAlimentacaoParenteralRepository tabAlimentacaoParenteralRepository;

	@ModelAttribute("selectcdpacientetipoalimentacaoparenteral")
	public List<TabAlimentacaoParenteralObj> selectcdpacientetipoalimentacaoparenteral() {
		return tabAlimentacaoParenteralRepository.findAll();
	}

	@Autowired
	private TabTipoVestirRepository tabTipoVestirRepository;

	@ModelAttribute("selectcdpacientemodovestiratualmente")
	public List<TabTipoVestirObj> selectcdpacientemodovestiratualmente() {
		return tabTipoVestirRepository.findAll();
	}

	@Autowired
	private TabTipoIdaBanheiroRepository tabTipoIdaBanheiroRepository;

	@ModelAttribute("selectcdpacientemodobanheiroatualmente")
	public List<TabTipoIdaBanheiroObj> selectcdpacientemodobanheiroatualmente() {
		return tabTipoIdaBanheiroRepository.findAll();
	}

	@Autowired
	private TabTipoControleBanheiroRepository tabTipoControleBanheiroRepository;

	@ModelAttribute("selectcdpacientevontadebanheiroatualmente")
	public List<TabTipoControleBanheiroObj> selectcdpacientevontadebanheiroatualmente() {
		return tabTipoControleBanheiroRepository.findAll();
	}

	@Autowired
	private TabTipoAspiracaoSondaRepository tabTipoAspiracaoSondaRepository;

	@ModelAttribute("selectcdpacienteaspiradosonda")
	public List<TabTipoAspiracaoSondaObj> selectcdpacienteaspiradosonda() {
		return tabTipoAspiracaoSondaRepository.findAll();
	}

	@Autowired
	private TabTipoAspiracaoBocaNarizRepository tabTipoAspiracaoBocaNarizRepository;

	@ModelAttribute("selectcdpacienteaspiradobocanariz")
	public List<TabTipoAspiracaoBocaNarizObj> selectcdpacienteaspiradobocanariz() {
		return tabTipoAspiracaoBocaNarizRepository.findAll();
	}

	@Autowired
	private TabTipoExeciciosVentilatoriosRepository tabTipoExeciciosVentilatoriosRepository;

	@ModelAttribute("selectcdpacienteexerciciosventilatorios")
	public List<TabTipoExeciciosVentilatoriosObj> selectcdpacienteexerciciosventilatorios() {
		return tabTipoExeciciosVentilatoriosRepository.findAll();
	}

	@Autowired
	private TabTipoUsoOxigenioRepository tabTipoUsoOxigenioRepository;

	@ModelAttribute("selectcdpacienteutilizandooxigenio")
	public List<TabTipoUsoOxigenioObj> selectcdpacienteutilizandooxigenio() {
		return tabTipoUsoOxigenioRepository.findAll();
	}

	@Autowired
	private TabUsoMedicacaoParentealRepository tabUsoMedicacaoParentealRepository;

	@ModelAttribute("selectcdpacientemedicacaoveia")
	public List<TabUsoMedicacaoParentealObj> selectcdpacientemedicacaoveia() {
		return tabUsoMedicacaoParentealRepository.findAll();
	}

	@Autowired
	private TabMedicacoesAdministradasRepository tabMedicacoesAdministradasRepository;

	@ModelAttribute("selectcdpacientemedicacaoadministradas")
	public List<TabMedicacoesAdministradasObj> selectcdpacientemedicacaoadministradas() {
		return tabMedicacoesAdministradasRepository.findAll();
	}
	
	@Autowired
	private TabTipoKatzRepository tabTipoKatzRepository;

	@ModelAttribute("selectcdpacientekatz")
	public List<TabTipoKatzObj> selectcdpacientekatz() {
		return tabTipoKatzRepository.findAll();
	}
	
	
	@Autowired
	private TabPendenciaTratamentoRepository TabPendenciaTratamentoRepository;

	@ModelAttribute("selectcdpacientependenciatratamento")
	public List<TabPendenciaTratamentoObj> selectcdpacientependenciatratamento() {
		return TabPendenciaTratamentoRepository.findAll();
	}
	
	
	@Autowired
	private TabNecessidadeAposAltaRepository tabNecessidadeAposAltaRepository;

	@ModelAttribute("selectcdpacientenecessidadesaposalta")
	public List<TabNecessidadeAposAltaObj> selectcdpacientenecessidadesaposalta() {
		return tabNecessidadeAposAltaRepository.findAll();
	}
	
	@Autowired
	private TabEmpresaHomecareRepository tabEmpresaHomecareRepository;

	@ModelAttribute("selectckpacienteempresahomecare")
	public List<TabEmpresaHomecareObj> selectckpacienteempresahomecare() {
		return tabEmpresaHomecareRepository.findAll();
	}

}
