package br.com.gohosp.modulos.tratativas.service;

import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.gohosp.dao.TransactionUtilBean;
import br.com.gohosp.dao.tratativas.TabEnviosObj;
import br.com.gohosp.dao.tratativas.TabPacienteObj;
import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaCapaObj;
import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaObj;
import br.com.gohosp.dao.tratativas.repository.TabEnviosRepository;
import br.com.gohosp.dao.tratativas.repository.TabPacienteRepository;
import br.com.gohosp.dao.tratativas.repository.TabRetornoConsultaTratativaCapaRepository;
import br.com.gohosp.dao.tratativas.repository.TabRetornoConsultaTratativaRepository;
import br.com.gohosp.soap.SulAmericaService;
import br.com.gohosp.soap.model.tratativa.AtualizarTratativa;
import br.com.gohosp.soap.model.tratativa.AtualizarTratativaResponse;
import br.com.gohosp.soap.model.tratativa.ConfirmarRecebimento;
import br.com.gohosp.soap.model.tratativa.ConfirmarRecebimentoResponse;
import br.com.gohosp.soap.model.tratativa.ConsultaTratativa;
import br.com.gohosp.soap.model.tratativa.ConsultaTratativaResponse;
import br.com.gohosp.soap.model.tratativa.EntradaAtualizarTratativa;
import br.com.gohosp.soap.model.tratativa.ObjectFactory;
import br.com.gohosp.soap.model.tratativa.RetornoConsultaTratativa;
import br.com.gohosp.util.GeneralParser;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.logErro.JTPLogErroBean;

@Service
public class TratativasService {

	@Autowired
	private SulAmericaService sulAmericaService;

	@Autowired
	private TabRetornoConsultaTratativaRepository tabRetornoConsultaTratativaRepository;

	@Autowired
	private TabRetornoConsultaTratativaCapaRepository tabRetornoConsultaTratativaCapaRepository;

	@Autowired
	private TransactionUtilBean transactionUtilBean;

	@Autowired
	private JTPLogErroBean jtpLogErroBean;

	@Autowired
	private TabEnviosRepository tabEnviosRepository;

	@Autowired
	private TabPacienteRepository tabPacienteRepository;

	@Scheduled(cron = "0 0/20 6-23 * * MON-SAT")
	public void verificaTratativas() {
		// System.out.println(new Date());
		String txTratativaId = "";
		String txMsgId = "";
		try {																		

			//if (GeneralUtil.getProducao()) {

				ConsultaTratativa consultaTratativa = new ConsultaTratativa();
				ConsultaTratativaResponse consultaTratativaResponse = sulAmericaService
						.consultaTratativa(consultaTratativa);

				List<RetornoConsultaTratativa> t2 = consultaTratativaResponse.getResult();

				for (RetornoConsultaTratativa atual : t2) {

					txTratativaId = atual.getTratativaId().getValue();
					txMsgId = atual.getMsgId().getValue();
					//System.out.println(atual.getDataSolicitacao().getValue());

					Date dtInicial = GeneralParser.retornaDataAnteriorDiasCorridos(new Date(), 5);
					Date dtSolicitacao = GeneralParser.parseDate(atual.getDataSolicitacao().getValue());
					
					if (dtSolicitacao.getTime() > dtInicial.getTime()) { 
						System.out.println(atual.getDataSolicitacao().getValue());
					List<TabRetornoConsultaTratativaObj> TabList = tabRetornoConsultaTratativaRepository
							.findByTxTratativaIdAndTxMsgIdOrderByTxTratativaIdAsc(atual.getTratativaId().getValue(),
									atual.getMsgId().getValue());

					if (TabList.isEmpty()) {

						Integer cdSequencia = retornaSequencia(atual.getTratativaId().getValue());

						TabRetornoConsultaTratativaObj Tab = new TabRetornoConsultaTratativaObj();

						// System.out.println(atual.getTratativaId().getValue());
						// System.out.println(atual.getNomeSegurado().getValue());
						// System.out.println(atual.getDataNascimento().getValue());
						// System.out.println(atual.getDataIncioVPP().getValue());
						// System.out.println(atual.getDataSolicitacao().getValue());
						// System.out.println(atual.getCpfSegurado().getValue());
						 
						
						Tab.setCdCodigo(null);
						Tab.setTxTratativaId(atual.getTratativaId().getValue());
						Tab.setTxAssunto(atual.getAssunto().getValue());
						Tab.setTxCarteira(atual.getCarteira().getValue());
						Tab.setTxCodSegurado(atual.getCodSegurado().getValue());
						Tab.setTxCpfSegurado(atual.getCpfSegurado().getValue());
						Tab.setDtDataIncioVpp(atual.getDataIncioVPP().getValue() != null
								? GeneralParser.parseDate(atual.getDataIncioVPP().getValue())
								: null);
						Tab.setDtDataNascimento(atual.getDataNascimento().getValue() != null
								? GeneralParser.parseDate(atual.getDataNascimento().getValue())
								: null);
						Tab.setDtDataSolicitacao(atual.getDataSolicitacao().getValue() != null
								? GeneralParser.parseDate(atual.getDataSolicitacao().getValue())
								: null);
						Tab.setTxEmpresa(atual.getEmpresa().getValue());
						Tab.setTxEstado(atual.getEstado().getValue());
						Tab.setTxMensagem(atual.getMensagem().getValue());
						Tab.setTxMsgId(atual.getMsgId().getValue());
						Tab.setTxNomeSegurado(atual.getNomeSegurado().getValue());
						Tab.setTxNumVpp(atual.getNumVPP().getValue());
						Tab.setTxPrestador(atual.getPrestador().getValue());
						Tab.setTxProdSaude(atual.getProdSaude().getValue());
						Tab.setTxSexo(atual.getSexo().getValue());
						Tab.setTxSlaTratativa(atual.getSlaTratativa().getValue());
						Tab.setTxTelCelular(atual.getTelCelular().getValue());
						Tab.setTxTelCobranca(atual.getTelCobranca().getValue());
						Tab.setTxTelComercial(atual.getTelComercial().getValue());
						Tab.setTxTelContato(atual.getTelContato().getValue());
						Tab.setTxTelContato2(atual.getTelContato2().getValue());
						Tab.setTxTelContato3(atual.getTelContato3().getValue());
						Tab.setTxTelPrestador(atual.getTelPrestador().getValue());
						Tab.setTxTelResencial(atual.getTelResencial().getValue());
						Tab.setCkConfirmRecebimento(0);
						Tab.setCkEncerrado(0);
						Tab.setCdSequencia(cdSequencia);
						Tab.setCdTipo(1);

						if (atual.getMensagem().getValue().toUpperCase().contains("#COVID")) {
							Tab.setCdCcCovid19(1);
							System.out.println(txTratativaId + "/" + txMsgId + " - Covid");

						}

						if (cdSequencia > 1) {
							TabRetornoConsultaTratativaObj TabView = tabRetornoConsultaTratativaRepository
									.findByTxTratativaIdAndCdSequencia(atual.getTratativaId().getValue(), 1);

							if (TabView != null) {
								Tab.setTxTelCelular(TabView.getTxTelCelular());
								Tab.setTxNomeResponsavel(TabView.getTxNomeResponsavel());
								Tab.setTxEmailResponsavel(TabView.getTxEmailResponsavel());
								Tab.setTxTelHospital(TabView.getTxTelHospital());
								Tab.setTxCondTriagem(TabView.getTxCondTriagem());
								Tab.setTxRealizouContato(TabView.getTxRealizouContato());
								Tab.setDtPrimContato(TabView.getDtPrimContato());
								Tab.setTxDesfecho(TabView.getTxDesfecho());
								Tab.setTxMedico(TabView.getTxMedico());
								Tab.setTxNomeContato(TabView.getTxNomeContato());
								Tab.setTxGrauParentesco(TabView.getTxGrauParentesco());
								Tab.setDtInternacao(TabView.getDtInternacao());
								Tab.setTxMotivoNaoContato(TabView.getTxMotivoNaoContato());
								Tab.setTxTipoInternacao(TabView.getTxTipoInternacao());
								Tab.setTxCid(TabView.getTxCid());
								Tab.setTxDescCid(TabView.getTxDescCid());
								Tab.setTxMotivoAlta(TabView.getTxMotivoAlta());
								Tab.setTxTipoAssistencia(TabView.getTxTipoAssistencia());
								Tab.setCdTipo(1);

								String txMensagem = atual.getMensagem().getValue().toUpperCase();
 								if (txMensagem.contains("#FINALIZA")) {
									if (baixaTratativaTipoAlta(txTratativaId, cdSequencia, 2)) {
										Tab.setCkEncerrado(1);
										Tab.setCdStatus(8);
										Tab.setTxNomeUsuario("Sulamérica automático");
										Tab.setTxMensagemResposta("Finalizada Tratativa com status automático!");

										if (txMensagem.contains("#COVID")) {
											Tab.setCdCcCovid19(1);
										}

										baixatratativacapa(txTratativaId);
									}
									System.out.println(txTratativaId + "/" + txMsgId + " - Finalizada");

								} 

							}
						}

						Tab = tabRetornoConsultaTratativaRepository.save(Tab);

						if (cdSequencia == 1) {

							TabRetornoConsultaTratativaCapaObj TabCapa = new TabRetornoConsultaTratativaCapaObj();
							TabCapa.setTxTratativaId(txTratativaId);
							TabCapa.setDtDataIncioVpp(Tab.getDtDataIncioVpp());
							TabCapa.setDtDataNascimento(Tab.getDtDataNascimento());
							TabCapa.setDtDataSolicitacao(Tab.getDtDataSolicitacao());
							TabCapa.setTxAssunto(Tab.getTxAssunto());
							TabCapa.setTxCarteira(Tab.getTxCarteira());
							TabCapa.setTxCodSegurado(Tab.getTxCodSegurado());
							TabCapa.setTxCpfSegurado(Tab.getTxCpfSegurado());
							TabCapa.setTxEmpresa(Tab.getTxEmpresa());
							TabCapa.setTxEstado(Tab.getTxEstado());
							TabCapa.setTxNomeSegurado(Tab.getTxNomeSegurado());
							TabCapa.setTxNumVpp(Tab.getTxNumVpp());
							TabCapa.setTxPrestador(Tab.getTxPrestador());
							TabCapa.setTxProdSaude(Tab.getTxProdSaude());
							TabCapa.setTxSexo(Tab.getTxSexo());
							TabCapa.setTxSlaTratativa(Tab.getTxSlaTratativa());
							TabCapa.setTxTelCelular(Tab.getTxTelCelular());
							TabCapa.setTxTelCobranca(Tab.getTxTelCobranca());
							TabCapa.setTxTelComercial(Tab.getTxTelComercial());
							TabCapa.setTxTelContato(Tab.getTxTelContato());
							TabCapa.setTxTelContato2(Tab.getTxTelContato2());
							TabCapa.setTxTelContato3(Tab.getTxTelContato3());
							TabCapa.setTxTelPrestador(Tab.getTxTelPrestador());
							TabCapa.setTxTelResencial(Tab.getTxTelResencial());
							String txSla = GeneralUtil.TiraNaonumero(Tab.getTxSlaTratativa());
							if (!Validator.isBlankOrNull(txSla)) {
								TabCapa.setDtSlaTratativa(GeneralParser.retornaDataPosteriorDiasCorridos(
										Tab.getDtDataSolicitacao(), GeneralParser.parseInt(txSla)));
							}

							tabRetornoConsultaTratativaCapaRepository.save(TabCapa);
						} else {

							TabRetornoConsultaTratativaCapaObj TabCapa = tabRetornoConsultaTratativaCapaRepository
									.findByTxTratativaId(txTratativaId);

							if (TabCapa != null) {
								if (TabCapa.getCdStatusTratativa() == 2) { // Se estiver fechada
									System.out.println("Reabriu: " + txTratativaId);
									TabCapa.setCdStatusTratativa(1);
									tabRetornoConsultaTratativaCapaRepository.save(TabCapa);
								}
							}
						}

						// Paciente
						TabPacienteObj TabPac = tabPacienteRepository.findByTxCpfSegurado(Tab.getTxCpfSegurado());

						if (TabPac == null) {
							TabPac = new TabPacienteObj();
							TabPac.setDtDataNascimento(Tab.getDtDataNascimento());
							TabPac.setTxCarteira(Tab.getTxCarteira());
							TabPac.setTxCodSegurado(Tab.getTxCodSegurado());
							TabPac.setTxCpfSegurado(Tab.getTxCpfSegurado());
							TabPac.setTxNomeSegurado(Tab.getTxNomeSegurado());
							TabPac.setTxProdSaude(Tab.getTxProdSaude());
							TabPac.setTxSexo(Tab.getTxSexo());
							TabPac.setTxTelCelular(Tab.getTxTelCelular());
							TabPac.setTxTelCobranca(Tab.getTxTelCobranca());
							TabPac.setTxTelComercial(Tab.getTxTelComercial());
							TabPac.setTxTelContato(Tab.getTxTelContato());
							TabPac.setTxTelContato2(Tab.getTxTelContato2());
							TabPac.setTxTelContato3(Tab.getTxTelContato3());
							TabPac.setTxTelPrestador(Tab.getTxTelPrestador());
							TabPac.setTxTelResencial(Tab.getTxTelResencial());
							TabPac.setCdStatusPaciente(1);
							TabPac.setDtUltimaAtualizacao(new Date());
							tabPacienteRepository.save(TabPac);
						}

						System.out.println(
								atual.getTratativaId().getValue() + "/" + atual.getMsgId().getValue() + " incluido!");
				    				
					} else {
						// System.out.println(atual.getTratativaId().getValue()+"/"+atual.getMsgId().getValue()+"
						// duplicado!");
					} 
				  }else { //If comparação de datas
					  confirmaRecebimentoTratativas(atual.getMsgId().getValue());
				  }
				}
				if (!t2.isEmpty()) {
					confirmaRecebimentoTratativas();
				}
			//}
		} catch (Exception ex) {
			System.out.println(
					txTratativaId + "/" + txMsgId + " - " + ex.getClass().getSimpleName() + " - " + ex.getMessage());
			jtpLogErroBean.addLog(ex.getClass().getSimpleName(), ex.getMessage(), ex);
		}

	}

	private boolean baixaTratativaTipoAlta(String txTratativaId, Integer cdSequencia, Integer cdTipo) {

		// 1 - Alta/Óbito; 2 - Finalizar

		try {

			List<TabRetornoConsultaTratativaObj> listTratativa = tabRetornoConsultaTratativaRepository
					.findByTratativasNotInSequenciaNotEncerradoQuery(txTratativaId, cdSequencia);

			for (TabRetornoConsultaTratativaObj atual : listTratativa) {

				TabRetornoConsultaTratativaObj Tab = tabRetornoConsultaTratativaRepository
						.findByTxTratativaIdAndTxMsgId(txTratativaId, atual.getTxMsgId());
				Tab.setCdStatus(9);
				Tab.setTxNomeUsuario("Sulamérica automático");
				Tab.setCkEncerrado(1);
				Tab.setDtNovoContato(null);
				Tab.setVlQtdeDiasNovoContato(null);
				if (cdTipo == 1) {
					Tab.setTxMensagemResposta(
							atual.getTxMensagemResposta() + " - Alta/Óbito automático pela Sulamerica!");
				} else {
					Tab.setTxMensagemResposta(
							atual.getTxMensagemResposta() + " - Finalização automática pela Sulamerica!");
				}
				tabRetornoConsultaTratativaRepository.save(Tab);

			}

			return true;
		} catch (Exception ex) {
			jtpLogErroBean.addLog(ex.getClass().getSimpleName(), ex.getMessage(), ex);
			return false;
		}
	}

	private void baixaTratativaTratativaAnterior(String txTratativaId, Integer cdSequencia) {

		try {

			List<TabRetornoConsultaTratativaObj> listTratativa = tabRetornoConsultaTratativaRepository
					.findByTratativasMenorSequenciaNotEncerradoQuery(txTratativaId, cdSequencia);

			for (TabRetornoConsultaTratativaObj atual : listTratativa) {

				TabRetornoConsultaTratativaObj Tab = tabRetornoConsultaTratativaRepository
						.findByTxTratativaIdAndTxMsgId(txTratativaId, atual.getTxMsgId());
				Tab.setCdStatus(8);
				Tab.setTxNomeUsuario("Sulamérica automático");
				Tab.setCkEncerrado(1);
				Tab.setDtNovoContato(null);
				Tab.setTxMensagemResposta("Tratativa com status automático!");
				tabRetornoConsultaTratativaRepository.save(Tab);

			}

		} catch (Exception ex) {
			jtpLogErroBean.addLog(ex.getClass().getSimpleName(), ex.getMessage(), ex);

		}

	}

	private void baixatratativacapa(String txTratativaId) {

		TabRetornoConsultaTratativaCapaObj Tab = tabRetornoConsultaTratativaCapaRepository
				.findByTxTratativaId(txTratativaId);

		if (Tab != null) {

			Tab.setCdStatusTratativa(2);
			Tab.setCdUsuario(20);
			Tab.setTxUsuario("Sulamérica");
			tabRetornoConsultaTratativaCapaRepository.save(Tab);

			System.out.println(txTratativaId + "/baixa capa tratativa");

		}

	}

	private Integer retornaSequencia(String txTratativaId) {

		List list = transactionUtilBean.querySelect(
				"select count(*) from TabRetornoConsultaTratativaObj where txTratativaId = '" + txTratativaId + "'");

		Integer cdSequencia = 1;
		if (!list.isEmpty()) {
			cdSequencia = GeneralParser.parseInt(list.get(0).toString()) + 1;
		}

		return cdSequencia;
	}

	public void confirmaRecebimentoTratativas() {

		try {

			List<TabRetornoConsultaTratativaObj> Tab = tabRetornoConsultaTratativaRepository
					.findByCkConfirmRecebimento(0);
			ConfirmarRecebimento confirmarRecebimento = new ConfirmarRecebimento();

			for (TabRetornoConsultaTratativaObj atual : Tab) {
				confirmarRecebimento.getMsgId().add(atual.getTxMsgId());
				// Thread.sleep(1000);
			}

			ConfirmarRecebimentoResponse confirmarRecebimentoResponse = sulAmericaService
					.confirmarRecebimento(confirmarRecebimento);

			// Seta confirmação de recebimento
			transactionUtilBean.queryInsertUpdate(
					"update TabRetornoConsultaTratativaObj set ckConfirmRecebimento = 1, dtConfirmRecebimento = now() where ckConfirmRecebimento = 0");

		} catch (Exception ex) {

			jtpLogErroBean.addLog(ex.getClass().getSimpleName(), ex.getMessage(), ex);

		}

	}

	public void confirmaRecebimentoTratativas(String txMsgId) {

		try {

			//List<TabRetornoConsultaTratativaObj> Tab = tabRetornoConsultaTratativaRepository
			//		.findByCkConfirmRecebimento(0);
			ConfirmarRecebimento confirmarRecebimento = new ConfirmarRecebimento();

			//for (TabRetornoConsultaTratativaObj atual : Tab) {
				confirmarRecebimento.getMsgId().add(txMsgId);
				// Thread.sleep(1000);
			//}

			ConfirmarRecebimentoResponse confirmarRecebimentoResponse = sulAmericaService
					.confirmarRecebimento(confirmarRecebimento);

			// Seta confirmação de recebimento
			transactionUtilBean.queryInsertUpdate(
					"update TabRetornoConsultaTratativaObj set ckConfirmRecebimento = 1, dtConfirmRecebimento = now() where txMsgId = '"+txMsgId+"'");

		} catch (Exception ex) {

			jtpLogErroBean.addLog(ex.getClass().getSimpleName(), ex.getMessage(), ex);

		}

	}

	public void atualizarTratativa() {
		try {
			AtualizarTratativa atualizarTratativa = new AtualizarTratativa();
			List<TabRetornoConsultaTratativaObj> Tab = tabRetornoConsultaTratativaRepository
					.findByCkEnviarAtualizacao(1);
			for (TabRetornoConsultaTratativaObj atual : Tab) {
				// Setar os campos entro do metodo buildEntradaAtualizaTratativa
				EntradaAtualizarTratativa entradaAtualizarTratativa = buildEntradaAtualizaTratativa(atual);
				atualizarTratativa.getMensagens().add(entradaAtualizarTratativa);
			}

			AtualizarTratativaResponse atualizarTratativaResponse = sulAmericaService
					.atualizarTratativa(atualizarTratativa);

		} catch (Exception e) {
			jtpLogErroBean.addLog(e.getClass().getSimpleName(), e.getMessage(), e);
		}
	}

	public String atualizarTratativa(Integer cdCodigo) {

		String txOk = "OK";
		boolean ckEnvia = false;
		try {
			AtualizarTratativa atualizarTratativa = new AtualizarTratativa();
			List<TabRetornoConsultaTratativaObj> Tab = tabRetornoConsultaTratativaRepository.findByCdCodigo(cdCodigo);
			for (TabRetornoConsultaTratativaObj atual : Tab) {

				if (!Validator.isBlankOrNull(atual.getDtUltimoEnvio())) {
					if (GeneralParser.DiferencaMinutosEntreData(atual.getDtUltimoEnvio(), new Date()) > 10) {

						// Setar os campos entro do metodo buildEntradaAtualizaTratativa
						EntradaAtualizarTratativa entradaAtualizarTratativa = buildEntradaAtualizaTratativa(atual);
						atualizarTratativa.getMensagens().add(entradaAtualizarTratativa);

						AtualizarTratativaResponse atualizarTratativaResponse = sulAmericaService
								.atualizarTratativa(atualizarTratativa);

						// Seta data de ultimo envio
						// transactionUtilBean.queryInsertUpdate("update TabRetornoConsultaTratativaObj
						// set dtUltimoEnvio = now(), ckEncerrado = 1 where cdCodigo ="+cdCodigo);

						TabEnviosObj TabEnvios = new TabEnviosObj();
						TabEnvios.setDtEnvio(new Date());
						TabEnvios.setTxTratativaId(atual.getTxTratativaId());
						TabEnvios.setTxUsuario(atual.getTxNomeUsuario());
						TabEnvios.setCkTipo(1);
						tabEnviosRepository.save(TabEnvios);

					} else {
						txOk = "Tratativa já enviada, para enviar novamente somente após 1 minuto!";

						TabEnviosObj TabEnvios = new TabEnviosObj();
						TabEnvios.setDtEnvio(new Date());
						TabEnvios.setTxTratativaId(atual.getTxTratativaId());
						TabEnvios.setTxUsuario(atual.getTxNomeUsuario());
						TabEnvios.setCkTipo(2);
						tabEnviosRepository.save(TabEnvios);

					}
				} else {
					// Setar os campos entro do metodo buildEntradaAtualizaTratativa
					EntradaAtualizarTratativa entradaAtualizarTratativa = buildEntradaAtualizaTratativa(atual);
					atualizarTratativa.getMensagens().add(entradaAtualizarTratativa);

					AtualizarTratativaResponse atualizarTratativaResponse = sulAmericaService
							.atualizarTratativa(atualizarTratativa);

					// Seta data de ultimo envio
					// transactionUtilBean.queryInsertUpdate("update TabRetornoConsultaTratativaObj
					// set dtUltimoEnvio = now(), ckEncerrado = 1 where cdCodigo ="+cdCodigo);

					TabEnviosObj TabEnvios = new TabEnviosObj();
					TabEnvios.setDtEnvio(new Date());
					TabEnvios.setTxTratativaId(atual.getTxTratativaId());
					TabEnvios.setTxUsuario(atual.getTxNomeUsuario());
					TabEnvios.setCkTipo(1);
					tabEnviosRepository.save(TabEnvios);

				}
			}
		} catch (Exception e) {
			txOk = e.getMessage();
			jtpLogErroBean.addLog(e.getClass().getSimpleName(), e.getMessage(), e);
		}

		return txOk;
	}

	private EntradaAtualizarTratativa buildEntradaAtualizaTratativa(TabRetornoConsultaTratativaObj atual) {
		ObjectFactory objectFactory = new ObjectFactory();
		JAXBElement<String> cid = objectFactory.createEntradaAtualizarTratativaCid(atual.getTxCid());
		JAXBElement<String> condTriagem = objectFactory
				.createEntradaAtualizarTratativaCondTriagem(atual.getTxCondTriagem());
		JAXBElement<String> dataInternacao = objectFactory.createEntradaAtualizarTratativaDataInternacao(
				atual.getDtInternacao() != null ? GeneralParser.format_dateBR2(atual.getDtInternacao()) : null);
		JAXBElement<String> dataPrimContato = objectFactory.createEntradaAtualizarTratativaDataPrimContato(
				atual.getDtPrimContato() != null ? GeneralParser.format_dateBR2(atual.getDtPrimContato()) : null);
		JAXBElement<String> descCID = objectFactory.createEntradaAtualizarTratativaDescCID(atual.getTxDescCid());
		JAXBElement<String> desfecho = objectFactory.createEntradaAtualizarTratativaDesfecho(atual.getTxDesfecho());
		JAXBElement<String> emailResponsavel = objectFactory
				.createEntradaAtualizarTratativaEmailResponsavel(atual.getTxEmailResponsavel());
		JAXBElement<Integer> eRespostaDefinitiva = objectFactory
				.createEntradaAtualizarTratativaERespostaDefinitiva(atual.getCdRespostaDefinitiva());
		JAXBElement<String> grauParentesco = objectFactory
				.createEntradaAtualizarTratativaGrauParentesco(atual.getTxGrauParentesco());
		JAXBElement<String> medico = objectFactory.createEntradaAtualizarTratativaMedico(atual.getTxMedico());
		JAXBElement<String> mensagem = objectFactory.createEntradaAtualizarTratativaMensagem(atual.getTxQuestionario());
		JAXBElement<String> motivoAlta = objectFactory
				.createEntradaAtualizarTratativaMotivoAlta(atual.getTxMotivoAlta());
		JAXBElement<String> motivoNContato = objectFactory
				.createEntradaAtualizarTratativaMotivoNContato(atual.getTxMotivoNaoContato());
		JAXBElement<String> msgId = objectFactory.createEntradaAtualizarTratativaMsgId(atual.getTxMsgId());
		JAXBElement<String> nomeContato = objectFactory
				.createEntradaAtualizarTratativaNomeContato(atual.getTxNomeContato());
		JAXBElement<String> nomeResponsavel = objectFactory
				.createEntradaAtualizarTratativaNomeResponsavel(atual.getTxNomeResponsavel());
		JAXBElement<String> nomeUsuario = objectFactory
				.createEntradaAtualizarTratativaNomeUsuario(atual.getTxNomeUsuario());
		JAXBElement<String> realizouContato = objectFactory
				.createEntradaAtualizarTratativaRealizouContato(atual.getTxRealizouContato());
		JAXBElement<String> telHospital = objectFactory
				.createEntradaAtualizarTratativaTelHospital(atual.getTxTelHospital());
		JAXBElement<String> tipoAcomodacao = objectFactory
				.createEntradaAtualizarTratativaTipoAcomodacao(atual.getTxTipoAcomodacao());
		JAXBElement<String> tipoAssistencia = objectFactory
				.createEntradaAtualizarTratativaTipoAssistencia(atual.getTxTipoAssistencia());
		JAXBElement<String> tipoInternacao = objectFactory
				.createEntradaAtualizarTratativaTipoInternacao(atual.getTxTipoInternacao());
		JAXBElement<String> tratativaId = objectFactory
				.createEntradaAtualizarTratativaTratativaId(atual.getTxTratativaId());

		EntradaAtualizarTratativa entradaAtualizarTratativa = new EntradaAtualizarTratativa();
		entradaAtualizarTratativa.setCid(cid);
		entradaAtualizarTratativa.setCondTriagem(condTriagem);
		entradaAtualizarTratativa.setDataInternacao(dataInternacao);
		entradaAtualizarTratativa.setDataPrimContato(dataPrimContato);
		entradaAtualizarTratativa.setDescCID(descCID);
		entradaAtualizarTratativa.setDesfecho(desfecho);
		entradaAtualizarTratativa.setEmailResponsavel(emailResponsavel);
		entradaAtualizarTratativa.setERespostaDefinitiva(eRespostaDefinitiva);
		entradaAtualizarTratativa.setGrauParentesco(grauParentesco);
		entradaAtualizarTratativa.setMedico(medico);
		entradaAtualizarTratativa.setMensagem(mensagem);
		entradaAtualizarTratativa.setMotivoAlta(motivoAlta);
		entradaAtualizarTratativa.setMotivoNContato(motivoNContato);
		entradaAtualizarTratativa.setMsgId(msgId);
		entradaAtualizarTratativa.setNomeContato(nomeContato);
		entradaAtualizarTratativa.setNomeResponsavel(nomeResponsavel);
		entradaAtualizarTratativa.setNomeUsuario(nomeUsuario);
		entradaAtualizarTratativa.setRealizouContato(realizouContato);
		entradaAtualizarTratativa.setTelHospital(telHospital);
		entradaAtualizarTratativa.setTipoAcomodacao(tipoAcomodacao);
		entradaAtualizarTratativa.setTipoAssistencia(tipoAssistencia);
		entradaAtualizarTratativa.setTipoInternacao(tipoInternacao);
		entradaAtualizarTratativa.setTratativaId(tratativaId);

		return entradaAtualizarTratativa;
	}

	public List<TabRetornoConsultaTratativaObj> listar(Integer cdTipo) {

		List<TabRetornoConsultaTratativaObj> list = null;

		if (cdTipo == 1) { // Encerrado
			list = tabRetornoConsultaTratativaRepository
					.findByCkEncerradoAndDtCancelamentoIsNullOrderByDtDataSolicitacaoDesc(1);
		} else if (cdTipo == 2) { // Novos
			list = tabRetornoConsultaTratativaRepository.findByTratativasNovasQuery();
		} else if (cdTipo == 3) { // Alerta resposta - Resposta Sulamerica
			list = tabRetornoConsultaTratativaRepository.findByTratativasAlertaRespostaQuery();
		} else if (cdTipo == 4) { // Inativos
			list = tabRetornoConsultaTratativaRepository.findByDtCancelamentoIsNotNull();
		} else if (cdTipo == 5) { // Novo Contato
			list = tabRetornoConsultaTratativaRepository.findByTratativasNovoContatoQuery();
		} else if (cdTipo == 6) { // Contato Posterior
			list = tabRetornoConsultaTratativaRepository.findByTratativasContatoPosteriorQuery();
		}

		return list;
	}

	public void transferir(@PathVariable Integer cdTipo) {

		List<TabRetornoConsultaTratativaObj> lst = listar(cdTipo);

		for (TabRetornoConsultaTratativaObj atual : lst) {

			TabRetornoConsultaTratativaObj Tab = tabRetornoConsultaTratativaRepository.findOne(atual.getCdCodigo());
			TabRetornoConsultaTratativaCapaObj TabCapa = null;
			if (Tab != null) {

				TabPacienteObj TabPaciente = tabPacienteRepository.findByTxCpfSegurado(Tab.getTxCpfSegurado());

				if (TabPaciente == null) {

					TabPacienteObj TabPac = new TabPacienteObj();
					TabPac.setDtDataNascimento(Tab.getDtDataNascimento());
					TabPac.setTxCarteira(Tab.getTxCarteira());
					TabPac.setTxCodSegurado(Tab.getTxCodSegurado());
					TabPac.setTxCpfSegurado(Tab.getTxCpfSegurado());
					TabPac.setTxNomeSegurado(Tab.getTxNomeSegurado());
					TabPac.setTxProdSaude(Tab.getTxProdSaude());
					TabPac.setTxSexo(Tab.getTxSexo());
					TabPac.setTxTelCelular(Tab.getTxTelCelular());
					TabPac.setTxTelCobranca(Tab.getTxTelCobranca());
					TabPac.setTxTelComercial(Tab.getTxTelComercial());
					TabPac.setTxTelContato(Tab.getTxTelContato());
					TabPac.setTxTelContato2(Tab.getTxTelContato2());
					TabPac.setTxTelContato3(Tab.getTxTelContato3());
					TabPac.setTxTelPrestador(Tab.getTxTelPrestador());
					TabPac.setTxTelResencial(Tab.getTxTelResencial());
					TabPac.setCdStatusPaciente(1);
					TabPac.setDtUltimaAtualizacao(new Date());
					tabPacienteRepository.save(TabPac);

					// Capa
					TabCapa = new TabRetornoConsultaTratativaCapaObj();
					TabCapa.setTxTratativaId(Tab.getTxTratativaId());
					TabCapa.setDtDataIncioVpp(Tab.getDtDataIncioVpp());
					TabCapa.setDtDataNascimento(Tab.getDtDataNascimento());
					TabCapa.setDtDataSolicitacao(Tab.getDtDataSolicitacao());
					TabCapa.setTxAssunto(Tab.getTxAssunto());
					TabCapa.setTxCarteira(Tab.getTxCarteira());
					TabCapa.setTxCodSegurado(Tab.getTxCodSegurado());
					TabCapa.setTxCpfSegurado(Tab.getTxCpfSegurado());
					TabCapa.setTxEmpresa(Tab.getTxEmpresa());
					TabCapa.setTxEstado(Tab.getTxEstado());
					TabCapa.setTxNomeSegurado(Tab.getTxNomeSegurado());
					TabCapa.setTxNumVpp(Tab.getTxNumVpp());
					TabCapa.setTxPrestador(Tab.getTxPrestador());
					TabCapa.setTxProdSaude(Tab.getTxProdSaude());
					TabCapa.setTxSexo(Tab.getTxSexo());
					TabCapa.setTxSlaTratativa(Tab.getTxSlaTratativa());
					TabCapa.setTxTelCelular(Tab.getTxTelCelular());
					TabCapa.setTxTelCobranca(Tab.getTxTelCobranca());
					TabCapa.setTxTelComercial(Tab.getTxTelComercial());
					TabCapa.setTxTelContato(Tab.getTxTelContato());
					TabCapa.setTxTelContato2(Tab.getTxTelContato2());
					TabCapa.setTxTelContato3(Tab.getTxTelContato3());
					TabCapa.setTxTelPrestador(Tab.getTxTelPrestador());
					TabCapa.setTxTelResencial(Tab.getTxTelResencial());
					TabCapa.setDtNovoContato(Tab.getDtNovoContato());
					TabCapa.setCdStatusTratativa(1);
					TabCapa = tabRetornoConsultaTratativaCapaRepository.save(TabCapa);
					System.out.println("Fez " + TabCapa.getTxTratativaId());

				}
			}
		}

		System.out.println("Acabou");
	}

}
