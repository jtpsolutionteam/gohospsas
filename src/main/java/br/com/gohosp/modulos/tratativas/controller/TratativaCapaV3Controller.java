package br.com.gohosp.modulos.tratativas.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.TransactionUtilBean;
import br.com.gohosp.dao.cadastros.varios.TabNivelConscienciaObj;
import br.com.gohosp.dao.cadastros.varios.TabStatusTratativaObj;
import br.com.gohosp.dao.cadastros.varios.repository.TabNivelConscienciaRepository;
import br.com.gohosp.dao.cadastros.varios.repository.TabStatusTratativaRepository;
import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaCapaObj;
import br.com.gohosp.dao.tratativas.TabRetornoConsultaTratativaObj;
import br.com.gohosp.dao.tratativas.VwTabRetornoConsultaTratativaCapaObj;
import br.com.gohosp.dao.tratativas.VwTabRetornoConsultaTratativaObj;
import br.com.gohosp.modulos.tratativas.service.TabRetornoConsultaTratativaService;
import br.com.gohosp.modulos.tratativas.service.TratativaCapaService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.logErro.JTPLogErroBean;
import br.com.gohosp.util.regras.RegrasBean;

@Controller
@RequestMapping("/tratativacapav3")
public class TratativaCapaV3Controller extends allController {


	
	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TratativaCapaService tabService;
	
	@Autowired
	private TabRetornoConsultaTratativaService tabRetornoConsultaTratativaService;
	

	@Autowired
	private RegrasBean regrasBean;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	
	@Autowired
	private TransactionUtilBean transactionUtilBean;
	
	private String txUrlTela = Constants.TEMPLATE_PATH_TRATATIVAS+"/tratativacapav3";
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo() {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabRetornoConsultaTratativaObj());

		return mv;
	}

	

	
	

	@RequestMapping(value = "/consultar/{cdTratativa}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdTratativa, HttpServletRequest request) {
	
	   ModelAndView mv = new ModelAndView(txUrlTela);	
		
	   VwTabRetornoConsultaTratativaCapaObj TabView = tabService.consultar(cdTratativa);
	   
	   
		if (TabView != null) {
		  mv.addObject("tabRetornoConsultaTratativaCapaObj",TabView);		  
		  request.getSession().setAttribute("cdTratativa", cdTratativa);
		  
		  List<VwTabRetornoConsultaTratativaObj> listTratativas = tabRetornoConsultaTratativaService.listarTratativaIdView(TabView.getTxTratativaId());
		  mv.addObject("listTratativa",listTratativas);
		}else {
		  mv.addObject(new TabRetornoConsultaTratativaCapaObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}

	@RequestMapping(value = "/criarresposta/{cdTratativa}/{cdCodigo}", method = RequestMethod.GET)
	public @ResponseBody String criarResposta(@PathVariable Integer cdTratativa, @PathVariable Integer cdCodigo) {
		
		TabRetornoConsultaTratativaObj Tab = tabRetornoConsultaTratativaService.consultar(cdCodigo);
		
		TabRetornoConsultaTratativaObj TabNew = new TabRetornoConsultaTratativaObj();
		
		if (Tab != null) {
			
			TabNew.setTxTratativaId(Tab.getTxTratativaId());
			TabNew.setTxMsgId(Tab.getTxMsgId());
			TabNew.setDtDataIncioVpp(Tab.getDtDataIncioVpp());
			TabNew.setDtDataNascimento(Tab.getDtDataNascimento());
			TabNew.setDtDataSolicitacao(Tab.getDtDataSolicitacao());
			TabNew.setTxAssunto(Tab.getTxAssunto());
			TabNew.setTxCarteira(Tab.getTxCarteira());
			TabNew.setTxCodSegurado(Tab.getTxCodSegurado());
			TabNew.setTxCpfSegurado(Tab.getTxCpfSegurado());
			TabNew.setTxEmpresa(Tab.getTxEmpresa());
			TabNew.setTxEstado(Tab.getTxEstado());
			TabNew.setTxNomeSegurado(Tab.getTxNomeSegurado());
			TabNew.setTxNumVpp(Tab.getTxNumVpp());
			TabNew.setTxPrestador(Tab.getTxPrestador());
			TabNew.setTxProdSaude(Tab.getTxProdSaude());
			TabNew.setTxSexo(Tab.getTxSexo());
			TabNew.setTxSlaTratativa(Tab.getTxSlaTratativa());
			TabNew.setTxTelCelular(Tab.getTxTelCelular());
			TabNew.setTxTelCobranca(Tab.getTxTelCobranca());
			TabNew.setTxTelComercial(Tab.getTxTelComercial());
			TabNew.setTxTelContato(Tab.getTxTelContato());
			TabNew.setTxTelContato2(Tab.getTxTelContato2());
			TabNew.setTxTelContato3(Tab.getTxTelContato3());
			TabNew.setTxTelPrestador(Tab.getTxTelPrestador());
			TabNew.setTxTelResencial(Tab.getTxTelResencial());
			TabNew.setCdTipo(2);
			TabNew.setTxMensagem(Tab.getTxMensagem());
			
			//Dados da Tratativa anterior
			TabRetornoConsultaTratativaObj TabMsg = verificaUltimaMensagem(Tab.getTxTratativaId());
				if (TabMsg != null) {
					TabNew.setCdProbEncontrados(TabMsg.getCdProbEncontrados());
					TabNew.setCdCcCovid19(TabMsg.getCdCcCovid19());
					TabNew.setTxCid2(TabMsg.getTxCid2());
					TabNew.setTxNomeResponsavel(TabMsg.getTxNomeResponsavel());
					TabNew.setTxEmailResponsavel(TabMsg.getTxEmailResponsavel());
					TabNew.setTxNomeContato(TabMsg.getTxNomeContato());
					TabNew.setTxGrauParentesco(TabMsg.getTxGrauParentesco());
					TabNew.setTxRealizouContato(TabMsg.getTxRealizouContato());
					TabNew.setTxMotivoNaoContato(TabMsg.getTxMotivoNaoContato());
					TabNew.setDtPrimContato(TabMsg.getDtPrimContato());
					TabNew.setTxTelHospital(TabMsg.getTxTelHospital());
					TabNew.setDtInternacao(TabMsg.getDtInternacao());
					TabNew.setTxTipoInternacao(TabMsg.getTxTipoInternacao());
					TabNew.setTxTipoAcomodacao(TabMsg.getTxTipoAcomodacao());
					
					TabNew.setTxCid3(TabMsg.getTxCid3()); 
					TabNew.setCdNivelConsciencia(TabMsg.getCdNivelConsciencia()); 
					TabNew.setCdSinaisVitais(TabMsg.getCdSinaisVitais()); 
					TabNew.setCdCcGeral(TabMsg.getCdCcGeral()); 
					TabNew.setCdCcOxigenoterapia(TabMsg.getCdCcOxigenoterapia()); 
					TabNew.setCkCcAspViasAereas(TabMsg.getCkCcAspViasAereas()); 
					TabNew.setCdCcApsViasAereas(TabMsg.getCdCcApsViasAereas()); 
					TabNew.setCkCcAcessoVenoso(TabMsg.getCkCcAcessoVenoso()); 
					TabNew.setCdCcAcessoVenoso(TabMsg.getCdCcAcessoVenoso()); 
					TabNew.setCdCcAlimentacao(TabMsg.getCdCcAlimentacao()); 
					TabNew.setCdCcEfNormal(TabMsg.getCdCcEfNormal()); 
					TabNew.setCdCcEfAusencia(TabMsg.getCdCcEfAusencia()); 
					TabNew.setCdCcEfControle(TabMsg.getCdCcEfControle()); 
					TabNew.setCdCcEfColostomia(TabMsg.getCdCcEfColostomia()); 
					TabNew.setCdCcEfCistostomia(TabMsg.getCdCcEfCistostomia()); 
					TabNew.setCdCcEfNefrostomia(TabMsg.getCdCcEfNefrostomia()); 
					TabNew.setCdCcEfSva(TabMsg.getCdCcEfSva()); 
					TabNew.setCdCcEfSvd(TabMsg.getCdCcEfSvd()); 
					TabNew.setCdCcMuHabituais(TabMsg.getCdCcMuHabituais()); 
					TabNew.setCdCcMuNovos(TabMsg.getCdCcMuNovos()); 
					TabNew.setCdCcMuParenteral(TabMsg.getCdCcMuParenteral()); 
					TabNew.setCdCcMuHipodermoclise(TabMsg.getCdCcMuHipodermoclise()); 
					TabNew.setTxCcMuObs(TabMsg.getTxCcMuObs()); 
					TabNew.setCkCcMeAnticoagulantes(TabMsg.getCkCcMeAnticoagulantes()); 
					TabNew.setCdCcMeAnticoagulantes(TabMsg.getCdCcMeAnticoagulantes()); 
					TabNew.setCkCcMeAntibiotico(TabMsg.getCkCcMeAntibiotico()); 
					TabNew.setCdCcMeAntibiotico(TabMsg.getCdCcMeAntibiotico()); 
					TabNew.setCkCcMeQuimioterapicos(TabMsg.getCkCcMeQuimioterapicos()); 
					TabNew.setCdCcMeQuimioterapicos(TabMsg.getCdCcMeQuimioterapicos()); 
					TabNew.setDtCcMeInicioQuimioterapicos(TabMsg.getDtCcMeInicioQuimioterapicos()); 
					TabNew.setDtCcMeFimQuimioterapicos(TabMsg.getDtCcMeFimQuimioterapicos()); 
					TabNew.setCkCcMeImunoglobulinas(TabMsg.getCkCcMeImunoglobulinas()); 
					TabNew.setCdCcMeImunoglobulinas(TabMsg.getCdCcMeImunoglobulinas()); 
					TabNew.setDtCcMeInicioImunoglobulinas(TabMsg.getDtCcMeInicioImunoglobulinas()); 
					TabNew.setDtCcMeFimImunoglobulinas(TabMsg.getDtCcMeFimImunoglobulinas()); 
					TabNew.setCkCcMeImunobiologico(TabMsg.getCkCcMeImunobiologico()); 
					TabNew.setCdCcMeImunobiologico(TabMsg.getCdCcMeImunobiologico()); 
					TabNew.setDtCcMeInicioImunobiologico(TabMsg.getDtCcMeInicioImunobiologico()); 
					TabNew.setDtCcMeFimImunobiologico(TabMsg.getDtCcMeFimImunobiologico()); 
					TabNew.setCkCcRadioterapia(TabMsg.getCkCcRadioterapia()); 
					TabNew.setCdCcRadioterapia(TabMsg.getCdCcRadioterapia()); 
					TabNew.setDtCcInicioRadioterapia(TabMsg.getDtCcInicioRadioterapia()); 
					TabNew.setDtCcFimRadioterapia(TabMsg.getDtCcFimRadioterapia()); 
					TabNew.setCkCcVos(TabMsg.getCkCcVos()); 
					TabNew.setCdCcVos(TabMsg.getCdCcVos()); 
					TabNew.setTxCcVosObs(TabMsg.getTxCcVosObs()); 					 
					TabNew.setCkCcEqMultiprofissional(TabMsg.getCkCcEqMultiprofissional()); 
					TabNew.setCdCcEqMultiprofissional(TabMsg.getCdCcEqMultiprofissional()); 
					TabNew.setTxCcEqMultiprofissional(TabMsg.getTxCcEqMultiprofissional()); 
					TabNew.setCkCcOhb(TabMsg.getCkCcOhb()); 
					TabNew.setCdCcOhb(TabMsg.getCdCcOhb()); 
					TabNew.setCdCcOhbExterna(TabMsg.getCdCcOhbExterna()); 
					TabNew.setDtCcOhbInicio(TabMsg.getDtCcOhbInicio()); 
					TabNew.setDtCcOhbFim(TabMsg.getDtCcOhbFim()); 
					TabNew.setCdCcImppressaoGeral(TabMsg.getCdCcImppressaoGeral()); 
					TabNew.setCkPtMedicoAcredita(TabMsg.getCkPtMedicoAcredita()); 
					TabNew.setCkPtMedicoDeacordo(TabMsg.getCkPtMedicoDeacordo()); 
					TabNew.setCkPtMedicoProximasEtapas(TabMsg.getCkPtMedicoProximasEtapas()); 
					TabNew.setTxPtMedicoProximasEtapas(TabMsg.getTxPtMedicoProximasEtapas()); 
					TabNew.setCdPtMedicoExpectativas(TabMsg.getCdPtMedicoExpectativas()); 
					TabNew.setCkPtMedicoAlgumProcedimento(TabMsg.getCkPtMedicoAlgumProcedimento()); 
					TabNew.setTxPtMedicoAlgumProcedimento(TabMsg.getTxPtMedicoAlgumProcedimento()); 
					TabNew.setCkPtMedicoCienciaRecursos(TabMsg.getCkPtMedicoCienciaRecursos()); 
					TabNew.setCdPtMedicoCienciaRecursos(TabMsg.getCdPtMedicoCienciaRecursos()); 
					TabNew.setCkDsAlgumApontamento(TabMsg.getCkDsAlgumApontamento()); 
					TabNew.setTxDsAlgumApontamento(TabMsg.getTxDsAlgumApontamento()); 
					TabNew.setCkDsEsposo(TabMsg.getCkDsEsposo()); 
					TabNew.setCkDsFilho(TabMsg.getCkDsFilho()); 
					TabNew.setCkDsPai(TabMsg.getCkDsPai()); 
					TabNew.setCkDsMae(TabMsg.getCkDsMae()); 
					TabNew.setCkDsIrmao(TabMsg.getCkDsIrmao()); 
					TabNew.setCkDsSogro(TabMsg.getCkDsSogro()); 
					TabNew.setCkDsOutros(TabMsg.getCkDsOutros()); 
					TabNew.setTxDsOutros(TabMsg.getTxDsOutros()); 
					TabNew.setCkDsCuidador(TabMsg.getCkDsCuidador()); 
					TabNew.setCdDsCuidador(TabMsg.getCdDsCuidador()); 
					TabNew.setCdDsIdade(TabMsg.getCdDsIdade()); 
					TabNew.setTxDsCuidadorParentesco(TabMsg.getTxDsCuidadorParentesco()); 
					TabNew.setCkDsPendenciaFamiliar(TabMsg.getCkDsPendenciaFamiliar()); 
					TabNew.setTxDsPendenciaFamiliar(TabMsg.getTxDsPendenciaFamiliar());
					
				}
			
			tabRetornoConsultaTratativaService.gravar(TabNew);	
			
			TabRetornoConsultaTratativaCapaObj TabCapa = tabService.consultarTratativa(cdTratativa);
			TabCapa.setCdStatusTratativa(1);
			tabService.gravar(TabCapa);
			
		}

		return "/tratativav3/consultar/"+TabNew.getCdCodigo();
	}

	
	@RequestMapping("/excluir/{cdTratativa}/{cdCodigo}")
	public String excluir(@PathVariable Integer cdTratativa, @PathVariable Integer cdCodigo) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();

		TabRetornoConsultaTratativaObj Tab = tabRetornoConsultaTratativaService.consultar(cdCodigo);
		Tab.setDtCancelamento(new Date());
		Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		tabRetornoConsultaTratativaService.gravar(Tab);
		
		return "redirect:/tratativacapav3/consultar/"+cdTratativa;
		
		//return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public ModelAndView gravar(@Validated TabRetornoConsultaTratativaCapaObj tabRetornoConsultaTratativaCapaObj, Errors erros, HttpServletRequest request) {
		
		DadosUser user = tabUsuariosService.DadosUsuario();
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		
		if (erros.hasErrors()) {
		  mv.addObject("listaErros", erros.getAllErrors());
		  return mv;
		}else {
			List<ObjectError> error = new ArrayList<ObjectError>();
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  mv.addObject("listaErros", error);
				  List<VwTabRetornoConsultaTratativaObj> listTratativas = tabRetornoConsultaTratativaService.listarTratativaIdView(tabRetornoConsultaTratativaCapaObj.getTxTratativaId());
				  mv.addObject("listTratativa",listTratativas);
				  mv.addObject("tabRetornoConsultaTratativaCapaObj",tabRetornoConsultaTratativaCapaObj);
				  return mv;	
			}	  
		}
		
		/*
		if (tabRetornoConsultaTratativaCapaObj.getCdStatusTratativa() == 1 && tabRetornoConsultaTratativaCapaObj.getDtNovoContato() == null) {
			
			VwTabRetornoConsultaTratativaCapaObj TabView = tabService.consultar(tabRetornoConsultaTratativaCapaObj.getCdTratativa());
			
			List<VwTabRetornoConsultaTratativaObj> listTratativas = tabRetornoConsultaTratativaService.listarTratativaIdView(TabView.getTxTratativaId());
			mv.addObject("listTratativa",listTratativas);
			mv.addObject("tabRetornoConsultaTratativaCapaObj",tabRetornoConsultaTratativaCapaObj);
			mv.addObject("txMensagem", "Tratativas em aberto precisam de Data de Novo Contato!");
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
			return mv;
		
		}*/
		
		
		tabRetornoConsultaTratativaCapaObj.setCdUsuario(user.getCdUsuario());
		
	    TabRetornoConsultaTratativaCapaObj Tab = tabService.gravar(tabRetornoConsultaTratativaCapaObj);
	    
	    VwTabRetornoConsultaTratativaCapaObj TabView = tabService.consultar(Tab.getCdTratativa());
	    
	    List<VwTabRetornoConsultaTratativaObj> listTratativas = tabRetornoConsultaTratativaService.listarTratativaIdView(TabView.getTxTratativaId());
		mv.addObject("listTratativa",listTratativas);
		mv.addObject("tabRetornoConsultaTratativaCapaObj",TabView);
		mv.addObject("txMensagem", Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
		mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_SUCCESS);
		return mv;
	}
	
	
	private TabRetornoConsultaTratativaObj verificaUltimaMensagem(String txTratativaId) {
		
		List<TabRetornoConsultaTratativaObj> l = tabRetornoConsultaTratativaService.listarTratativaId(txTratativaId, 2);
		
		Integer vlTot = l.size();
		
		TabRetornoConsultaTratativaObj Tab = null;
		if (vlTot > 0) {
			Tab = l.get(vlTot-1);
		}
		
		/*
		for (TabRetornoConsultaTratativaObj atual : l) {
			
			TabRetornoConsultaTratativaObj Tab = new TabRetornoConsultaTratativaObj();
			Tab.setTxCid2(atual.getTxCid2());
			
		}*/
		//System.out.println(Tab.getDtUltimoEnvio());
		return Tab;
	}
	
	
	@Autowired
	private TabStatusTratativaRepository tabStatusTratativaRepository;
	
	@ModelAttribute("selectcdstatustratativa")	
	public List<TabStatusTratativaObj> selectcdstatustratativa() {
		return tabStatusTratativaRepository.findAll();
	}

	
}
