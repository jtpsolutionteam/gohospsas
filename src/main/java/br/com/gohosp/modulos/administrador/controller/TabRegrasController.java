package br.com.gohosp.modulos.administrador.controller;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.administracao.TabGrupoVisaoObj;
import br.com.gohosp.dao.administracao.TabRegrasObj;
import br.com.gohosp.dao.administracao.TabRegrasTipoObj;
import br.com.gohosp.dao.administracao.TabTelasObj;
import br.com.gohosp.dao.administracao.vw.VwTabRegrasObj;
import br.com.gohosp.mensagens.TabRetornoSucessoObj;
import br.com.gohosp.modulos.administrador.bean.TabFiltroRegrasObj;
import br.com.gohosp.modulos.administrador.service.TabGrupoVisaoService;
import br.com.gohosp.modulos.administrador.service.TabRegrasService;
import br.com.gohosp.modulos.administrador.service.TabRegrasTipoService;
import br.com.gohosp.modulos.administrador.service.TabTelasService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralParser;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.regras.RegrasBean;

@Controller
@RequestMapping("/admregras")
public class TabRegrasController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabRegrasService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_ADM+"/admregras";
	

 	
 	@GetMapping
	@Transactional(readOnly = true)
	public ModelAndView pesquisa(TabFiltroRegrasObj tabFiltroRegrasObj, HttpServletRequest request) {
		
		tabFiltroRegrasObj = verificaFiltro(tabFiltroRegrasObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabRegrasObj());
		mv.addObject("tabFiltroRegrasObj", tabFiltroRegrasObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroRegrasObj));
		mv.addObject("cdTela", tabFiltroRegrasObj.getCdTelaFiltro());
		mv.addObject("cdGrupoVisao", tabFiltroRegrasObj.getCdGrupoVisaoFiltro());		

		
		
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroRegrasObj tabFiltroRegrasObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(VwTabRegrasObj.class);
		
		adicionarFiltro(tabFiltroRegrasObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroRegrasObj verificaFiltro(TabFiltroRegrasObj tabFiltroRegrasObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroRegrasObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroRegrasObj = mapper.readValue(jsonFiltro, TabFiltroRegrasObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroRegrasObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroRegrasObj;
	}

	
	private void adicionarFiltro(TabFiltroRegrasObj filtro, Criteria criteria) {
		
		boolean ckFiltro = false;

		if (filtro != null) {

			if (filtro.getCdTelaFiltro() != null) {
				criteria.add(Restrictions.eq("cdTela", filtro.getCdTelaFiltro()));
				ckFiltro = true;
			}

			if (filtro.getCdGrupoVisaoFiltro() != null) {
				criteria.add(Restrictions.eq("cdGrupoVisao", filtro.getCdGrupoVisaoFiltro()));
				ckFiltro = true;
			}

			if (!ckFiltro) {
				criteria.add(Restrictions.eq("cdTela", 0));
			}

			criteria.addOrder(Order.asc("cdOrdem"));
		}
	  }
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabRegrasObj tabRegrasObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabRegrasObj Tab = tabService.gravar(tabRegrasObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo(HttpServletRequest request) {
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabRegrasObj());
		mv.addObject("tabFiltroRegrasObj", verificaFiltro(new TabFiltroRegrasObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroRegrasObj(), request)));
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{CdRegra}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer CdRegra, HttpServletRequest request) {
	
	   TabRegrasObj TabView = tabService.consultar(CdRegra);
	   TabFiltroRegrasObj tF = verificaFiltro(new TabFiltroRegrasObj(), request);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroRegrasObj",tF);	
		  mv.addObject("tabRegrasObj",TabView);
		  mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
		  mv.addObject("txEdit", "edit");
		  mv.addObject("cdTela", TabView.getCdTela());
		  mv.addObject("cdGrupoVisao", TabView.getCdGrupoVisao());

		}else {
		  mv.addObject("tabFiltroRegrasObj",tF);	
		  mv.addObject(new TabRegrasObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	
	@RequestMapping(value = "/excluir/{cdRegras}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable Integer cdRegras, HttpServletRequest request) {

		tabService.excluir(cdRegras);

		return "OK!";
	}
	
	
	//Modelo
	@Autowired
	private TabGrupoVisaoService tabGrupoVisaoService;
	
	@ModelAttribute("selectcdgrupovisao")
	public List<TabGrupoVisaoObj> selectcdgrupovisao() {
		return tabGrupoVisaoService.listar();
	}
	
	@Autowired
	private TabTelasService tabTelasService;
	
	@ModelAttribute("selectcdtela")
	public List<TabTelasObj> selectcdtela() {
		return tabTelasService.listar();
	}

	@Autowired
	private TabRegrasTipoService tabRegrasTipoService;
	
	@ModelAttribute("selectcdtipo")
	public List<TabRegrasTipoObj> selectcdtipo() {
		return tabRegrasTipoService.listar();
	}
	
	
}
