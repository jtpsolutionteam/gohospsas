package br.com.gohosp.modulos.administrador.bean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.gohosp.dao.administracao.TabGrupoVisaoObj;
import br.com.gohosp.security.UsuarioBean;

@Service
public class TabFiltroGrupoVisaoBean {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private UsuarioBean tabDadosUsuariosBean;
  
  @SuppressWarnings("unchecked")
  @Transactional(readOnly = true)
  public Page<TabGrupoVisaoObj> filtrar(TabFiltroGrupoVisaoObj  filtro, Pageable pageable) {
	
	  Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabGrupoVisaoObj.class);
	  
	  int paginaAtual = pageable.getPageNumber();
	  int totalRegistroPagina = pageable.getPageSize();
	  int primeiroRegistro = paginaAtual * totalRegistroPagina;
	  
	  criteria.setFirstResult(primeiroRegistro);
	  criteria.setMaxResults(totalRegistroPagina);
	  
	  adicionarFiltro(filtro, criteria);
		
	  return new PageImpl<>(criteria.list(), pageable, total(filtro));
	  
  }
	
  private Long total(TabFiltroGrupoVisaoObj filtro) {
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabGrupoVisaoObj.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
}
  
  private void adicionarFiltro(TabFiltroGrupoVisaoObj filtro, Criteria criteria) {

	  if (filtro != null) {
		  
		  boolean ckFiltro = false;
		  
		  
		  if (!StringUtils.isEmpty(filtro.getTxGrupoVisaoFiltro())) {
			  criteria.add(Restrictions.ilike("txGrupoVisao", "%"+filtro.getTxGrupoVisaoFiltro()+"%"));
			  ckFiltro = true;
		  }

		  /*
		  if (!ckFiltro) {
			  criteria.add(Restrictions.eq("txPlanoContas", "")); 
		  }*/
		  
	  }

	  
  }

}
 