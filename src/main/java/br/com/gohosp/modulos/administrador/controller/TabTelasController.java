package br.com.gohosp.modulos.administrador.controller;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.administracao.TabTelasObj;
import br.com.gohosp.mensagens.TabRetornoSucessoObj;
import br.com.gohosp.modulos.administrador.service.TabTelasService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.regras.RegrasBean;

@Controller
@RequestMapping("/admtelas")
public class TabTelasController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabTelasService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_ADM+"/admtelas";
 	
 	@GetMapping
	@Transactional(readOnly = true)
	public ModelAndView pesquisa() {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabTelasObj());
		mv.addObject("listadados", retornoFiltro());
		
		return mv;
	}
	
	private List<?> retornoFiltro() {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabTelasObj.class);
		
		adicionarFiltro(criteria);
 		
 		return criteria.list();
 	}

	
	private void adicionarFiltro(Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		 
			  
			  criteria.addOrder(Order.asc("txTela"));
		 
	  }
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabTelasObj tabTelasObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabTelasObj Tab = tabService.gravar(tabTelasObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo(@PageableDefault(size = 20) Pageable pageable, HttpServletRequest httpServletRequest) {
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabTelasObj());		
		mv.addObject("listadados",retornoFiltro());
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{cdTela}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdTela, HttpServletRequest httpServletRequest) {
	
	   TabTelasObj TabView = tabService.consultar(cdTela);
	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabTelasObj",TabView);
		  mv.addObject("listadados",retornoFiltro());
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject(new TabTelasObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	
}
