package br.com.gohosp.modulos.administrador.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.administracao.TabRegrasTipoObj;
import br.com.gohosp.dao.administracao.repository.TabRegrasTipoRepository;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.log.LogBean;
import br.com.gohosp.util.logErro.JTPLogErroBean;
	

@Service
public class TabRegrasTipoService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabRegrasTipoRepository  tabRepository;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;

	
	
	public List<TabRegrasTipoObj> listar() {
		return tabRepository.findAll();
		
	}
	
	
}
