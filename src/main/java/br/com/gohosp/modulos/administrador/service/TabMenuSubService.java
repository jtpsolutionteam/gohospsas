package br.com.gohosp.modulos.administrador.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.administracao.TabMenuSubObj;
import br.com.gohosp.dao.administracao.repository.TabMenuSubRepository;
import br.com.gohosp.dao.util.log.TabLogObj;
import br.com.gohosp.exceptions.ErrosConstraintException;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.log.LogBean;
import br.com.gohosp.util.logErro.JTPLogErroBean;

@Service
public class TabMenuSubService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabMenuSubRepository  tabRepository;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;


	
	public List<TabMenuSubObj> listar() {
		return tabRepository.findAll();
		
	}
	



	public TabMenuSubObj gravar(TabMenuSubObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabMenuSubObj tAtual = new TabMenuSubObj();
		TabMenuSubObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdSubmenu() != null && !Validator.isBlankOrNull(Tab.getCdSubmenu())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdSubmenu());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdSubmenu()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdSubmenu()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabMenuSubObj consultar(Integer cdSubmenu) {
	 TabMenuSubObj Tab = tabRepository.findOne(cdSubmenu);
	 
	   return Tab;
	 
	}

	
		public void excluir(Integer cdSubmenu) {
		   tabRepository.delete(cdSubmenu);			 
		}
	
}
