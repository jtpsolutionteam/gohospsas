package br.com.gohosp.modulos.administrador.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.administracao.TabBotaoObj;
import br.com.gohosp.dao.administracao.repository.TabBotaoRepository;
import br.com.gohosp.dao.administracao.repository.VwTabBotaoRepository;
import br.com.gohosp.dao.administracao.vw.VwTabBotaoObj;
import br.com.gohosp.dao.util.log.TabLogObj;
import br.com.gohosp.exceptions.ErrosConstraintException;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.log.LogBean;

@Service
public class TabBotaoService {
	
	@Autowired
	private TabBotaoRepository  tabRepository;
	
	@Autowired
	private VwTabBotaoRepository  tabVwRepository;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;

	
	public List<TabBotaoObj> listar() {
		return tabRepository.findAll();
		
	}


	public TabBotaoObj gravar(TabBotaoObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabBotaoObj tAtual = new TabBotaoObj();
		TabBotaoObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdBotao() != null && !Validator.isBlankOrNull(Tab.getCdBotao())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdBotao());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdBotao()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdBotao()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabBotaoObj consultar(Integer CdBotao) {
	 TabBotaoObj Tab = tabRepository.findOne(CdBotao);
	 
	   return Tab;
	 
	}

	
		public void excluir(Integer CdBotao) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();

		   tabRepository.delete(CdBotao);		   
		  
			 
		}

	
	public List<TabBotaoObj> pesquisa(Integer cdGrupoVisao, Integer cdTela) {
		
		List<TabBotaoObj> Tab = tabRepository.findByCdGrupoVisaoCdTelaQuery(cdGrupoVisao, cdTela);
		 
		return Tab;
		 
	}
	
	
	public List<VwTabBotaoObj> pesquisaPermissoes(Integer cdGrupoVisao, String txController) {
		
		List<VwTabBotaoObj> Tab = tabVwRepository.findBycdGrupoVisaotxControllerQuery(cdGrupoVisao, txController);
		 
		return Tab;
		 
	}
	
	
}
