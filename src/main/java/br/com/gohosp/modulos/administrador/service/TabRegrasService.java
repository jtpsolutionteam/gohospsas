package br.com.gohosp.modulos.administrador.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.administracao.TabRegrasObj;
import br.com.gohosp.dao.administracao.repository.TabRegrasRepository;
import br.com.gohosp.dao.administracao.repository.VwTabRegrasRepository;
import br.com.gohosp.dao.administracao.vw.VwTabRegrasObj;
import br.com.gohosp.dao.util.log.TabLogObj;
import br.com.gohosp.exceptions.ErrosConstraintException;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.log.LogBean;

@Service
public class TabRegrasService {
	
	@Autowired
	private TabRegrasRepository  tabRepository;
	
	@Autowired
	private VwTabRegrasRepository  tabVwRepository;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;

	
	public List<VwTabRegrasObj> listar() {
		return tabVwRepository.findAll();
		
	}


	public TabRegrasObj gravar(TabRegrasObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabRegrasObj tAtual = new TabRegrasObj();
		TabRegrasObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdRegra() != null && !Validator.isBlankOrNull(Tab.getCdRegra())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdRegra());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdRegra()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdRegra()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabRegrasObj consultar(Integer cdRegra) {
		TabRegrasObj Tab = tabRepository.findOne(cdRegra);
	 
	   return Tab;
	 
	}

	
		public void excluir(Integer cdRegra) {

		
		   tabRepository.delete(cdRegra);		   
		  
			 
		}

	
	public List<VwTabRegrasObj> pesquisa(Integer cdGrupoVisao, Integer cdTela) {
		
		List<VwTabRegrasObj> Tab = tabVwRepository.findByCdGrupoVisaoCdTelaQuery(cdGrupoVisao, cdTela);
		 
		return Tab;
		 
	}
	
	
	public List<VwTabRegrasObj> pesquisaPermissoes(Integer cdGrupoVisao, String txController) {
		
		List<VwTabRegrasObj> Tab = tabVwRepository.findBycdGrupoVisaotxControllerQuery(cdGrupoVisao, txController);
		 
		return Tab;
		 
	}

	

}
