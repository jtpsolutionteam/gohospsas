package br.com.gohosp.modulos.administrador.controller;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.administracao.TabMenuObj;
import br.com.gohosp.mensagens.TabRetornoSucessoObj;
import br.com.gohosp.modulos.administrador.bean.TabFiltroMenuObj;
import br.com.gohosp.modulos.administrador.service.TabMenuService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralParser;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.regras.RegrasBean;

@Controller
@RequestMapping("/admmenu")
public class TabMenuController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabMenuService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_ADM+"/admmenu";
	
 	
 	@GetMapping
	@Transactional(readOnly = true)
	public ModelAndView pesquisa(TabFiltroMenuObj tabFiltroMenuObj, HttpServletRequest request) {
		
		tabFiltroMenuObj = verificaFiltro(tabFiltroMenuObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabMenuObj());			
		mv.addObject("tabFiltroMenuObj", tabFiltroMenuObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroMenuObj));
		
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroMenuObj tabFiltroMenuObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabMenuObj.class);
		
		adicionarFiltro(tabFiltroMenuObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroMenuObj verificaFiltro(TabFiltroMenuObj tabFiltroMenuObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroMenuObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroMenuObj = mapper.readValue(jsonFiltro, TabFiltroMenuObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroMenuObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroMenuObj;
	}

	
	private void adicionarFiltro(TabFiltroMenuObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
			  
			  criteria.addOrder(Order.asc("cdOrdem"));
	  }
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabMenuObj tabMenuObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabMenuObj Tab = tabService.gravar(tabMenuObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo(HttpServletRequest request) {
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabMenuObj());
		
		mv.addObject("tabFiltroMenuObj", verificaFiltro(new TabFiltroMenuObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroMenuObj(), request)));
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{cdMenu}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdMenu, HttpServletRequest request) {
	
	   TabMenuObj TabView = tabService.consultar(cdMenu);
	   TabFiltroMenuObj tF = verificaFiltro(new TabFiltroMenuObj(), request);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroMenuObj",tF);	
		  mv.addObject("tabMenuObj",TabView);
		  mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject("tabFiltroMenuObj",tF);	
		  mv.addObject(new TabMenuObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	
	@RequestMapping(value = "/excluir/{cdMenu}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable Integer cdMenu, HttpServletRequest request) {

		tabService.excluir(cdMenu);

		return "OK!";
	}
	
}
