package br.com.gohosp.modulos.administrador.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.administracao.TabCamposObj;
import br.com.gohosp.dao.administracao.repository.TabCamposRepository;
import br.com.gohosp.dao.administracao.repository.VwTabCamposRepository;
import br.com.gohosp.dao.administracao.vw.VwTabCamposObj;
import br.com.gohosp.dao.util.log.TabLogObj;
import br.com.gohosp.exceptions.ErrosConstraintException;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.log.LogBean;

@Service
public class TabCamposService {

	@Autowired
	private TabCamposRepository tabRepository;
	
	@Autowired
	private VwTabCamposRepository tabVwRepository;

	@Autowired
	private UsuarioBean tabUsuarioService;

	@Autowired
	private LogBean tabLogBeanService;

	public List<TabCamposObj> listar() {
		return tabRepository.findAll();

	}

	public List<VwTabCamposObj> listar(String txService, Integer ckVisualizar) {
		return tabVwRepository.findByTxServiceAndCkVisualizarOrderByTxCampoAsc(txService, ckVisualizar);

	}
	

	
	public TabCamposObj gravar(TabCamposObj Tab) {

		boolean ckAlteracao = false;

		DadosUser user = tabUsuarioService.DadosUsuario();

		TabCamposObj tAtual = new TabCamposObj();
		TabCamposObj tNovo = Tab;

		// tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;

		if (Tab.getCdCampo() != null && !Validator.isBlankOrNull(Tab.getCdCampo())) {
			ckAlteracao = true;
			// Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdCampo());
			// listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()),
			// user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdCampo()), user.getCdUsuario(),
					getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}

		try {
			tNovo = tabRepository.save(tNovo);
		} catch (ConstraintViolationException ex) {

			throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex),
					ex.getConstraintViolations());
		}

		// Log de campos
		if (!ckAlteracao) {
			// tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()),
			// user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
			tabLogBeanService.LogBean(String.valueOf(tNovo.getCdCampo()), user.getCdUsuario(),
					getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		} else {
			tabLogBeanService.GravarListaLog(listaLog);
		}
		return tNovo;

	}

	public TabCamposObj consultar(Integer CdCampo) {
		TabCamposObj Tab = tabRepository.findOne(CdCampo);

		return Tab;

	}
	

	

	public VwTabCamposObj consultar(String txService, String txObjCampo) {
		VwTabCamposObj Tab = tabVwRepository.findByTxObjCampoQuery(txService, txObjCampo);

		return Tab;

	}
	
	

	
	public void excluir(Integer CdCampo) {

		tabRepository.delete(CdCampo);

	}

	public List<TabCamposObj> pesquisa(Integer cdGrupoVisao, Integer cdTela) {
		List<TabCamposObj> Tab = tabRepository.findByCdGrupoCdTelaQuery(cdGrupoVisao, cdTela);

		return Tab;

	}

	public List<VwTabCamposObj> pesquisaPermissoes(Integer cdGrupoVisao, String txController) {

		List<VwTabCamposObj> Tab = tabVwRepository.findBycdGrupoVisaotxControllerQuery(cdGrupoVisao, txController);

		return Tab;

	}

}
