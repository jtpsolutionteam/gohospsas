package br.com.gohosp.modulos.administrador.controller;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.administracao.TabMenuObj;
import br.com.gohosp.dao.administracao.TabMenuSubObj;
import br.com.gohosp.mensagens.TabRetornoSucessoObj;
import br.com.gohosp.modulos.administrador.bean.TabFiltroMenuSubObj;
import br.com.gohosp.modulos.administrador.service.TabMenuService;
import br.com.gohosp.modulos.administrador.service.TabMenuSubService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralParser;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.regras.RegrasBean;

@Controller
@RequestMapping("/admsubmenu")
public class TabMenuSubController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabMenuSubService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_ADM+"/admsubmenu";
	

 	
 	@GetMapping
	@Transactional(readOnly = true)
	public ModelAndView pesquisa(TabFiltroMenuSubObj tabFiltroMenuSubObj, HttpServletRequest request) {
		
		tabFiltroMenuSubObj = verificaFiltro(tabFiltroMenuSubObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabMenuSubObj());			
		mv.addObject("tabFiltroMenuSubObj", tabFiltroMenuSubObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroMenuSubObj));
		mv.addObject("cdMenu", tabFiltroMenuSubObj.getCdMenuFiltro());
		
		
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroMenuSubObj tabFiltroMenuSubObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabMenuSubObj.class);
		
		adicionarFiltro(tabFiltroMenuSubObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroMenuSubObj verificaFiltro(TabFiltroMenuSubObj tabFiltroMenuSubObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroMenuSubObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroMenuSubObj = mapper.readValue(jsonFiltro, TabFiltroMenuSubObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroMenuSubObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroMenuSubObj;
	}

	
	private void adicionarFiltro(TabFiltroMenuSubObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  if (filtro != null) {
			  
			  if (filtro.getCdMenuFiltro() != null) {
				  criteria.add(Restrictions.eq("cdMenu", filtro.getCdMenuFiltro())); 
				  ckFiltro = true;
			  }
			  
			  if (!ckFiltro) {
				  criteria.add(Restrictions.eq("cdMenu", 0));
			  }
			  
			  criteria.addOrder(Order.asc("cdOrdem"));
		  }
	  }
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabMenuSubObj tabMenuSubObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabMenuSubObj Tab = tabService.gravar(tabMenuSubObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo/{cdMenu}")
	public ModelAndView novo(@PathVariable Integer cdMenu, HttpServletRequest request) {
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabMenuSubObj());
		mv.addObject("tabFiltroMenuSubObj", verificaFiltro(new TabFiltroMenuSubObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroMenuSubObj(), request)));
		mv.addObject("txEdit", "edit");
		mv.addObject("cdMenu", cdMenu);
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{cdSubmenu}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdSubmenu, HttpServletRequest request) {
	
	   TabMenuSubObj TabView = tabService.consultar(cdSubmenu);
	   TabFiltroMenuSubObj tF = verificaFiltro(new TabFiltroMenuSubObj(), request);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroMenuSubObj",tF);	
		  mv.addObject("tabMenuSubObj",TabView);
		  mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
		  mv.addObject("txEdit", "edit");
		  mv.addObject("cdMenu", TabView.getCdMenu());
		}else {
		  mv.addObject("tabFiltroMenuSubObj",tF);	
		  mv.addObject(new TabMenuSubObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	
	@RequestMapping(value = "/excluir/{cdSubmenu}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable Integer cdSubmenu, HttpServletRequest request) {

		tabService.excluir(cdSubmenu);

		return "OK!";
	}
	
	@Autowired
	private TabMenuService tabMenuService;
	
	@ModelAttribute("selectcdmenu")
	public List<TabMenuObj> selectcdmenu() {
		return tabMenuService.listar();
	}
	
}
