package br.com.gohosp.modulos.administrador.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.administracao.TabBotaoObj;
import br.com.gohosp.dao.administracao.TabGrupoVisaoObj;
import br.com.gohosp.dao.administracao.TabTelasObj;
import br.com.gohosp.mensagens.TabRetornoSucessoObj;
import br.com.gohosp.modulos.administrador.bean.TabFiltroBotaoObj;
import br.com.gohosp.modulos.administrador.service.TabBotaoService;
import br.com.gohosp.modulos.administrador.service.TabGrupoVisaoService;
import br.com.gohosp.modulos.administrador.service.TabTelasService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralParser;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.regras.RegrasBean;

@Controller
@RequestMapping("/admbotao")
public class TabBotaoController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabBotaoService tabService;

	@Autowired
	private RegrasBean regrasBean;

	@PersistenceContext
	private EntityManager entityManager;

	private String txUrlTela = Constants.TEMPLATE_PATH_ADM + "/admbotao";

	@GetMapping
	@Transactional(readOnly = true)
	public ModelAndView pesquisa(TabFiltroBotaoObj tabFiltroBotaoObj, HttpServletRequest request) {

		tabFiltroBotaoObj = verificaFiltro(tabFiltroBotaoObj, request);
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabBotaoObj());
		mv.addObject("tabFiltroBotaoObj", tabFiltroBotaoObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroBotaoObj));
		mv.addObject("cdTela", tabFiltroBotaoObj.getCdTelaFiltro());
		mv.addObject("cdGrupoVisao", tabFiltroBotaoObj.getCdGrupoVisaoFiltro());		
		return mv;
	}
	
	

 	
	private TabFiltroBotaoObj verificaFiltro(TabFiltroBotaoObj tabFiltroBotaoObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroBotaoObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroBotaoObj = mapper.readValue(jsonFiltro, TabFiltroBotaoObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroBotaoObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroBotaoObj;
	}

	private List<?> retornoFiltro(TabFiltroBotaoObj tabFiltroBotaoObj) {

		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabBotaoObj.class);

		adicionarFiltro(tabFiltroBotaoObj, criteria);

		return criteria.list();
	}

	private void adicionarFiltro(TabFiltroBotaoObj filtro, Criteria criteria) {

		boolean ckFiltro = false;

		if (filtro != null) {

			if (filtro.getCdTelaFiltro() != null) {
				criteria.add(Restrictions.eq("cdTela", filtro.getCdTelaFiltro()));
				ckFiltro = true;
			}

			if (filtro.getCdGrupoVisaoFiltro() != null) {
				criteria.add(Restrictions.eq("cdGrupoVisao", filtro.getCdGrupoVisaoFiltro()));
				ckFiltro = true;
			}

			if (!ckFiltro) {
				criteria.add(Restrictions.eq("cdTela", 0));
			}

			criteria.addOrder(Order.asc("txObjBotao"));
		}
	}

	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabBotaoObj TabBotaoObj, Errors erros,
			HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
			// mv.addObject("listaErros", erros.getAllErrors());
			error.addAll(erros.getAllErrors());
			return error;
		} else {
			error = regrasBean.verificaregras(getClass().getSimpleName(), request);
			if (!error.isEmpty()) {
				return error;
			}
		}

		TabBotaoObj Tab = tabService.gravar(TabBotaoObj);

		List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();
		TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
		tSuccess.setCd_mensagem(1);
		tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
		success.add(tSuccess);

		return success;
	}

	@RequestMapping(value = "/novo/{cdTela}/{cdGrupoVisao}")
	public ModelAndView novo(@PathVariable Integer cdTela, @PathVariable Integer cdGrupoVisao,
			HttpServletRequest request) {
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabBotaoObj());
		mv.addObject("tabFiltroBotaoObj", verificaFiltro(new TabFiltroBotaoObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroBotaoObj(), request)));
		mv.addObject("txEdit", "edit");
		return mv;
	}

	@RequestMapping(value = "/consultar/{cdBotao}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdBotao, HttpServletRequest request) {

		TabBotaoObj TabView = tabService.consultar(cdBotao);
		TabFiltroBotaoObj tF = verificaFiltro(new TabFiltroBotaoObj(), request);

		ModelAndView mv = new ModelAndView(txUrlTela);
		if (TabView != null) {
			mv.addObject("tabFiltroBotaoObj", tF);
			mv.addObject("TabBotaoObj", TabView);
			mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
			mv.addObject("txEdit", "edit");
			mv.addObject("cdTela", TabView.getCdTela());
			mv.addObject("cdGrupoVisao", TabView.getCdGrupoVisao());
		} else {
			mv.addObject("tabFiltroBotaoObj", tF);
			mv.addObject(new TabBotaoObj());
			mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}

	@RequestMapping(value = "/excluir/{cdBotao}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable Integer cdBotao, HttpServletRequest request) {

		tabService.excluir(cdBotao);

		return "OK!";
	}

	
	
	
	@Autowired
	private TabGrupoVisaoService tabGrupoVisaoService;
	
	@ModelAttribute("selectcdgrupovisao")
	public List<TabGrupoVisaoObj> selectcdgrupovisao() {
		return tabGrupoVisaoService.listar();
	}
	
	@Autowired
	private TabTelasService tabTelasService;
	
	@ModelAttribute("selectcdtela")
	public List<TabTelasObj> selectcdtela() {
		return tabTelasService.listar();
	}
	
}
