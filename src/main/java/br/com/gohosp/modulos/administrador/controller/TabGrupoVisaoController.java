package br.com.gohosp.modulos.administrador.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.administracao.TabGrupoVisaoObj;
import br.com.gohosp.mensagens.TabRetornoSucessoObj;
import br.com.gohosp.modulos.administrador.bean.TabFiltroGrupoVisaoObj;
import br.com.gohosp.modulos.administrador.service.TabGrupoVisaoService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.paginacao.PageWrapper;
import br.com.gohosp.util.regras.RegrasBean;

@Controller
@RequestMapping("/admgrupovisao")
public class TabGrupoVisaoController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabGrupoVisaoService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_ADM+"/admgrupovisao";
	
	@GetMapping
	public ModelAndView pesquisa(TabFiltroGrupoVisaoObj tabFiltroGrupoVisaoObj, Errors errors, @PageableDefault(size = 20) Pageable pageable, HttpServletRequest httpServletRequest) {
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject("tabFiltroGrupoVisaoObj", tabFiltroGrupoVisaoObj);
		
		PageWrapper<TabGrupoVisaoObj> TabView = new PageWrapper<>(tabService.telapesquisar(tabFiltroGrupoVisaoObj, pageable),httpServletRequest);
		
		if (TabView.getPage() != null) {	
		  mv.addObject("listadados",TabView);
		  mv.addObject(new TabGrupoVisaoObj());
		}else {	
		  mv.addObject(new TabGrupoVisaoObj());	
		  mv.addObject("txMensagem", Constants.FILTRO_NAO_EXISTE); 	
		}
		
		return mv;
	}
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabGrupoVisaoObj tabGrupoVisaoObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabGrupoVisaoObj Tab = tabService.gravar(tabGrupoVisaoObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo(@PageableDefault(size = 20) Pageable pageable, HttpServletRequest httpServletRequest) {
		
	  TabFiltroGrupoVisaoObj tF = new TabFiltroGrupoVisaoObj();
	  tF.setTxGrupoVisaoFiltro("");
	  PageWrapper<TabGrupoVisaoObj> TabViewPesquisa = new PageWrapper<>(tabService.telapesquisar(tF, pageable),httpServletRequest);
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabGrupoVisaoObj());
		mv.addObject("tabFiltroGrupoVisaoObj", new TabFiltroGrupoVisaoObj());
		mv.addObject("listadados",TabViewPesquisa);
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{cdGrupoVisao}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdGrupoVisao,
								  @PageableDefault(size = 20) Pageable pageable, HttpServletRequest httpServletRequest) {
	
	   TabGrupoVisaoObj TabView = tabService.consultar(cdGrupoVisao);
	   TabFiltroGrupoVisaoObj tF = new TabFiltroGrupoVisaoObj();
	   tF.setTxGrupoVisaoFiltro(TabView.getTxGrupoVisao());
	   PageWrapper<TabGrupoVisaoObj> TabViewPesquisa = new PageWrapper<>(tabService.telapesquisar(tF, pageable),httpServletRequest);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroGrupoVisaoObj",tF);	
		  mv.addObject("tabGrupoVisaoObj",TabView);
		  mv.addObject("listadados",TabViewPesquisa);
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject("tabFiltroGrupoVisaoObj",tF);	
		  mv.addObject(new TabGrupoVisaoObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	
}
