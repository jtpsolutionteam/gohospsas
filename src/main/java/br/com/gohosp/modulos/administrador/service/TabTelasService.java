package br.com.gohosp.modulos.administrador.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gohosp.dao.administracao.TabTelasObj;
import br.com.gohosp.dao.administracao.repository.TabTelasRepository;
import br.com.gohosp.dao.util.log.TabLogObj;
import br.com.gohosp.exceptions.ErrosConstraintException;
import br.com.gohosp.security.DadosUser;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralUtil;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.log.LogBean;

@Service
public class TabTelasService {
	
	@Autowired
	private TabTelasRepository  tabRepository;
		
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;

	
	public List<TabTelasObj> listar() {
		return tabRepository.findByOrderByTxTelaAsc();
		
	}
	
	


	public TabTelasObj gravar(TabTelasObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabTelasObj tAtual = new TabTelasObj();
		TabTelasObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdTela() != null && !Validator.isBlankOrNull(Tab.getCdTela())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdTela());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdTela()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdTela()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabTelasObj consultar(Integer CdTela) {
	 TabTelasObj Tab = tabRepository.findOne(CdTela);
	 
	   return Tab;
	 
	}

	public TabTelasObj consultarService(String txService) {
		 TabTelasObj Tab = tabRepository.findTop1ByTxService(txService);
		 
		   return Tab;
		 
		}

	public TabTelasObj consultarController(String txController) {
		 TabTelasObj Tab = tabRepository.findByTxController(txController);
		 
		   return Tab;
		 
		}
	
	
		public void excluir(Integer CdTela) {

		   TabTelasObj Tab = tabRepository.findOne(CdTela);		   
		   tabRepository.save(Tab);
			 
	}
	
		/*
	public TabTelasObj pesquisa(String txPesquisa) {
		 VwTabTelasObj Tab = tabVwRepository.findByTxPesquisar(txPesquisa);
		 
		 if (Tab == null) {
			 throw new ErrosException(Constants.REGISTRO_NAO_EXISTE);
		 }
		 
		   return Tab;
		 
		}
	
	
	public List<VwTabTelasObj> lkpesquisar(String txUrlPesquisar) {
		 DadosUser user  = tabUsuarioService.DadosUsuario();
		 List<VwTabTelasObj> Tab = tabVwRepository.findByTxUrlPesquisarLikeQuery(txUrlPesquisar, user.getCdGrupoAcesso());
		 		 
		   return Tab;
		 
	}*/
	
}
