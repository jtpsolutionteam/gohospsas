package br.com.gohosp.modulos.administrador.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.gohosp.Constants;
import br.com.gohosp.dao.administracao.TabCamposObj;
import br.com.gohosp.dao.administracao.TabGrupoVisaoObj;
import br.com.gohosp.dao.administracao.TabTelasObj;
import br.com.gohosp.mensagens.TabRetornoSucessoObj;
import br.com.gohosp.modulos.administrador.bean.TabFiltroCamposObj;
import br.com.gohosp.modulos.administrador.service.TabCamposService;
import br.com.gohosp.modulos.administrador.service.TabGrupoVisaoService;
import br.com.gohosp.modulos.administrador.service.TabTelasService;
import br.com.gohosp.modulos.util.geralcontroller.allController;
import br.com.gohosp.security.UsuarioBean;
import br.com.gohosp.util.GeneralParser;
import br.com.gohosp.util.Validator;
import br.com.gohosp.util.regras.RegrasBean;

@Controller
@RequestMapping("/admcampos")
public class TabCamposController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabCamposService tabService;

	@Autowired
	private RegrasBean regrasBean;

	@PersistenceContext
	private EntityManager entityManager;

	private String txUrlTela = Constants.TEMPLATE_PATH_ADM + "/admcampos";

	@GetMapping
	@Transactional(readOnly = true)
	public ModelAndView pesquisa(TabFiltroCamposObj tabFiltroCamposObj, HttpServletRequest request) {

		tabFiltroCamposObj = verificaFiltro(tabFiltroCamposObj, request);
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabCamposObj());
		mv.addObject("tabFiltroCamposObj", tabFiltroCamposObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroCamposObj));
		mv.addObject("cdTela", tabFiltroCamposObj.getCdTelaFiltro());
		mv.addObject("cdGrupoVisao", tabFiltroCamposObj.getCdGrupoVisaoFiltro());		
		return mv;
	}
	
	

 	
	private TabFiltroCamposObj verificaFiltro(TabFiltroCamposObj tabFiltroCamposObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroCamposObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroCamposObj = mapper.readValue(jsonFiltro, TabFiltroCamposObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroCamposObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroCamposObj;
	}

	private List<?> retornoFiltro(TabFiltroCamposObj tabFiltroCamposObj) {

		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabCamposObj.class);

		adicionarFiltro(tabFiltroCamposObj, criteria);

		return criteria.list();
	}

	private void adicionarFiltro(TabFiltroCamposObj filtro, Criteria criteria) {

		boolean ckFiltro = false;

		if (filtro != null) {

			if (filtro.getCdTelaFiltro() != null) {
				criteria.add(Restrictions.eq("cdTela", filtro.getCdTelaFiltro()));
				ckFiltro = true;
			}

			if (filtro.getCdGrupoVisaoFiltro() != null) {
				criteria.add(Restrictions.eq("cdGrupoVisao", filtro.getCdGrupoVisaoFiltro()));
				ckFiltro = true;
			}

			if (!ckFiltro) {
				criteria.add(Restrictions.eq("cdTela", 0));
			}

			criteria.addOrder(Order.asc("txCampo"));
		}
	}

	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabCamposObj tabCamposObj, Errors erros,
			HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
			// mv.addObject("listaErros", erros.getAllErrors());
			error.addAll(erros.getAllErrors());
			return error;
		} else {
			error = regrasBean.verificaregras(getClass().getSimpleName(), request);
			if (!error.isEmpty()) {
				return error;
			}
		}

		TabCamposObj Tab = tabService.gravar(tabCamposObj);

		List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();
		TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
		tSuccess.setCd_mensagem(1);
		tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
		success.add(tSuccess);

		return success;
	}

	@RequestMapping(value = "/novo/{cdTela}/{cdGrupoVisao}")
	public ModelAndView novo(@PathVariable Integer cdTela, @PathVariable Integer cdGrupoVisao,
			HttpServletRequest request) {
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabCamposObj());
		mv.addObject("tabFiltroCamposObj", verificaFiltro(new TabFiltroCamposObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroCamposObj(), request)));
		mv.addObject("txEdit", "edit");
		mv.addObject("cdTela", cdTela);
		mv.addObject("cdGrupoVisao", cdGrupoVisao);
		return mv;
	}

	@RequestMapping(value = "/consultar/{CdCampo}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer CdCampo, HttpServletRequest request) {

		TabCamposObj TabView = tabService.consultar(CdCampo);
		TabFiltroCamposObj tF = verificaFiltro(new TabFiltroCamposObj(), request);

		ModelAndView mv = new ModelAndView(txUrlTela);
		if (TabView != null) {
			mv.addObject("tabFiltroCamposObj", tF);
			mv.addObject("tabCamposObj", TabView);
			mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
			mv.addObject("txEdit", "edit");
			mv.addObject("cdTela", TabView.getCdTela());
			mv.addObject("cdGrupoVisao", TabView.getCdGrupoVisao());
		} else {
			mv.addObject("tabFiltroCamposObj", tF);
			mv.addObject(new TabCamposObj());
			mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}

	@RequestMapping(value = "/excluir/{cdCampo}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable Integer cdCampo, HttpServletRequest request) {

		tabService.excluir(cdCampo);

		return "OK!";
	}

	
	
	
	@Autowired
	private TabGrupoVisaoService tabGrupoVisaoService;
	
	@ModelAttribute("selectcdgrupovisao")
	public List<TabGrupoVisaoObj> selectcdgrupovisao() {
		return tabGrupoVisaoService.listar();
	}
	
	@Autowired
	private TabTelasService tabTelasService;
	
	@ModelAttribute("selectcdtela")
	public List<TabTelasObj> selectcdtela() {
		return tabTelasService.listar();
	}
	
}
