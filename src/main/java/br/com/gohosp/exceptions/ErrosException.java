package br.com.gohosp.exceptions;

public class ErrosException extends RuntimeException {

	public ErrosException(String mensagem) {		
		super(mensagem);		
	}
	
	public ErrosException(String mensagem, Throwable causa) {
		super(mensagem, causa);
	}
	
	
}

