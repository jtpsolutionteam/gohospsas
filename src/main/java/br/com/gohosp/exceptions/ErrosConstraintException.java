package br.com.gohosp.exceptions;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

public class ErrosConstraintException extends ConstraintViolationException {

	public ErrosConstraintException(String message, Set<? extends ConstraintViolation<?>> constraintViolations) {

		super(message, constraintViolations);
		// TODO Auto-generated constructor stub
	}

	

}
