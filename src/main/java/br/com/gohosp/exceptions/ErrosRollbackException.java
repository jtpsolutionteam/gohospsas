package br.com.gohosp.exceptions;

import javax.persistence.RollbackException;

public class ErrosRollbackException extends RollbackException  {

	public ErrosRollbackException(String mensagem) {		
		super(mensagem);		
	}
	
	
}
