package br.com.gohosp.mensagens;

public class TabRetornoSucessoObj {

	
	private String tx_chave;
	
	private Integer cd_mensagem;
	
	private String tx_mensagem;
	
	private Integer cd_tipo_pagto; 
	
	private Integer cd_tipo_lancto;
	
	private Long dt_system;

	public Long getDt_system() {
		return dt_system;
	}

	public void setDt_system(Long dt_system) {
		this.dt_system = dt_system;
	}

	public String getTx_chave() {
		return tx_chave;
	}

	public void setTx_chave(String tx_chave) {
		this.tx_chave = tx_chave;
	}

	public Integer getCd_mensagem() {
		return cd_mensagem;
	}

	public void setCd_mensagem(Integer cd_mensagem) {
		this.cd_mensagem = cd_mensagem;
	}

	public String getTx_mensagem() {
		return tx_mensagem;
	}

	public void setTx_mensagem(String tx_mensagem) {
		this.tx_mensagem = tx_mensagem;
	}

	public Integer getCd_tipo_pagto() {
		return cd_tipo_pagto;
	}

	public void setCd_tipo_pagto(Integer cd_tipo_pagto) {
		this.cd_tipo_pagto = cd_tipo_pagto;
	}

	public Integer getCd_tipo_lancto() {
		return cd_tipo_lancto;
	}

	public void setCd_tipo_lancto(Integer cd_tipo_lancto) {
		this.cd_tipo_lancto = cd_tipo_lancto;
	}

	
	
	
	
	
}
